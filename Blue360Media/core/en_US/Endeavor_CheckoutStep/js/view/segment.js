define(
        [
            'ko',
            'uiComponent',
            'underscore',
            'jquery',
            'mage/url',
            'Magento_Customer/js/customer-data',
            'Magento_Checkout/js/model/step-navigator'
        ],
        function (
                ko,
                Component,
                _,
                $,
                url,
                customerData,
                stepNavigator
                ) {
            'use strict';
            
            /**
             *
             * mystep - is the name of the component's .html template, 
             * Endeavor_CheckoutStep - is the name of the your module directory.
             * 
             */
            return Component.extend({
                defaults: {
                    template: 'Endeavor_CheckoutStep/segment'
                },

                //add here your logic to display step,
                isVisible: ko.observable(false),

                /**
                 *
                 * @returns {*}
                 */
                initialize: function () {
                    var customer = window.customerData;
                    var customerSegment = null; 
                    if(customer.custom_attributes) {
                        customerSegment = customer.custom_attributes.customer_market_segment;
                    }
                    this._super();
                    if (!customerSegment) {
                        stepNavigator.registerStep(
                            'segment',
                            'segment',
                            'About You',
                            this.isVisible,
                            _.bind(this.navigate, this),
                            15
                        );
                    }
                    return this;
                },

                /**
                 * The navigate() method is responsible for navigation between checkout step
                 * during checkout. You can add custom logic, for example some conditions
                 * for switching to your custom step 
                 */
                navigate: function () {
                    this.isVisible(false);
                    this.isVisible = false;
                },
               
                /**
                 * @returns void
                 */
                navigateToNextStep: function () {
                    stepNavigator.next();
                },
                
                fillData: function() {
                    var urlAjaxLoad = url.build('checkoutstep/data/data');
                    var urlAjaxSave = url.build('checkoutstep/data/save');
                    
                    // save segments attribute in current quote when click next.
                    $('#ajax-segment-action').click(function(){
                        var segmentValue = $('#segmentType').val();
                        var secondarySegmentValue = $('#secondarySegmentType').val();
                        if (segmentValue !== 0 && segmentValue !== '' ) {
                            $.ajax({
                                type: 'POST',
                                url: urlAjaxSave,
                                data: {'segmentType' : segmentValue ,'secondaryMarket' : secondarySegmentValue, 'customerId' : window.customerData.id},
                                showLoader: false,
                                success: function() {
                                    console.log('Data saved successfully.');
                                },
                                error: function () {
                                    console.log('Something wrong in saving data.');
                                }
                            });
                        }

                    });
                    
                    // load Secondary Market when select segment.
                    $("#segmentType").change(function(){
                        var selectedSegment = $('#segmentType').val(); 
                        $.ajax({
                            type: 'POST',
                            url: urlAjaxLoad,
                            data: {'segment' : selectedSegment },
                            showLoader: false,
                            success: function(html) {
                                $('#secondarySegmentType').html(html['secondarySegmentMarket']);
                            },
                            error: function () {
                                console.log('Something wrong in loading about you data.');
                            }
                        });
                    });
                    
                    // load Market Segment
                    $.ajax({
                        type: 'POST',
                        url: urlAjaxLoad,
                        showLoader: false,
                        success: function(html) {
                            $('#segmentType').html(html['segmentMarket']);
                            $('#secondarySegmentType').html(html['secondarySegmentMarket']);
                        },
                        error: function () {
                            console.log('Something wrong in loading about you data.');
                        }
                    });
                    
                },
                getFormKey: function() {
                    return window.checkoutConfig.formKey;
                }
             
            });
        }
);