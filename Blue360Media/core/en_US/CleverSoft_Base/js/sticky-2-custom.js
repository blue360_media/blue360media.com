define([
    "jquery",
    "jquery/ui",
    "cleverInnerZoom",
    "fotoramaVideoEvents"
], function ($) {
    'use strict';

    /**
     //      * @private
     //      */
    function parseHref(href) {
        var a = document.createElement('a');

        a.href = href;

        return a;
    }

    /**
     * @private
     */
    function parseURL(href, forceVideo) {
        var id,
            type,
            ampersandPosition,
            vimeoRegex;

        /**
         * Get youtube ID
         * @param {String} srcid
         * @returns {{}}
         */
        function _getYoutubeId(srcid) {
            if (srcid) {
                ampersandPosition = srcid.indexOf('&');

                if (ampersandPosition === -1) {
                    return srcid;
                }

                srcid = srcid.substring(0, ampersandPosition);
            }

            return srcid;
        }

        if (typeof href !== 'string') {
            return href;
        }

        href = parseHref(href);

        if (href.host.match(/youtube\.com/) && href.search) {
            id = href.search.split('v=')[1];

            if (id) {
                id = _getYoutubeId(id);
                type = 'youtube';
            }
        } else if (href.host.match(/youtube\.com|youtu\.be/)) {
            id = href.pathname.replace(/^\/(embed\/|v\/)?/, '').replace(/\/.*/, '');
            type = 'youtube';
        } else if (href.host.match(/vimeo\.com/)) {
            type = 'vimeo';
            vimeoRegex = new RegExp(['https?:\\/\\/(?:www\\.|player\\.)?vimeo.com\\/(?:channels\\/(?:\\w+\\/)',
                '?|groups\\/([^\\/]*)\\/videos\\/|album\\/(\\d+)\\/video\\/|video\\/|)(\\d+)(?:$|\\/|\\?)'
            ].join(''));
            id = href.href.match(vimeoRegex)[3];
        }

        if ((!id || !type) && forceVideo) {
            id = href.href;
            type = 'custom';
        }

        return id ? {
            id: id, type: type, s: href.search.replace(/^\?/, '')
        } : false;
    }

    /**
     * Use to update image and video for product page with sticky_2 layout
     */
    $.widget('cleversoft.cleverBaseApllyImageAndVideoEvent', {
        options: {
            class: {
                sticky_gallery_content: '.gallery-sticky2',
                thumb_content: '.gallery-sticky2-image-thumb-col',
                image_content: '.gallery-sticky2-image-col',
                image_wrapper: '.gallery-sticky2-image-wrapper',
                video_wrapper: '.gallery-sticky2-video-wrapper',
            },
        },
        _create: function(){
            console.log('run cleverBaseApllyImageAndVideoEvent');
            var gallery_content = $(this.options.class.sticky_gallery_content);
            var thumb_content = gallery_content.find(this.options.class.thumb_content);
            var image_content = gallery_content.find(this.options.class.image_content);

            image_content.find(this.options.class.image_wrapper).cleverInnerZoom();

            image_content.find(this.options.class.video_wrapper + " a").click(function(event){
                event.preventDefault();
            });

            image_content.find(this.options.class.video_wrapper).click(function () {
                var url = $(this).data('media-url');
                var video_data =  parseURL(url);
                $(this).data('related', '0')
                    .data('loop', '0')
                    .data('responsive', false)
                    .data('type', video_data.type)
                    .data('code', video_data.id)
                    .data('width', $(this).width())
                    .data('height', $(this).height());

                $(this).productVideoLoader();
            });


            console.log('end run cleverBaseApllyImageAndVideoEvent');
        }

    });

    $.widget('cleversoft.cleverBaseUpdateImageAndVideo', {
        options: {
            images: {},
            class: {
                sticky_gallery_content: '.gallery-sticky2',
                thumb_content: '.gallery-sticky2-image-thumb-col',
                image_content: '.gallery-sticky2-image-col',
                image_wrapper: '.gallery-sticky2-image-wrapper',
                video_wrapper: '.gallery-sticky2-video-wrapper',
            },
        },
        _create: function(){
            console.log('sacccccc');
            console.log('run cleverBaseUpdateImageAndVideo');

            var galerry_data = this.options.images;

            var gallery_content = $(this.options.class.sticky_gallery_content);
            var thumb_content = gallery_content.find(this.options.class.thumb_content);
            var image_content = gallery_content.find(this.options.class.image_content);

            var html = '';
            $.each( galerry_data, function( key, item ) {

                if ( item.media_type == 'external-video') {
                    html += '<div class="gallery-sticky2-video-wrapper" data-media-type="external-video" data-media-url="' + item.video_url + '">';
                } else {
                    html += '<div class="gallery-sticky2-image-wrapper">';
                }

                html += '<a href="' + item.full + '">';
                html += '<img src="' + item.img + '">';
                html += '</a>';
                html += '</div>';
            });
            image_content.html(html);

            $.cleversoft.cleverBaseApllyImageAndVideoEvent(this.options);
            console.log('end run cleverBaseUpdateImageAndVideo');
        },
    });

});
