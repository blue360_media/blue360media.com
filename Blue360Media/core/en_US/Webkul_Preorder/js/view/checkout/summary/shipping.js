/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'Magento_Checkout/js/view/summary/shipping',
        'Magento_Checkout/js/model/quote',
        'mage/url',
        'moment'
    ],
    function ($, Component, quote, url, moment) {
        var displayMode = window.checkoutConfig.reviewShippingDisplayMode;
        return Component.extend({
            defaults: {
                displayMode: displayMode,
                template: 'Magento_Tax/checkout/summary/shipping'
            },
            isBothPricesDisplayed: function() {
                return 'both' == this.displayMode
            },
            isIncludingDisplayed: function() {
                return 'including' == this.displayMode;
            },
            isExcludingDisplayed: function() {
                return 'excluding' == this.displayMode;
            },
            isCalculated: function() {
                return this.totals() && this.isFullMode() && null != quote.shippingMethod();
            },
            getIncludingValue: function() {
                if (!this.isCalculated()) {
                    return this.notCalculatedMessage;
                }
                var price =  this.totals().shipping_incl_tax;
                return this.getFormattedPrice(price);
            },
            getExcludingValue: function() {
                if (!this.isCalculated()) {
                    return this.notCalculatedMessage;
                }
                var price =  this.totals().shipping_amount;
                return this.getFormattedPrice(price);
            },
            getEstimateDate: function() {
                var shippingMethod = quote.shippingMethod().method_title;
                var estimateDeliveryAjaxUrl = url.build('checkout/shippingtime/estimate');
                var estimateDate = '';
                $.ajax({
                    type: 'POST',
                    url: estimateDeliveryAjaxUrl,
                    data: {'quote' : quote.getQuoteId },
                    async: !1,
                    showLoader: false,
                    success: function($output) {
                        var standard = new Date($output['estimateDeliveryDate']);
						var ups = new Date($output['estimateDeliveryDate']);
                        var next2Day = new Date($output['estimateDeliveryDate']);
                        var nextDay = new Date($output['estimateDeliveryDate']);
                        if ($output['isPreOrder']) {
                            estimateDate = 'After 5 business days from shipping date';
                        } else {
                            // add another day if time after 3 PM
                            if (standard.getHours() >= 15){
                                standard = addWorkDays(standard, 1);
								ups = addWorkDays(nextDay, 2);
                                next2Day = addWorkDays(next2Day, 1);
                                nextDay = addWorkDays(nextDay, 1);
                            }

                            standard = addWorkDays(standard, 4);
							ups = addWorkDays(nextDay, 2);
                            next2Day = addWorkDays(next2Day, 2);
                            nextDay = addWorkDays(nextDay, 1);
                            var standardDate = moment(standard).format("dddd, MMMM Do YYYY");
							var upsDate = moment(ups).format("dddd, MMMM Do YYYY");
                            var next2DayDate = moment(next2Day).format("dddd, MMMM Do YYYY");
                            var nextDayDate = moment(nextDay).format("dddd, MMMM Do YYYY");
                            if (shippingMethod == 'Standard'  || methodTitle == 'Free Shipping') {
                                estimateDate = standardDate;
                            } else if (shippingMethod == 'UPS'){
                                estimateDate = upsDate; 
                            }else if (shippingMethod == 'Next 2-Day'){
                                estimateDate = next2DayDate; 
                            } else {
                                estimateDate = nextDayDate;
                            }
                            
                        }
                    },
                    error: function () {
                        console.log('Can\'t get Current DateTime From server.');
                    }
                });
                
                return estimateDate;
            }
        });
        
        function addWorkDays(startDate, days) {
            // Get the day of the week as a number (0 = Sunday, 1 = Monday, .... 6 = Saturday)
            var dow = startDate.getDay();
            var daysToAdd = days;
            // If the current day is Sunday add one day
            if (dow === 0)
                daysToAdd++;
            // If the start date plus the additional days falls on or after the closest Saturday calculate weekends
            if (dow + daysToAdd >= 6) {
                //Subtract days in current working week from work days
                var remainingWorkDays = daysToAdd - (5 - dow);
                //Add current working week's weekend
                daysToAdd += 2;
                if (remainingWorkDays > 5) {
                    //Add two days for each working week by calculating how many weeks are included
                    daysToAdd += 2 * Math.floor(remainingWorkDays / 5);
                    //Exclude final weekend if remainingWorkDays resolves to an exact number of weeks
                    if (remainingWorkDays % 5 === 0)
                        daysToAdd -= 2;
                }
            }
            startDate.setDate(startDate.getDate() + daysToAdd);
            return startDate;
        };
        
    }
);
