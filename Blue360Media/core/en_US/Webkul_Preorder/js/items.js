/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
"jquery",
"jquery/ui",
], function ($) {
    'use strict';
    $.widget('preorder.items', {
        options: {},
        _create: function () {
            var self = this;
            $(document).ready(function () {
                var options = self.options.option;
                var info = self.options.info;
                var flag = self.options.flag;
                var orderId = self.options.orderid;
                var incrementId = self.options.incrementId;
                var preorderCompleteProductId = self.options.preorderCompleteProductId;
                var url = self.options.url;
                var formKey = "";
                $("body input[type='hidden']").each(function () {
                    var name = $(this).attr("name");
                    if (name == "form_key") {
                        formKey = $(this).val();
                    }
                });
                if (flag == 1) {
                    var msgBox = $('<div/>').addClass("wk-msg-box wk-info").text("This order contains Preorder Products.");
                    $(".page-title-wrapper").append(msgBox);
                }
                $(document).on('click', '.wk-preorder-complete', function (event) {
                    $(".wk-loading-mask").removeClass("wk-display-none");
                    var option = {};
                    var key = $(this).attr("data-key");
                    var productId = info[key]['product_id'];
                    var itemId = info[key]['item_id'];
                    var qty = info[key]['qty'];
                    var name = info[key]['product_name'];
                    $.each(options, function (k, v) {
                        var optionId = v.id;
                        var optionTitle = v.title;
                        if (optionTitle == 'Product Name') {
                            option[optionId] = name;
                        }
                        if (optionTitle == 'Order Refernce') {
                            option[optionId] = incrementId;
                        }
                    });
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: { pro_id:productId, form_key:formKey, options:option, order_id:orderId, item_id : itemId, product : preorderCompleteProductId, qty:qty },
                        success: function (data) {
                            $(".wk-loading-mask").addClass("wk-display-none");
                        }
                    });
                });
            });
        }
    });
    return $.preorder.items;
});

