<?php

namespace Blue360media\OrderStatusChange\Observer;
use Magento\Sales\Model\Order;

/**
 * @author: Vishal Dhurat
 * @desc: to change order status from pending to processing for zero subtotal order
 * 
 */

class Orderstatuschange implements \Magento\Framework\Event\ObserverInterface
{
  public function execute(\Magento\Framework\Event\Observer $observer)
  {
     
  	$order = $observer->getEvent()->getOrder();
	  $paymentMethod = $order->getPayment()->getMethod();
	
  	if($order->getGrandTotal() == 0 && $paymentMethod == "free"){
		$orderState = Order::STATE_PROCESSING;
		$order->setState($orderState)->setStatus(Order::STATE_PROCESSING);
	}

    return $this;
  }
}