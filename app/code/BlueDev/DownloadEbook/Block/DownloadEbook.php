<?php
namespace BlueDev\DownloadEbook\Block;
 
class DownloadEbook extends \Magento\Framework\View\Element\Template
{
	protected $_productRepository;
	protected $_resource;
	protected $connection;
	public $_storeManager;
	
	/**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     * Keys for pass through variables in OutstandingInvoice/OutstandingInvoice_payment
     * Uses additional_information as storage
     */
    const XML_GET_PRODUCT_IDS = 'downloadebook/settings/product_ids';
    const XML_GET_EMAIL_DOMAIN = 'downloadebook/settings/email_domain';
	
	public function __construct(
								\Magento\Framework\View\Element\Template\Context $context,
								\Magento\Framework\App\ResourceConnection $resource,
								\Magento\Catalog\Model\ProductRepository $productRepository,
								\Magento\Store\Model\StoreManagerInterface $storeManager,
								\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
								array $data = []
    ) {
		$this->_productRepository = $productRepository;
		$this->_resource = $resource;
		$this->_storeManager=$storeManager;
		$this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }
	
	public function _prepareLayout() {
        return parent::_prepareLayout();
    }
	
	public function getFormAction()
    {
        return $this->getUrl('downloadebook/index/post', ['_secure' => true]);
    }
	
	protected function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        }
        return $this->connection;
    }
	
	public function getDirectQuery($id)
    {
        $table=$this->_resource->getTableName('downloadable_link'); 
        $downloaddata = $this->getConnection()->fetchRow('SELECT * FROM ' . $table. ' where product_id ='.$id );
        return $downloaddata;
    }
	
	public function getEmailMapping($email)
    {
        $table=$this->_resource->getTableName('bluedev_email_mapping'); 
        $data = $this->getConnection()->fetchRow('SELECT * FROM ' . $table. ' where email ="'.$email.'"' );
        return $data;
    }
	
	public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }
    
    public function getProductBySku($sku)
    {
        return $this->_productRepository->get($sku);
    }

    public function getConfigProductIds()
    {
        return $this->_scopeConfig->getValue(self::XML_GET_PRODUCT_IDS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
	
	public function getConfigEmailDomain()
    {
        return $this->_scopeConfig->getValue(self::XML_GET_EMAIL_DOMAIN, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
    
	public function getDownloads($email)
    {
		 $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
         $downloadEbookModel = $objectManager->create('BlueDev\DownloadEbook\Model\DownloadEbook'); 
		 $modelData = $downloadEbookModel->getCollection()->addFieldToFilter('email',$email)->getData();
        return $modelData;
    }
   
	public function getDownloadLink()
    {
            // companymodule is given in routes.xml
            // controller_name is folder name inside controller folder
            // action is php file name inside above controller_name folder

        return '/downloadebook/';
        // here controller_name is index, action is booking
    }
	
	
	
}