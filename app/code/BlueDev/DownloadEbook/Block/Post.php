<?php
namespace BlueDev\DownloadEbook\Block;
 
class Post extends \Magento\Framework\View\Element\Template
{
	protected $_productRepository;
	protected $_resource;
	protected $connection;
	public $_storeManager;
	
	/**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
	
	public function __construct(
								\Magento\Framework\View\Element\Template\Context $context,
								\Magento\Framework\App\ResourceConnection $resource,
								\Magento\Catalog\Model\ProductRepository $productRepository,
								\Magento\Store\Model\StoreManagerInterface $storeManager,
								\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
								array $data = []
    ) {
		$this->_productRepository = $productRepository;
		$this->_resource = $resource;
		$this->_storeManager=$storeManager;
		$this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    public function _prepareLayout() {
        return parent::_prepareLayout();
    }
	
	
	protected function getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        }
        return $this->connection;
    }
	
	public function getDirectQuery($email)
    {
        $table=$this->_resource->getTableName('bluedev_email_mapping'); 
        $downloaddata = $this->getConnection()->fetchRow('SELECT * FROM ' . $table. ' where email ="'.$email.'"' );
        return $downloaddata;
    }
}