<?php
namespace BlueDev\DownloadEbook\Controller\Index;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class Download extends \Magento\Framework\App\Action\Action
{
	
	
	
	protected $_productRepository;
	protected $_resource;
	protected $connection;
	public $_storeManager;
	public $_directory_list;
	
	public function __construct(
								 \Magento\Framework\App\Action\Context $context,
								\Magento\Framework\App\ResourceConnection $resource,
								\Magento\Catalog\Model\ProductRepository $productRepository,
								\Magento\Store\Model\StoreManagerInterface $storeManager,
								\Magento\Framework\App\Filesystem\DirectoryList $directoryList,
								array $data = []
    ) {
		$this->_productRepository = $productRepository;
		$this->_resource = $resource;
		$this->_storeManager=$storeManager;
		$this->_directory_list = $directoryList;
        parent::__construct($context);
    }	
	
	
    
    /**
     * Post user question
     *
     * @return void
     * @throws \Exception
     */
    public function execute()
    {
       // parent::execute();
        $id = $this->getRequest()->getParam('id', 0);
        if (!$id) {
            $this->_redirect('*/*/');
            return;
        }
		

        //$this->inlineTranslation->suspend();
        try {
			
			$downloadInfo = $this->_getDirectQuery($id);


			$linkUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
			$filePath = $linkUrl."downloadable/files/links".$downloadInfo['link_file'];
			$baseUrlWithOutIndexPhp = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
			$dirpath = $this->_directory_list->getPath('media')."/downloadable/files/links";
			
			$fullPath = $dirpath.$downloadInfo['link_file'];//exit;

			if (file_exists($fullPath)) {
				//$path = '/home/someuser/products/data.tar.gz'; // the file made available for download via this PHP file
				$mm_type="application/octet-stream"; // modify accordingly to the file type of $path, but in most cases no need to do so

				header("Pragma: public");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Type: " . $mm_type);
				header("Content-Length: " .(string)(filesize($fullPath)) );
				header('Content-Disposition: attachment; filename="'.basename($fullPath).'"');
				header("Content-Transfer-Encoding: binary\n");

				readfile($fullPath); // outputs the content of the file

				exit();
			} else {
				die('File does not exist');
			}
          
        } catch (\Exception $e) {
			echo $e->getMessage();
			exit;
            //$this->inlineTranslation->resume();
            $this->messageManager->addError(
                __('We can\'t process your request right now. Sorry, that\'s all we know.')
            );
            //$this->getDataPersistor()->set('contact_us', $post);
            $this->_redirect('downloadebook/index/index');
            return;
        }
    }
	
	protected function _getConnection()
    {
        if (!$this->connection) {
            $this->connection = $this->_resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        }
        return $this->connection;
    }
	
	public function _getDirectQuery($id)
    {
        $table=$this->_resource->getTableName('downloadable_link'); 
        $downloaddata = $this->_getConnection()->fetchRow('SELECT * FROM ' . $table. ' where product_id ='.$id );
        return $downloaddata;
    }

}
