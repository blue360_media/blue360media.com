<?php
namespace BlueDev\DownloadEbook\Controller\Index;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class Post extends \Magento\Framework\App\Action\Action
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * Post user question
     *
     * @return void
     * @throws \Exception
     */
    public function execute()
    {
       // parent::execute();
        $post = $this->getRequest()->getPostValue();
        if (!$post) {
            $this->_redirect('*/*/');
            return;
        }
		

        //$this->inlineTranslation->suspend();
        try {

          //    the next code to add data to database

             $created_date = date("Y-m-d H:i:s");
             //$postObject = new \Magento\Framework\DataObject();
             //$postObject->setData($post);
             $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
             $downloadEbookModel = $objectManager->create('BlueDev\DownloadEbook\Model\DownloadEbook');  
             
			 
			$error = false;

            if (!\Zend_Validate::is(trim($post['first_name']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['last_name']), 'NotEmpty')) {
                $error = true;
            }
            if (!\Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                $error = true;
            }
            if ($error) {
                throw new \Exception();
            } 
			
			$modelData = $downloadEbookModel->getCollection()->addFieldToFilter('email',$post['email'])->getData();
			//echo "<pre>";print_r($post);  exit;
			//echo $modelData[0]['id'];exit;
			if(!empty($modelData)){
			//if($modelData[0]['id'] > 0){
				$updateModel =  $downloadEbookModel->load($modelData[0]['id']); 
				$downloads = $modelData[0]['downloaded'] + 1;
				if($downloads < 11){
				
					if(isset($post['newsletter_permission'])){
					$newsletter_permission  = $post['newsletter_permission'];
					}
					else{
						$newsletter_permission  = 0;
					}	
					$updateModel->setDownloaded($downloads);
					$downloadEbookModel->setNewsletterPermission($newsletter_permission);
					$downloadEbookModel->setAllowedDownloads(10);
					$downloadEbookModel->setStatus(1);
					$downloadEbookModel->setUpdatedAt($created_date);
					$downloadEbookModel->setIpAddress($_SERVER['REMOTE_ADDR']);
					$updateModel->save();			
				
					$cookie_name = "DownloadEbook_email";
					$cookie_value = $post['email'];
					setcookie($cookie_name, $cookie_value, time()+3600); /* expire in 1 hour */
					$this->messageManager->addSuccessMessage('Successfully Updated and Verified the details, You can download the Ebooks now');
					$this->_redirect('downloadebook/index/index');
					return;				
				}
				else{
					setcookie("DownloadEbook_email", "", time()-3600); /* expire in 1 hour */
					$this->messageManager->addErrorMessage('You have reached to the Max Allowed Download Limit, Please call Blue360 Media Support for more Downloads');
					$this->_redirect('downloadebook/index/index');					
				}	
				//echo "<pre>";print_r($modelData);  exit;
				// Display the succes form validation message
				
				
			//}
			}			
			else{
				// Retrieve your form data
				$firstname   = $post['first_name'];
				$lastname    = $post['last_name'];
				$email       = $post['email'];
				$phone       = $post['phone'];
				$department  = $post['department'];
				
				$block= $this->_objectManager->get('BlueDev\DownloadEbook\Block\Post');    
				$emailMapping = $block->getDirectQuery($email); 
				//print_r($emailMapping);exit;
				
				
				if(isset($post['newsletter_permission'])){
				$newsletter_permission  = $post['newsletter_permission'];
				}
				else{
					$newsletter_permission  = 0;
				}
				$emailvalidate = explode('@',$email);
                $email_domain = explode(",",$post['email_domain']);
                //print_r($email_domain);exit;				
				//if($emailvalidate[1]== $post['email_domain']){	
                if (in_array($emailvalidate[1], $email_domain)  || !empty($emailMapping)){				
				//$test =  $model->load(1); 
				$downloadEbookModel->setFirstName($firstname);
				$downloadEbookModel->setLastName($lastname);
				$downloadEbookModel->setEmail($email);
				$downloadEbookModel->setPhone($phone);
				$downloadEbookModel->setDepartment($department);
				$downloadEbookModel->setNewsletterPermission($newsletter_permission);
				$downloadEbookModel->setDownloaded(1);
				$downloadEbookModel->setAllowedDownloads(10);
				$downloadEbookModel->setStatus(1);
				$downloadEbookModel->setCreatedAt($created_date);
				$downloadEbookModel->setUpdatedAt($created_date);
				$downloadEbookModel->setIpAddress($_SERVER['REMOTE_ADDR']);
				//$downloadEbookModel->setData($post);
				$downloadEbookModel->save();			

				// Display the succes form validation message
				$this->messageManager->addSuccessMessage('Successfully Added and Verified the details, You can download the EBooks Now');
				$cookie_name = "DownloadEbook_email";
				$cookie_value = $email;
				setcookie($cookie_name, $cookie_value, time()+3600); /* expire in 1 hour */
				}
				else{
					$this->messageManager->addErrorMessage('Please fill Official Email');
					setcookie("DownloadEbook_email", "", time()-3600);
				}
				// Redirect to your form page (or anywhere you want...)
				$this->_redirect('downloadebook/index/index');
				return;
			} 
        } catch (\Exception $e) {
			echo $e->getMessage();
			exit;
            //$this->inlineTranslation->resume();
            $this->messageManager->addError(
                __('We can\'t process your request right now. Sorry, that\'s all we know.')
            );
            //$this->getDataPersistor()->set('contact_us', $post);
            $this->_redirect('downloadebook/index/index');
            return;
        }
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }
}
