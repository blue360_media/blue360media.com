<?php

namespace BlueDev\DownloadEbook\Model;
use Magento\Framework\Model\AbstractModel;
class DownloadEbook extends AbstractModel
{
        public function _construct() {
         $this->_init('BlueDev\DownloadEbook\Model\Resource\DownloadEbook');
    }     
}