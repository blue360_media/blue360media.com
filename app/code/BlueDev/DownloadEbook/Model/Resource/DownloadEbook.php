<?php 

namespace BlueDev\DownloadEbook\Model\Resource;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
class DownloadEbook extends  AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        /* Custom Table Name */
         $this->_init('bluedev_downloadebook','id');
    }
}