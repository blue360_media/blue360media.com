<?php

namespace BlueDev\DownloadEbook\Model\Resource\DownloadEbook;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init('BlueDev\DownloadEbook\Model\DownloadEbook', 'BlueDev\DownloadEbook\Model\Resource\DownloadEbook');
    }
}