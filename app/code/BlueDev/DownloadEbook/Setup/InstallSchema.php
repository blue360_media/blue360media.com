<?php
namespace BlueDev\DownloadEbook\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        if (!$installer->tableExists('bluedev_downloadebook')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('bluedev_downloadebook'))
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
                    'Id'
                )
                ->addColumn(
                    'department',
                    Table::TYPE_TEXT, 
                    100, 
                    ['nullable' => false],
                    'Department'
                )
                ->addColumn(
                    'first_name',
                    Table::TYPE_TEXT, 
                    25, 
                    ['nullable' => false],
                    'First Name'
                )    
                ->addColumn(
                    'last_name',
                    Table::TYPE_TEXT, 
                    25, 
                    ['nullable' => false],
                    'Last Name'
                )
                ->addColumn(
                    'email', 
                    Table::TYPE_TEXT,
                    50, 
                    ['nullable' => false],
                    'Official Email'
                )
                ->addColumn(
                    'phone',
                    Table::TYPE_TEXT,
                    12,
                    ['default' => ''],
                    'Primary Contact Phone'
                )
                ->addColumn(
                    'product_id',
                    Table::TYPE_TEXT,
                    255, 
                    ['nullable' => false],
                    'Product Id'
                )
                ->addColumn(
                    'SKU',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Product Sku'
                )
				->addColumn(
                    'allowed_downloads',
                    Table::TYPE_SMALLINT,
                    1,
                    ['nullable' => true],
                    'Allowed Downloads'
                )
				->addColumn(
                    'downloaded',
                    Table::TYPE_SMALLINT,
                    1,
                    ['nullable' => true],
                    'No of Downloads'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_SMALLINT,
                    1,
                    ['nullable' => true],
                    'Status'
                )
				 ->addColumn(
                    'newsletter_permission',
                    Table::TYPE_SMALLINT,
                    0,
                    ['nullable' => true],
                    'Do we have permission to send you Notifications'
                )
				->addColumn(
					'created_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => true],
					'Post Creation Time'
				)
				->addColumn(
					'updated_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => true],
					'Post Modification Time'
				)               
                ->addColumn(
                    'ip_address',
                    Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'Ip Address'
                )
                ->setComment('BlueDev DownloadEbook');

            $installer->getConnection()->createTable($table);
        }
		
		if (!$installer->tableExists('bluedev_email_mapping')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('bluedev_email_mapping1'))
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
                    'Id'
                )
                ->addColumn(
                    'email',
                    Table::TYPE_TEXT,
                    255, 
                    ['nullable' => false],
                    'Email'
                )
                ->addColumn(
                    'products',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Products'
                )
                ->addColumn(
                    'status',
                    Table::TYPE_SMALLINT,
                    1,
                    ['nullable' => true],
                    'Status'
                )
				->addColumn(
					'created_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => true],
					'Post Creation Time'
				)
				->addColumn(
					'updated_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					['nullable' => true],
					'Post Modification Time'
				)
                ->setComment('BlueDev Email Mapping');

            $installer->getConnection()->createTable($table);
        }
        
        $installer->endSetup();
    }
}
