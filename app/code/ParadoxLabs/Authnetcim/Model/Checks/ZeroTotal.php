<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ParadoxLabs\Authnetcim\Model\Checks;

use Magento\Payment\Model\MethodInterface;
use Magento\Quote\Model\Quote;

class ZeroTotal extends \Magento\Payment\Model\Checks\ZeroTotal
{
    /**
     * Check whether payment method is applicable to quote,
     * And make AuthnetCIM Payment Method always applicable.
     *
     * @param MethodInterface $paymentMethod
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool
     */
    public function isApplicable(MethodInterface $paymentMethod, Quote $quote)
    {
        $isApplicable = parent::isApplicable($paymentMethod, $quote);
        return $isApplicable || $paymentMethod->getCode() == 'authnetcim';
    }
}
