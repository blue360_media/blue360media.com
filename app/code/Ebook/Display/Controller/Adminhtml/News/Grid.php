<?php
namespace Ebook\Display\Controller\Adminhtml\News;
use Ebook\Display\Controller\Adminhtml\News;
class Grid extends News
{
/**
* @return void
*/
public function execute()
{
return $this->_resultPageFactory->create();
}
}