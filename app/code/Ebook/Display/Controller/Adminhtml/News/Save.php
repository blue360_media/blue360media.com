<?php
namespace Ebook\Display\Controller\Adminhtml\News;
use Ebook\Display\Controller\Adminhtml\News;
class Save extends News
{
/**
* @return void
*/
public function execute()
{


    



	$isPost = $this->getRequest()->getPost();
	if ($isPost) {
		$post = $this->getRequest()->getPostValue();

	    $order_item_id=$post['order_item_id'];
	    $product_id=$post['product_id'];
	    $number_of_downloads_bought=$post['number_of_downloads_bought'];
	    $number_of_downloads_used=$post['number_of_downloads_used'];
	    $status=$post['status'];
	    //$emp_joindate=$post['emp_joindate'];

	$newsModel = $this->_modelContactFactory->create();
	$newsId = $this->getRequest()->getParam('id');

	//echo"<BR>==".$newsId;
	if ($newsId) {
	$newsModel->load($newsId);
	}

	$formData = $this->getRequest()->getParam('news');

	//print_r($formData);
	//exit();

	 $newsModel->addData([
	        "order_item_id" => "$order_item_id",
	        "product_id" => "$product_id",
	        "number_of_downloads_bought" => "$number_of_downloads_bought",
	        "number_of_downloads_used" => "$number_of_downloads_used",
	        "status" => "$status"
	        ]);


	   $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	   $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	   $connection = $resource->getConnection();
	   $tableName = $resource->getTableName('downloadable_link_purchased_item'); 
      


	$newsModel->setData($formData);
	try {
	// Save news
	$newsModel->save();

    /* Starts Update downloadable_link_purchased_item */

      $sql = "update $tableName 
               set number_of_downloads_bought=$number_of_downloads_bought, 
               number_of_downloads_used=$number_of_downloads_used,
               status='$status' where order_item_id= ".$order_item_id;

    /* Finish Update downloadable_link_purchased_item */

	   $result = $connection->query($sql); 


	// Display success message
	$this->messageManager->addSuccess(__('The news has been saved.'));
	// Check if 'Save and Continue'
	if ($this->getRequest()->getParam('back')) {
	$this->_redirect('*/*/edit', ['id' => $newsModel->getId(), '_current' => true]);
	return;
	}
	// Go to grid page
	$this->_redirect('*/*/');
	return;
	} 
	catch (\Exception $e) 
	{
	$this->messageManager->addError($e->getMessage());
	}
	$this->_getSession()->setFormData($formData);
	$this->_redirect('*/*/edit', ['id' => $newsId]);
	}
	}
}