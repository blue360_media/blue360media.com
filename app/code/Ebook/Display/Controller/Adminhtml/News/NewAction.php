<?php
namespace Ebook\Display\Controller\Adminhtml\News;
use Ebook\Display\Controller\Adminhtml\News;
class NewAction extends News
{
/**
* Create new news action
*
* @return void
*/
public function execute()
{
$this->_forward('index');
}
}