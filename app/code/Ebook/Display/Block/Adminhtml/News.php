<?php
/**
 * Copyright © 2015 Ebook . All rights reserved.
 */
namespace Ebook\Display\Block\Adminhtml;
class News extends \Magento\Backend\Block\Widget\Grid\Container
{

	protected function _construct()
	{
		$this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'Ebook_Display';
        $this->_headerText = __('Manage Ebook Display');
        $this->_addButtonLabel = __('Ebook Display');
        parent::_construct();
	}
}

