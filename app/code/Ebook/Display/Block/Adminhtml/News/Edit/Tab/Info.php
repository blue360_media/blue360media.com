<?php
namespace Ebook\Display\Block\Adminhtml\News\Edit\Tab;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Cms\Model\Wysiwyg\Config;
use Ebook\Display\Model\System\Config\Status;
class Info extends Generic implements TabInterface
{
/**
* @var \Magento\Cms\Model\Wysiwyg\Config
*/
protected $_wysiwygConfig;
/**
* @var \Tutorial\SimpleNews\Model\Config\Status
*/
protected $_newsStatus;
/**
* @param Context $context
* @param Registry $registry
* @param FormFactory $formFactory
* @param Config $wysiwygConfig
* @param Status $newsStatus
* @param array $data
*/
public function __construct(
Context $context,
Registry $registry,
FormFactory $formFactory,
Config $wysiwygConfig,
Status $newsStatus,
array $data = []
) {
$this->_wysiwygConfig = $wysiwygConfig;
$this->_newsStatus = $newsStatus;
parent::__construct($context, $registry, $formFactory, $data);
}
/**
* Prepare form fields
*
* @return \Magento\Backend\Block\Widget\Form
*/
protected function _prepareForm()
{
/** @var $model \Ebook\Display\Model\Contact */
$model = $this->_coreRegistry->registry('display_news');
/** @var \Magento\Framework\Data\Form $form */
$form = $this->_formFactory->create();
$form->setHtmlIdPrefix('');
$form->setFieldNameSuffix('');
$fieldset = $form->addFieldset(
'base_fieldset',
['legend' => __('General')]
);
if ($model->getId()) {
$fieldset->addField('id','hidden',['name' => 'id']);
}
$fieldset->addField('order_item_id','text',['name' => 'order_item_id','label' => __('Order Item Id'),'readonly' => true,'required' => true]);
$fieldset->addField('product_id','text',['name' => 'product_id','label' => __('Product ID'), 'readonly' => true,'required' => true]);

$fieldset->addField('number_of_downloads_bought','text',['name' => 'number_of_downloads_bought','label' => __('Number of downloads bought'),'required' => true,'required' => true
]
);

$fieldset->addField('number_of_downloads_used','text',['name' => 'number_of_downloads_used','label' => __('Number of downloads used'),'required' => true,'required' => true
]
);


$fieldset->addField(
'status',
'select',
array(
'name' => 'status',
'label' => __('Status'),
'title' => __('Status'),
'required' => true,
'values' => array(
	              array('value'=>'', 'label'=>'Status'),
	              array('value'=>'available', 'label'=>'available'),
                  array('value'=>'pending', 'label'=>'pending'),
                  array('value'=>'expired', 'label'=>'expired'),
                 )
));



$data = $model->getData();
$form->setValues($data);
$this->setForm($form);
return parent::_prepareForm();
}
/**
* Prepare label for tab
*
* @return string
*/
public function getTabLabel()
{
return __('News Info');
}
/**
* Prepare title for tab
*
* @return string
*/
public function getTabTitle()
{
return __('News Info');
}
/**
* {@inheritdoc}
*/
public function canShowTab()
{
return true;
}
/**
* {@inheritdoc}
*/
public function isHidden()
{
return false;
}
}