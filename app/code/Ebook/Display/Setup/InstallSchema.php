<?php
namespace Ebook\Display\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        // Get tutorial_simplenews table
        $tableName = $installer->getTable('Ebook_Display');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            // Create tutorial_simplenews table
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'order_item_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false, 'default' => '0'],
                    'Order Item Id'
                )
               ->addColumn(
                    'product_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false, 'default' => '0'],
                    'Product ID'
                )
                 ->addColumn(
                    'number_of_downloads_bought',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false,'default' => '0'],
                    'Number of downloads bought'
                )

                ->addColumn(
                    'number_of_downloads_used',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false,'default' => '0'],
                    'Number of downloads used'
                 )
                 ->addColumn(
                    'status',
                    Table::TYPE_TEXT,
                    50,
                    ['nullable' => true, 'default' => 'NULL'],
                    'Status'
                 ) 
                ->setComment('Ebook Display')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
