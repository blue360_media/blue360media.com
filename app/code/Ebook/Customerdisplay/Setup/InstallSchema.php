<?php
namespace Ebook\Customerdisplay\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        // Get tutorial_simplenews table
        $tableName = $installer->getTable('Ebook_Customerdisplay');
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            // Create tutorial_simplenews table
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'purchased_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false, 'default' => '0'],
                    'Purchased ID'
                )
               ->addColumn(
                    'order_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false, 'default' => '0'],
                    'Order ID'
                )
                 ->addColumn(
                    'order_increment_id',
                    Table::TYPE_TEXT,
                    50,
                    ['nullable' => false,'default' => '0'],
                    'Order Increment ID'
                )

                ->addColumn(
                    'order_item_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false,'default' => '0'],
                    'Order Item ID'
                 )

                 ->addColumn(
                    'customer_id',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false,'default' => '0'],
                    'Customerdisplay ID'
                 )

                 ->addColumn(
                    'product_name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => 'NULL'],
                    'Product Name'
                 ) 

                  ->addColumn(
                    'product_sku',
                    Table::TYPE_TEXT,
                    50,
                    ['nullable' => true, 'default' => 'NULL'],
                    'Product Sku'
                 ) 


                ->setComment('Ebook Customerdisplay')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
