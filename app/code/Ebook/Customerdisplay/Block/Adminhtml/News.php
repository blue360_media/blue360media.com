<?php
/**
 * Copyright © 2015 Ebook . All rights reserved.
 */
namespace Ebook\Customerdisplay\Block\Adminhtml;
class News extends \Magento\Backend\Block\Widget\Grid\Container
{

	protected function _construct()
	{
		$this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'Ebook_Customerdisplay';
        $this->_headerText = __('Manage Ebook Customerdisplay');
        $this->_addButtonLabel = __('Ebook Customerdisplay');
        parent::_construct();
	}
}

