<?php
/**
 *
 * Copyright © 2015 Employeecommerce. All rights reserved.
 */
namespace Ebook\Customerdisplay\Controller\Adminhtml\Index;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
class Index extends \Magento\Backend\App\Action
{

	/**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }
    /**
     * Check the permission to run it
     *
     * @return bool
     */
   /*  protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Cms::page');
    } */

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */

       /*Insert Values in ebook_customerdisplay */
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
       $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
       $connection = $resource->getConnection();
       $tableName = $resource->getTableName('ebook_customerdisplay'); 
      
         $sql_1 = "INSERT INTO 
                  ebook_customerdisplay (order_item_id,product_id,number_of_downloads_bought,number_of_downloads_used,status)
                  SELECT order_item_id,product_id,number_of_downloads_bought,number_of_downloads_used,status
                  FROM downloadable_link_purchased_item222";
         $result_1 = $connection->query($sql_1);
      /*Finish Values in ebook_customerdisplay */
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
