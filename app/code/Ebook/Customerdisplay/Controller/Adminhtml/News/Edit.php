<?php
namespace Ebook\Customerdisplay\Controller\Adminhtml\News;
use Ebook\Customerdisplay\Controller\Adminhtml\News;

class Edit extends News
{
/**
* @return void
*/
public function execute()
{
$newsId = $this->getRequest()->getParam('id');
/** @var \Tutorial\SimpleNews\Model\Contact $model */
$model = $this->_modelContactFactory->create();
if ($newsId) {
$model->load($newsId);
if (!$model->getId()) {
$this->messageManager->addError(__('This news no longer exists.'));
$this->_redirect('*/*/');
return;
}
}
// Restore previously entered form data from session
$data = $this->_session->getnewsData(true);
if (!empty($data)) {
$model->setData($data);
}
$this->_coreRegistry->register('customer_news', $model);
/** @var \Magento\Backend\Model\View\Result\Page $resultPage */
$resultPage = $this->_resultPageFactory->create();
$resultPage->setActiveMenu('Ebook_Customerdisplay::main_menu');
$resultPage->getConfig()->getTitle()->prepend(__('Ebook Customerdisplay'));
return $resultPage;
}
}