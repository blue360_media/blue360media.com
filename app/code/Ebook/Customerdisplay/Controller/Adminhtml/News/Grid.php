<?php
namespace Ebook\Customerdisplay\Controller\Adminhtml\News;
use Ebook\Customerdisplay\Controller\Adminhtml\News;
class Grid extends News
{
/**
* @return void
*/
public function execute()
{
return $this->_resultPageFactory->create();
}
}