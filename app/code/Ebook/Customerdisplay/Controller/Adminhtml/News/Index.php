<?php
/**
 *
 * Copyright © 2015 Employeecommerce. All rights reserved.
 */
namespace Ebook\Customerdisplay\Controller\Adminhtml\News;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Ebook\Customerdisplay\Controller\Adminhtml\News;
class Index extends \Magento\Backend\App\Action
{

	/**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(Context $context,PageFactory $resultPageFactory) {
		
    parent::__construct($context);
    $this->_resultPageFactory = $resultPageFactory;

}
    /**
     * Check the permission to run it
     *
     * @return bool
     */
   /*  protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magento_Cms::page');
    } */

    /**
     * Index action
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
       

        /* Starts Process To Insert Values in ebook_customerdisplay from  downloadable_link_purchased*/

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('downloadable_link_purchased'); 

         /*Starts Truncate Table ebook_customerdisplay To Fille New Upadated Data*/
            $sql_t="truncate table ebook_customerdisplay";
            $result_t = $connection->query($sql_t);  
          /*Finish Truncate Table ebook_customerdisplay To Fille New Upadated Data*/

        $sql_1 = "SELECT * FROM $tableName";
        $result_1 = $connection->fetchAll($sql_1); 
        $count_1=count($result_1);


        $tableName = $resource->getTableName('ebook_customerdisplay'); 

        $sql_2 = "SELECT * FROM $tableName";

        $result_2 = $connection->fetchAll($sql_2); 
        $count_2=count($result_2);

        if($count_1==$count_2) {

        } else {

 
        /*$sql_3 = "INSERT INTO 
                 ebook_display (order_item_id,product_id,number_of_downloads_bought,number_of_downloads_used,status)
                 SELECT order_item_id,product_id,number_of_downloads_bought,number_of_downloads_used,status
                 FROM downloadable_link_purchased_item222";
        $result_3 = $connection->query($sql_3);  */       

            $sql_3="SELECT  t1.order_item_id 
                    FROM   downloadable_link_purchased  t1
                    WHERE   t1.order_item_id NOT IN
                    (
                    SELECT  order_item_id
                    FROM    ebook_customerdisplay t2
                    )";      

            $result_3 = $connection->fetchAll($sql_3); 

            if(count($result_3)>0) {
         
            $total="";
            foreach($result_3 as $list) {
                $list['order_item_id'];
                $total=$total.$list['order_item_id'].",";
            } 
            $total1=substr($total, 0, -1);

           $sql_4="INSERT INTO 
         ebook_customerdisplay (purchased_id,order_id,order_increment_id,order_item_id,customer_id,product_name,product_sku)
                   SELECT t1.purchased_id,t1.order_id,t1.order_increment_id,t1.order_item_id,t1.customer_id,t1.product_name,t1.product_sku
                   FROM downloadable_link_purchased t1 where t1.order_item_id in "."(".$total1.")";

           $result_4 = $connection->query($sql_4);    

           

           //Starts Email Update Process
           $sql_5="update ebook_customerdisplay t1,customer_entity t2

                   set t1.customer_id=t2.email

                   where t1.customer_id=t2.entity_id";

           $result_5 = $connection->query($sql_5);    
           //Finish Email Update Process


           } else {
            echo"<BR>Something missing, Please try again.";
           } 

              //exit();  

        }
    /* Finish Process To Insert Values in ebook_customerdisplay from  downloadable_link_purchased */




        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
         if($this->getRequest()->getQuery('ajax')) {
           $this->_forward('grid');
           return;
           }

         $resultPage = $this->_resultPageFactory->create();
         $resultPage->setActiveMenu('Ebook_Customerdisplay\::main_menu');
         $resultPage->getConfig()->getTitle()->prepend(__('Ebook Customerdisplay'));
         return $resultPage;
    }
}

