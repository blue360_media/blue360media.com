<?php
namespace Ebook\Customerdisplay\Controller\Adminhtml\News;
use Ebook\Customerdisplay\Controller\Adminhtml\News;
class Save extends News
{
/**
* @return void
*/
public function execute()
{


    



	$isPost = $this->getRequest()->getPost();
	if ($isPost) {
		$post = $this->getRequest()->getPostValue();

	    $purchased_id=$post['purchased_id'];
	    $order_id=$post['order_id'];
	    $order_increment_id=$post['order_increment_id'];
	    $order_item_id=$post['order_item_id'];
	    $customer_id=$post['customer_id'];
	    $product_name=$post['product_name'];
	    $product_sku=$post['product_sku'];
	    //$emp_joindate=$post['emp_joindate'];

	$newsModel = $this->_modelContactFactory->create();
	$newsId = $this->getRequest()->getParam('id');

	//echo"<BR>==".$newsId;
	if ($newsId) {
	$newsModel->load($newsId);
	}

	$formData = $this->getRequest()->getParam('news');

	//print_r($formData);
	//exit();

	 $newsModel->addData([
	        "order_item_id" => "$purchased_id",
	        "order_id" => "$order_id",
	        "order_increment_id" => "$order_increment_id",
	        "order_item_id" => "$order_item_id",
	        "customer_id" => "$customer_id",
	         "product_name" => "$product_name",
	        "product_sku" => "$product_sku"
	        ]);


	$newsModel->setData($formData);
	try {
	// Save news
	$newsModel->save();

	// Customerdisplay success message
	$this->messageManager->addSuccess(__('The news has been saved.'));
	// Check if 'Save and Continue'
	if ($this->getRequest()->getParam('back')) {
	$this->_redirect('*/*/edit', ['id' => $newsModel->getId(), '_current' => true]);
	return;
	}
	// Go to grid page
	$this->_redirect('*/*/');
	return;
	} 
	catch (\Exception $e) 
	{
	$this->messageManager->addError($e->getMessage());
	}
	$this->_getSession()->setFormData($formData);
	$this->_redirect('*/*/edit', ['id' => $newsId]);
	}
	}
}