<?php
namespace Ebook\Customerdisplay\Controller\Adminhtml\News;
use Ebook\Customerdisplay\Controller\Adminhtml\News;
class NewAction extends News
{
/**
* Create new news action
*
* @return void
*/
public function execute()
{
$this->_forward('index');
}
}