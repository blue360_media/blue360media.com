<?php
/**
 *
 * Copyright © 2015 Employeecommerce. All rights reserved.
 */
namespace Ebook\Customerdisplay\Controller\Index;

use Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\ResultFactory;
use Ebook\Customerdisplay\Model\ContactFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_modelContactFactory;
	
	
    /**
     * @var \Magento\Framework\App\Cache\TypeCustomerInterface
     */
    protected $_cacheTypeCustomer;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeCustomerInterface $cacheTypeCustomer
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeCustomerInterface $cacheTypeCustomer,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		ContactFactory $modelContactFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeCustomer = $cacheTypeCustomer;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
        $this->_messageManager = $messageManager;
		$this->_modelContactFactory = $modelContactFactory;
    }
    
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		
		 	/* Starts Process For Base URL */
	         $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
             $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
             $baseUrl = $storeManager->getStore()->getBaseUrl();	
			 
			 $red_url=$baseUrl."customerdisplay/"."customerdisplay/customerdisplay";
	        /* Finish Process For Base URL */

         $post = $this->getRequest()->getPostValue();
         if(isset($post) && !empty($post)) {

         $engineer_name=$post['engineer_name'];
         $engineer_dept=$post['engineer_dept'];
         $engineer_add=$post['engineer_add'];
         $engineer_salary=$post['engineer_salary'];
         $engineer_joindate=$post['engineer_joindate'];

        $contact = $this->_objectManager->create('Ebook\Customerdisplay\Model\Contact');
        $contact->addData([
            "engineer_name" => "$engineer_name",
            "engineer_dept" => "$engineer_dept",
            "engineer_add" => "$engineer_add",
            "engineer_salary" => "$engineer_salary",
            "engineer_joindate" => "$engineer_joindate"
        ]);

        $saveData = $contact->save();

        if($saveData) {
           $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
           $resultRedirect->setUrl("$red_url");
           return $resultRedirect;
          } else {
           $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
          $resultRedirect->setUrl("$red_url");
          }
       
	   
	   
	
	   
        
		
      
       }
	   
	    $this->resultPage = $this->resultPageFactory->create();  
         return $this->resultPage;
      
	  
	  
	  /*$post = $this->_postFactory->create();
		$collection = $post->getCollection();
		foreach($collection as $item){
			echo "<pre>";
			print_r($item->getData());
			echo "</pre>";
		}
		exit();
		return $this->_pageFactory->create();*/

       
     
    }
}
