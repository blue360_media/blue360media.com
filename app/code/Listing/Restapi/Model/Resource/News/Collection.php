<?php
namespace Listing\Restapi\Model\Resource\News;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Listing\Restapi\Model\News',
            'Listing\Restapi\Model\Resource\News'
        );
    }
}