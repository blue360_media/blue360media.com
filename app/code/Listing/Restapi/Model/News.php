<?php
namespace Listing\Restapi\Model;
use Magento\Framework\Model\AbstractModel;
class News extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Listing\Restapi\Model\Resource\News');
    }
}