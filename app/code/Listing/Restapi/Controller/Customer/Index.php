<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Customer;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		
	
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $baseUrl = $storeManager->getStore()->getBaseUrl();	
	
	$adminUrl="$baseUrl"."index.php/rest/V1/integration/admin/token";  
		
		$ch = curl_init();
		//$data = array("username" => "john1", "password" => "admin123!!");    
		$data = array("username" => "john", "password" => "john234!@#");                                                                 
		$data_string = json_encode($data);                       
		$ch = curl_init($adminUrl); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);       

		$token = curl_exec($ch);
		$token=  json_decode($token); 

		//Use above token into header

		$headers = array("Authorization: Bearer $token"); 

		/* $requestUrl="$baseUrl"."index.php/rest/V1/directory/currency";
		 $ch = curl_init();
		 $ch = curl_init($requestUrl);	
		 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
		 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
		 $result = curl_exec($ch);*/
		 
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	    $connection = $resource->getConnection();
	    $tableName = $resource->getTableName('customer_entity'); 
	
		$sql = "Select * FROM " . $tableName. " order by entity_id DESC limit 0,100 ";
		//$sql = "Select * FROM " . $tableName. " order by entity_id ";
		$result = $connection->fetchAll($sql); 

		//print_r($result);
		//exit();
		
		//$result=  json_decode($result, true);	
			
        //$result=  json_decode($result, true);	
		
		$data[]=array('error'=>'false','message'=>'Customer Listing','status_code'=>'200','data'=>$result);
		
		unset($data['username']);
	    unset($data['password']); 

    	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);
        
    }
}
