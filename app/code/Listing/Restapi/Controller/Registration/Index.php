<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Registration;
class Index extends \Magento\Framework\App\Action\Action
{
	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;
    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;
    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
       \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->resultPageFactory = $resultPageFactory;
    }
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
	
	//Authentication rest API magento2.Please change url accordingly your url
		
	$post = $this->getRequest()->getPostValue();

    //echo "<pre>";
    //print_r($post);
	
	$cust_username=$_POST['cust_username'];
	$cust_firstname=$_POST['cust_firstname'];
	$cust_lastname=$_POST['cust_lastname'];
	$cust_phoneno=$_POST['cust_phoneno'];
	$cust_address=$_POST['cust_address'];
	$cust_pass=$_POST['cust_pass'];
	$cust_confirmpass=$_POST['cust_confirmpass'];
    //exit;
		
	  $customerData = [
        'customer' => [
            "email" => "$cust_username",
            "firstname" => "$cust_firstname",
            "lastname" => "$cust_lastname",
            "storeId" => 1,
            "websiteId" => 1
        ],
        "password" => "$cust_pass"
    ];	
	
	$adminUrl='http://127.0.0.1/magento222/index.php/rest/V1/integration/admin/token';
	
	$ch = curl_init();
	$data = array("username" => "john", "password" => "john234!@#");                                                                    
	$data_string = json_encode($data);                       
	$ch = curl_init($adminUrl); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		'Content-Type: application/json',                                                                                
		'Content-Length: ' . strlen($data_string))                                                                       
	);       

	$token = curl_exec($ch);
	$token=  json_decode($token); 
	//exit();

	//echo $token;  
	//Use above token into header
	//$headers = array("Authorization: Bearer $token");  // Change
		
	$requestUrl='http://127.0.0.1/magento222/index.php/rest/V1/customers';	

	//echo json_encode($result, JSON_UNESCAPED_SLASHES);	

    $email_id="mahesh21@example.com";

    $this->_resources = \Magento\Framework\App\ObjectManager::getInstance()
        ->get('Magento\Framework\App\ResourceConnection');
    $connection= $this->_resources->getConnection();
 
    $select = $connection->select()
        ->from(
            ['o' =>  $this->_resources->getTableName('customer_entity')]
        )->where('o.email=?',$email_id);
 
    $result = $connection->fetchAll ($select);
    $total=count($result);
	

	
       if($total==1) {
	$ch = curl_init();
	$ch = curl_init($requestUrl);	
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   //Change
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

	$result = curl_exec($ch);
	$result=  json_decode($result);	
	
	//echo '<pre>';print_r($result);
		   
	$data[]=array('error'=>'true','message'=>'A customer with the same email already exists in an associated website.','status_code'=>'404','data'=>$result);
		} else {
	$ch = curl_init();
	$ch = curl_init($requestUrl);	
	//curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");   //Change
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($customerData));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Authorization: Bearer " . json_decode($token)));

	$result = curl_exec($ch);
	$result=  json_decode($result);	
	
	//echo '<pre>';print_r($result);
			
			
			
		$data[]=array('error'=>'false','message'=>'Registration has been done successfully, User Credentails has beeen sent to your registered email','status_code'=>'200','data'=>$result);
		}

		unset($data['username']);
	    unset($data['password']); 

		echo json_encode($data[0], JSON_UNESCAPED_SLASHES);	

    }
}
