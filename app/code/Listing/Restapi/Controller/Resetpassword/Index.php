<?php

namespace Listing\Restapi\Controller\Resetpassword;

class Index extends \Magento\Framework\App\Action\Action
{

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Encryption\Encryptor $encryptor,
        \Magento\Customer\Model\CustomerRegistry $customerRegistry
		) 
	{
		parent::__construct($context);
		$this->_customerRepository = $customerRepository;
		$this->_encryptor          = $encryptor;
		$this->_customerRegistry   = $customerRegistry;
	}

	public function execute()
	{
		/*if(isset($_POST['emailid'])){
			$emailid=$_POST['emailid'];
		}*/

		if(isset($_POST['newpassword'])){
			$newpassword=$_POST['newpassword'];	
			$newpassword_confirm=$_POST['newpassword_confirm'];
		}
		
		$username=$_POST['username'];		
		
		$customer = $this->_customerRepository->get($username);

		if(isset($_POST['newpassword']))
		{
			if($newpassword == $newpassword_confirm){

				$passwordHash = $this->_encryptor->getHash("$newpassword", true);
				$customerSecure = $this->_customerRegistry->retrieveSecureData($customer->getId());
				$customerSecure->setRpToken(null);
				$customerSecure->setRpTokenCreatedAt(null);
				$customerSecure->setPasswordHash($passwordHash);
				$this->_customerRepository->save($customer, $passwordHash);   
	 
				$data[]=array('error'=>'false','message'=>'Your password has been changed successfully','status_code'=>'200');

			} else if($newpassword!=$newpassword_confirm) { 
				  
				$data[]=array('error'=>'true','message'=>'Please fill confirm password as password','status_code'=>'404');
       	 
			} else {
					  
				$data[]=array('error'=>'true','message'=>'Please fill required fields','status_code'=>'404');
					
			}
		}

		/*if(isset($_POST['emailid'])){

			$customer->setEmail($emailid);
			$this->_customerRepository->save($customer);

			$data[]=array('error'=>'false','message'=>'Your email id has been changed successfully','status_code'=>'200');

		}*/

		//echo json_encode($data[0], JSON_UNESCAPED_SLASHES);
    }
}
