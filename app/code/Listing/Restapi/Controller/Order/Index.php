<?php
/**
 *
 * Copyright © 2015 Listingcommerce. All rights reserved.
 */
namespace Listing\Restapi\Controller\Order;

class Index extends \Magento\Framework\App\Action\Action
{

	/**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var \Magento\Framework\App\Cache\StateInterface
     */
    protected $_cacheState;

    /**
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
	
	/**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     * Keys for pass through variables in OutstandingInvoice/OutstandingInvoice_payment
     * Uses additional_information as storage
     */
    const XML_GET_PRODUCT_SKUS = 'downloadebook/appsettings/app_product_skus';
  

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\StateInterface $cacheState
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\StateInterface $cacheState,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheState = $cacheState;
        $this->_cacheFrontendPool = $cacheFrontendPool;
		$this->_scopeConfig = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
    }
	
    /**
     * Flush cache storage
     *
     */
    public function execute()
    {
		
	
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
    $baseUrl = $storeManager->getStore()->getBaseUrl();	
	
	$adminUrl="$baseUrl"."index.php/rest/V1/integration/admin/token";  
		
		$ch = curl_init();
		//$data = array("username" => "john1", "password" => "admin123!!");    
		$data = array("username" => "shobhit", "password" => "Hrhk@24shbt");                                                                 
		$data_string = json_encode($data);                       
		$ch = curl_init($adminUrl); 
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);       

		$token = curl_exec($ch);
		$token=  json_decode($token); 

		//Use above token into header

		$headers = array("Authorization: Bearer $token"); 

		/* $requestUrl="$baseUrl"."index.php/rest/V1/directory/currency";
		 $ch = curl_init();
		 $ch = curl_init($requestUrl);	
		 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
		 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
		 $result = curl_exec($ch);*/
		 
		 
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	    $connection = $resource->getConnection();
	    //$tableName = $resource->getTableName('customer_entity'); 
		//$products = array("3545335ME01","3545334ME01","3670723BE01");
		$products = $this->_scopeConfig->getValue(self::XML_GET_PRODUCT_SKUS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
		$products = explode(",",$products);
		$products = implode('","',$products);
		
		
		$sql = 'SELECT o.increment_id, o.customer_email, o.customer_id, oi.sku,oi.product_id FROM sales_order_item oi INNER JOIN sales_order o ON o.entity_id = oi.order_id WHERE sku in ("'.$products.'") ORDER BY o.increment_id DESC';
		
		$result = $connection->fetchAll($sql); 

		//print_r($result);exit();
		
		//$result=  json_decode($result, true);	
			
        //$result=  json_decode($result, true);	
		
		$data[]=array('error'=>'false','message'=>'Product Orders with Customer','status_code'=>'200','data'=>$result);
		
		unset($data['username']);
	    unset($data['password']); 

    	echo json_encode($data[0], JSON_UNESCAPED_SLASHES);
        
    }
}
