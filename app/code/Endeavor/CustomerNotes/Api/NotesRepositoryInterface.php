<?php
namespace Endeavor\CustomerNotes\Api;

interface NotesRepositoryInterface
{
    /**
     * Save Notes
     * @param \Endeavor\CustomerNotes\Api\Data\NotesInterface $notes
     * @return \Endeavor\CustomerNotes\Api\Data\NotesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Endeavor\CustomerNotes\Api\Data\NotesInterface $notes
    );

    /**
     * Retrieve Notes
     * @param string $notesId
     * @return \Endeavor\CustomerNotes\Api\Data\NotesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($notesId);

    /**
     * Retrieve Notes matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Endeavor\CustomerNotes\Api\Data\NotesSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Notes
     * @param \Endeavor\CustomerNotes\Api\Data\NotesInterface $notes
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Endeavor\CustomerNotes\Api\Data\NotesInterface $notes
    );

    /**
     * Delete Notes by ID
     * @param string $notesId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($notesId);
}
