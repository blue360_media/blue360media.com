<?php
namespace Endeavor\CustomerNotes\Api\Data;

interface NotesSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get Notes list.
     * @return \Endeavor\CustomerNotes\Api\Data\NotesInterface[]
     */
    public function getItems();

    /**
     * Set content list.
     * @param \Endeavor\CustomerNotes\Api\Data\NotesInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
