<?php
namespace Endeavor\CustomerNotes\Api\Data;

interface NotesInterface
{
    /*
     * colums name in database table
     * 
     * @var string
     */
    const CUSTOMER_ID = 'customer_id';
    const CREATED_AT = 'created_at';
    const CONTENT = 'content';
    const NOTES_ID = 'notes_id';


    /**
     * Get notes_id
     * @return string|null
     */
    public function getNotesId();

    /**
     * Set notes_id
     * @param string $notesId
     * @return Endeavor\CustomerNotes\Api\Data\NotesInterface
     */
    public function setNotesId($notesId);

    /**
     * Get content
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     * @param string $content
     * @return Endeavor\CustomerNotes\Api\Data\NotesInterface
     */
    public function setContent($content);

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customer_id
     * @return Endeavor\CustomerNotes\Api\Data\NotesInterface
     */
    public function setCustomerId($customer_id);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $created_at
     * @return Endeavor\CustomerNotes\Api\Data\NotesInterface
     */
    public function setCreatedAt($created_at);
}
