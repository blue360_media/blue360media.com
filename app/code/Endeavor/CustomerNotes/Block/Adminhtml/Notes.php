<?php
namespace Endeavor\CustomerNotes\Block\Adminhtml;

class Notes extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_notes';
        $this->_blockGroup = 'Endeavor_CustomerNotes';
        $this->_headerText = __('Manage Customer Notes');

        parent::_construct();

        if ($this->_isAllowedAction('Endeavor_CustomerNotes::Notes_save')) {
            $this->buttonList->update('add', 'label', __('Add New Notes'));
        } else {
            $this->buttonList->remove('add');
        }
    }

    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
