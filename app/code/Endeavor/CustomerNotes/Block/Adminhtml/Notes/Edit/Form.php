<?php
namespace Endeavor\CustomerNotes\Block\Adminhtml\Notes\Edit;

/**
 * Adminhtml Customer Notes edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;


    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Cms\Model\Wysiwyg\Config $systemStore
     * @param \Magento\Store\Model\System\Store $wysiwygConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, 
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->_wysiwygConfig =$wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('notes_form');
        $this->setTitle(__('Notes Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Endeavor\CustomerNotes\Model\Notes\ $model */
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        //Load product by product id
        $model = $objectManager->create('Endeavor\CustomerNotes\Model\Notes');

//        $model = $this->_coreRegistry->registry('endeavor_customernotes_notes');

          /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'note_form',
                'action' => $this->getData('action'),
                'method' => 'post',
                'data-form-part' =>'customer_form'
                ]
            ]
        );


        $form->setHtmlIdPrefix('note_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Customer Notes'), 'class' => 'fieldset-wide']
        );

        if ($model->getNotesId()) {
            $fieldset->addField('notes_id', 'hidden', ['name' => 'notes_id']);
        }
        
       $this->_wysiwygConfig->getConfig()->setData('height','20em');

        $fieldset->addField(
            'content',
            'editor',
            [
                'name' => 'content',
                'label' => __('New Note'),
                'title' => __('New Note'),
                'style' => 'height:10em',
                'required' => true,
                'config' => $this->_wysiwygConfig->getConfig()
            ]
        );

        $fieldset->addField(
            'customer_id',
            'hidden',
            [
                'name' => 'customer_id',
                'label' => __('Customer Id'),
                'title' => __('Customer Id'),
                'style' => 'height:5em',
                'required' => false
            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
