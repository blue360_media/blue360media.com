<?php
namespace Endeavor\CustomerNotes\Block\Adminhtml\Notes;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Initialize blog post edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'notes_id';
        $this->_controller = 'adminhtml_notes';
        $this->_blockGroup = 'Endeavor_CustomerNotes';

        parent::_construct();
        $this->buttonList->remove('save');

        $this->buttonList->remove('delete');
        $this->buttonList->remove('saveandcontinue');
        $this->buttonList->remove('reset');
        $this->buttonList->remove('back');
    }

    /**
     * Retrieve text for header element depending on loaded post
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('endeavor_customernotes_notes')->getNotesId()) {
            return __("Edit Note '%1'", $this->escapeHtml($this->_coreRegistry->registry('endeavor_customernotes_notes')->getTitle()));
        } else {
            return __('New Note');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

}
