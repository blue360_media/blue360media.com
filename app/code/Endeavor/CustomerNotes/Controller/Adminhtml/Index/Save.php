<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\CustomerNotes\Controller\Adminhtml\Index;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Customer\Controller\Adminhtml\Index\Save
{
    
    /**
     * Save customer action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    { 
        $resultRedirect = parent::execute();

        $data = $this->getRequest()->getPostValue();

        if ($data['notes']["content"]) {
            
            $id = $this->getCurrentCustomerId();

            $model = $this->_objectManager->create('Endeavor\CustomerNotes\Model\Notes');

            $data['notes']['customer_id'] = $id;
            $model->setData($data['notes']);

            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved the Notes.'));
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Notes.'));
            }
        }
        
        return $resultRedirect;
    }

    /**
     * Retrieve current customer ID
     *
     * @return int
     */
    private function getCurrentCustomerId()
    {
        $originalRequestData = $this->getRequest()->getPostValue('customer');

        $customerId = isset($originalRequestData['entity_id'])
            ? $originalRequestData['entity_id']
            : null;

        return $customerId;
    }
    
}
