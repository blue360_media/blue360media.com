<?php
namespace Endeavor\CustomerNotes\Model;

use Magento\Framework\Reflection\DataObjectProcessor;
use Endeavor\CustomerNotes\Api\NotesRepositoryInterface;
use Endeavor\CustomerNotes\Model\ResourceModel\Notes as ResourceNotes;
use Endeavor\CustomerNotes\Api\Data\NotesSearchResultsInterfaceFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SortOrder;
use Endeavor\CustomerNotes\Model\ResourceModel\Notes\CollectionFactory as NotesCollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\DataObjectHelper;
use Endeavor\CustomerNotes\Api\Data\NotesInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Store\Model\StoreManagerInterface;

class NotesRepository implements NotesRepositoryInterface
{
    /**
     *
     * DataObjectHelper 
     */
    protected $dataObjectHelper;

    /**
     *
     * @var NotesSearchResultsInterfaceFactory 
     */
    protected $searchResultsFactory;

    /**
     *
     * @var Endeavor\CustomerNotes\Model\ResourceModel\Notes\CollectionFactory 
     */
    protected $NotesCollectionFactory;

    /**
     *
     * @var Magento\Store\Model\StoreManagerInterface 
     */
    private $storeManager;

    /**
     *
     * @var Endeavor\CustomerNotes\Model\ResourceModel\Notes 
     */
    protected $resource;

    /**
     *
     * @var DataObjectProcessor 
     */
    protected $dataObjectProcessor;

    /**
     *
     * @var Endeavor\CustomerNotes\Api\Data\NotesInterfaceFactory 
     */
    protected $dataNotesFactory;

    /**
     *
     * @var Endeavor\CustomerNotes\Model\Notes 
     */
    protected $NotesFactory;


    /**
     * @param ResourceNotes $resource
     * @param NotesFactory $notesFactory
     * @param NotesInterfaceFactory $dataNotesFactory
     * @param NotesCollectionFactory $notesCollectionFactory
     * @param NotesSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceNotes $resource,
        NotesFactory $notesFactory,
        NotesInterfaceFactory $dataNotesFactory,
        NotesCollectionFactory $notesCollectionFactory,
        NotesSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->notesFactory = $notesFactory;
        $this->notesCollectionFactory = $notesCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataNotesFactory = $dataNotesFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Endeavor\CustomerNotes\Api\Data\NotesInterface $notes
    ) {
        try {
            $this->resource->save($notes);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the notes: %1',
                $exception->getMessage()
            ));
        }
        return $notes;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($notesId)
    {
        $notes = $this->notesFactory->create();
        $notes->load($notesId);
        if (!$notes->getId()) {
            throw new NoSuchEntityException(__('Notes with id "%1" does not exist.', $notesId));
        }
        return $notes;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $collection = $this->notesCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $items = [];
        
        foreach ($collection as $notesModel) {
            $notesData = $this->dataNotesFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $notesData,
                $notesModel->getData(),
                'Endeavor\CustomerNotes\Api\Data\NotesInterface'
            );
            $items[] = $this->dataObjectProcessor->buildOutputDataArray(
                $notesData,
                'Endeavor\CustomerNotes\Api\Data\NotesInterface'
            );
        }
        $searchResults->setItems($items);
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Endeavor\CustomerNotes\Api\Data\NotesInterface $notes
    ) {
        try {
            $this->resource->delete($notes);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Notes: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($notesId)
    {
        return $this->delete($this->getById($notesId));
    }
}
