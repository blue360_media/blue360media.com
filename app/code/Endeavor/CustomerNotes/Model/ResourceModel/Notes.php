<?php
namespace Endeavor\CustomerNotes\Model\ResourceModel;

class Notes extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('endeavor_notes', 'notes_id');
    }
}
