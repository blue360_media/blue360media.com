<?php
namespace Endeavor\CustomerNotes\Model\ResourceModel\Notes;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Endeavor\CustomerNotes\Model\Notes',
            'Endeavor\CustomerNotes\Model\ResourceModel\Notes'
        );
    }
}
