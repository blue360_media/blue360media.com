<?php
namespace Endeavor\CustomerNotes\Model;

use Endeavor\CustomerNotes\Api\Data\NotesInterface;

class Notes extends \Magento\Framework\Model\AbstractModel implements NotesInterface
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\CustomerNotes\Model\ResourceModel\Notes');
    }

    /**
     * Get notes_id
     * @return string
     */
    public function getNotesId()
    {
        return $this->getData(self::NOTES_ID);
    }

    /**
     * Set notes_id
     * @param string $notesId
     * @return Endeavor\CustomerNotes\Api\Data\NotesInterface
     */
    public function setNotesId($notesId)
    {
        return $this->setData(self::NOTES_ID, $notesId);
    }

    /**
     * Get content
     * @return string
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Set content
     * @param string $content
     * @return Endeavor\CustomerNotes\Api\Data\NotesInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Get customer_id
     * @return string
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set customer_id
     * @param string $customer_id
     * @return Endeavor\CustomerNotes\Api\Data\NotesInterface
     */
    public function setCustomerId($customer_id)
    {
        return $this->setData(self::CUSTOMER_ID, $customer_id);
    }

    /**
     * Get created_at
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $created_at
     * @return Endeavor\CustomerNotes\Api\Data\NotesInterface
     */
    public function setCreatedAt($created_at)
    {
        return $this->setData(self::CREATED_AT, $created_at);
    }
}
