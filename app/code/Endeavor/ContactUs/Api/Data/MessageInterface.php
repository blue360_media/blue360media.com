<?php
namespace Endeavor\ContactUs\Api\Data;

interface MessageInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const CONTACT_ID = 'contact_id';
    const TYPY_QUESTION = 'type_question';
    const CONTACT_NAME = 'contact_name';
    const SUBJECT = 'subject';
    const EMAIL = 'email';
    const PHONE = 'phone';
    const COMMENT = 'comment';
    const CREATION_TIME = 'creation_time';
   
   /**
    * get message id
    * 
    * @return int 
    */
    public function getId();
    
    /**
     * get type of question
     * 
     * @return int
     */
    public function getTypeQuestion();
    
    /**
     * @return string
     */
    public function getContactName();
    
    /**
     * @return string
     */
    public function getSubject();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getPhone();
    
    /**
     * @return string
     */
    public function getComment();
    
    /**
     * @return date
     */
    public function getCreationTime();

    /**
     * 
     * @param type $id
     */
    public function setId($id);
    
    /**
     * 
     * @param type $type_question
     */
    public function setTypeQuestion($type_question);
    
    /**
     * 
     * @param type $contact_name
     */
    public function setContactName($contact_name);
    
    /**
     * 
     * @param type $subject
     */
    public function setSubject($subject);
    
    /**
     * 
     * @param type $email
     */
    public function setEmail($email);

    /**
     * 
     * @param type $phone
     */
    public function setPhone($phone);
    
    /**
     * 
     * @param type $comment
     */
    public function setComment($comment);
    
    /**
     * 
     * @param type $creationTime
     */
    public function setCreationTime($creationTime);

   
}

