<?php
namespace Endeavor\ContactUs\Api\Data;

interface TypeInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const TYEP_ID = 'tyep_id';
    const TYPY_QUESTION = 'type_question';
    const CREATION_TIME = 'creation_time';
   
   /**
    * @return int
    */
    public function getId();
    
    /**
    * @return int
    */
    public function getTypeQuestion();
    
    /**
    * @return date
    */
    public function getCreationTime();

    /**
     * 
     * @param type $id
     */
    public function setId($id);
    
    /**
     * 
     * @param type $type_question
     */
    public function setTypeQuestion($type_question);
    
    /**
     * 
     * @param type $creationTime
     */
    public function setCreationTime($creationTime);

   
}
