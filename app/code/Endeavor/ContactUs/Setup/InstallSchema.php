<?php
namespace Endeavor\ContactUs\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        
        if (!$installer->tableExists('endeavor_type')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('endeavor_type'))
                ->addColumn(
                    'type_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
                    'Type Id'    
                )
                ->addColumn(
                    'type_question', 
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'type question'
                )
                ->addColumn(
                    'creation_time',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null, 
                    ['nullable' => false ,'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Creation Time'
                )
                ->setComment('Endeavor Question Type');
            $installer->getConnection()->createTable($table);
        }
        if (!$installer->tableExists('endeavor_contact')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('endeavor_contact'))
                ->addColumn(
                    'contact_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true]
                )
                ->addColumn(
                    'type_question',
                    Table::TYPE_INTEGER,
                    10, ['nullable' => true,
                    'unsigned' => true],
                    'type question'
                )
                ->addColumn(
                    'contact_name',
                    Table::TYPE_TEXT,
                    255, 
                    ['nullable' => false],
                    'Contact Name'
                )
                ->addColumn(
                    'subject',
                    Table::TYPE_TEXT,
                    null, 
                    ['nullable' => true],
                    'subject'
                )
                ->addColumn(
                    'email',
                    Table::TYPE_TEXT,
                    null, 
                    ['nullable' => true],
                    'email'
                )
                ->addColumn(
                    'phone', 
                    Table::TYPE_TEXT,
                    12,
                    ['default' => ''],
                    'mobile phone'
                )
                ->addColumn(
                    'comment',
                    Table::TYPE_TEXT,
                    '2M', 
                    [], 
                    'comment'
                )
                ->addColumn(
                    'creation_time',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => false],
                    'Creation Time'
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'endeavor_contact',
                        'contact_id',
                        'endeavor_type',
                        'type_id'
                    ),
                    'type_question',
                    $installer->getTable('endeavor_type'), 
                    'type_id',
                    \Magento\Framework\DB\Ddl\Table::ACTION_SET_NULL
                )
                ->setComment('Endeavor Contact Us');

            $installer->getConnection()->createTable($table);
        }
        
        $installer->endSetup();
    }
}
