<?php
namespace Endeavor\ContactUs\Block;

class TypeList extends \Magento\Framework\View\Element\Template implements
    \Magento\Framework\DataObject\IdentityInterface
{
    /**
     *
     * @var Endeavor\ContactUs\Model\ResourceModel\Type\CollectionFactory 
     */
    protected $_typeCollectionFactory;
         
   
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Endeavor\ContactUs\Model\ResourceModel\Type\CollectionFactory $typeCollectionFactory,
           
        array $data = []
    ) {
       
        parent::__construct($context, $data);
        $this->_typeCollectionFactory = $typeCollectionFactory;
    }

   /**
    * 
    * @return Endeavor\ContactUs\Model\Type|null
    */
    public function getTypes()
    {
        if (!$this->hasData('types')) {
            $types = $this->_typeCollectionFactory
                ->create();
                
                
            $this->setData('types', $types);
        }
        return $this->getData('types');
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Endeavor\ContactUs\Model\Type::CACHE_TAG . '_' . 'list'];
    }
       
}
