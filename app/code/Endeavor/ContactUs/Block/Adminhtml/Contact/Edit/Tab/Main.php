<?php
namespace Endeavor\ContactUs\Block\Adminhtml\Contact\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
         array $data = []
    ) {
       
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Endeavor\ContactUs\Model\Contact */
        $model = $this->_coreRegistry->registry('end_contact');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('contact_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Contact Information')]);

        if ($model->getId()) {
            $fieldset->addField('contact_id', 'hidden', ['name' => 'contact_id']);
        }
        
        $fieldset->addField(
            'contact_name',
            'label',
            [
                'name' => 'contact_name',
                'label' => __('Name'),
                'title' => __('Name'),
                
            ]
        );

        $fieldset->addField(
            'subject',
            'label',
            [
                'name' => 'subject',
                'label' => __('Subject'),
                'title' => __('Subject'),
                
            ]
        );
        $fieldset->addField(
            'email',
            'label',
            [
                'name' => 'email',
                'label' => __('Email'),
                'title' => __('Email'),
                
            ]
        );
        $fieldset->addField(
            'phone',
            'label',
            [
                'name' => 'phone',
                'label' => __('Phone'),
                'title' => __('Phone'),
                
            ]
        );
 
        $fieldset->addField(
            'comment',
            'label',
            [
                'name' => 'comment',
                'label' => __('Comment'),
                'title' => __('Comment'),
                
            ]
        );
         
         $fieldset->addField(
            'creation_time',
            'label',
            [
                'name' => 'creation_time',
                'label' => __('Creation On'),
                'title' => __('Creation On'),
               
            ]
        );
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Main');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Main');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
