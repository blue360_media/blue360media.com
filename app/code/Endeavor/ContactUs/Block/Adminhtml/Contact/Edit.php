<?php
namespace Endeavor\ContactUs\Block\Adminhtml\Contact;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'contact_id';
        $this->_blockGroup = 'Endeavor_ContactUs';
        $this->_controller = 'adminhtml_contact';

        parent::_construct();

         $this->buttonList->remove('save');
         $this->buttonList->remove('reset');

        $this->buttonList->update('delete', 'label', __('Delete Contact'));
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('endcontactus/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }

}
