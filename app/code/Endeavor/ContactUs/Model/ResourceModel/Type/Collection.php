<?php
namespace Endeavor\ContactUs\Model\ResourceModel\Type;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'type_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\ContactUs\Model\Type', 'Endeavor\ContactUs\Model\ResourceModel\Type');
    }
}
