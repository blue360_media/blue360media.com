<?php
namespace Endeavor\ContactUs\Model\Contact\Source;

class TypeQuestion implements \Magento\Framework\Data\OptionSourceInterface
{
   
//    /**
//     * @var \Endeavor\Career\Model\ResourceModel\Type\CollectionFactory
    
    protected $typeCollectionFactory;
    /**
     * @var array
     */
    protected $options;

    /**
     * Initialize dependencies.
     *
     * @param \Endeavor\Career\Model\ResourceModel\Category\CollectionFactory $authorCollectionFactory
     * @param void
     */
    public function __construct(
    \Endeavor\ContactUs\Model\ResourceModel\Type\CollectionFactory $typeCollectionFactory
    ) {
        $this->typeCollectionFactory = $typeCollectionFactory;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = [['label' => __('Default'), 'value' => null]];
            $collection = $this->typeCollectionFactory->create();
  
            foreach ($collection as $item) {
                 
                $this->options[] = [
                    'label' =>  $item->getTypeQuestion()
                        ,
                    'value' => $item->getId(),
                ];
            }
        }

        return $this->options;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $array = [];
        foreach ($this->toOptionArray() as $item) {
            $array[$item['value']] = $item['label'];
        }
        return $array;
    }

    

    
}
