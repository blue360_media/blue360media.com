<?php

namespace Endeavor\ContactUs\Model;

use Magento\Framework\DataObject\IdentityInterface;

class Type extends \Magento\Framework\Model\AbstractModel implements IdentityInterface
{
  
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'end_type';

    /**
     * @var string
     */
    protected $_cacheTag = 'end_type';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'end_type';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\ContactUs\Model\ResourceModel\Type');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
 
}