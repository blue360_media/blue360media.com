<?php
namespace Endeavor\ContactUs\Controller\Adminhtml\Contacts;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('contact_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Endeavor\ContactUs\Model\Contact');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The contact has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['contact_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a contact to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
