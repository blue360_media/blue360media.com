<?php
namespace Endeavor\ContactUs\Controller\Adminhtml\Types;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('type_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Endeavor\ContactUs\Model\Type');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The type has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['type_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a type to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
