<?php
/**
 * Copyright © 2018 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 18-01-2018
 * @desc send reset password for users.
 */

namespace Endeavor\SFIntegration\Api;

/**
 * Class EbookAccess
 *
 * @api
 */
interface CustomerResetPasswordInterface
{
    /**
     * @param string $customerEmail
     * @return mixed
     */
    public function execute(
        $customerEmail
    );
}
