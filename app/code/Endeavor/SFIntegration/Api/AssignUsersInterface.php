<?php

namespace Endeavor\SFIntegration\Api;

/**
 * Class EbookAccess
 *
 * @api
 */
interface AssignUsersInterface
{
    /**
     * @param string $sf_order_number
     * @param string $ecom_order_number
     * @param string $username
     * @param string $part_number
     * @param mixed $users
     * @return mixed
     */
    public function execute(
        $sf_order_number,
        $ecom_order_number,
        $username,
        $part_number,
        $users
    );
    
    /**
     * @return mixed
     */
    public function getAssignedUsers();
    
    /**
     * @param string $purchasedLineItemId
     * @param string $addNumberOfDownloads
     * @return mixed
     */
    public function addMoreDownloadsForAssigned($purchasedLineItemId, $addNumberOfDownloads);
}
