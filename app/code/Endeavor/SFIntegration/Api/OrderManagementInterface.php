<?php
namespace Endeavor\SFIntegration\Api;

interface OrderManagementInterface
{
    /**
     * POST for Order api
     * 
     * @api
     * @param mixed $order
     * @return mixed
     */
    public function postOrder($order);

     /**
     * POST for Order Status api
     * 
     * @api
     * @param mixed $order
     * @return Endeavor\SFIntegration\Api\Data\ResultSearchResultInterface[]
     */
    public function putOrderStatus($order);

    /**
     * Cancle orders, and add cancle resaon in Customer order notes.
     * 
     * @api
     * @param mixed $order
     * @return Endeavor\SFIntegration\Api\Data\ResultSearchResultInterface[]
     */
    public function putOrderCancel($order);

    
    /**
     * GET for Order api filter by from/to date
     * 
     * @api
     * @return \Endeavor\SFIntegration\Api\Data\OrderSearchResultInterface
     */
    public function getOrder();
}
