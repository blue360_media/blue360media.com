<?php
namespace Endeavor\SFIntegration\Api;

interface TrackingNumberManagementInterface
{
    /**
     * POST for Shipping Tracking Number
     * 
     * @api
     * @param mixed $order
     * @return Endeavor\SFIntegration\Api\Data\ResultSearchResultInterface
     */
    public function postTrackingNumber($order);

} 

