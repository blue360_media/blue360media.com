<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 27-12-2017.
 * @desc eBook access for customers with those features:
 *          1. Expired: to increase downloads for items which called in API.
 *          2. MergeAccount: under execute function to merge accounts which called in API.
 *          3. Allow Assign: to change customer role for order which called in API.
 *          4. update to record history and update status for eBooks.
 * 
 */

namespace Endeavor\SFIntegration\Api;

/**
 * Class EbookAccess
 *
 * @api
 */
interface EbookAccessInterface
{
    /**
     * @param string $sfOrderNumber
     * @param string $ecomOrderNumber
     * @param boolean $mergeAllOrders
     * @param string $userName
     * @param mixed $customerData
     * @return mixed
     */
    public function execute(
        $sfOrderNumber,
        $ecomOrderNumber,
        $mergeAllOrders,
        $userName,
        $customerData
    );
    
    /**
     * @param string $sfOrderNumber
     * @param string $ecom_order_number
     * @param string $action
     * @param mixed $items
     * @return mixed
     */
    public function expired(
        $sfOrderNumber,
        $ecom_order_number,
        $action,
        $items
    );
    
    /**
     * @param string $sfOrderNumber
     * @param string $ecom_order_number
     * @return mixed
     */
    public function allowAssign(
        $sfOrderNumber,
        $ecom_order_number
    );
}
