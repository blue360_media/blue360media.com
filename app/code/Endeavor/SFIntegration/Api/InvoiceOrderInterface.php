<?php
/**
 * Copyright ©2017 Endeavor Technology, Inc. All rights reserved.
 * 
 * @author: Murad Dweikat
 * @date: 2017/11/21
 * @desc: Create invoice for all Pre-Orders that contains specific Publication Number.
 *        And Make it as complete, and make pre-order product as avaliable.
 * 
 */

namespace Endeavor\SFIntegration\Api;

/**
 * Class InvoiceOrderInterface
 *
 * @api
 */
interface InvoiceOrderInterface
{
    /**
     * @param string $publicationNumber
     * @param string $releaseNumber
     * @param bool|false $capture
     * @param \Magento\Sales\Api\Data\InvoiceItemCreationInterface[] $items
     * @param bool|false $notify
     * @param bool|false $appendComment
     * @param Data\InvoiceCommentCreationInterface|null $comment
     * @param Data\InvoiceCreationArgumentsInterface|null $arguments
     * @return mixed
     */
    public function execute(
        $publicationNumber,
        $releaseNumber,
        $capture = true,
        array $items = [],
        $notify = true,
        $appendComment = false,
        \Magento\Sales\Api\Data\InvoiceCommentCreationInterface $comment = null,
        \Magento\Sales\Api\Data\InvoiceCreationArgumentsInterface $arguments = null
    );
    
    /**
     * @param int $orderId
     * @param bool $capture
     * @param array $items
     * @param bool $notify
     * @param bool $appendComment
     * @param \Magento\Sales\Api\Data\InvoiceCommentCreationInterface|null $comment
     * @param \Magento\Sales\Api\Data\InvoiceCreationArgumentsInterface|null $arguments
     * @return int
     */
    public function callInvoice(
        $orderId,
        $capture = true,
        array $items = [],
        $notify = true,
        $appendComment = false,
        \Magento\Sales\Api\Data\InvoiceCommentCreationInterface $comment = null,
        \Magento\Sales\Api\Data\InvoiceCreationArgumentsInterface $arguments = null
    );
    
}
