<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 27-12-2017.
 * @desc Response Order Info for order number which called in API
 */

namespace Endeavor\SFIntegration\Api;

/**
 * Class EbookAccess
 *
 * @api
 */
interface OrderInfoInterface
{
    /**
     * @return mixed
     */
    public function getOrderEbooks();
}
