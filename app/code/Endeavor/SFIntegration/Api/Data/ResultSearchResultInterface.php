<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\SFIntegration\Api\Data;

/**
 * Inventory search result interface.
 *
 * An order is a document that a web store issues to a customer. Magento generates a sales order that lists the product
 * items, billing and shipping addresses, and shipping and payment methods. A corresponding external document, known as
 * a purchase order, is emailed to the customer.
 * @api
 */
interface ResultSearchResultInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const msg = 'msg';
    const success = 'success';
    const fail = 'fail';
    /**#@-*/

    /**
     * Get message
     *
     * @return string
     */
    public function getMsg();

    /**
     * Set message
     *
     * @param string $msg
     * @return $this
     */
    public function setMsg($msg);

    /**
     * Get sucess product SKUs
     *
     * @return string[]
     */
    public function getSuccess();

    /**
     * Set sucess product SKUs
     *
     * @param string[] $sku
     * @return $this
     */
    public function setSuccess($sku);
    /**
     * Get fail product SKUs
     *
     * @return string[]
     */
    public function getFail();

    /**
     * Set failer product SKUs
     *
     * @param string[] $sku
     * @return $this
     */
    public function setFail($sku);


}
