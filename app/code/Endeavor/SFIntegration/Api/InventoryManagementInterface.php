<?php
namespace Endeavor\SFIntegration\Api;

interface InventoryManagementInterface
{
    /**
     * POST for Inventory api
     * 
     * @api
     * @param mixed $items
     * @return Endeavor\SFIntegration\Api\Data\ResultSearchResultInterface
     */
    public function putInventory($items);

}
