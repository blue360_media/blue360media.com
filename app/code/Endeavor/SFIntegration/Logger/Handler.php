<?php
namespace Endeavor\SFIntegration\Logger;

use Monolog\Logger;

/**
 * Description of Handler
 *
 * @author Murad Dweikat
 */
class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */ 
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/salesforce_magento_api.log';
}
