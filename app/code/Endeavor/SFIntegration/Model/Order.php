<?php
namespace Endeavor\SFIntegration\Model;

/**
 * Marketplace Product Model.
 *
 */
class Order extends \Magento\Framework\Model\AbstractModel implements \Endeavor\SFIntegration\Api\Data\OrderSearchResultInterface
{    
    const KEY_NAME = 'orders';

    /**
     * 
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    
    /**
     * 
     * @return string
     */
    public function getName() {
        return $this->getData(self::ORDERS);
    }

    /**
     * 
     * @param string $name
     */
    public function setName($name) {
        return $this->setData(self::ORDERS, $name);
    }

}