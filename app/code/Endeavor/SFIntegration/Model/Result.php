<?php
namespace Endeavor\SFIntegration\Model;

/**
 * Marketplace Product Model.
 *
 */
class Result implements \Endeavor\SFIntegration\Api\Data\ResultSearchResultInterface
{    
    var $msg = '';
    var $success = '';
    var $fail = '';
    
    /**
     * Get message
     *
     * @return string
     */
    public function getMsg(){
        return $this->msg;
    }

    /**
     * Set message
     *
     * @param string $msg
     * @return $this
     */
    public function setMsg($msg){
        $this->msg = $msg;
        return $this->msg;
    }

    /**
     * Get sucess product SKUs
     *
     * @return string[]
     */
    public function getSuccess(){
        return $this->success;
    }

    /**
     * Set sucess product SKUs
     *
     * @param string[] $sku
     * @return $this
     */
    public function setSuccess($sku){
        $this->success = $sku;
        return $this->success;
    }
    /**
     * Get fail product SKU
     *
     * @return string[]
     */
    public function getFail(){
        return $this->fail;
    }

    /**
     * Set fail product SKU
     *
     * @param string[] $sku
     * @return $this
     */
    public function setFail($sku){
        $this->fail = $sku;
        return $this->fail;
    }

}