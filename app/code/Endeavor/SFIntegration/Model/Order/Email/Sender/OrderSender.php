<?php
/**
 * Handle doesn't send order email when create by API.
 *
 * @author Murad Dweikat
 * @desc: 2017/12/18
 */
namespace Endeavor\SFIntegration\Model\Order\Email\Sender;

use Magento\Sales\Model\Order;

class OrderSender extends \Magento\Sales\Model\Order\Email\Sender\OrderSender {

    public function send(Order $order, $forceSyncMode = false)
    {
        // Doesn't Send order email if payment method Check Money (if Created by API).
        $paymentCode = $order->getPayment()->getMethodInstance()->getCode();

        if ($paymentCode == 'checkmo') {
            return false;
        }

        $order->setSendEmail(true);

        if (!$this->globalConfig->getValue('sales_email/general/async_sending') || $forceSyncMode) {
            if ($this->checkAndSend($order)) {
                $order->setEmailSent(true);
                $this->orderResource->saveAttribute($order, ['send_email', 'email_sent']);
                return true;
            }
        }

        $this->orderResource->saveAttribute($order, 'send_email');

        return false;
    }
}