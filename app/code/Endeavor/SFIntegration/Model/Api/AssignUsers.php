<?php
/**
 * Copyright © 2018 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 08-01-2018.
 * @desc Assign Users Using API.
 *          1. assign users to eBook.
 *          2. view data for assigned users.
 */

namespace Endeavor\SFIntegration\Model\Api;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\Math\Random;
use Magento\Framework\Stdlib\DateTime;

class AssignUsers implements \Endeavor\SFIntegration\Api\AssignUsersInterface
{
    /**
     * Logging instance
     * @var \Endeavor\SFIntegration\Logger\Logger
     */
    protected $_logger;
    
    /**
     *
     * @var Magento\Framework\App\Request\Http 
     */
    protected $request;
    
    /**
     * Downloadable Purchased Related Item object
     *
     * @var \Endeavor\Downloadable\Model\RelatedItemsFactory 
     */
    protected $_relatedItemsFactory;

    /**
     * Downloadable Purchased Item object
     *
     * @var \Magento\Downloadable\Model\Link\Purchased\ItemFactory 
     */
    protected $_purchasedItemFactory;

    /**
     * Downloadable Link object
     *
     * @var \Magento\Downloadable\Model\Link 
     */
    protected $_linkFactory;

    /**
     * Downloadable Purchased Link object
     *
     * @var \Magento\Downloadable\Model\Link\PurchasedFactory 
     */
    protected $_purchasedLinkFactory;

    /**
     * Customer Object
     *
     * @var \Magento\Customer\Model\CustomerFactory 
     */
    protected $_customerFactory;

    /**
     * Store website manager 
     *
     * @var \Magento\Store\Model\StoreManagerInterface 
     */
    protected $_storeManager;

    /**
     * @var AccountManagementInterface
     */
    protected $_customerAccountManagement;

    /**
     * @var Random
     */
    private $mathRandom;

    /**
     * @var Endeavor\Downloadable\Model\Import
     */
    private $import;
    
    /**
     * 
     * @param \Magento\CatalogInventory\Model\StockRegistry $stock
     * @param \Magento\Catalog\Model\Product $product
     * @param \Endeavor\SFIntegration\Model\Result $result
     */
    public function __construct(
        \Endeavor\Downloadable\Model\Import $import,
        \Magento\Downloadable\Model\Link\Purchased\ItemFactory $PurchasedItemFactory,
        \Magento\Downloadable\Model\LinkFactory $LinkFactory,
        \Magento\Downloadable\Model\Link\PurchasedFactory $PurchasedLinkFactory,
        \Magento\Customer\Model\CustomerFactory $CustomerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,   
        AccountManagementInterface $customerAccountManagement,
        \Endeavor\Downloadable\Model\RelatedItemsFactory $RelatedItems,
        Random $mathRandom,
        \Magento\Framework\App\Request\Http $request,
        \Endeavor\SFIntegration\Logger\Logger $logger
    ) {
        $this->_logger = $logger;
        $this->import = $import;
        $this->_purchasedItemFactory = $PurchasedItemFactory;
        $this->_linkFactory = $LinkFactory;
        $this->_purchasedLinkFactory = $PurchasedLinkFactory;
        $this->_customerFactory = $CustomerFactory;
        $this->_storeManager = $storeManager;
        $this->_customerAccountManagement = $customerAccountManagement;
        $this->_relatedItemsFactory = $RelatedItems;
        $this->mathRandom = $mathRandom;
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     * Sample Post Data {"sf_order_number":"17001255771","username":"Amjad","part_number":"3301038EM01","users":[{"email":"abanimattter@endeavorpal.com","firstname":"Amjad","lastname":"banimattar","qty":"1"},{"email":"abanimattter@endeavorpal.com","firstname":"Amjad","lastname":"banimattar","qty":"1"}]}
     * Sample Return Data 
     */
    public function execute($sf_order_number, $ecom_order_number, $username, $part_number, $users) {
        
        $this->_logger->info('POST execute Assign Users API - Start');
        $request = ['sf_order_number' => $sf_order_number, 'userName' => $username, 'part_number' => $part_number, 'users' => $users];
        $this->_logger->info('POST execute Assign Users API - Request', $request);
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $moveOrderToHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
        if ($ecom_order_number != null) {
            $orderId = $moveOrderToHelper->getOrderIdFromSFOrderId($sf_order_number, $ecom_order_number);

        } else {
            $orderId = $moveOrderToHelper->getOrderIdFromSFOrderId($sf_order_number);
        }
        $purchasedId = $this->getPurchasedIdForItem($orderId, $part_number);

        $purchasedItems = $this->_purchasedItemFactory->create()->getCollection()->addFieldToFilter('purchased_id', $purchasedId);
        $purchasedItem = $purchasedItems->getFirstItem();
        $downloadableLink = $this->_linkFactory->create()->load($purchasedItem->getLinkId());
        $purchasedLink = $this->_purchasedLinkFactory->create()->load($purchasedItem->getPurchasedId());
        $relatedItemsCollection = $this->_relatedItemsFactory->create()->getCollection();
        $customer = $this->_customerFactory->create();
        $currentCustomer = $this->_customerFactory->create();

        $productId = $purchasedItem->getProductId();
        $orderItem = $objectManager->create('Magento\Sales\Model\Order\Item')->load($purchasedItem->getOrderItemId());

        $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderItem->getOrderId());

        $currentCustomerId = $purchasedLink->getCustomerId();
        $orderCustomerId = $order->getCustomerId();
        $customerRule = $this->validCustomerRule($orderCustomerId);

        if (!$customerRule) {
            $response = ['status' => false, 'message' => 'Please make sure that customer rule is customer'];
            $this->_logger->info('POST execute Assign Users API - Response', $response);
            $this->_logger->info('POST execute Assign Users API - End');
            return $response;
        }

        $currentCustomer->load($orderCustomerId);
        $totalAssignUser = 0;
        foreach ($order->getAllItems() as $item) {
            if (($item->getProductId() == $productId) && ($currentCustomerId == $orderCustomerId)) {
                if ($downloadableLink->getNumberOfDownloads() != 0) {
                    $totalAssignUser = intval($this->getRemainingDownloads($purchasedItem) / $downloadableLink->getNumberOfDownloads());
                } else {
                    $response [] = ['status' => false, 'message' => 'You don\'t have enough quantity'];
                    $this->_logger->info('POST execute Assign Users API - Response', $response);
                    $this->_logger->info('POST execute Assign Users API - End');
                    return $response;
                }
            }
        }

        // Number of Assign User enter
        $assignUserFilled = 0;
        $totalQuantity = 0;
        foreach ($users as $user) {
            if (isset($user['email'])) {
                $assignUserFilled++;
                if (isset($user['qty'])) {
                    $totalQuantity += $user['qty'];
                } else {
                    $user['qty'] = 1;
                    $totalQuantity += $user['qty'];
                }
            } else {
                $response [] = ['status' => 'false', 'message' => 'Please enter email address.'];
                $this->_logger->info('POST execute Assign Users API - Response', $response);
                $this->_logger->info('POST execute Assign Users API - End');
                return $response;
            }
        }

        if ($totalQuantity > $totalAssignUser) {
            $response [] = ['status' => 'false', 'message' => 'You don\'t have enough quantity.'];
            
            $this->_logger->info('POST execute Assign Users API - Response', $response);
            $this->_logger->info('POST execute Assign Users API - End');
            
            return $response;
        }

        $totalQuantityAssigned = 0;
        foreach ($users as $user) {
            $newPurchasedLink = $this->_purchasedLinkFactory->create();
            $newPurchasedItem = $this->_purchasedItemFactory->create();
            $relatedItems = $this->_relatedItemsFactory->create();
            $relatedItemsCollection = $this->_relatedItemsFactory->create()->getCollection();
            $tempPurchasedLink = $this->_purchasedLinkFactory->create()->getCollection();
            $tempPurchasedItem = $this->_purchasedItemFactory->create();

            $email = $user['email'];
            $firstname = $user['firstname'];
            $lastname = $user['lastname'];
            $qty = $user['qty'];
            
            $websiteId = $this->_storeManager->getWebsite()->getWebsiteId();
            $customer->setWebsiteId($websiteId);
            $customer->loadByEmail($email);

            $customerId = $customer->getEntityId();
            $dataPurchasedLink = $purchasedLink->getData();
            unset($dataPurchasedLink['purchased_id']);
            unset($dataPurchasedLink['created_at']);
            unset($dataPurchasedLink['updated_at']);
            $newPurchasedLink->setData($dataPurchasedLink);

            $createPurchaseItems = false;
            if (!empty($customerId)) {
                $tempPurchasedLink->addFieldToFilter('order_id', ['eq' => $purchasedLink->getOrderId()])
                        ->addFieldToFilter('order_item_id', ['eq' => $purchasedLink->getOrderItemId()])
                        ->addFieldToFilter('customer_id', ['eq' => $customerId]);

                if ($customer->getPasswordHash() === null) {
                    $newPasswordToken = $this->mathRandom->getUniqueHash();
                    $customer->setRpToken($newPasswordToken);
                    $customer->setRpTokenCreatedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT));
                    $customer->save();
                }
                
                if (!count($tempPurchasedLink->getItems())) {
                    $newPurchasedLink->setCustomerId($customerId);
                    $newPurchasedLink->save();
                    
                    $createPurchaseItems = true;
                }
            } else {
                $createPurchaseItems = true;
                $customer->setEmail($email);
                $customer->setFirstname($firstname);
                $customer->setLastname($lastname);
                $customer->setAddresses(null);
                $storeId = $this->_storeManager->getWebsite($websiteId)->getDefaultStore()->getId();
                $customer->setStoreId($storeId);

                $storeName = $this->_storeManager->getStore($customer->getStoreId())->getName();
                $customer->setCreatedIn($storeName);

                $newPasswordToken = $this->mathRandom->getUniqueHash();
                $customer->setRpToken($newPasswordToken);
                $customer->setRpTokenCreatedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT));

                $customer->save();
                
                $lastCustomerId = $customer->getCollection()->getLastItem()->getEntityId();
                $newPurchasedLink->setCustomerId($lastCustomerId);
                $newPurchasedLink->save();
                
                $newCustomerId = $customer->getEntityId();
                $moveOrderToHelper->sendWelcomeMessage($newCustomerId);
                
                $tempPurchasedLink->addFieldToFilter('order_id', ['eq' => $purchasedLink->getOrderId()])
                        ->addFieldToFilter('order_item_id', ['eq' => $purchasedLink->getOrderItemId()])
                        ->addFieldToFilter('customer_id', ['eq' => $customer->getId()]);

                $response = [];
                $response['notes'] = 'This email: ' . $email . ' is not registered in our website. So we sent a message with details of the account.';
                $this->_logger->info('POST execute Assign Users API - notes', $response);
                
            }
            if ($createPurchaseItems) {
                foreach($purchasedItems as $pItem) {
                    $dataPurchasedItem = $pItem->getData();
                    unset($dataPurchasedItem['item_id']);
                    unset($dataPurchasedItem['created_at']);
                    unset($dataPurchasedItem['updated_at']);
                    $newPurchasedItem->setData($dataPurchasedItem);
                    $newPurchasedItem->setStatus('available');
                    $newPurchasedItem->setOrderItemId($purchasedLink->getOrderItemId());
                    $newPurchasedItem->setNumberOfDownloadsBought($downloadableLink->getNumberOfDownloads() * $qty);
                    $newPurchasedItem->setNumberOfDownloadsUsed(0);
                    $newPurchasedItem->setPurchasedId($newPurchasedLink->getId());
                    $linkHash = strtr(
                            base64_encode(
                                    microtime() . $orderItem->getId() . $dataPurchasedItem['product_id']
                            ), '+/=', '-_,'
                    );

                    $newPurchasedItem->setLinkHash($linkHash);
                    $newPurchasedItem->save();
                    
                    $relatedItems = $this->_relatedItemsFactory->create();
                    $relatedItems->load($newPurchasedItem->getId(), 'related_item_id');
                    if (empty($relatedItems->getId())) {
                        $relatedItems = $this->_relatedItemsFactory->create();
                        $relatedItems->setItemId($pItem->getId());
                        $relatedItems->setRelatedItemId($newPurchasedItem->getId());
                        $relatedItems->setQty($qty);
                        $relatedItems->setCreatedBy($username);
                        $relatedItems->save();
                        
                    } else {
                        $relatedItems->setQty($relatedItems->getQty() + $qty);
                        $relatedItems->save();
                        
                    }
                }
            } else {
                foreach ($tempPurchasedLink->getItems() as $newItems) {
                    $tempPurchasedItems = $this->_purchasedItemFactory->create()->getCollection()->addFieldToFilter('purchased_id', $newItems->getId());
                    foreach ($tempPurchasedItems as $tempPurchasedItem) {
                        $tempPurchasedItem->setNumberOfDownloadsBought($tempPurchasedItem->getNumberOfDownloadsBought() + $downloadableLink->getNumberOfDownloads() * $qty);
                        $tempPurchasedItem->save();

                        $relatedItems = $this->_relatedItemsFactory->create();
                        $relatedItems->load($tempPurchasedItem->getId(), 'related_item_id');

                        if (empty($relatedItems->getId())) {
                            $relatedItems->setItemId($purchasedItem->getId());
                            $relatedItems->setRelatedItemId($tempPurchasedItem->getId());
                            $relatedItems->setQty($qty);
                            $relatedItems->setCreatedBy($username);
                            $relatedItems->save();
                            
                        } else {
                            $relatedItems->setQty($relatedItems->getQty() + $qty);
                            $relatedItems->save();
                            
                        }
                    }
                    
                }
                
            }
            
            
            $receiverInfo = ['name' => $customer->getFirstname() . ' ' . $customer->getLastname(),
                'email' => $email];

            $emailData = [];
            $emailData['sender_name'] = $currentCustomer->getFirstname() . ' ' . $currentCustomer->getLastname();
            $emailData['customer'] = $customer;
            $emailData['product_name'] = $purchasedLink->getProductName();
            $emailData['product_qty'] = $qty;

            $totalQuantityAssigned += $qty;
            $this->sendAssingUserMailMessage($emailData, $receiverInfo);
            $this->_logger->info('POST execute Assign Users API - Done');
            
        }
        
        foreach($purchasedItems as $pItem) {
            $pItem->setNumberOfDownloadsUsed($pItem->getNumberOfDownloadsUsed() + $downloadableLink->getNumberOfDownloads() * $totalQuantityAssigned);
            if (!$this->getRemainingDownloads($pItem)) {
                $pItem->setStatus('expired');
                
            }
            $pItem->save();
            
        }

        $response = ['status' => 'true', 'message' => 'Assign user completely successfully.'];
        $this->_logger->info('POST execute Assign Users API - Response', $response);
        $this->_logger->info('POST execute Assign Users API - End');
        return $response;
    }

    /**
     * Return number of remaining downloads
     *
     * @param Item $item
     * @return \Magento\Framework\Phrase|int
     */
    public function getRemainingDownloads($item) {
        $downloads = 0;
        if ($item->getNumberOfDownloadsBought()) {
            $downloads = $item->getNumberOfDownloadsBought() - $item->getNumberOfDownloadsUsed();
            
        }
        return $downloads;
        
    }
    
    /**
     * Send email Message to notify assigned user by downloadable product 
     *
     * @param array $senderInfo
     * @param array $receiverInfo
     * @param array $emailData
     * @return void
     */
    public function sendAssingUserMailMessage($emailData,$receiverInfo) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        /* 
         * call send mail method from helper or where you define it
         */ 
        $objectManager->get('Endeavor\Downloadable\Helper\Email')->assignedUserSendMailMessage(
              $emailData,
              $receiverInfo
        );
        
    }
    
    /*
     * @desc get Purchased Id For Item.
     * @param string $orderId
     * @param string $partNumber
     * 
     * @return string $purchasedId
     */
    protected function getPurchasedIdForItem($orderId, $partNumber) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $purchaseLink = $objectManager->get('\Magento\Downloadable\Model\Link\PurchasedFactory');
        $downloadablePurchasedLinks = $purchaseLink->create()->getCollection()->addFieldToFilter('order_id', $orderId);

        $purchasedId = null;
        if ($downloadablePurchasedLinks) {
            foreach ($downloadablePurchasedLinks as $downloadablePurchasedLink) {
                $productSku = $downloadablePurchasedLink->getProductSku();
                if ($productSku == $partNumber) {
                    $purchasedId = $downloadablePurchasedLink->getPurchasedId();
                    break;
                    
                }
                
            }
            
        }
        
        return $purchasedId;
        
    }
    
    protected function validCustomerRule($customerId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $assignUserHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
        $customerAttributes = $assignUserHelper->getCustomerAttributes($customerId);
        if (isset($customerAttributes['customer_role']) && $customerAttributes['customer_role'] == 'Customer') {
            return true;
            
        } else {
            return false;
            
        }
    }
    
    /**
     * @return mixed
     */
    public function getAssignedUsers() {
        
        $this->_logger->info('GET execute Assign Users API - Start');
        
        $postData = $this->request->getParams();
        $ecomOrderNumber = null;
        $sfOrderNumber = null;
        // Check SalesForce Order Number.
        if (isset($postData['ecom_order_number'])) {
            $ecomOrderNumber = $postData['ecom_order_number'];
            $this->_logger->info('GET execute Assign Users API - Request Url', $postData);
            
        } else if (isset($postData['sf_order_Number'])) {
            $sfOrderNumber = $postData['sf_order_Number'];
            $this->_logger->info('GET execute Assign Users API - Request Url', $postData);
            
        } else {
            $response = ['status' => false, 'message' => 'please enter sf_order_Number in url.'];
            $this->_logger->info('GET execute Assign Users API - Response', $response);
            $this->_logger->info('GET execute Assign Users API - End');
            return $response;
            
        }
        
        // Check Part Number
        if (isset($postData['part_number'])) {
            $part_number = $postData['part_number'];
            
        } else {
            $response = ['status' => false, 'message' => 'please enter part_number in url.'];
            $this->_logger->info('GET execute Assign Users API - Response', $response);
            $this->_logger->info('GET execute Assign Users API - End');
            return $response;
            
        }
        // Get Items Ids which assigned
        $relatedItemsIds = $this->getRelatedItemIds($sfOrderNumber, $part_number, $ecomOrderNumber);
     
        // Get assigned users data for eachitem.
        $assignedUser = $this->getAssignedUsersAsArray($relatedItemsIds);
        
        //Return Response
        $response[] = ['status' => true, 'users' => $assignedUser];
        $this->_logger->info('GET execute Assign Users API - Response', $response);
        $this->_logger->info('GET execute Assign Users API - End');
        return $response;
        
    }
    
    /*
     * @desc get Related Item Ids.
     * @method Protected getRelatedItemIds.
     * @param $sfOrderNumber String
     * @param $part_number String
     * return Array $relatedItemsIds
     */
    protected function getRelatedItemIds($sfOrderNumber, $part_number, $ecomOrderNumber = null) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $moveOrderToHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
        $orderId = $moveOrderToHelper->getOrderIdFromSFOrderId($sfOrderNumber, $ecomOrderNumber);
        $purchasedId = $this->getPurchasedIdForItem($orderId, $part_number);
        $purchasedItems = $this->_purchasedItemFactory->create()->getCollection()->addFieldToFilter('purchased_id', $purchasedId);
        $purchasedItemsId = $purchasedItems->getFirstItem()->getId();
        $relatedItems = $objectManager->create('Endeavor\Downloadable\Model\RelatedItems')->getCollection()->addFieldToFilter('item_id', $purchasedItemsId);
            
        $relatedItemsIds = [];
        foreach ($relatedItems->getItems() as $item) {
            $relatedItemsIds[$item->getRelatedItemId()] = $item->getQty();
            
        }
        return $relatedItemsIds;
        
    }
    
    /*
     * @desc get Assigned Users As Array.
     * @method Protected getAssignedUsersAsArray.
     * @param $relatedItemsIds Array
     * return Array $assignedUser
     */
    protected function getAssignedUsersAsArray($relatedItemsIds) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $assignedUser = [];
        if (!empty($relatedItemsIds)) {
            foreach ($relatedItemsIds as $relatedItemId => $qty) {
                $purchasedItem = $objectManager->create('Magento\Downloadable\Model\Link\Purchased\Item')->load($relatedItemId);
                $purchased = $objectManager->create('Magento\Downloadable\Model\Link\Purchased')->load($purchasedItem->getPurchasedId());
                $customerModel = $objectManager->create('Magento\Customer\Model\Customer')->load($purchased->getCustomerId());
                
                $numberOfDownloadsBought = $purchasedItem->getNumberOfDownloadsBought();
                $numberOfDownloadsUsed = $purchasedItem->getNumberOfDownloadsUsed();
                $numberOfDownloadsForLink = $this->getNumberOfDownloadablesForLink($purchasedItem->getItemId());
                $customerData['email'] = $customerModel->getEmail();
                $customerData['firstname'] = $customerModel->getFirstname();
                $customerData['lastname'] = $customerModel->getLastname();
                $customerData['qty'] = $qty;
                $customerData['username'] = $this->getChangedBy($relatedItemId);
                $customerData['purchased_line_item_id'] = $purchasedItem->getItemId();
                $customerData['number_of_downloads_bought'] = $numberOfDownloadsBought;
                $customerData['number_of_downloads_used'] = $numberOfDownloadsUsed;
                $customerData['number_of_downloads_for_link'] = $numberOfDownloadsForLink;
                $customerData['remaining_qty'] = (int) $this->getRemainingQty($numberOfDownloadsBought, $numberOfDownloadsUsed, $numberOfDownloadsForLink);
                $customerData['status'] = ucfirst($purchasedItem->getStatus());
                $customerData['last_login_date'] = $this->getLastLogin($purchased->getCustomerId());
                $assignedUser [] = $customerData;
            }
            
        }
        return $assignedUser;
        
    }
    
    /*
     * @desc: get Last Login Date.
     * @param $customerId string
     * return string $lastLoginAt
     */
    protected function getLastLogin($customerId) {
        //getId
        //\Magento\Customer\Model\Logger
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $logData = $objectManager->get('\Magento\Customer\Model\Logger')->get($customerId);
        $lastLogin = $logData->getLastLoginAt();
        $lastLoginAt = 'Never (Offline)';
        if (!empty($lastLogin)) {
            $date = new \DateTime($lastLogin);
            $date->setTimezone(new \DateTimeZone('America/New_York'));
            $lastLoginAt = date('Y/m/d h:i:s A', strtotime($date->format('Y-m-d H:i:s')));

        }
        
        return $lastLoginAt;
    }
    
    /**
     * @desc get Remaining QTY for Purchased Item.
     * @param string $downloadesBought
     * @param string $downloadesUsed
     * @param string $numberOfDownload
     * 
     * @return string $remainingQty
     */
    protected function getRemainingQty($downloadesBought, $downloadesUsed, $numberOfDownload) {

        if ($numberOfDownload != 0) {
            $remainingQty = ( $downloadesBought - $downloadesUsed ) / $numberOfDownload;
            
        } else {
            $remainingQty = $downloadesBought - $downloadesUsed;
            
        }
        return $remainingQty;
        
    }
    
    /**
     * @desc get number of downloads for link
     * @param string $purchasedLineItemId
     * 
     * @return string $numberOfDownloads
     */
    protected function getNumberOfDownloadablesForLink($purchasedLineItemId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $purchasedLinkItem = $objectManager->get('\Magento\Downloadable\Model\Link\Purchased\ItemFactory');
        $purchasedItem = $purchasedLinkItem->create()->load($purchasedLineItemId);
        $downloadableLink = $this->_linkFactory->create()->load($purchasedItem->getLinkId());
        $numberOfDownloads = $downloadableLink->getNumberOfDownloads();
        return $numberOfDownloads;
        
    }
    
    /**
     * @desc get changed by user which make assignment.
     * @param string $relatedItemId
     * @return 
     */
    protected function getChangedBy($relatedItemId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $relatedItems = $objectManager->create('Endeavor\Downloadable\Model\RelatedItems')->getCollection();
        foreach ($relatedItems->getItems() as $item) {
            if ($item->getRelatedItemId() == $relatedItemId) {
                $changedBy = $item->getCreatedBy();
                break;
                
            }
            
        }
        return $changedBy;
        
    }
    
    /*
     * {@inheritdoc}
     */
    public function addMoreDownloadsForAssigned($purchasedLineItemId, $addNumberOfDownloads) {
        $this->_logger->info('POST addMoreDownloadsForAssigned API - Start');
        $request = ['purchasedLineItemId' => $purchasedLineItemId, 'addNumberOfDownloads' => $addNumberOfDownloads];
        $this->_logger->info('POST addMoreDownloadsForAssigned API - Request', $request);
        
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $purchasedLinkItem = $objectManager->get('\Magento\Downloadable\Model\Link\Purchased\ItemFactory');
            $purchasedItem = $purchasedLinkItem->create()->load($purchasedLineItemId);
            
            $oldNumberOfDownloads = $purchasedItem->getNumberOfDownloadsBought();
            $newNumberOfDownloads = $oldNumberOfDownloads + $addNumberOfDownloads;
            $purchasedItem->setNumberOfDownloadsBought($newNumberOfDownloads);
            $purchasedItem->setStatus('available');
            $purchasedItem->save();
            
            $response [] = ['status' => true, 'message' => 'more downloads for assigned'];
            
        } catch (\Exception $e) {
            $response [] = ['status' => false, 'message' => $e->getMessage()];
            $this->_logger->crit('POST addMoreDownloadsForAssigned API - Exception: purchasedLineItemId#' . $purchasedLineItemId . ': ' . $e->getMessage());
            
        }
        $this->_logger->info('POST addMoreDownloadsForAssigned API - Response:', $response);
        $this->_logger->info('POST addMoreDownloadsForAssigned API - End' . PHP_EOL);
        
        return $response;
        
    }

}
