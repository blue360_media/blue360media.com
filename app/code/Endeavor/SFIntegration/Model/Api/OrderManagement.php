<?php
namespace Endeavor\SFIntegration\Model\Api;

use Magento\Framework\Math\Random;
use Magento\Framework\Stdlib\DateTime;

/**
 * @author: Murad Dweikat
 * @desc: Used for out REST Api to handle sales order in eCommerce.
 * 
 */
class OrderManagement implements \Endeavor\SFIntegration\Api\OrderManagementInterface
{
    const ADDRESS_MAX_LENGTH = 80;
    const CUSTOMER_GROUP_GENERAL_ID = 1;
    const CUSTOMER_GROUP_GENERAL_LABEL = 'General';
    const CUSTOMER_GROUP_TAX_EXEMPT_LABEL = 'Tax Exempt';
    const SHIPPNG_METHOD_STANDARD = 'Standard';
	const SHIPPNG_METHOD_UPS = 'UPS';
    const SHIPPING_METHOD_NEXT_DAY = 'Next Day';
    const SHIPPNG_METHOD_NEXT_2_DAY = 'Next 2-Day';
    const SHIPPNG_METHOD_STANDARD_DESCRIPTION = 'Shipping Method - Standard';
	// start change by vishal shipping lable, description, SHIPPING_METHOD_STANDARD_WH_ID from 95 to UG
    const SHIPPNG_METHOD_UPS_DESCRIPTION = 'Standard Shipping - UPS Ground';
    const SHIPPING_METHOD_NEXT_DAY_DESCRIPTION = 'Order Placed Before 3 pm EST - Next Day';
    const SHIPPNG_METHOD_NEXT_2_DAY_DESCRIPTION = 'Order Placed Before 3 pm EST - Next Two Day';
    const SHIPPING_METHOD_STANDARD_WH_ID = 'UG';
    // end change by vishal shipping lable, description, SHIPPING_METHOD_STANDARD_WH_ID from 95 to UG
    
	const SHIPPING_METHOD_UPS_WH_ID = 'UG';
    const SHIPPING_METHOD_NEXT_DAY_WH_ID = '91';
    const SHIPPNG_METHOD_NEXT_2_DAY_WH_ID = '92';


    
    /**
     *
     * @var Magento\Framework\App\Request\Http 
     */
    protected $request;

    /**
     *
     * @var Magento\Catalog\Model\ProductFactory 
     */
    protected $_ProductFactory;
    
    /**
     * Downloadable Purchased Item object
     *
     * @var \Magento\Downloadable\Model\Link\Purchased\ItemFactory 
     */
    protected $_purchasedItemFactory;
   
    /**
     *
     * @var \Magento\Store\Model\StoreManagerInterface 
     */
    protected $_storeManager;
            
    /**
     *
     * @var \Magento\Customer\Model\CustomerFactory 
     */
    protected $customerFactory;
    
    /**
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface 
     */
    protected $cartRepositoryInterface ;
    
    /**
     *
     * @var \Magento\Quote\Api\CartManagementInterface 
     */
    protected $cartManagementInterface ;
    
    /**
     *
     * @var \Magento\Quote\Model\Quote\Address\Rate 
     */
    protected $shippingRate;
        
    /**
     * @var Random
     */
    private $mathRandom;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     *
     * @var \Endeavor\SFIntegration\Model\OrderFactory
     */
    protected $orderFactory;
        
    /**
     *
     * @var Magento\Catalog\Model\Product 
     */
    protected $product;

    /**
     * Logging instance
     * @var \Endeavor\SFIntegration\Logger\Logger
     */
    protected $_logger;
    
    /**
    * @var Magento\CatalogInventory\Model\StockRegistry 
    */
    protected $_stockRegistry;
         
    /**
     * @var \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\Matrixrate\CollectionFactory
     */
    protected $MatrixRateCollectionFactory;
    
    /**
     *
     * @var Magento\Sales\Model\ResourceModel\Order\CollectionFactory 
     */
    protected $_orderCollectionFactory;

    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;
    
    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender
     */
    protected $orderCommentSender;

    /**
     * 
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Catalog\Model\ProductFactory $ProductFactory
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Downloadable\Model\Link\Purchased\ItemFactory $PurchasedItemFactory
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender $orderCommentSender
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagementInterface
     * @param \Magento\Quote\Model\Quote\Address\Rate $shippingRate
     * @param \Endeavor\SFIntegration\Model\OrderFactory $OrderFactory
     * @param \Magento\CatalogInventory\Model\StockRegistry $stock
     * @param \Endeavor\SFIntegration\Logger\Logger $logger
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\Matrixrate\CollectionFactory $MatrixRateCollectionFactory
     * @param Random $mathRandom
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Catalog\Model\ProductFactory $ProductFactory,
        \Magento\Catalog\Model\Product $product,
        \Magento\Downloadable\Model\Link\Purchased\ItemFactory $PurchasedItemFactory,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender $orderCommentSender,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Quote\Model\Quote\Address\Rate $shippingRate,
        \Endeavor\SFIntegration\Model\OrderFactory $OrderFactory,
        \Magento\CatalogInventory\Model\StockRegistry $stock,
        \Endeavor\SFIntegration\Logger\Logger $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\Matrixrate\CollectionFactory $MatrixRateCollectionFactory,
        Random $mathRandom
    ){
        $this->request = $request;
        $this->_ProductFactory = $ProductFactory;
        $this->_purchasedItemFactory = $PurchasedItemFactory;
        $this->_storeManager = $storeManager;
        $this->customerFactory = $customerFactory;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->shippingRate = $shippingRate;
        $this->orderFactory = $OrderFactory;
        $this->mathRandom = $mathRandom;
        $this->product = $product;
        $this->_logger = $logger;
        $this->_stockRegistry = $stock;
        $this->MatrixRateCollectionFactory = $MatrixRateCollectionFactory;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->orderCommentSender = $orderCommentSender;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * @author: Murad Dweikat
     * @desc: Get processing eCommerce Orders that not exported to Salesforce yet.
     * 
     * @update_at: 2017/11/01
     * @desc: if lastname is empty we used following rules: 
     *          1) if firstname have fullname we split fullname into firstname and lastname.
     *          2) if firstname have just one name store lastname as firstname.
     * 
     * @updateBy Mohammad Hamadneh <mhamadneh@endeavorpal.com>
     * @date 2-1-2017.
     * @desc update to add customer market segments and secondary market segments.
     * 
     *
     * @update_by: Murad Dweikat <mdweikat@endeavorpal.com>
     * @update_date: 2018/02/19
     * @desc: To make sure that the attribute "is_special_multi_shipping_flow" always return in response api.
     *
     * @return mixed
     */
    public function getOrder() 
    {
        // start change by vishal as per discuss with naveen to assign states to sales person change

        $salesNames = [
            'Alabama'=>'Mary',
            'Alaska'=>'Carli',
            'American Samoa'=>'Mary',
            'Arizona'=>'Carli',
            'Arkansas'=>'Carli',
            'Armed Forces Other'=>'Mary',
            'Armed Forces US'=>'Mary',
            'Armed Forces Pacific'=>'Mary',
            'California'=>'Carli',
            'Colorado'=>'Carli',
            'Connecticut'=>'Mary',
            'Delaware'=>'Mary',
            'District of Columbia'=>'Mary',
            'Micronesia'=>'Mary',       
            'Florida'=>'Carli',
            'Georgia'=>'Mary',
            'Guam'=>'Mary',         
            'Hawaii'=>'Carli',
            'Idaho'=>'Carli',
            'Illinois'=>'Mary',
            'Indiana'=>'Mary',
            'Iowa'=>'Carli',
            'Kansas'=>'Carli',
            'Kentucky'=>'Mary',
            'Louisiana'=>'Carli',
            'Maine'=>'Mary',
            'Marshall Island'=>'Mary',
            'Maryland'=>'Mary',
            'Massachusetts'=>'Mary',
            'Michigan'=>'Mary',
            'Minnesota'=>'Carli',
            'Mississippi'=>'Carli',
            'Missouri'=>'Carli',
            'Montana'=>'Carli',
            'Nebraska'=>'Carli',
            'Nevada'=>'Carli',
            'New Hampshire'=>'Mary',
            'New Jersey'=>'Mary',
            'New Mexico'=>'Mary',
            'New York'=>'Mary',
            'North Carolina'=>'Mary',
            'North Dakota'=>'Carli',
            'Northern Mariana Isl'=>'Mary',
            'Ohio'=>'Mary',
            'Oklahoma'=>'Carli',
            'Oregon'=>'Carli',
            'Palau'=>'Mary',
            'Pennsylvania'=>'Mary',
            'Puerto Rico'=>'Carli',
            'Rhode Island'=>'Mary',
            'South Carolina'=>'Mary',
            'South Dakota'=>'Carli',
            'Tennessee'=>'Mary',
            'Texas'=>'Carli',
            'Utah'=>'Carli',
            'Vermont'=>'Mary',
            'Virgin Islands'=>'Mary',
            'Virginia'=>'Mary',
            'Washington'=>'Carli',
            'West Virginia'=>'Mary',
            'Wisconsin'=>'Carli',
            'Wyoming'=>'Carli'];

            // end change by vishal as per discuss with naveen to assign states to sales person change
                
        $ownderCodes = ['Carli' => 'cliso360','Barbara' => 'BKB360', 'Linda' => 'LYB360', 'Susan' => 'SHanc', 
            'Kimberly' => 'KTB360', 'Mary' => 'MRB360'];
        
        $this->_logger->info('Get Sales Orders API - Start');

        //Get Object Manager Instance
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $preOrderHelper = $objectManager->create('\Webkul\Preorder\Helper\Data');

        $post = $this->request->getParams();
        if (isset($post['from'])) {
            $from = $post['from'];
            
        } else {
            $from = date('Y-m-d H:i:s',strtotime(''));
            
        }
        
        if (isset($post['to'])) {
            $to = $post['to'];
            
        } else {
            $to = date('Y-m-d H:i:s');
            
        }
        if (isset($post['complete_payment']) && $post['complete_payment'] == 'true') {
            $completePayment = true;
            
        } else {
            $completePayment = false;
            
        }
        
        $fromDate = date('Y-m-d H:i:s', strtotime($from));
        $toDate = date('Y-m-d H:i:s', strtotime($to));
        
        $this->_logger->info('Get Sales Orders API: FromDate:' . $fromDate . ', ToDate : ' . $toDate . ', completePayment: ' . $completePayment);
        $orders = [];
        $orderCollection = null;
        
        try {
            /**
             * @updatedBy: Mohammad Hamadneh <mhamadneh@endeavorpal.com>
             * @date 01-24-2018.
             * @desc in order to separate payment object while importing Process for Payment.
             */
            if ($completePayment) {
                $orderCollection = $objectManager->create('Magento\Sales\Model\Order')
                        ->getCollection()
                        ->addFieldToFilter(['source_order', 'status'], [['neq' => 'SF'], ['eq' => 'complete_payment']])
                        ->addFieldToFilter('status', [['in' => ['complete_payment']]])
                        ->addFieldToFilter('sf_exported_status', ['neq' => '1'])
                        ->addFieldToSelect(
                                [
                                    'entity_id',
                                    'quote_id',
                                    'status',
                                    'shipping_description',
                                    'shipping_amount',
                                    'customer_id',
                                    'discount_amount',
                                    'subtotal',
                                    'grand_total',
                                    'tax_amount',
                                    'total_paid',
                                    'last_paid_amount',
                                    'total_qty_ordered',
                                    'billing_address_id',
                                    'shipping_address_id',
                                    'sf_order_id',
                                    'increment_id',
                                    'created_at',
                                    'updated_at'
                                ]
                        )
                        ->addFieldToFilter('updated_at', ['lteq' => $toDate])
                        ->addFieldToFilter('updated_at', ['gteq' => $fromDate]);
                
            } else {
                $orderCollection = $objectManager->create('Magento\Sales\Model\Order')
                        ->getCollection()
                        ->addFieldToFilter(['source_order'], [['neq' => 'SF']])
                        ->addFieldToFilter('status', [['in' => ['complete', 'processing', 'pending']]])
                        ->addFieldToFilter('sf_exported_status', ['neq' => '1'])
                        ->addFieldToSelect(
                                [
                                    'entity_id',
                                    'quote_id',
                                    'status',
                                    'shipping_description',
                                    'shipping_amount',
                                    'customer_id',
                                    'discount_amount',
                                    'subtotal',
                                    'grand_total',
                                    'tax_amount',
                                    'total_paid',
                                    'last_paid_amount',
                                    'total_qty_ordered',
                                    'billing_address_id',
                                    'shipping_address_id',
                                    'sf_order_id',
                                    'increment_id',
                                    'created_at',
                                    'updated_at'
                                ]
                        )
                        ->addFieldToFilter('updated_at', ['lteq' => $toDate])
                        ->addFieldToFilter('updated_at', ['gteq' => $fromDate]);
                
            }
            
        } catch (\Exception $e) {
            $this->_logger->crit('Get Sales Order API - Exception: '.$e->getMessage());
            $this->_logger->crit('Get Sales Order API - Exception Trace: '. $e->getTraceAsString());
            
        }
        
        $orderItemKeys = [
            'sku' => '','qty_ordered' => '','price' => '','custom_increment_id' => '',
            'tax_amount' => '','discount_amount' => '','shipping_discount' => '','row_total' => '',
            'product_type' => '','row_total_incl_tax' => '', 'free_shipping' => ''
        ];
        
        foreach ($orderCollection->getItems() as $salesOrder) {
                $order = $salesOrder;
            try {
                    
                // skip availabe order for pending order.
                $isPreOrder = $preOrderHelper->isContainPreOrderBundle($order);
                if ($order->getStatus() == 'pending' && !$isPreOrder) {
                    continue;
                }
                $order->addData(['isPreOrder' => $isPreOrder]);
                
                $items = [];
                $productIdJurisdiction = null;
                $isContainFreeShippingItem = false;
                foreach ($order->getItems() as $orderItem) {
                    $orderItemData = $orderItem->getData();

                    if ($orderItemData['product_type'] == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
                        continue;

                    }
                    if ($productIdJurisdiction == null) {
                        $productIdJurisdiction = $orderItemData['product_id'];
                    }
                    if (!$isContainFreeShippingItem && $orderItemData['free_shipping']) {
                        $isContainFreeShippingItem = true;
                        
                    }
                    $orderArray = array_intersect_key($orderItemData, $orderItemKeys);
                    $items[] = $orderArray;
                }
                $order->addData(['items' => $items]);
                $order->addData(['isFreeShipping' => $isContainFreeShippingItem]);
                
                $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($order->getCustomerId());
                $customerKeys = ['email' => '', 'firstname' => '', 'lastname' => '',
                    'customer_blue_account_id' => '', 'customer_blue_customer_number' => '', 'is_special_multi_shipping_flow' => ''];
                $customerArray = array_intersect_key($customer->getData(), $customerKeys);
                
                /**
                 * @update_by: Murad Dweikat
                 * @update_date: 2018/02/19
                 * @desc: To make sure that the attribute "is_special_multi_shipping_flow" always return in response api.
                 */
                if (isset($customerArray['is_special_multi_shipping_flow']) && $customerArray['is_special_multi_shipping_flow'] == '1') {
                    $customerArray['is_special_multi_shipping_flow'] = true;
                } else {
                    $customerArray['is_special_multi_shipping_flow'] = false;
                }
                
                $order->addData(['customer' => $customerArray]);

                if ($order->getShippingDescription() == self::SHIPPING_METHOD_NEXT_DAY_DESCRIPTION) {
                    $order->setShippingDescription(self::SHIPPING_METHOD_NEXT_DAY_WH_ID);

                } else if ($order->getShippingDescription() == self::SHIPPNG_METHOD_NEXT_2_DAY_DESCRIPTION) {
                    $order->setShippingDescription(self::SHIPPNG_METHOD_NEXT_2_DAY_WH_ID);

                } else if ($order->getShippingDescription() == self::SHIPPNG_METHOD_UPS_DESCRIPTION) {
                    $order->setShippingDescription(self::SHIPPING_METHOD_UPS_WH_ID);

                } else {
                    $order->setShippingDescription(self::SHIPPING_METHOD_STANDARD_WH_ID);

                }

                if (!empty($order->getTotalPaid())) {
                    $order->setTotalPaid('Y');                

                } else {
                    $order->setTotalPaid('N');

                }

                // start by vishal added shipping company name
                
                $orderAddressKeys = [
                    'company' => '','firstname' => '','lastname' => '','postcode' => '',
                    'city' => '','telephone' => '','fax' => '','region' => '','country_id' => '',
                    'email' => '', 'shipping_email' => ''
                ];

                // end by vishal added shipping company name

                $orderAddress = $objectManager->create('Magento\Sales\Model\Order\Address')
                        ->load($order->getShippingAddressId());
                
                $shippingAddressEmail = $orderAddress->getShippingEmail();
                $customerAddressId = $orderAddress->getCustomerAddressId();
                if (empty($shippingAddressEmail) && !empty($customerAddressId)) {
                    $shippingAddressRepository = $objectManager->get('Magento\Customer\Api\AddressRepositoryInterface');
                    $shippingAddressRep = $shippingAddressRepository->getById($customerAddressId);
                    $shippingAddressEmailAttribute = $shippingAddressRep->getCustomAttribute('shipping_address_email');
                    if ($shippingAddressEmailAttribute != null) {
                        $shippingAddressEmail = $shippingAddressEmailAttribute->getValue();

                    }

                }
                
                /**
                 * If Shipping Email not exist used customer email.
                 */
                if (!$shippingAddressEmail) {
                    $shippingAddressEmail = $order->getCustomerEmail();

                }
                
                $orderAddressArray = array_intersect_key($orderAddress->getData(), $orderAddressKeys);
                
                if (empty($orderAddressArray['shipping_email'])) {
                    $orderAddressArray['shipping_email'] = $shippingAddressEmail;

                }
                
                if (!empty($orderAddressArray)) {
                    if (empty($orderAddressArray['lastname']) && !empty($orderAddressArray['firstname'])) {
                        
                        $name = explode(' ',trim($orderAddressArray['firstname']));
                        if (count($name) == 1) {
                            $orderAddressArray['lastname'] = $name[0];
                        } else if (count($name) == 2) {
                            $orderAddressArray['firstname'] = $name[0];
                            $orderAddressArray['lastname'] = $name[1];
                        }
                    }
                    if (empty($orderAddressArray['region'])) {
                        $regionName = $objectManager->create('Magento\Directory\Model\Region')->load($orderAddress->getData('region_id'));
                        $orderAddressArray['region'] = $regionName['name'];

                    }


                    // start change by vishal added shipping company name

                    $shipcompany = $orderAddress->getCompany();
                    if (isset($shipcompany)) {
                        $orderAddressArray['company'] = $orderAddress->getCompany();

                    } else {
                        $orderAddressArray['company'] = '';

                    }

                    // end change by vishal added shipping company name   




                    if (isset($orderAddress->getStreet()[0])) {
                        $orderAddressArray['address1'] = $orderAddress->getStreet()[0];

                    } else{
                        $orderAddressArray['address1'] = '';

                    }

                    if (isset($orderAddress->getStreet()[1])) {
                        $orderAddressArray['address2'] = $orderAddress->getStreet()[1];

                    } else {
                        $orderAddressArray['address2'] = '';

                    }


                    // start change by vishal added shipping company name
                    if (!empty($orderAddressArray['company'])) {

                    $orderAddressArray['company'] = substr($orderAddressArray['company'], 0, self::ADDRESS_MAX_LENGTH);

                    }
                    // end change by vishal added shipping company name


                    $orderAddressArray['address1'] = substr($orderAddressArray['address1'], 0, self::ADDRESS_MAX_LENGTH);
                    $orderAddressArray['address2'] = substr($orderAddressArray['address2'], 0, self::ADDRESS_MAX_LENGTH);
                    
                    $quoteHelper = $objectManager->get('\Magento\Quote\Model\Quote')->load($order->getQuoteId());
                    $quoteIsMultiShipping = $quoteHelper->getIsMultiShipping();
                    
                    if ($quoteIsMultiShipping) {
                        $order->addData(['isMultiShipping' => true]);
                        
                    } else {
                        $order->addData(['isMultiShipping' => false]);
                        
                    }
                    
                } else {
                    $orderAddressArray = [];
                }
                $order->addData(['shipping' => $orderAddressArray]);
                $billingAddressArray = null;
                $paymentCode = $order->getPayment()->getMethodInstance()->getCode();
                $paymentAdditionalInformation = $order->getPayment()->getData('additional_information');
                
                if ($paymentCode == 'paypal_express') {
                    if ($order->getPayment()->getId() != '' && $order->getPayment()->getLastTransId() != '') {
                        $paymentData['trans_id'] = $order->getPayment()->getLastTransId();
                    }
                } else {
                    if (isset($paymentAdditionalInformation['transaction_id'])) {
                        $paymentData['trans_id'] = $paymentAdditionalInformation['transaction_id'];
                    }
                }
                $paymentData['payment_method'] = $paymentCode;
                $order->addData(['payment' => $paymentData]);

                // start change by vishal for making isFreeShipping true for free coupon code

                if($paymentCode == 'free' && $order->getGrandTotal() == 0){
                    
                    $order->setShippingDiscount($order->getShippingAmount());
                }

                // end change by vishal for making isFreeShipping true for free coupon code
                
                if ($order->getBillingAddressId() != null) {
                    $billingAddress = $objectManager->create('Magento\Sales\Model\Order\Address')
                            ->load($order->getBillingAddressId());

                    if ($billingAddress->getStreet() == null || $billingAddress->getRegion() == null 
                            || $billingAddress->getPostcode() == null) {
                        if ($paymentCode == 'paypal_express') {
                            $billingAddressArray = [
                                'firstname' => $customer->getFirstname(),'lastname' => $customer->getLastname(),
                                'postcode' => '84098', 'city' => 'Park City','telephone' => '8445992887',
                                'fax' => '4356046970','region' => 'Utah','country_id' => 'US',
                                'email' => $customer->getEmail(),'address1' => 'PayPal Order',
                                'address2' => '2750 Rasmussen Road, Suite 107'
                            ];
                        } else {
                            $billingAddressArray = [
                                'firstname' => 'dummy','lastname' => 'dummy','postcode' => '12345',
                                'city' => 'dummy','telephone' => null,'fax' => null,'region' => 'New York','country_id' => 'US',
                                'email' => $customer->getEmail(),'address1' => 'dummy','address2' => null
                            ];
                        }
                    } else {
                        $billingAddressArray = array_intersect_key($billingAddress->getData(), $orderAddressKeys);
                        if (isset($billingAddress->getStreet()[0])) {
                            $billingAddressArray['address1'] = $billingAddress->getStreet()[0];

                        } else {
                            $billingAddressArray['address1'] = '';

                        }

                        
                        // start chage by vishal add billing company name

                        $billcompany = $billingAddress->getCompany();
                        if (isset($billcompany)) {
                            $billingAddressArray['company'] = $billingAddress->getCompany();

                        } else {
                            $billingAddressArray['company'] = '';

                        }

                        // end chage by vishal add billing company name





                        if (isset($billingAddress->getStreet()[1])) {
                            $billingAddressArray['address2'] = $billingAddress->getStreet()[1];


                        } else {
                                $billingAddressArray['address2'] = '';

                        }
                    }
                } else {
                    if ($paymentCode == 'paypal_express') {
                        
                        $billingAddressArray = [
                            'firstname' => $customer->getFirstname(),'lastname' => $customer->getLastname(),
                            'postcode' => '84098', 'city' => 'Park City','telephone' => '8445992887',
                            'fax' => '4356046970','region' => 'Utah','country_id' => 'US',
                            'email' => $customer->getEmail(),'address1' => 'PayPal Order',
                            'address2' => '2750 Rasmussen Road, Suite 107'
                        ];
                    } else {
                        $billingAddressArray = [
                            'firstname' => 'dummy','lastname' => 'dummy','postcode' => '12345',
                            'city' => 'dummy','telephone' => null,'fax' => null,'region' => 'New York','country_id' => 'US',
                            'email' => $customer->getEmail(),'address1' => 'dummy','address2' => null
                        ];
                    }
                }
                if (empty($orderAddressArray)) {
                    $orderAddressArray = $billingAddressArray;
                    $order->addData(['shipping' => $orderAddressArray]);
                }

                if (empty($billingAddressArray['region'])) {
                    $regionName = $objectManager->create('Magento\Directory\Model\Region')->load($billingAddress->getData('region_id'));
                    $billingAddressArray['region'] = $regionName['name'];

                }


                // start change by vishal add company in billing address
                if (!empty($billingAddressArray['company'])) {
                $billingAddressArray['company'] = substr($billingAddressArray['company'],0,self::ADDRESS_MAX_LENGTH);
                   }
                // end change by vishal add company in billing address


                $billingAddressArray['address1'] = substr($billingAddressArray['address1'],0,self::ADDRESS_MAX_LENGTH);
                $billingAddressArray['address2'] = substr($billingAddressArray['address2'],0,self::ADDRESS_MAX_LENGTH);

                if ($billingAddressArray['lastname'] == '' || $billingAddressArray['lastname'] == null) {
                    $name = explode(' ',trim($billingAddressArray['firstname']));
                    if (count($name) == 1) {
                        $billingAddressArray['lastname'] = $name[0];
                    } else if (count($name) == 2) {
                        $billingAddressArray['firstname'] = $name[0];
                        $billingAddressArray['lastname'] = $name[1];
                    }
                }

                if ($order->getShippingAddressId() != null) {
                    if (!empty($orderAddressArray['region'])) {
                        $regionName = $objectManager->create('Magento\Directory\Model\Region')
                                ->load($orderAddress->getData('region_id'));
                        if (!empty($regionName->getName())) {
                            $salesName = $salesNames[$regionName->getName()];
                            $order->addData(['salesName' => $salesName]);
                            $order->addData(['ownerCode' => $ownderCodes[$salesName]]);
                        } else {
                            $order->addData(['salesName' => '']);
                            $order->addData(['ownerCode' => '']);
                        }

                    }
                } else if ($order->getBillingAddressId() != null) {
                    if (!empty($billingAddressArray['region'])) {
                        $regionName = $objectManager->create('Magento\Directory\Model\Region')
                                ->load($billingAddress->getData('region_id'));
                        if (!empty($regionName->getName())) {
                            $salesName = $salesNames[$regionName->getName()];
                            $order->addData(['salesName' => $salesName]);
                            $order->addData(['ownerCode' => $ownderCodes[$salesName]]);
                        } else {
                            $order->addData(['salesName' => '']);
                            $order->addData(['ownerCode' => '']);
                        }

                    }
                }
                if ($billingAddressArray !== $orderAddressArray) {
                    $order->addData(['isBillingSameAsShipping' => false]);

                } else {
                    $order->addData(['isBillingSameAsShipping' => true]);

                }

                $marketSegmentValue = '';
                $secondaryMarketValue = '';
                $marketSegment = 'customer_market_segment';
                $secondaryMarket = 'customer_secondary_market';
                /*Starts Previous Condition Kimberly Assigned*/

                /*if (!$customer->getId()) {
                    $quote = $objectManager->create('Magento\Quote\Model\Quote')
                            ->load($order->getQuoteId());
                    $segment = explode("|",$quote->getGuestSegment());
                    $marketSegmentValue = (isset($segment[0]) && $segment[0] != null) ? $segment[0] : '';
                    $secondaryMarketValue = (isset($segment[1]) && $segment[1] != null) ? $segment[1] : '';

                } else {
                    $customerData = $customer->getDataModel();
                    $marketSegmentOption = $customerData->getCustomAttribute($marketSegment);
                    $marketSegmentValue = (isset($marketSegmentOption)) ? $marketSegmentOption->getValue() : ''; 
                    $secondaryMarketOption = $customerData->getCustomAttribute($secondaryMarket); 
                    $secondaryMarketValue = (isset($secondaryMarketOption)) ? $secondaryMarketOption->getValue() : '';
                    */
                    
                    /**
                     * @desc: store Sales Name in customer attribute.
                     */
                  /*  $salesNameOption = $customerData->getCustomAttribute('customer_sales_rep');
                    $customerSalesName = (isset($salesNameOption)) ? $salesNameOption->getValue() : ''; 
                    if (empty($customerSalesName)) {
                        $customerRepository = $objectManager->create('Magento\Customer\Api\CustomerRepositoryInterface');
                        $customerRep = $customerRepository->getById($customer->getId());
                        $customerRep->setCustomAttribute('customer_sales_rep',$order->getData('salesName'));
                        $customerRepository->save($customerRep);
                    }
                }

                $secondarySegmentCollecttion = $objectManager->create('Endeavor\CustomerAttributes\Model\SecondarySegment')
                    ->getCollection();  
                $secondarySegmentCollecttion->addFieldToFilter('name',['eq' => 'Students']);

                $secondaryMarketId = $secondarySegmentCollecttion->getFirstItem()->getSegmentId();

                $attributeModel = $objectManager->create('Magento\Customer\Model\Attribute'
                        )->load($marketSegment,'attribute_code');
                $options = $attributeModel->getSource()->getAllOptions();
                foreach ($options as &$item) {
                    if (($item["label"] == 'Academic' && $marketSegmentValue == $item["value"]) || 
                            ($item["label"] == 'Corporate' && $marketSegmentValue == $item["value"]
                                && $secondaryMarketValue == $secondaryMarketId)) {
                         if (!empty($regionName->getName()) && $regionName->getName() != "New Mexico") {				
							$salesName = 'Kimberly';
							$order->addData(['salesName' => $salesName]);
							$order->addData(['ownerCode' => $ownderCodes[$salesName]]);
							break;
						 }

                    }
                }
                */

                /*Finish Previous Condition Kimberly Assigned*/
                $customerSegments = ['market_segment' => '', 'secondary_market_segment' => ''];
                $order->addData(['customer_segements' => $customerSegments]);


                
                if (!empty($customer->getId())) {
                    $customerData = $customer->getDataModel();
                    $marketSegmentOption = $customerData->getCustomAttribute($marketSegment);
                    $marketSegmentValue = (isset($marketSegmentOption)) ? $marketSegmentOption->getValue() : ''; 
                    $secondaryMarketOption = $customerData->getCustomAttribute($secondaryMarket); 
                    $secondaryMarketValue = (isset($secondaryMarketOption)) ? $secondaryMarketOption->getValue() : '';
                    
                    /**
                     * @desc: store Sales Name in customer attribute.
                     */
                    $salesNameOption = $customerData->getCustomAttribute('customer_sales_rep');
                    $customerSalesName = (isset($salesNameOption)) ? $salesNameOption->getValue() : ''; 
                    if (empty($customerSalesName)) {
                        $customerRepository = $objectManager->create('Magento\Customer\Api\CustomerRepositoryInterface');
                        $customerRep = $customerRepository->getById($customer->getId());
                        $customerRep->setCustomAttribute('customer_sales_rep',$order->getData('salesName'));
                        $customerRepository->save($customerRep);
                    }
                    
                    $marketAttributeModel = $objectManager->create('Magento\Customer\Model\Attribute')
                                ->load($marketSegment,'attribute_code');

                    $options = $marketAttributeModel->getSource()->getAllOptions();
                    $optionId = '';
                    if ($customerData->getCustomAttribute($marketSegment)) {
                        $optionId = $customerData->getCustomAttribute($marketSegment)->getValue();
                    }
                    if ($marketSegmentValue == 0) {
                        $customerSegments['market_segment'] = '';
                    } else {
                        foreach ($options as $value) {
                            if ($value['value'] == $marketSegmentValue) {
                                $customerSegments['market_segment'] = $value['label'];
                            }
                        }
                    }
                    $attributeModel = $objectManager->create('Magento\Customer\Model\Attribute')
                                ->load($secondaryMarket,'attribute_code');

                    $options = $attributeModel->getSource()->getAllOptions();
                    $optionId = '';
                    if ($customerData->getCustomAttribute($secondaryMarket)) {
                        $optionId = $customerData->getCustomAttribute($secondaryMarket)->getValue();
                    }
                    
                    if ($secondaryMarketValue == 0) {
                        $customerSegments['secondary_market_segment'] = '';
                    } else {
                        $secondarySegmentCollecttion = $objectManager->create('Endeavor\CustomerAttributes\Model\SecondarySegment')
                                ->getCollection()
                                ->addFieldToFilter('segment_id', ['eq' => $secondaryMarketValue]);
                        foreach ($secondarySegmentCollecttion as $secondarySegment) {
                            $customerSegments['secondary_market_segment'] = $secondarySegment->getName();
                        }
                    }
                    $order->addData(['customer_segements' => $customerSegments]);
            
                    /*Starts Condition For Kimberly assigned Process */

                    // start by vishal to assign kimberly as sales persion by market segment value

                    if ( ($customerSegments['market_segment'] == 'Bookstore / Distributor') ||
                        ($customerSegments['market_segment'] == 'Library' && $customerSegments['secondary_market_segment'] == 'Public Library' || $customerSegments['secondary_market_segment'] == 'University Library') ||
                        ($customerSegments['market_segment'] == 'Police Academy' && $customerSegments['secondary_market_segment'] == 'Private Academy' || $customerSegments['secondary_market_segment'] == 'Individual Student' || $customerSegments['secondary_market_segment'] == 'University Police Academy') || 
                        ($customerSegments['market_segment'] == 'Police Department' && $customerSegments['secondary_market_segment'] == 'University Police') ||
                        ($customerSegments['market_segment'] == 'Other' && $customerSegments['secondary_market_segment'] == 'Student') ||
                        ($customerSegments['market_segment'] == 'Public School / Board / Career Center') ||   
                        ($customerSegments['market_segment'] == 'Academic') || 
                        ($customerSegments['market_segment'] == 'University') ||
                        ($customerSegments['market_segment'] == 'Corporate'))                       
                        {
                         if (!empty($regionName->getName()) && $regionName->getName() != "New Mexico") {                
                            $salesName = 'Kimberly';
                            $order->addData(['salesName' => $salesName]);
                            $order->addData(['ownerCode' => $ownderCodes[$salesName]]);
                         }
                    }

// end by vishal to assign kimberly as sales persion by market segment value



                    /*Finish Condition For Kimberly assigned Process */





                }
                
                if ($billingAddress->getStreet() == null || $billingAddress->getRegion() == null 
                            || $billingAddress->getPostcode() == null) {
                    $salesName = 'Susan';
                    
                    /*
                     * @updated_by: mohammad hamadneh <mhamadneh@endeavorpal.com>
                     * @date: 01-08-2018.
                     * @desc: Check product jurisdiction.
                     */
                    $productAttributes = $objectManager->create('Magento\Catalog\Model\Product')->load($productIdJurisdiction);
                    $productJurisdictions = $productAttributes->getResource()->getAttribute('jurisdiction')->getFrontend()->getValue($productAttributes);
                    $productJurisdictionsAsArray = explode(",", $productJurisdictions);
                    $itemJursdiction = null;
                    foreach ($productJurisdictionsAsArray as $productJurisdictions) {
                        if ($itemJursdiction == null && $productJurisdictions != 'National') {
                            $itemJursdiction = $productJurisdictions;
                        }
                    }
                    
                    if ($itemJursdiction != null && !empty($salesNames[$itemJursdiction])) {
                        $salesName = $salesNames[$itemJursdiction];
                    }
                    $order->addData(['salesName' => $salesName]);
                    $order->addData(['ownerCode' => $ownderCodes[$salesName]]);

                }
                
                $order->addData(['billing' => $billingAddressArray]);
                
                $orders[] = $order->getData();
            } catch (\Exception $e) {
                $this->_logger->crit('Get Sales Order API - Exception: Order Number: '.$order->getIncrementId());
                $this->_logger->crit('Get Sales Order API - Exception: Error Message: '.$e->getMessage());
                $this->_logger->crit('Get Sales Order API - Exception Trace: '. $e->getTraceAsString());
            }

        }
        
        $this->_logger->info('Get Sales Orders API - Response:', $orders);
        $this->_logger->info('Get Sales Orders API - End'.PHP_EOL);
        return $orders;
    }
    
    /**
     * {@inheritdoc}
     */
    public function postOrder($order)
    {
        $this->_logger->info('Create Sales Order API - Start');
        $this->_logger->info('Create Sales Order API - Request:',$order);

        $result = $this->createSalesOrder($order);        
        return $result;
    }
    
    /**
     * @author: Murad Dweikat
     * @desc: Create Sales Order. 
     *
     * @update_at: 2017/10/31
     * @desc: return assigned customer email with Salesforce order number in response.
     * 
     * @updateBy Mohammad Hamadneh <mhamadneh@endeavorpal.com>
     * @date 4-1-2017.
     * @desc update to handle order which have only exclude Media Types.
     * 
     * @param array $orderData
     * @return mixed $result
     */
    public function createSalesOrder($orderData)
    {
        $output = array([
                'message' => [],
                'success' => [],
                'emails' => [],
                'fail' => []
            ]);
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $emails = [];
        foreach ($orderData as $salesOrder) {
            $store = $this->_storeManager->getStore();
            //init the customer
            $customerModel = $this->customerFactory->create();
            $customerModel->setWebsiteId($websiteId);
            $customerEmail = '';
            $dummyEmail = '';
            $customer = $this->customerFactory->create();
            $customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface');
            $isContainsEbooks = false;
            $publicationTitle = '';
            try {
                $orderCollection = $this->_orderCollectionFactory->create();
                $orderCollection->addFieldToSelect(['sf_order_id','source_order','increment_id'])
                                ->addFieldToFilter('sf_order_id',['eq' => $salesOrder['order_id']])
                                ->addFieldToFilter('source_order',['eq' => 'SF']);
                if ($orderCollection->getSize()) {
                    $output[0]['message'][] = 'Order already created. SF Order#:'
                            .$salesOrder['order_id'].', MagentoId#:' .$orderCollection->getFirstItem()->getIncrementId();
                    $output[0]['success'][] = 'SF Order#'.$salesOrder['order_id'];
                    continue;
                    
                }
                // always save address for customer.
                $salesOrder['billing_address']['save_in_address_book'] = 1;
                
                if (empty( $salesOrder['shipping_address']['telephone'])){
                    $salesOrder['shipping_address']['telephone'] = '0000000000';

                }
                if (empty( $salesOrder['billing_address']['telephone'])){
                    $salesOrder['billing_address']['telephone'] = '0000000000';

                }
                
                $filteredItems = [];
                $excludeMediaType = ['MKT', 'C', 'X', 'Z'];

                foreach ($salesOrder['items'] as $item) {
                    $isContainsExclude = false;
                    foreach ($excludeMediaType as $mediaType) {
                        if (strpos($item['sku'], 'B') === false && strpos($item['sku'], $mediaType) !== false) {
                            $isContainsExclude = true;
                            break;
                        }
                    }
                    if ($isContainsExclude) {
                        continue;
                    }
                    if (isset($filteredItems[$item['sku']])) {
                        $filteredItems[$item['sku']]['qty'] = $filteredItems[$item['sku']]['qty'] + $item['qty'];
                    } else {
                        $filteredItems[$item['sku']] = $item;
                    }
                }
                if (empty($filteredItems)) {
                    $output[0]['message'][] = 'Order doesnt have valid items. SF Order#:'
                            .$salesOrder['order_id'];
                    $output[0]['fail'][] = 'SF Order#'.$salesOrder['order_id'];
                    continue;
                }
                /**
                 * @desc: 1. Check validate email, and send email to support team for invalid email.
                 *        2. Create dummy email by using b360AddressID (SFContactId).
                 */        
                if (!empty($salesOrder['email'])) {
                    if (!\Zend_Validate::is(trim($salesOrder['email']), 'EmailAddress')) {
                        $dummyEmail = $this->generateDummyEmail($salesOrder['billing_address']['b360_address_id']);
                        
                        $subject = 'Create Dummy email when create order.';
                        $message = 'Order Request: Order#: '.$salesOrder['order_id'];
                        $message .= '<br>Create dummy email for customer email: '.$salesOrder['email'].' when creating order.<br><br>';
                        $this->sendErrorEmailMessage($subject, $message);
                        $customer = $customerModel->loadByEmail($dummyEmail);
                    } else {
                        /* Check if the customer have already account by using unique b360_address_id field */
                        $customer = $customerModel->loadByEmail($salesOrder['email']);
                        if (empty($customer->getId())) {
                            $dummyEmailForCheck = $this->generateDummyEmail($salesOrder['billing_address']['b360_address_id']);
                            $customerModel = $this->customerFactory->create();
                            $customerModel->setWebsiteId($websiteId);
                            $customerByDummyEmail = $customerModel->loadByEmail($dummyEmailForCheck);
                            if (!empty($customerByDummyEmail->getId())) {
                                $customer = $customerByDummyEmail;
                                $customer->setEmail($salesOrder['email']);
                                $customer->save();
                                $subject = 'Changing Customer Information for order#'.$salesOrder['order_id'];
                                $message = 'Change Customer Information for order#'.$salesOrder['order_id'].'<br>The email of customer #'.$customer->getId().' Changed From: '.$dummyEmailForCheck.' To: '.$salesOrder['email'];
                                $this->sendErrorEmailMessage($subject, $message);
                                
                            }
                        }
                        
                    }

                } else {
                    $dummyEmail = $this->generateDummyEmail($salesOrder['billing_address']['b360_address_id']);
                    $customer = $customerModel->loadByEmail($dummyEmail);
                    if (!empty($customer->getId())) {
                        $dummyEmail = '';
                    }
                }

                if ($customer && empty($customer->getId())) {

                    if (empty($dummyEmail)){
                        $customerEmail = $salesOrder['email'];
                        
                    }

                    if (!empty($salesOrder['account_id']) && empty($dummyEmail)) {
                        $customerModel = $customerModel->getCollection()
                            ->addAttributeToFilter('customer_blue_account_id',$salesOrder['account_id'])
                            ->getLastItem();
                        
                    }

                    if (!$customerModel->getEntityId() || (strpos($customerModel->getEmail(),"dummy") === false)) {

                        if (!empty($salesOrder['shipping_address']['firstname'])
                                && !empty($salesOrder['shipping_address']['lastname'])) {
                            $customer->setFirstname($salesOrder['shipping_address']['firstname']);
                            $customer->setLastname($salesOrder['shipping_address']['lastname']);

                        } else {
                            $output[0]['message'][] = 'Firstname & Lastname is require.';
                            return $output;
                        }

                        $attributeName = 'customer_role';

                        $attributeModel = $objectManager->create('Magento\Customer\Model\Attribute')
                                ->load($attributeName,'attribute_code');
                        $options = $attributeModel->getSource()->getAllOptions();

                        $optionId = 0;
                        foreach ($options as $value) {
                            if (strtolower($value['label']) == strtolower($salesOrder['customer_type'])) {
                               $optionId =  $value['value'];
                               
                            }
                        }

                        $newPasswordToken = $this->mathRandom->getUniqueHash();
                        $customer->setRpToken($newPasswordToken);
                        $customer->setRpTokenCreatedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT));
                        if (empty($dummyEmail)) {
                            $customer->setEmail($salesOrder['email']);

                        } else {
                            $customer->setEmail($dummyEmail);

                        }

                        try {
                            if (!$customer->getId()) {
                                $newCustomer = $customer->save();
                            } else {
                                $newCustomer = $customer;
                            }

                        } catch (\Exception $e) {
                            $this->_logger->crit('Create Sales Order API - Can\'t Save customer Exception: '. $e->getMessage());
                            $this->_logger->crit('Create Sales Order API - Can\'t Save customer Exception: duplicate request and customer already created');
                            $this->_logger->crit('Create Sales Order API - Load Customer by Email' . $salesOrder['email'].'SF: '.$salesOrder['order_id']);
                            continue;
                            
                        }

                        $customerRep = $customerRepository->getById($newCustomer->getId());
                        
                        if (isset($salesOrder['customer_id'])) {
                            $customerRep->setCustomAttribute('customer_blue_customer_number', $salesOrder['customer_id']);

                        }
                        if (isset($salesOrder['salesperson_name'])) {
                            $customerRep->setCustomAttribute('customer_sales_rep', $salesOrder['salesperson_name']);

                        }
                        if (isset($salesOrder['account_id'])) {
                            $customerRep->setCustomAttribute('customer_blue_account_id', $salesOrder['account_id']);

                        }
                        $customerRep->setCustomAttribute('customer_role',$optionId);
                        $customerRepository->save($customerRep);
                    } else {
                        $customerModel->setEmail($salesOrder['email']);
                        $customerModel->save();
                    }
                } else {
                    $customerEmail = $customer->getEmail();

                }
            } catch (\Exception $e) {
                $this->_logger->crit('Create Sales Order API - Exception: '.$e->getMessage(),$output[0]['fail']);
                $this->_logger->crit('Create Sales Order API - Exception Trace: '. $e->getTraceAsString());
                $output[0]['message'][] = $e->getMessage();

                return $output;

            }
            
            $cart_id = $this->cartManagementInterface->createEmptyCart();
            $cart = $this->cartRepositoryInterface->get($cart_id);
            $cart->setStore($store);
            
            $customerAssign = null;
            if (!empty($customer->getEntityId())) {
                $customerAssign = $customerRepository->getById($customer->getEntityId());

            } else if (!empty($customerModel->getEntityId())) {
                $customerAssign = $customerRepository->getById($customerModel->getEntityId());

            }
            
            $group_id = self::CUSTOMER_GROUP_GENERAL_ID;
            $applyChanges = false;
            $taxCustomerGroup = '';
            
            if ($salesOrder['isTaxExempt']) {
                $taxCustomerGroup = self::CUSTOMER_GROUP_TAX_EXEMPT_LABEL;
                
            } else {
                $taxCustomerGroup = self::CUSTOMER_GROUP_GENERAL_LABEL;
                
            }
            $customerGroupId = $customerAssign->getGroupId();

            $groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')
                        ->toOptionArray();
            foreach ($groupOptions as $group) {
                if ($group['label'] == self::CUSTOMER_GROUP_GENERAL_LABEL || $group['label'] == self::CUSTOMER_GROUP_TAX_EXEMPT_LABEL) {
                    if ($group['value'] == $customerGroupId) {
                        $applyChanges = true;
                    }
                }
                if ($group['label'] == $taxCustomerGroup) {
                    $group_id = $group['value'];
                }
            }
            
            if ($applyChanges) {
                $customerAssign->setCustomAttribute('customer_tax_exempt_status', (int)$salesOrder['isTaxExempt']);
                $customerAssign->setGroupId($group_id);
                $customerRepository->save($customerAssign);

            }

            $cart->setCurrency();
            $cart->assignCustomer($customerAssign);

            $emails[] = ['sf_order_number' =>$salesOrder['order_id'], 'email' => $customerAssign->getEmail()];
            
            
            $sub_total = 0;
            $totalTaxAmount = 0;
            $totalDiscountAmount = 0;
            $nonFilterItems = $filteredItems;
            $filteredItems = array_values($filteredItems);

            foreach ($filteredItems as $item){
                try {
                    if (isset ($item['part_number'])) {
                        $product = $this->_ProductFactory->create()->loadByAttribute('part_number', $item['part_number']);

                    } else if (isset($item['sku'])) {
                        $product = $this->_ProductFactory->create()->loadByAttribute('sku', $item['sku']);

                    } else if (isset($item['isbn'])) {
                        $product = $this->_ProductFactory->create()->loadByAttribute('isbn', $item['isbn']);
 
                    }

                    if (!($product)) {
                        if (isset($item['sku'])) {
                            $output[0]['fail'][] = $item['sku'];

                        } else if(isset($item['isbn'])) {
                            $output[0]['fail'][] = $item['isbn'];
                            
                        }
                        continue;
                    }

                    $product->setPrice($item['price']);
                    $sub_total += $product->getPrice() * $item['qty'];
                    $totalTaxAmount += $item['tax_amount'];
                    $totalDiscountAmount += $item['discount_amount'];
                    $stockItem = $this->_stockRegistry->getStockItemBySku($product->getSku());
                    $maxSaleQty = $stockItem->getMaxSaleQty();
                    $maxSaleQtyConfig = $stockItem->getUseConfigMaxSaleQty();

                    $stockItem->setMaxSaleQty(999999);
                    $stockItem->setUseConfigMaxSaleQty(0);
                    $stockItem->save();

                    $cart->addProduct($product, intval($item['qty']));
                    
                    if ($publicationTitle == '') {
                        $productId = $product->getId();
                        $productSelection = $objectManager->create('Magento\Bundle\Model\Selection')->load($productId, 'product_id');
                        $bundleProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($productSelection->getParentProductId());
                        $publicationTitle = $bundleProduct->getName();
                        
                    }
                    
                    $stockItem->setUseConfigMaxSaleQty($maxSaleQtyConfig);
                    $stockItem->setMaxSaleQty($maxSaleQty);
                    $stockItem->save();

                    if (isset($item['sku'])) {
                        $output[0]['success'][] = 'SF Order#'.$salesOrder['order_id'].': '.$item['sku'];

                    } else if (isset($item['isbn'])) {
                        $output[0]['success'][] = 'SF Order#'.$salesOrder['order_id'].': '.$item['isbn'];

                    }

                    // check if order contains Virtual products.
                    if ($product->getTypeId() == \Endeavor\CustomProduct\Model\Product\Type::TYPE_COMBO
                            || $product->getTypeId() == \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE) {
                        $isContainsEbooks = true;
                    }
                    
                } catch (\Exception $e) {
                    if (isset($item['sku'])) {
                        $output[0]['fail'][] = $item['sku'];

                    } else if(isset($item['isbn'])){
                        $output[0]['fail'][] = $item['isbn'];

                    }
                    $output[0]['message'][] = $e->getMessage();
                    $this->_logger->crit('Create Sales Order API - Exception: '.$e->getMessage(),$output[0]['fail']);
                    $this->_logger->crit('Create Sales Order API - Exception Trace: '. $e->getTraceAsString());
                }
            }

            try {
                
                // check if customer have same address depending on b360_address_id that saved in address as attribute.
                $addNewCustomerAddress = true;
                $customerAddressRepository = $objectManager->get('Magento\Customer\Api\AddressRepositoryInterface');
                if (isset($salesOrder['billing_address']['b360_address_id'])) {
                    foreach ($customerAssign->getAddresses() as $addresses) {
                        $customerAddressRep = $customerAddressRepository->getById($addresses->getId());
                        $customerAddressAttribute = $customerAddressRep->getCustomAttribute('b360_address_id');
                        if (isset($customerAddressAttribute)) {
                            $customerAddressAttributeValue = $customerAddressAttribute->getValue();
                            if ($customerAddressAttributeValue == $salesOrder['billing_address']['b360_address_id']) {
                                $salesOrder['billing_address']['save_in_address_book'] = 0;
                                $addNewCustomerAddress = false;
                                break;
                            }
                        }
                    }
                }
                $orderShippingRegion = $objectManager->create('Magento\Directory\Model\Region')
                    ->load($salesOrder['shipping_address']['region'], 'code');

                $orderBillingRegion = $objectManager->create('Magento\Directory\Model\Region')
                    ->load($salesOrder['billing_address']['region'], 'code');

                
                $cart->getBillingAddress()->addData($salesOrder['billing_address']);
                $cart->getBillingAddress()->setRegionId($orderBillingRegion->getId());
                $cart->getShippingAddress()->addData($salesOrder['shipping_address']);
                $cart->getShippingAddress()->setRegionId($orderShippingRegion->getId());
                
                $shippingStreet = [];
                $shippingStreet[] = $salesOrder['shipping_address']['address1'] != null ? $salesOrder['shipping_address']['address1'] : '';
                $shippingStreet[] = $salesOrder['shipping_address']['address2'] != null ? $salesOrder['shipping_address']['address2'] : '';

                $cart->getShippingAddress()->setStreet($shippingStreet);

                $billingStreet = [];
                $billingStreet[] = $salesOrder['billing_address']['address1'] != null ? $salesOrder['billing_address']['address1'] : '';
                $billingStreet[] = $salesOrder['billing_address']['address2'] != null ? $salesOrder['billing_address']['address2'] : '';

                $cart->getBillingAddress()->setStreet($billingStreet);

                foreach ($cart->getItems() as $quoteItem) {
                    $output[0]['fail'][] = $quoteItem->getData();

                    if ($quoteItem->getProduct()->isVirtual() || !$quoteItem->getParentItemId()) {
                        continue;
                    }
                }

                $shippingMethod = '';
                if ($salesOrder['shipping_method_code'] == self::SHIPPING_METHOD_NEXT_DAY_WH_ID) {
                    $shippingMethod = self::SHIPPING_METHOD_NEXT_DAY;
                    
                } else if ($salesOrder['shipping_method_code'] == self::SHIPPNG_METHOD_NEXT_2_DAY_WH_ID) {
                    $shippingMethod = self::SHIPPNG_METHOD_NEXT_2_DAY;
                    
                } else if ($salesOrder['shipping_method_code'] == self::SHIPPING_METHOD_UPS_WH_ID) {
                    $shippingMethod = self::SHIPPNG_METHOD_UPS;
                    
                } else {
                    $shippingMethod = self::SHIPPNG_METHOD_STANDARD;
                    
                }

                $shippingRateCollection = $this->MatrixRateCollectionFactory->create();
                $shippingRateCollection->addFieldToFilter('shipping_method',['eq' => $shippingMethod])
                        ->addFieldToFilter('dest_region_id',['eq' => $orderShippingRegion->getId()])
                        ->addFieldToFilter('condition_to_value',['gteq' => $sub_total])
                        ->addFieldToFilter('condition_from_value',['lteq' => $sub_total]);

                $pk = '';
                if ($shippingRateCollection->getSize()) {
                    $shippingRateItem = $shippingRateCollection->getFirstItem();
                    $pk = $shippingRateItem->getPk();

                } else {
                    $shippingRateCollection = $this->MatrixRateCollectionFactory->create();
                    $shippingRateCollection->addFieldToFilter('shipping_method',['eq' => $shippingMethod])
                            ->addFieldToFilter('dest_region_id',['eq' => '0'])
                            ->addFieldToFilter('condition_to_value',['gteq' => $sub_total])
                            ->addFieldToFilter('condition_from_value',['lteq' => $sub_total]);
                    if($shippingRateCollection->getSize()){
                        $shippingRateItem = $shippingRateCollection->getFirstItem();
                        $pk = $shippingRateItem->getPk();
                    }
                }
                $this->shippingRate->setCode('matrixrate_matrixrate_' . $pk);

                $shippingAddress = $cart->getShippingAddress();
                
                $shippingAddress->setCollectShippingRates(true)->collectShippingRates()
                        ->setShippingMethod('matrixrate_matrixrate_' . $pk);
                $cart->getShippingAddress()->addShippingRate($this->shippingRate);
                
                $cart->setPaymentMethod('checkmo');
                $cart->setInventoryProcessed(false);
                $cart->getPayment()->importData(['method' => 'checkmo']);
                $cart->collectTotals();

                $orderCollection = $this->_orderCollectionFactory->create();
                $orderCollection->addFieldToSelect(['sf_order_id','source_order','increment_id'])
                                ->addFieldToFilter('sf_order_id',['eq' => $salesOrder['order_id']])
                                ->addFieldToFilter('source_order',['eq' => 'SF']);
                if ($orderCollection->getSize()) {
                    $output[0]['message'][] = 'Order already created. SF Order#:'
                            .$salesOrder['order_id'].', MagentoId#:' .$orderCollection->getFirstItem()->getIncrementId();
                    return $output;
                }
                
                $cart->save();
                $cartId = $cart->getId();
                
                $cart = $this->cartRepositoryInterface->get($cartId);
                $order_id = $this->cartManagementInterface->placeOrder($cartId);
                $tempSaleOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($order_id);
                $tempOrderItems = $tempSaleOrder->getAllItems();
                
                // handle saveing new customer address with b360addressId attribute.
                $customerLastBillAddrId =  $tempSaleOrder->getBillingAddress()->getCustomerAddressId();
                if (isset($salesOrder['billing_address']['b360_address_id'])) {
                    if ($addNewCustomerAddress && !empty($customerLastBillAddrId)) {
                        $customerAddressRepository = $objectManager->get('Magento\Customer\Api\AddressRepositoryInterface');
                        $customerAddressRep = $customerAddressRepository->getById($customerLastBillAddrId);
                        $customerAddressRep->setCustomAttribute('b360_address_id', $salesOrder['billing_address']['b360_address_id']);
                        $customerAddressRepository->save($customerAddressRep);
                    }
                }
                
                $increment_id = 0;

                foreach ($tempOrderItems as $orderItem) {
                    if ($nonFilterItems[$orderItem->getSku()]['is_back_order']) {
                        $itemQty = $nonFilterItems[$orderItem->getSku()]['qty'];
                        $orderItem->setQtyBackordered($itemQty);
                        $purchasedItem = $this->_purchasedItemFactory->create()
                                ->getCollection()
                                ->addFieldToFilter('order_item_id',['eq' => $orderItem->getId()]);
                        foreach ($purchasedItem as $purchItem) {
                            $purchItem->setStatus('pending');
                            $purchItem->save();

                        }
                    } else {
                        $purchasedItem = $this->_purchasedItemFactory->create()
                                ->getCollection()
                                ->addFieldToFilter('order_item_id',['eq' => $orderItem->getId()]);
                        foreach ($purchasedItem as $purchItem) {
                            $purchItem->setStatus('available');
                            $purchItem->save();

                        }

                    }
                    $increment_id = $increment_id + 1;
                    $orderItem->setCustomIncrementId(str_pad($increment_id,6,'0',STR_PAD_LEFT));
                    $orderItem->save();

                }

                $salesOrderModel = $objectManager->create('Magento\Sales\Model\Order');
                $order = $salesOrderModel->load($order_id);
                $order->setSourceOrder('SF');
                $order->setSfOrderId($salesOrder['order_id']);
                $order->setSfExportedStatus('0');
                
                $shippingPrice = $salesOrder['shipping_amount'];
                $order->setShippingAmount($shippingPrice);
                $order->setBaseShippingAmount($shippingPrice);
                $totalTaxAmount = round($totalTaxAmount, 2);
                $totalDiscountAmount = round($totalDiscountAmount, 2);
                //$order->setGrandTotal($order->getGrandTotal() + $shippingPrice);
                //$order->setBaseGrandTotal($order->getBaseGrandTotal() + $shippingPrice);
				
				$order->setTaxAmount($totalTaxAmount);
				$order->setBaseTaxAmount($totalTaxAmount);
				$order->setDiscountAmount($totalDiscountAmount);
				$order->setBaseDiscountAmount($totalDiscountAmount);
				
                $order->setGrandTotal(($order->getGrandTotal() + $shippingPrice + $totalTaxAmount) - $totalDiscountAmount);
                $order->setBaseGrandTotal(($order->getBaseGrandTotal() + $shippingPrice + $totalTaxAmount) - $totalDiscountAmount);
				
                
                if ($salesOrder['shipping_method_code'] == self::SHIPPING_METHOD_NEXT_DAY_WH_ID) {
                    $order->setShippingDescription(self::SHIPPING_METHOD_NEXT_DAY_DESCRIPTION);
                    
                } else if ($salesOrder['shipping_method_code'] == self::SHIPPNG_METHOD_NEXT_2_DAY_WH_ID) {
                    $order->setShippingDescription(self::SHIPPNG_METHOD_NEXT_2_DAY_DESCRIPTION);
                    
                } else if ($salesOrder['shipping_method_code'] == self::SHIPPING_METHOD_UPS_WH_ID) {
                    $order->setShippingDescription(self::SHIPPNG_METHOD_UPS_DESCRIPTION);
                    
                } else {
                    $order->setShippingDescription(self::SHIPPNG_METHOD_STANDARD_DESCRIPTION);
                    
                }
                
                if ($salesOrder['status'] === 'Shipped' || $salesOrder['status'] === 'Partial Shipped') {
                    $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
                    $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
                }
                if (isset($salesOrder['created_at'])) {
                    $order->setCreatedAt(date('Y-m-d H:i:s', strtotime($salesOrder['created_at'])));
                }
                $order->save();

                /* Receiver Detail  */
                $receiverInfo = [
                    'name' => $customer->getFirstname().' '.$customer->getLastname(),
                    'email' => $customerEmail
                ];

                $emailData = [];
                $emailData['customer'] = $customer;
                $emailData['is_contains_ebooks'] = $isContainsEbooks;
                $emailData['publication_title'] = $publicationTitle;
                $emailData['customer_name'] = $customer->getFirstname().' '.$customer->getLastname();
                $emailData['order'] = $order;
                $sendEmail = true;
                if (isset($salesOrder['send_email'])) {
                    $sendEmail = $salesOrder['send_email'];
                    
                }
                if (!empty($customerEmail) && $sendEmail) {
                    $isSent = $this->sendSalesOrderApiMailMessage($emailData, $receiverInfo, $salesOrder['status']);
                    if ($isSent) {
                        $order->setEmailSent(1);
                        $order->setSendEmail(1);
                        $order->save();
                    }
                }

            } catch (\Exception $e) {
                $this->_logger->crit('Create Sales Order API - Exception: '.$e->getMessage(), $output[0]['fail']);
                $this->_logger->crit('Create Sales Order API - Exception Trace: '. $e->getTraceAsString());
                $output[0]['message'][] = $e->getMessage();

            }
            if (isset($order_id)) {
                if (empty($output[0]['fail'])) {
                    $output[0]['message'][] = 'SF Order#'.$salesOrder['order_id'].': '.'Create Order successfully completed.';
                    $this->_logger->info('Create Sales Order API - Create Order successfully completed.');

                } else {
                    $output[0]['message'][] =  'SF Order#'.$salesOrder['order_id'].': '.'Create Order successfully with some missing product.';
                    $this->_logger->crit('Create Sales Order API - Create Order successfully with some missing product.');

                }
            }
        }
        $output[0]['emails'] = $emails;
        $this->_logger->info('Create Sales Order API - Response:',$output);
        $this->_logger->info('Create Sales Order API - End'.PHP_EOL);
        return $output;
    }
    
    /**
     * @author: Murad Dweikat
     * @desc: change sales order exported status. 
     * 
     * @param mix $order
     * @return \Endeavor\SFIntegration\Model\Result[]
     */
    public function putOrderStatus($order)
    {
        $this->_logger->info('Sales Order Export Status API - Start');
        $this->_logger->info('Sales Order Export Status API - Request: ', $order);
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $successIds =[];
        $faildedIds =[];
        $output =[];
        foreach ($order as $salesOrder) {
            $result = $objectManager->create('Endeavor\SFIntegration\Model\Result');
            try {
                $salesOrderModel = $objectManager->create('Magento\Sales\Model\Order')->load($salesOrder['order_id'],'increment_id');
                if ($salesOrderModel->getStatus() == $salesOrder['eCommerce_status']) {
                    if ($salesOrder['is_exported']) {
                        $salesOrderModel->setSfExportedStatus('1');

                    } else {
                        $salesOrderModel->setSfExportedStatus('0');

                    }
                    $salesOrderModel->save();
                    $successIds[] = $salesOrder['order_id'];
                }
            } catch (\Exception $e) {
                $faildedIds[] = $salesOrder['order_id'];
                $this->_logger->crit('Sales Order Export Status API - Exception: Order#'.$salesOrder['order_id'].': '.$e->getMessage());
                $this->_logger->crit('Sales Order Export Status API - Exception Trace: '.$e->getTraceAsString());

                $result->setMsg($e->getMessage());
                
            }
            $output[] = $result;
        }

        $result->setSuccess($successIds);
        $result->setFail($faildedIds);
        if (empty($faildedIds)) {
            $this->_logger->info('Sales Order Export Status API - Response: All The orders exported successfully.');
            $result->setMsg('All The orders exported successfully.');
            
        } else {
            $this->_logger->info('Sales Order Export Status API - Response: The follwing orders faild: ', $faildedIds);
            
        }
        
        $this->_logger->info('Sales Order Export Status API - End'.PHP_EOL);
        return $output;

    }
    
    /**
     * @author: Murad Dweikat
     * @updateAt: 10/11/2017
     * @desc: Cancel eCommerce and SF order, and add cancel reason in order note.
     * 
     * 
     * @param mixed $order
     * @return \Endeavor\SFIntegration\Model\Result[]
     * 
     */
    public function putOrderCancel($order) {
        
        $this->_logger->info('Sales Order Cancel API - Start');
        $this->_logger->info('Sales Order Cancel API - Request: ', $order);
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $output = [];
        foreach ($order as $requestItem) {
            $result = $objectManager->create('Endeavor\SFIntegration\Model\Result');
            $_salesOrder = $objectManager->create('Magento\Sales\Model\Order');
            $salesOrderModel = null;
            $isEcommerceOrder = false;
            $msg = '';
            $result->setFail('');
            $result->setSuccess('');
            $result->setMsg('');
            try {
                if (isset($requestItem['order_number']) && $requestItem['order_number'] != null) {
                    $salesOrderModel = $_salesOrder->load($requestItem['order_number'],'sf_order_id');
                    
                }

                if (!$salesOrderModel->getId() && isset($requestItem['ecom_order_number']) 
                        && $requestItem['ecom_order_number'] != null) {
                    $salesOrderModel = $_salesOrder->load($requestItem['ecom_order_number'],'increment_id');
                    $isEcommerceOrder = true;
                    
                }

                if ($salesOrderModel->getId()) {
                    $orderModel = $this->orderRepository->get($salesOrderModel->getId());
                    if ((bool)$orderModel->cancel()) {
                        $orderItems = $salesOrderModel->getAllItems();
                        foreach ($orderItems as $orderItem) {
                            if ($orderItem->getProductType() == \Endeavor\CustomProduct\Model\Product\Type::TYPE_COMBO
                                    || $orderItem->getProductType() == \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE) {
                                $collectionPurchasedItems = $objectManager->create('Magento\Downloadable\Model\Link\Purchased\Item')
                                        ->getCollection();
                                $collectionPurchasedItems->addFieldToFilter('order_item_id',['eq' => $orderItem->getId()]);
                                foreach($collectionPurchasedItems->getItems() as $purchasedItem) {
                                    if($purchasedItem->getNumberOfDownloadsUsed() != 0) {
                                        $msg .= 'PartNumber: '.$orderItem->getSku().', Quantity: '.(int)$orderItem->getQtyOrdered();
                                        $msg .= ', Price: '.$orderItem->getPrice().' <br>';
                                        
                                    }
                                }
                            }
                        }
                        if ($isEcommerceOrder) {
                            $orderModel->setSfOrderId($requestItem['order_number']); 
                            
                        }
                        $statusHistory = $objectManager->create('Magento\Sales\Api\Data\OrderStatusHistoryInterface');

                        $comment = '<strong>Reason: </strong>'.$requestItem['cancel_reason'].'<br><strong>Details: </strong>'.$requestItem['cancel_reason_details']
                                    .'<br><strong>Code: </strong>'.$requestItem['cancel_code'];
                        $statusHistory->setComment($comment);
                        $statusHistory->setStatus(\Magento\Sales\Model\Order::STATE_CANCELED);
                        $statusHistory->setIsVisibleOnFront(1);
                        $statusHistory->setIsCustomerNotified(0);
                        $statusHistory->setEntityName('order');
                        $statusHistory->setParentId($orderModel->getId());
                        $statusHistory->save();

                        $orderModel->addStatusHistory($statusHistory);
                        $this->orderRepository->save($orderModel);
                        $notify = isset($requestItem['is_customer_notified']) ? $requestItem['is_customer_notified'] : false;
                        $comment = trim(strip_tags($statusHistory->getComment()));
                        $this->orderCommentSender->send($orderModel, $notify, $comment);
                        
                        $result->setMsg($msg);
                        $result->setSuccess($orderModel->getSfOrderId().'|'.$orderModel->getIncrementId());
                        
                        // Remove canceled pre-order record from wk_preorder_items table.
                        $preOrderCollection = $objectManager->create('Webkul\Preorder\Model\Preorder')
                                ->getCollection()
                                ->addFieldToFilter('order_id',['eq' => $salesOrderModel->getId()]);
                        foreach ($preOrderCollection as $preOrder) {
                            $preOrder->delete();
                        }
                        
                    } else {
                        $this->_logger->crit('Sales Order Cancel API - order# '.$requestItem['order_number']);
                        $result->setMsg('can\'t Cancel Order #'.$requestItem['order_number']);
                        $result->setFail($requestItem['order_number'].'|'.$requestItem['ecom_order_number']);
                        
                    }
                    
                } else {
                    $this->_logger->crit('Sales Order Cancel API - SF Order# '.$requestItem['order_number']
                            .'|Order#'.$salesOrderModel->getIncrementId().' not found.');
                    $result->setMsg('Order #'.$requestItem['order_number'].' not found.');
                    $result->setFail($requestItem['order_number'].'|'.$requestItem['ecom_order_number']);
                    
                }
            
            } catch (\Exception $ex) {
                $this->_logger->crit('Sales Order Cancel API - Exception: SF Order#'.$requestItem['order_number']
                        .', Order#'.$requestItem['ecom_order_number'].$ex->getMessage());
                $this->_logger->crit('Sales Order Cancel API - Exception Trace: '. $ex->getTraceAsString());

                $result->setMsg($ex->getMessage());
                $result->setFail($requestItem['order_number'].'|'.$requestItem['ecom_order_number']);
                
            }

            $output[] = $result;
        }

        $this->_logger->info('Sales Order Cancel API - Response: ', $output);
        $this->_logger->info('Sales Order Cancel API - End'.PHP_EOL);

        return $output;
            
    }
    
    /**
     * Generate dummy email.
     * 
     * @return string
     */
    public function generateDummyEmail($name){
//        SRAND ((double) MICROTIME() * 1000000);
        $nextmail = "dummy";
//        $nextmail .= RAND();
        $nextmail .= $name;
        $nextmail .= "@blue360media.com";
        return $nextmail;
    }

    /**
     * Send Order Confirmation email. 
     *
     * @param type $emailData
     * @param type $receiverInfo
     * @param type $emailTemplate
     * @param type $sendStatus
     * @return type
     */
    public function sendSalesOrderApiMailMessage($emailData,$receiverInfo, $emailTemplate)
    {
        //Get Object Manager Instance
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        /* call send mail method from helper or where you define it*/ 
        return $objectManager->get('Endeavor\SFIntegration\Helper\Email')->salesOrderApiSendMailMessage(
                $emailData,
                $receiverInfo,
                $emailTemplate
            );
    }
    
    /**
     * Send Error Email.
     * 
     * @param type $orderSuccessRespone
     * @param type $orderFailRespone
     * @param type $sqlResualt
     */
    private function sendErrorEmailMessage($subject, $message) {
        
        $emailData = [];
        $emailData['subject'] = $subject;
        $emailData['message'] = $message;
        
        // call send mail method from helper  
        $emailHelper = $this->_objectManager->create('Endeavor\PaymentComplete\Helper\Email');
                
        $emailHelper->sendErrorEmailMessage(
            $emailData
        );
    }

}
