<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 27-12-2017.
 * @desc eBook access for customers with those features:
 *          1. Expired: to increase downloads for items which called in API.
 *          2. MergeAccount: under execute function to merge accounts which called in API.
 *          3. Allow Assign: to change customer role for order which called in API.
 * 
 * @update_by: Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 27-12-2017.
 * @desc 1. in order to response for orders which doesn't found in e-commerce.
 *       2. update to record history and update status for eBooks.
 */

namespace Endeavor\SFIntegration\Model\Api;

use Magento\Framework\Math\Random;
use Magento\Customer\Api\CustomerRepositoryInterface;

class EbookAccess implements \Endeavor\SFIntegration\Api\EbookAccessInterface
{
    const DOWNLOADS_LIMIT = 5;
    const MORE_EBOOK = 'more_ebook';
    const STATUS = 'status';
    /**
     *
     * @var Magento\CatalogInventory\Model\StockRegistry 
     */
    protected $_stockRegistry;
    
    /**
     * @var Random
     */
    private $mathRandom;

    /**
     *
     * @var Endeavor\SFIntegration\Model\Result 
     */
    protected $result;
    
    /**
     *
     * @var Magento\Catalog\Model\Product 
     */
    protected $product;

    /**
     * Logging instance
     * @var \Endeavor\SFIntegration\Logger\Logger
     */
    protected $_logger;
    
    /**
     * @var \Magento\Downloadable\Model\Link\PurchasedFactory
     */
    protected $purchasedLinkFactory;
    
    /**
     * Customer Object
     *
     * @var \Magento\Customer\Model\CustomerFactory 
     */
    protected $_customerFactory;
    
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;
    
    /**
     * 
     * @param \Magento\CatalogInventory\Model\StockRegistry $stock
     * @param \Magento\Catalog\Model\Product $product
     * @param \Endeavor\SFIntegration\Model\Result $result
     * @param \Endeavor\SFIntegration\Logger\Logger $logger
     */
    public function __construct(
        \Magento\CatalogInventory\Model\StockRegistry $stock,
        \Magento\Catalog\Model\Product $product,
        \Endeavor\SFIntegration\Model\Result $result,
        \Magento\Downloadable\Model\Link\PurchasedFactory $PurchasedLinkFactory,
        \Magento\Customer\Model\CustomerFactory $CustomerFactory,
        CustomerRepositoryInterface $customerRepository,
        \Endeavor\SFIntegration\Logger\Logger $logger,
        Random $mathRandom
    ) {
        $this->_logger = $logger;
        $this->_customerFactory = $CustomerFactory;
        $this->_purchasedLinkFactory = $PurchasedLinkFactory;
        $this->customerRepository = $customerRepository;
        $this->_stockRegistry = $stock;
        $this->result = $result;
        $this->product = $product;
        $this->mathRandom = $mathRandom;
    }
    
    /**
     * {@inheritdoc}
     * Sample Post Data:
     * add more: {"sfOrderNumber":"180059753","action":"more_ebook","items":[{"part_number":"32348EP01","downloads":"5"}]}
     * change status: {"sfOrderNumber":"180059753","action":"status","items":[{"part_number":"32348EP01","downloads":""}]}
     * 
     * Sample Return Data:
     * 
     * Fail1: [{"status": false,"message": "order not found"}]
     * Fail2: [{"status": false,"message": "Item not found in order."}]
     * Fail3: [{"status": false,"message": "you exceeded the limit"}]
     * Fail4: [{"status": false,"message": "Please enter positive number"}]
     * Fail5: [{"status": false,"message": "Item not found in order."}]
     * Fail6: [{"status": false,"message": "/Exception"}]
     * 
     * success: [{"status": success,"message": "all items is updated."}]
     * 
     * 
     */
    public function expired(
        $sf_order_number,
        $ecom_order_number,
        $action,
        $items
    ) {
        $this->_logger->info('POST expired API - Start');
        $request = ['sf_order_number' => $sf_order_number, 'action' => $action, 'items' => $items];
        $this->_logger->info('POST expired API - Request', $request);
        
        try {
            $sfOrderNumber = $sf_order_number;
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $moveOrderToHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
            $orderId = $moveOrderToHelper->getOrderIdFromSFOrderId($sfOrderNumber, $ecom_order_number);
            $response = null;
            if (empty($orderId)) {
                $response [] = ['status' => false, 'message' => 'order not found'];
                
                $this->_logger->info('POST expired API - Response:', $response);
                $this->_logger->info('POST expired API - End'.PHP_EOL);
                
            } else {
            
                foreach ($items as $item) {
                    if ($action == self::STATUS) {
                        $downloads = 0;
                        $partNumber = $item['part_number'];
                        $purchasedIds = $this->getPurchasedIdForItem($orderId, $partNumber);
                        if (empty($purchasedIds)) {

                            $response [] = ['status' => false, 'message' => 'Items not found in order.'];

                            $this->_logger->info('POST expired API - Response:', $response);
                            $this->_logger->info('POST expired API - End'.PHP_EOL);
                            break;
                            
                        } else {
                            foreach ($purchasedIds as $purchasedId) {
                                $this->setDownloadableBought($purchasedId, $downloads, $action);
                            }
                        }
                    } else {
                        $partNumber = $item['part_number'];
                        $downloads = $item['downloads'];

                        if ($downloads > SELF::DOWNLOADS_LIMIT) {
                            $response [] = ['status' => false, 'message' => 'you exceeded the limit'];

                            $this->_logger->info('POST expired API - Response:', $response);
                            $this->_logger->info('POST expired API - End'.PHP_EOL);
                            break;
                            
                        } else {

                            if ($downloads <= 0) {
                                $response [] = ['status' => false, 'message' => 'Please enter positive number'];

                                $this->_logger->info('POST expired API - Response:', $response);
                                $this->_logger->info('POST expired API - End'.PHP_EOL);
                                break;
                                
                            } else {

                                $purchasedIds = $this->getPurchasedIdForItem($orderId, $partNumber);

                                if (empty($purchasedIds)) {
                                    $response [] = ['status' => false, 'message' => 'Items not found in order.'];

                                    $this->_logger->info('POST expired API - Response:', $response);
                                    $this->_logger->info('POST expired API - End'.PHP_EOL);
                                    break;
                                    
                                } else {
                                    
                                    foreach ($purchasedIds as $purchasedId) {
                                        $this->setDownloadableBought($purchasedId, $downloads, $action);
                                    }
                                }
                            }
                        }
                    }
                }
                if ($response == null) {
                    $response [] = ['status' => true, 'message' => 'all items is updated.'];

                    $this->_logger->info('POST expired API - Response:', $response);
                    $this->_logger->info('POST expired API - End'.PHP_EOL);
                }
            }
            
            return $response;
            
        } catch (\Exception $e) {
            $response = [];
            $response [] = ['status' => false, 'message' => $e->getMessage()];
            
            $this->_logger->crit('POST expired API - Exception: sfOrder#'.$sf_order_number.': '.$e->getMessage());
            $this->_logger->info('POST expired API - Response:', $response);
            $this->_logger->info('POST expired API - End'.PHP_EOL);
            
            return $response;
        }
        
    }

    /*
     * @desc set Number of Downloads Bought.
     * @param string $purchasedId
     * @param string $downloads
     */
    protected function setDownloadableBought($purchasedId, $downloads, $action = null) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $purchasedLinkItem = $objectManager->get('\Magento\Downloadable\Model\Link\Purchased\ItemFactory');
        $purchasedItems = $purchasedLinkItem->create()->getCollection()->addFieldToFilter('purchased_id', $purchasedId);
        
        if ($purchasedItems) {
            foreach ($purchasedItems as $purchasedItem) {
                if ($action == self::MORE_EBOOK || $action == null) {
                    $boughts = $purchasedItem->getNumberOfDownloadsBought();
                    $downloadsToSet = $boughts + $downloads;
                    $purchasedItem->setNumberOfDownloadsBought($downloadsToSet);
                    $purchasedItem->setStatus('available');
                    $purchasedItem->save();
                }
                if ($action == self::STATUS) {
                    $purchasedItem->setStatus('available');
                    $purchasedItem->save();
                }
                
            }
        }
    }

    /*
     * @desc get Purchased Id For Item.
     * @param string $orderId
     * @param string $partNumber
     * 
     * @return string $purchasedId
     */
    protected function getPurchasedIdForItem($orderId, $partNumber) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $purchaseLink = $objectManager->get('\Magento\Downloadable\Model\Link\PurchasedFactory');
        $downloadablePurchasedLinks = $purchaseLink->create()->getCollection()
                ->addFieldToFilter('order_id', $orderId)
                ->addFieldToFilter('product_sku', $partNumber)
                ->getFirstItem();
        
        $purchasedIds = [];
        
        if ($downloadablePurchasedLinks) {
            $purchasedIds [] = $downloadablePurchasedLinks->getPurchasedId();
        }
        return $purchasedIds;
    }

    /**
     * {@inheritdoc}
     * Sample Post Data:
     * {"sfOrderNumber":"170057382","userName":"admin","customerData":{"email":"mo7ammad7am2016@gmail.com"}}
     * 
     * Sample Return Data:
     * Fails:
     * Fail1: [{"status": false,"message": "order is already assigned under same customer"}]
     * Fail2: [{"status": false,"message": "Order not found."}]
     * Fail3: [{"status": false,"message": "Order not found."}]
     * Fail4: [{"status": false,"message": "/Exception"}]
     * 
     * Success:
     * success1: [{"status": false,"message": "customer is moved and old customer:" . $oldCustomerId . " is inActivated."}]
     * success1: [{"status": false,"message": "customer email is changed."}]
     * 
     */
    public function execute(
        $sfOrderNumber,
        $ecomOrderNumber,
        $mergeAllOrders,
        $userName,
        $customerData
    ) {
        
        $this->_logger->info('POST mergeAccounts execute API - Start');
        $request = ['sfOrderNumber' => $sfOrderNumber, 'userName' => $userName, 'customerData' => $customerData, 'mergeAllOrders' => $mergeAllOrders];
        $this->_logger->info('POST mergeAccounts execute API - Request', $request);
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $moveOrderToHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');

            $newCustomerEmail = $customerData['email'];
            $orderId = $moveOrderToHelper->getOrderIdFromSFOrderId($sfOrderNumber, $ecomOrderNumber);

            if (empty($orderId)) {
                $response [] = ['status' => false, 'message' => 'Order not found.'];
                
            } else {
                
                $customerExistInMagento = $this->isCustomerExist($newCustomerEmail);
                
                // get customer data for new customer
                $newCustomerData = $moveOrderToHelper->getCustomerData($newCustomerEmail);
                $customerId = $newCustomerData['customerid'];

                $customerAssignedInOrder = $this->isCustomerExistInOrder($orderId, $customerId);
                if (!$customerAssignedInOrder) {
                    if ($customerExistInMagento) {
                        if ($mergeAllOrders) {
                            // get attributes from customer in current order
                            $oldCustomerId = $moveOrderToHelper->getCustomerIdFromOrder($orderId);
                            $oldCustomerAttributes = $moveOrderToHelper->getCustomerAttributes($oldCustomerId);

                            // move attributes to new customer
                            $moveOrderToHelper->setCustomerAttributes($customerId, $oldCustomerAttributes);

                            $moveOrderToHelper->setCustomerToAllOrders($oldCustomerId, $newCustomerData, $newCustomerEmail, $userName);

                            $this->inactiveOldCustomer($oldCustomerId);

                            $response = [];
                            $response [] = ['status' => true, 'message' => "customer is moved to all orders and old customer is inactivated."];
                            
                        } else {
                            
                            $oldCustomerId = $moveOrderToHelper->getCustomerIdFromOrder($orderId);
                            
                            $moveOrderToHelper->setCustomerToCurrentOrder($oldCustomerId, $newCustomerData, $newCustomerEmail, $orderId, $userName);
                            
                            $response = [];
                            $response [] = ['status' => true, 'message' => "current order is moved successfully."];
                            
                        }

                    } else {
                        if ($mergeAllOrders) {
                            $moveOrderToHelper->changeOldCustomerEmail($newCustomerEmail, $orderId, $userName);
                            $response = [];
                            $response [] = ['status' => true, 'message' => "customer email is changed."];
                            
                        } else {
                            $newCustomerName = $this->getCustomerNameFromOrder($orderId);
                            
                            $oldCustomerId = $moveOrderToHelper->getCustomerIdFromOrder($orderId);
                            
                            $customerId = $this->createNewCustomer($newCustomerEmail, $newCustomerName);
                            
                            $newCustomerData = $moveOrderToHelper->getCustomerData($newCustomerEmail);
                            
                            $moveOrderToHelper->setCustomerToCurrentOrder($oldCustomerId, $newCustomerData, $newCustomerEmail, $orderId, $userName);
                            
                            $moveOrderToHelper->sendWelcomeMessage($customerId);
                            
                            $response = [];
                            $response [] = ['status' => true, 'message' => "new customer is created and order is moved successfully."];
                            
                        }
                        
                        $this->sendResetPassword($newCustomerEmail);
                        
                    }
                } else {
                    $response [] = ['status' => false, 'message' => 'order is already assigned under same customer'];

                }
            }
            
        } catch (\Exception $e) {
            $response = [];
            $response [] = ['status' => false, 'message' => $e->getMessage()];
            
        }
        
        $this->_logger->info('POST mergeAccounts execute API - Response:', $response);
        $this->_logger->info('POST mergeAccounts execute API - End'.PHP_EOL);
        
        return $response;
    }
    
    /**
     * @param string $orderId
     * @return array $customerName
     */
    protected function getCustomerNameFromOrder($orderId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
        
        $customerFirstname = $salesOrder->getCustomerFirstname();
        $customerLastname = $salesOrder->getCustomerLastname();
        
        $customerName = ['firstname' => $customerFirstname, 'lastname' => $customerLastname];
        
        return $customerName;
    }
    
    /**
     * check if customer exist or not.
     * @param String $newCustomerEmail
     * 
     * @return boolean
     */
    protected function isCustomerExist($newCustomerEmail) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $moveOrderToHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
        $oldCustomer = $moveOrderToHelper->getOldCustomerObject($newCustomerEmail);
        $customerFound = true;
        if (!$oldCustomer->getId()) {
            $customerFound = false;
        }
        return $customerFound;
    }

    /**
     * check if Customer Exist In Order or not.
     * @param String $orderId
     * @param String $customerId
     * 
     * @return boolean
     */
    protected function isCustomerExistInOrder($orderId, $customerId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
        $orderCustomerId = $salesOrder->getCustomerId();
        $result = false;
        if ($orderCustomerId == $customerId) {
            $result = true;
        }
        
        return $result;
    }
    
    /**
     * send Reset Password email.
     * @param String $newCustomerEmail
     * 
     */
    protected function sendResetPassword($newCustomerEmail) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $moveOrderToHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
        $customer = $moveOrderToHelper->getOldCustomerObject($newCustomerEmail);

        $sendResetPasswordHelper = $objectManager->get('Endeavor\SFIntegration\Helper\ResetPassword');
        $sendResetPasswordHelper->sendPasswordResetMail($customer);
    }

    /**
     * set old customer to be inActive.
     * @param String $customerId
     * 
     */
    protected function inactiveOldCustomer($customerId) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteId = $storeManager->getStore()->getWebsiteId();

        $oldCustomer = $objectManager->create('Magento\Customer\Model\Customer')
                ->setWebsiteId($websiteId)
                ->load($customerId);

        $oldCustomer->setIsActive(0);
        $oldCustomer->save();
    }

    /**
     * {@inheritdoc}
     * Sample Post Data {"sfOrderNumber":"1700125563"}
     * Sample return Data
     * onSuccess: [{"status": true,"message": "Customer rule is set"}]
     * onFail: [{"status": true,"message": "/Exception"}]
     */
    public function allowAssign($sfOrderNumber, $ecom_order_number) {
        
        $this->_logger->info('POST eBookAccess allowAssign API - Start');
        $request = ['sfOrderNumber' => $sfOrderNumber];
        $this->_logger->info('POST eBookAccess allowAssign API - Request', $request);
        
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $moveOrderToHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
            $orderId = $moveOrderToHelper->getOrderIdFromSFOrderId($sfOrderNumber, $ecom_order_number);

            if (empty($orderId)) {
                $response [] = ['status' => false, 'message' => 'Order not found.'];
                $this->_logger->info('POST eBookAccess allowAssign API - Response:', $response);
                $this->_logger->info('POST eBookAccess allowAssign API - End');
                
            } else {
                $customerId = $moveOrderToHelper->getCustomerIdFromOrder($orderId);

                $moveOrderToHelper->setCustomerRoleAttribute($customerId);

                $response [] = ['status' => true, 'message' => 'Customer rule is set'];

                $this->_logger->info('POST eBookAccess allowAssign API - Response:', $response);
                $this->_logger->info('POST eBookAccess allowAssign API - End'.PHP_EOL);
                
            }
            
            return $response;
            
        } catch (\Exception $e) {
            $response = [];
            $response [] = ['status' => false, 'message' => $e->getMessage()];
            $this->_logger->crit('POST expired API - Exception: sfOrder#'.$sfOrderNumber.': '.$e->getMessage());
            $this->_logger->info('POST eBookAccess allowAssign API - Response:', $response);
            $this->_logger->info('POST eBookAccess allowAssign API - End'.PHP_EOL);
            return $response;
        }
    }
    
    /**
     * @param string $customerEmail
     * @return string $customerId
     */
    protected function createNewCustomer($customerEmail, $customerName) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerFactory = $this->_customerFactory->create();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteId = $storeManager->getStore()->getWebsiteId();
        $newCustomer = $customerFactory->setWebsiteId($websiteId)->loadByEmail($customerEmail);
        
        $newCustomer->setEmail($customerEmail);
        $newCustomer->setFirstname($customerName['firstname']);
        $newCustomer->setLastname($customerName['lastname']);
        $newCustomer->setAddresses(null);
        $storeId = $storeManager->getWebsite($websiteId)->getDefaultStore()->getId();
        $newCustomer->setStoreId($storeId);

        $storeName = $storeManager->getStore($newCustomer->getStoreId())->getName();
        $newCustomer->setCreatedIn($storeName);
        
        $newCustomer->save();
        $customerId = $newCustomer->getEntityId();
            
        
        return $customerId;
    }
    
}