<?php

/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 27-12-2017.
 * @desc Response Order Info for order number which called in API.
 */

namespace Endeavor\SFIntegration\Model\Api;

class OrderInfo implements \Endeavor\SFIntegration\Api\OrderInfoInterface
{
    
    /**
     * Logging instance
     * @var \Endeavor\SFIntegration\Logger\Logger
     */
    protected $_logger;
    
    /**
     * @var \Magento\Downloadable\Model\Link\PurchasedFactory
     */
    protected $purchasedLinkFactory;
    
    /**
     * Downloadable Link object
     *
     * @var \Magento\Downloadable\Model\Link 
     */
    protected $_linkFactory;
    
    /**
     *
     * @var Magento\Framework\App\Request\Http 
     */
    protected $request;
    
    /**
     * Customer logger
     *
     * @var \Magento\Customer\Model\Logger
     */
    protected $customerLogger;
    
    const NUMBER_OF_DOWNLOAD_BOUGHT = 'number_of_download_bought';
    const NUMBER_OF_DOWNLOAD_USED = 'number_of_download_used';
    const NUMBER_OF_DOWNLOAD = 'number_of_download';
    const STATUS = 'status';
     
    /**
     * 
     * @param \Magento\CatalogInventory\Model\StockRegistry $stock
     * @param \Magento\Catalog\Model\Product $product
     * @param \Endeavor\SFIntegration\Model\Result $result
     */
    public function __construct(
        \Magento\Downloadable\Model\Link\PurchasedFactory $PurchasedLinkFactory,
        \Magento\Downloadable\Model\LinkFactory $LinkFactory,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Customer\Model\Logger $customerLogger,
        \Endeavor\SFIntegration\Logger\Logger $logger
    ) {
        $this->_logger = $logger;
        $this->_purchasedLinkFactory = $PurchasedLinkFactory;
        $this->_linkFactory = $LinkFactory;
        $this->request = $request;
        $this->customerLogger = $customerLogger;
    }

    /**
     * {@inheritdoc}
     * 
     * sample Get Data: http://local.blue360media.com/rest/V1/sf/order/orderinfo?sf_order_Number=17001255817
     * 
     * Sample return data:
     * 
     * [{"customer_name": "Mohammad Hamadneh","customer_email": "mhamadneh420@hotmail.com","last_login_at": "2018/01/10 11:32:45 PM",
       "order_number": "000000209","customer_Attributes": {"tax_exempt_status": "1","customer_blue_customer_number": "","customer_blue_account_id": "","customer_role": "Customer"},
       "items": [{"name": " The Maryland Vehicle Law Annotated-ePub","status": "available","part_number": "26335-maryland-9781522117117-1","downloades_bought": 5,"downloades_used": 1,"filename": "/9/7/9781641300827.jpg"}]}]
     */
    public function getOrderEbooks() {
        
        $this->_logger->info('GET getOrderEbooks API - Start');
        
        $post = $this->request->getParams();
        if (isset($post['ecom_order_number']) || isset($post['sf_order_Number'])) {
            $ecomOrderNumber = isset($post['ecom_order_number']) ? $post['ecom_order_number'] : null;
            $sfOrderNumber = isset($post['sf_order_Number']) ? $post['sf_order_Number'] : null;
            $request [] = $ecomOrderNumber;
            $request [] = $sfOrderNumber;
            $this->_logger->info('GET getOrderEbooks API - Request', $request);
            $response = $this->getOrderData($sfOrderNumber, $ecomOrderNumber);
            $this->_logger->info('GET getOrderEbooks API - Respopnse', $response);
            $this->_logger->info('GET getOrderEbooks API - End');
            
        } else {
            $response [] = ['status' => false, 'message' => 'Please add sf_order_Number in url'];
            
            $this->_logger->info('GET getOrderEbooks API - Respopnse', $response);
            $this->_logger->info('GET getOrderEbooks API - End');

        }
        
        return $response;
        
    }

    /*
     * get Order Data with customers and downloadables number
     * @param $sfOrderId String
     * return Array $orderData
     */
    protected function getOrderData($sfOrderId, $ecomOrderNumber = null) {
        $result = [];
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            if ($ecomOrderNumber != null) {
                $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($ecomOrderNumber, 'increment_id');
                
            } else {
                $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($sfOrderId, 'sf_order_id');
                
            }
            if ($salesOrder->getId()) {
                $orderId = $salesOrder->getEntityId();
                $firstname = $salesOrder->getCustomerFirstname();
                $lastname = $salesOrder->getCustomerLastname();
                $middlename = $salesOrder->getCustomerMiddlename();
                $incrementId = $salesOrder->getIncrementId();
                if ($middlename) {
                    $middlename = $salesOrder->getCustomerMiddlename();
                    $name = $firstname . " " . $middlename . " " . $lastname;
                } else {
                    $name = $firstname . " " . $lastname;
                }
                $email = $salesOrder->getCustomerEmail();
                $customerId = $salesOrder->getCustomerId();
                $customerAttributes = $this->getCustomerAttributes($customerId);
                $items = $this->getOrderDownloadableItemData($orderId, $customerId);
                $lastLogin = $this->getLastLogin($customerId);
                
                $result[] = [
                    'customer_name' => $name,
                    'customer_email' => $email,
                    'last_login_at' => $lastLogin,
                    'order_number' => $incrementId,
                    'customer_Attributes' => $customerAttributes,
                    'items' => $items
                ];
                
            }
        } catch (\Exception $e) {
            $result = [];
            $this->_logger->info('GET getOrderEbooks API - ExceptionTrace : ' . $e->getTraceAsString());
            $result[] = ['status' => false, 'message' => $e->getMessage()];
            
        }
        
        return $result;
    }

    /*
     * get Customer Attributes
     * @param $customerId String
     * return Array $customerAttributes
     */
    protected function getCustomerAttributes($customerId) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $moveOrderToHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
        $customerAttributes = $moveOrderToHelper->getCustomerAttributes($customerId);
        return $customerAttributes;
    }

    /*
     * get downloadable items which assigned under same order id.
     * @param $orderId String
     * return Array $items
     */
    protected function getOrderDownloadableItemData($orderId, $customerId) {

        $items = [];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $purchaseLink = $objectManager->get('\Magento\Downloadable\Model\Link\PurchasedFactory');
        $downloadablePurchasedLinks = $purchaseLink->create()->getCollection()
                ->addFieldToFilter('order_id', $orderId)
                ->addFieldToFilter('customer_id', $customerId);

        if ($downloadablePurchasedLinks) {
            foreach ($downloadablePurchasedLinks as $downloadablePurchasedLink) {
                $purchasedId = $downloadablePurchasedLink->getPurchasedId();
                $partNumber = $downloadablePurchasedLink->getProductSku();
                $downloadesBought = $this->getNumberOfDownloadablesOrStatus($purchasedId, self::NUMBER_OF_DOWNLOAD_BOUGHT);
                $downloadesUsed = $this->getNumberOfDownloadablesOrStatus($purchasedId, self::NUMBER_OF_DOWNLOAD_USED);
                $numberOfDownload = $this->getNumberOfDownloadablesOrStatus($purchasedId, self::NUMBER_OF_DOWNLOAD);
                $remainingQty = $this->getRemainingQty($downloadesBought, $downloadesUsed, $numberOfDownload);
                $productName = $downloadablePurchasedLink->getProductName();
                $edition = $this->getProductEdition($partNumber);
                $productStatus = $this->getNumberOfDownloadablesOrStatus($purchasedId, self::STATUS);
                $filename = $this->getFileName($purchasedId);
                $qty = $this->getOrderLineItemQty($orderId, $partNumber);
                $items [] = [
                    'name' => $productName,
                    'edition' => $edition,
                    'status' => $productStatus,
                    'part_number' => $partNumber,
                    'downloades_bought' => $downloadesBought,
                    'downloades_used' => $downloadesUsed,
                    'number_of_download' => $numberOfDownload,
                    'remaining_qty' => (int) $remainingQty,
                    'filename' => $filename,
                    'qty' => $qty
                ];
            }
        }
        return $items;
    }
    
    /*
     * @desc get remaining qty for purchased products.
     * @param string $downloadesBought
     * @param string $downloadesUsed
     * @param string $numberOfDownload
     * 
     * return string $remainingQty
     */
    protected function getRemainingQty($downloadesBought, $downloadesUsed, $numberOfDownload) {

        if ($numberOfDownload != 0) {
            $remainingQty = ( $downloadesBought - $downloadesUsed ) / $numberOfDownload;
        } else {
            $remainingQty = $downloadesBought - $downloadesUsed;
        }
        return $remainingQty;
    }
    
    /*
     * @desc get product's edition.
     * @param string $sku
     * 
     * return string $edition
     */
    protected function getProductEdition($sku) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
        $product = $productRepository->get($sku);
        $edition = $product->getData('edition');
        return $edition;
    }
    
    /*
     * @desc get order line item qty.
     * @param string $orderId
     * @param string $sku
     * 
     * return string $qtyOrdered
     */
    protected function getOrderLineItemQty($orderId, $sku) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesOrderItem = $objectManager->create('\Magento\Sales\Model\Order\Item')
                ->getCollection()
                ->addFieldToFilter('order_id', $orderId)
                ->addFieldToFilter('sku', $sku)
                ->getFirstItem();
        $qtyOrdered = (int) $salesOrderItem->getQtyOrdered();
        return $qtyOrdered;
        
    }
    
    /*
     * get File Name
     * @param $purchasedId String
     * return String $linkfile
     */
    protected function getFileName($purchasedId){
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $purchasedLinkItem = $objectManager->get('\Magento\Downloadable\Model\Link\Purchased\ItemFactory');
        $purchasedItems = $purchasedLinkItem->create()->getCollection()->addFieldToFilter('purchased_id', $purchasedId);
        
        if ($purchasedItems) {
            foreach ($purchasedItems as $purchasedItem) {
                $linkfile = $purchasedItem->getLinkFile();
                return $linkfile;
            }
        }
    }
    
    /*
     * get Number Of Downloadables for item (used or bought).
     * @param $purchasedId String
     * @param $used boolean
     * return String $numberOfDownloads or Status
     */
    protected function getNumberOfDownloadablesOrStatus($purchasedId, $action = NUMBER_OF_DOWNLOAD_BOUGHT) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $purchasedLinkItem = $objectManager->get('\Magento\Downloadable\Model\Link\Purchased\ItemFactory');
        $purchasedItems = $purchasedLinkItem->create()->getCollection()->addFieldToFilter('purchased_id', $purchasedId);
        
        $result = '';
        if ($purchasedItems) {
            foreach ($purchasedItems as $purchasedItem) {
                if ($action === self::STATUS) {
                    $result = $purchasedItem->getStatus();
                    break;
                    
                } else if ($action === self::NUMBER_OF_DOWNLOAD_USED) {
                    $result = $purchasedItem->getNumberOfDownloadsUsed();
                    break;
                    
                } else if ($action === self::NUMBER_OF_DOWNLOAD) {
                    $downloadableLink = $this->_linkFactory->create()->load($purchasedItem->getLinkId());
                    $result = $downloadableLink->getNumberOfDownloads();
                    break;
                    
                } else {
                    $result = $purchasedItem->getNumberOfDownloadsBought();
                    break;
                    
                }
            }
        }
        return $result;
    }
    
    /*
     * @desc: get Last Login Date.
     * @param $customerId String
     * return String $lastLoginAt
     */
    protected function getLastLogin($customerId) {
        //getId
        $logData = $this->customerLogger->get($customerId);
        $lastLogin = $logData->getLastLoginAt();
        $lastLoginAt = 'Never (Offline)';
        if (!empty($lastLogin)) {
            $date = new \DateTime($lastLogin);
            $date->setTimezone(new \DateTimeZone('America/New_York'));
            $lastLoginAt = date('Y/m/d h:i:s A', strtotime($date->format('Y-m-d H:i:s')));

        }
        
        return $lastLoginAt;
    }

}
