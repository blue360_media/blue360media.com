<?php
namespace Endeavor\SFIntegration\Model\Api;

class TrackingNumberManagement implements \Endeavor\SFIntegration\Api\TrackingNumberManagementInterface {

    /**
     * @var Magento\Sales\Model\Order\Shipment\TrackFactory 
     */
    protected $trackFactory;

    /**
     * @var Endeavor\SFIntegration\Model\Result 
     */
    protected $result;

    /**
     * @var \Endeavor\SFIntegration\Logger\Logger
     */
    protected $_logger;

    /**
     * 
     * @param \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory
     * @param \Endeavor\SFIntegration\Model\Result $result
     * @param \Endeavor\SFIntegration\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Sales\Model\Order\Shipment\TrackFactory $trackFactory,
        \Endeavor\SFIntegration\Model\Result $result,
        \Endeavor\SFIntegration\Logger\Logger $logger
    ) {
        $this->_logger = $logger;
        $this->trackFactory = $trackFactory;
        $this->result = $result;
    }

    /**
     * {@inheritdoc}
     * 
     * @author: Murad Dweikat
     * @desc: Add tracking number for each shipment by each coming SF and eCommerece orders in the request.
     * 
     * @param mixed $order
     * @return \Endeavor\SFIntegration\Model\Result
     */
    public function postTrackingNumber($order) {
        $this->_logger->info('Create Tracking Number API - Start');
        $this->_logger->info('Create Tracking Number API - Request:', $order);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $sucessIds = [];
        $failIds = [];
        $currentItem = [];
        $cancelOrders = [];
        $ordersNotFound = [];
        
        try {
            $ordersId = [];
            $ordersTrackNum = [];
			$ordersShipMethod = [];


            $this->addBundleToItems($order, $failIds, $ordersNotFound, $cancelOrders);

            // resort request by tracking number and save it in $ordersTrackNum.
            // resort request by Order Number and save it in $ordersId to collecting all order item in same order.
			
			 foreach ($order as $item) {				 
				$ordersShipMethod[$item['tracking_number']] = $item['ship_method_actual'];				 
			 }

             $this->_logger->info('Create Tracking Number API - ordersShipMethod:', $ordersShipMethod);
			
			
            foreach ($order as $item) {
                $currentItem = $item;
                if (isset($ordersTrackNum[$item['tracking_number']])) {
                    if (!($item['ecom_order_number'] == $ordersTrackNum[$item['tracking_number']])) {
                        $ordersTrackNum[$item['tracking_number']] = $item['ecom_order_number'];
                    }
                } else {
                    $ordersTrackNum[$item['tracking_number']] = $item['ecom_order_number'];
                }

                if (isset($ordersId[$item['ecom_order_number']][$item['order_line_number']])) {
                    if (!in_array($item['order_line_number'], $ordersId[$item['ecom_order_number']][$item['order_line_number']])) {
                        $item['tracking_numbers'][] = $item['tracking_number'];
                        $ordersId[$item['ecom_order_number']][$item['order_line_number']] = $item;
                    } else {
                        $sf_product_code = $item['sf_product_code'];
                        if (strpos($sf_product_code, 'M') === false && !isset($item[\Magento\Catalog\Model\Product\Type::TYPE_BUNDLE])) {
                            $ordersId[$item['ecom_order_number']][$item['order_line_number']]['ship_quantity'] += $item['ship_quantity'];
                            if (!in_array($item['tracking_number'], $ordersId[$item['ecom_order_number']][$item['order_line_number']]['tracking_numbers'])) {
                                $ordersId[$item['ecom_order_number']][$item['order_line_number']]['tracking_numbers'][] = $item['tracking_number'];
                            }
                        } else {
                            if (!in_array($item['tracking_number'], $ordersId[$item['ecom_order_number']][$item['order_line_number']]['tracking_numbers'])) {
                                if (!isset($item[\Magento\Catalog\Model\Product\Type::TYPE_BUNDLE])) {
                                    $ordersId[$item['ecom_order_number']][$item['order_line_number']]['tracking_numbers'][] = $item['tracking_number'];
                                }
                            }
                        }
                    }
                } else {
                    $item['shipped'] = false;
                    $item['tracking_numbers'][] = $item['tracking_number'];
                    $ordersId[$item['ecom_order_number']][$item['order_line_number']] = $item;
                }
            }
        } catch (\Exception $e) {
            $this->_logger->crit('Create Tracking Number Order API - Exception: ' . $e->getMessage());
            $this->_logger->crit('Create Tracking Number Order API - Exception Trace: '.$e->getTraceAsString());
            $this->result->setMsg($e->getMessage());
            
            $subject = 'Exception Happened when Create Shipment order';
            $message = 'Exception Happened when Create Shipment for order:'.$currentItem['ecom_order_number']
                    .',<br>Shipment Request:<br>'.json_encode($currentItem).'<br> Exception: '.$e->getMessage()
                    .'<br>Exception Trace: '.$e->getTraceAsString();
            
            $this->sendErrorEmailMessage($subject,$message);
            return $this->result;
        }

        $sucessTrackingNum = [];

        foreach ($ordersTrackNum as $trackNumber => $orderId) {

            $orderModel = $objectManager->create('Magento\Sales\Model\Order')
                    ->loadByAttribute('increment_id', $orderId);

            // Check if order can be shipped or has already shipped
            if (!$orderModel->canShip()) {
                $this->result->setMsg('You can\'t create an shipment.');
            }

            // Initialize the order shipment object
            $convertOrder = $objectManager->create('Magento\Sales\Model\Convert\Order');
            $shipment = $convertOrder->toShipment($orderModel);
            
            $currentItemTracking = [];
            $sucessItem = [];
            try {
                foreach ($orderModel->getAllItems() AS $orderItem) {
                    $this->_logger->info('orderItem.Id(): ' . $orderItem->getId());
                    if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual() 
                            || !isset($ordersId[$orderId][$orderItem->getCustomIncrementId()])) {
                        if ($orderItem->getProductType() != \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
                            continue;

                        }

                    }
                    if (!isset($ordersId[$orderId][$orderItem->getCustomIncrementId()]) ||
                        $ordersId[$orderId][$orderItem->getCustomIncrementId()]['tracking_number'] != $trackNumber
                            || $ordersId[$orderId][$orderItem->getCustomIncrementId()]['shipped'] ) {
                        continue;

                    }

                    $ordersId[$orderId][$orderItem->getCustomIncrementId()]['shipped'] = true;
                    $trackingNumbers = $ordersId[$orderId][$orderItem->getCustomIncrementId()]['tracking_numbers'];
                    $sucessTrackingNum = array_merge($sucessTrackingNum,$trackingNumbers);

                    if ($orderItem->getProductType() != \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
                        $failIds = array_diff($failIds,[$orderId.'-'.$orderItem->getCustomIncrementId()]);
                        $currentItemTracking = array_merge($currentItemTracking, $trackingNumbers);
                    }

                    if (!in_array($orderId.'-'.$orderItem->getCustomIncrementId(), $sucessItem)) {
                        $sucessItem[] = $orderId.'-'.$orderItem->getCustomIncrementId();

                    }
                    $qtyShipped = $ordersId[$orderId][$orderItem->getCustomIncrementId()]['ship_quantity'];

                    if (!in_array($orderId.'-'.$orderItem->getCustomIncrementId(), $failIds)) {
                        $failIds = array_diff($failIds,[$orderId.'-'.$orderItem->getCustomIncrementId()]);

                    }

                    $shipmentItem = $convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);
                    $shipment->addItem($shipmentItem);
                }

                if ($shipment->getItems()) {
                    $shipment->register();
                    $shipment->getOrder()->setIsInProcess(true);
                }

               /* foreach ($currentItemTracking as $tmpTrackNumber) {
                    $trackingDetail = [
                        'carrier_code' => 'ups',
                        'title' => 'United Parcel Service',
                        'number' => $tmpTrackNumber
                    ];
                    $track = $this->trackFactory->create()->addData($trackingDetail);
                    $shipment->addTrack($track);
                }*/
				
				 foreach ($currentItemTracking as $tmpTrackNumber) {
					$this->_logger->info('Create Tracking Number API - currentItemTracking:', $currentItemTracking);
					$this->_logger->info('Tracking Detail: ' . $orderItem->getId());
					$this->_logger->info('Tracking shipping method: ' . $ordersShipMethod[$tmpTrackNumber]);
					if($ordersShipMethod[$tmpTrackNumber] == "FedEx" || $ordersShipMethod[$tmpTrackNumber] == "FedEx Ground" || $ordersShipMethod[$tmpTrackNumber] == "USPS Bound Printed Matter" ){
                         $trackingDetail = [
                        'carrier_code' => 'fedex',
                        'title' => $ordersShipMethod[$tmpTrackNumber],
                        'number' => $tmpTrackNumber
						];
						$this->_logger->info('Tracking Number: ' . $ordersShipMethod[$tmpTrackNumber]);
					}
					else{
						$trackingDetail = [
							'carrier_code' => 'ups',
							'title' => 'United Parcel Service',
							'number' => $tmpTrackNumber
						];	
						
					}	
                    $track = $this->trackFactory->create()->addData($trackingDetail);
                    $shipment->addTrack($track);
                }

                // Save created shipment and order
                $shipment->save();
                $shipment->getOrder()->save();

                $objectManager->create('Magento\Shipping\Model\ShipmentNotifier')->notify($shipment);

                $shipment->save();

                $sucessIds = array_merge($sucessIds,$sucessItem);
            } catch (\Exception $e) {
                if (!in_array($trackNumber,$sucessTrackingNum)) {
                    $shipmentOrderId = ($currentItem['ecom_order_number'] != '') ? $currentItem['ecom_order_number'] : $orderId;
                    $this->_logger->crit('Create Tracking Number Order API - Exception: Order# '.$shipmentOrderId. ' : ' . $e->getMessage());
                    $this->_logger->crit('Create Tracking Number Order API - Exception Trace: '.$e->getTraceAsString());
                    $this->result->setMsg($e->getMessage());
                }
            }
        }

        $this->result->setSuccess($sucessIds);
        $this->result->setFail($failIds);
        if (empty($failIds)) {
            $this->result->setMsg('The order Shipment has been successfully completed.');
            
        } else if (empty($sucessIds)) {
            $this->result->setMsg('The order Shipment has failure.');
            
        } else {
            $this->result->setMsg('The order Shipment has been success but with some failure.');
            
        }
        
        $message = '';
        if (!empty($failIds)) {
            $message .= 'The Following Order failed to create shipment:<br>'.json_encode($failIds);

        }
        
        
        if (!empty($cancelOrders)) {
            $message .= '<br><br>Try To Create Shipment for Canceled order:<br>Canceled Orders request:<br>'. json_encode($cancelOrders);

        }
        
        if (!empty($ordersNotFound)) {
            $message .= '<br><br>Try To Create Shipment for order not found:<br>Orders not found request:<br>'. json_encode($ordersNotFound);
        }
        
        // send email with exception details when happened.
        if (!empty($ordersNotFound) || !empty($cancelOrders) || !empty($failIds)) {
            $subject = 'Shipment Orders Failed';
            $this->sendErrorEmailMessage($subject,$message);
        }

        $this->_logger->info('Create Tracking Number Order API - Response:', (array)$this->result);
        $this->_logger->info('Create Tracking Number Order API - End' . PHP_EOL);

        return $this->result;
    }

    /**
     * @author: Murad Dweikat
     * @desc: For each product, we add his bundle to make complete shipment. 
     * 
     * @param mixed $order
     * @param string[] $failIds
     * @return mixed $order
     */
    public function addBundleToItems(&$order, &$failIds, &$ordersNotFound, &$cancelOrders){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        foreach ($order as $index => $item) {
            $tempItem = $item;
            $orderModel = null;
            
            // load order by eCommerce number or SF order number.
            if (isset($tempItem['ecom_order_number']) && $tempItem['ecom_order_number'] != null) {
                $orderModel = $objectManager->create('Magento\Sales\Model\Order')
                    ->loadByAttribute('increment_id', $tempItem['ecom_order_number']);

            } else if (isset($tempItem['order_number']) && $tempItem['order_number'] != null) {
                $orderModel = $objectManager->create('Magento\Sales\Model\Order')
                    ->loadByAttribute('sf_order_id', $tempItem['order_number']);
            }

            
            if (!empty($orderModel->getId())) {

                if ($orderModel->getStatus() === 'canceled') {
                    $cancelOrders[] = $order[$index];
                    unset($order[$index]);
                    continue;
                }
                
                $tempItem['ecom_order_number'] = $orderModel->getIncrementId();
                $order[$index]['ecom_order_number'] = $orderModel->getIncrementId();
            } else {
                $this->_logger->crit('Create Tracking Number Order API - Exception: The order not found.'.json_encode($order[$index]));
                $this->result->setMsg('Create Tracking Number Order API - Exception: The order not found.'.json_encode($order[$index]));
                $ordersNotFound[] = $order[$index];
                unset($order[$index]);
                continue;
            }

            $failIds[] = $tempItem['ecom_order_number'].'-'.$tempItem['order_line_number'];

            // add bundle to request for each product in request.
            foreach ($orderModel->getAllItems() AS $orderItem) {
                $parentOrderItemModel = $objectManager->create('Magento\Sales\Model\Order\Item')
                        ->load($orderItem->getParentItemId());

                if ($parentOrderItemModel->getId()) {
                    $tempItem['ship_quantity'] = 1;
                    $tempItem[\Magento\Catalog\Model\Product\Type::TYPE_BUNDLE] = true;
                    $tempItem['order_line_number'] = $parentOrderItemModel->getCustomIncrementId();
                    $order[] = $tempItem;
                    break;
                }
            }

        }

        return $order;
        
    }
    
    /**
     * Send Error Email.
     * 
     * @param type $orderSuccessRespone
     * @param type $orderFailRespone
     * @param type $sqlResualt
     */
    private function sendErrorEmailMessage($subject, $message)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $emailData = [];
        $emailData['subject'] = $subject;
        $emailData['message'] = $message;
        
        // call send mail method from helper  
        $emailHelper = $objectManager->create('Endeavor\PaymentComplete\Helper\Email');
                
        $emailHelper->sendErrorEmailMessage(
            $emailData
        );
    }
    
}
