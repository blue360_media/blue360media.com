<?php
/**
 * Copyright ©2017 Endeavor Technology, Inc. All rights reserved.
 * 
 * @author: Murad Dweikat
 * @date: 2017/11/21
 * @desc: Create invoice with capture for all Pre-Orders that contains specific Publication Number.
 *        And Make it as complete, and make pre-order product as avaliable.
 * 
 */
namespace Endeavor\SFIntegration\Model\Api;

use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Api\Data\InvoiceCommentCreationInterface;
use Magento\Sales\Api\Data\InvoiceCreationArgumentsInterface;
use Endeavor\SFIntegration\Api\InvoiceOrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order\Config as OrderConfig;
use Magento\Sales\Model\Order\Invoice\InvoiceValidatorInterface;
use Magento\Sales\Model\Order\Invoice\NotifierInterface;
use Magento\Sales\Model\Order\InvoiceDocumentFactory;
use Magento\Sales\Model\Order\InvoiceRepository;
use Magento\Sales\Model\Order\OrderStateResolverInterface;
use Magento\Sales\Model\Order\OrderValidatorInterface;
use Magento\Sales\Model\Order\PaymentAdapterInterface;
use Magento\Sales\Model\Order\Validation\InvoiceOrderInterface as InvoiceOrderValidator;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\ObjectManager;
use Webkul\Preorder\Model\ResourceModel\Complete\CollectionFactory;
use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory as Preorder;

/**
 * Class InvoiceOrder
 */
class InvoiceOrder implements InvoiceOrderInterface
{
    
    /**
     * Get Recipient email to receive Release Publication Status
     * 
     * @var String
     */
    const XML_PATH_RECIPIENT_EMAIL = 'endwebapi/email/recipient_emails';

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var InvoiceDocumentFactory
     */
    private $invoiceDocumentFactory;

    /**
     * @var PaymentAdapterInterface
     */
    private $paymentAdapter;

    /**
     * @var OrderStateResolverInterface
     */
    private $orderStateResolver;

    /**
     * @var OrderConfig
     */
    private $config;

    /**
     * @var InvoiceRepository
     */
    private $invoiceRepository;

    /**
     * @var InvoiceOrderValidator
     */
    private $invoiceOrderValidator;

    /**
     * @var NotifierInterface
     */
    private $notifierInterface;

    /**
     * @var LoggerInterface
     */
    private $logger;
    
    /**
     * Logging instance
     * @var \Endeavor\SFIntegration\Logger\Logger
     */
    protected $_ApiLogger;
    
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @var CollectionFactory
     */
    protected $_completeCollection;

    /**
     * @var Preorder
     */
    protected $_preorderCollection;
    
    /**
     *
     * @var type 
     */
    protected $_objectManager;
    
    /**
     *
     * @var Magento\Store\Model\StoreManagerInterface 
     */
    protected $storeManager;    
    
    /**
     *
     * @var Magento\Framework\App\Config\ScopeConfigInterface 
     */
    protected $scopeConfig;
   
    /**
     * 
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param ResourceConnection $resourceConnection
     * @param OrderRepositoryInterface $orderRepository
     * @param InvoiceDocumentFactory $invoiceDocumentFactory
     * @param InvoiceValidatorInterface $invoiceValidator
     * @param OrderValidatorInterface $orderValidator
     * @param PaymentAdapterInterface $paymentAdapter
     * @param OrderStateResolverInterface $orderStateResolver
     * @param OrderConfig $config
     * @param InvoiceRepository $invoiceRepository
     * @param NotifierInterface $notifierInterface
     * @param LoggerInterface $logger
     * @param \Endeavor\SFIntegration\Logger\Logger $apiLogger
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     * @param CollectionFactory $completeCollection
     * @param Preorder $preorderCollection
     * @param InvoiceOrderValidator $invoiceOrderValidator
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        ResourceConnection $resourceConnection,
        OrderRepositoryInterface $orderRepository,
        InvoiceDocumentFactory $invoiceDocumentFactory,
        InvoiceValidatorInterface $invoiceValidator,
        OrderValidatorInterface $orderValidator,
        PaymentAdapterInterface $paymentAdapter,
        OrderStateResolverInterface $orderStateResolver,
        OrderConfig $config,
        InvoiceRepository $invoiceRepository,
        NotifierInterface $notifierInterface,
        LoggerInterface $logger,
        \Endeavor\SFIntegration\Logger\Logger $apiLogger,
        \Webkul\Preorder\Helper\Data $preorderHelper,
        CollectionFactory $completeCollection,
        Preorder $preorderCollection,
        InvoiceOrderValidator $invoiceOrderValidator = null
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->orderRepository = $orderRepository;
        $this->invoiceDocumentFactory = $invoiceDocumentFactory;
        $this->paymentAdapter = $paymentAdapter;
        $this->orderStateResolver = $orderStateResolver;
        $this->config = $config;
        $this->invoiceRepository = $invoiceRepository;
        $this->notifierInterface = $notifierInterface;
        $this->logger = $logger;
        $this->invoiceOrderValidator = $invoiceOrderValidator ?: ObjectManager::getInstance()->get(
            InvoiceOrderValidator::class
        );
        $this->_ApiLogger = $apiLogger;
        $this->_preorderHelper = $preorderHelper;
        $this->_completeCollection = $completeCollection;
        $this->_preorderCollection = $preorderCollection;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->storeManager = $storeManager;        
        $this->scopeConfig = $scopeConfig;
        
    }

    /**
     * @param string $publicationNumber
     * @param string $releaseNumber
     * @param bool $capture
     * @param array $items
     * @param bool $notify
     * @param bool $appendComment
     * @param \Magento\Sales\Api\Data\InvoiceCommentCreationInterface|null $comment
     * @param \Magento\Sales\Api\Data\InvoiceCreationArgumentsInterface|null $arguments
     * @return mixed
     * @throws \Magento\Sales\Api\Exception\DocumentValidationExceptionInterface
     * @throws \Magento\Sales\Api\Exception\CouldNotInvoiceExceptionInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \DomainException
     */
    public function execute(
        $publicationNumber,
        $releaseNumber,
        $capture = true,
        array $items = [],
        $notify = true,
        $appendComment = false,
        InvoiceCommentCreationInterface $comment = null,
        InvoiceCreationArgumentsInterface $arguments = null
    ) {
        $this->_ApiLogger->info('Create Order Invoice API - Start');
        $this->_ApiLogger->info('Create Order Invoice API - Request: { publication_number: '.$publicationNumber.', releaseNumber: '.$releaseNumber.'}');
        
        $publicationNumber = trim($publicationNumber);
        $releaseNumber = trim($releaseNumber);
        
        /**
        * @desc: map between sales order Id and created invoice Id.
        */
        $salesOrderInvoiceMap = [];
        $salesOrderIdsMap = [];
        
        $salesOrderSuccess = [];
        $salesOrderFail = [];
        
        try {
            // Get All product that have Publication Number passed in request.
            $productCollection = $this->_objectManager->create('Magento\Catalog\Model\Product')
                    ->getCollection()
                    ->addFieldToSelect('entity_id')
                    ->addAttributeToFilter('publication_number',['eq' => $publicationNumber]);

            $productIds = $productCollection->getAllIds();
            
            // Get All product that have Differnet release number passed in request.
            $oldReleaseProductCollection = $this->_objectManager->create('Magento\Catalog\Model\Product')
                    ->getCollection()
                    ->addAttributeToFilter('publication_number',['eq' => $publicationNumber])
                    //handle simple & combo product
                    ->addFieldToFilter('sku',['nlike' => $publicationNumber.$releaseNumber.'%']) 
                    // handle bundle product
                    ->addFieldToFilter('sku',['nlike' => $publicationNumber.'-'.$releaseNumber.'%']);

            $oldReleaseProductIds = $oldReleaseProductCollection->getAllIds();
                        
            $preOrderProductCollection = $this->_objectManager->create('Webkul\Preorder\Model\Preorder')
                    ->getCollection()
                    ->addFieldToSelect
                    (
                        'id',
                        'order_id',
                        'product_id',
                        'preorder_status'
                    )
                    ->addFieldToFilter('product_id',['in' => $productIds])
                    ->addFieldToFilter('preorder_status',['eq' => '0']);

            $salesOrderIds = array_unique($preOrderProductCollection->getAllPreOrderIds());

            $salesOrder = $this->_objectManager->create('Magento\Sales\Model\Order')
                    ->getCollection()
                    ->addFieldToFilter('entity_id',['in' => $salesOrderIds]);
            
            foreach ($salesOrder as $order) {
                $salesOrderIdsMap[$order->getId()] = $order->getIncrementId();
            }
            
            $orderFailRespone = [];
            $orderSuccessRespone = [];
            
            foreach ($salesOrderIds as $salesOrderId) {
                $invoiceResponse = $this->callInvoice($salesOrderId, $capture, $items, $notify, $appendComment, $comment, $arguments);
                $eCommerceId = $salesOrderIdsMap[$salesOrderId];
                $respone = [];
                $respone['eCommerceNumber'] = $eCommerceId;
                // if Invoice Success.
                if (is_int($invoiceResponse)) {
                    $respone['status'] = true;
                    $respone['message'] = '';
                    $salesOrderSuccess[] = $salesOrderId;
                    $orderSuccessRespone[] = $respone;
                } else {
                    $respone['status'] = false;
                    $respone['message'] = $invoiceResponse;
                    $salesOrderFail[] = $salesOrderId;
                    $orderFailRespone[] = $respone;
                }
                
                $salesOrderInvoiceMap[] = $respone;
            }
            
            // make pre-orders as complete.
            $this->makePreOrderComplate($salesOrderSuccess, $productIds);
            $this->disableProducts($oldReleaseProductIds);
            
            $updatedPurchaseResult = $this->updatePurchaseLinks($publicationNumber, $releaseNumber);
            
            $this->sendPreOrderStatus($publicationNumber, $releaseNumber, $orderSuccessRespone, $orderFailRespone, $updatedPurchaseResult);
            
        } catch (\Exception $ex) {
            $this->_ApiLogger->crit('Create Order Invoice API - Exception: ' . $ex->getMessage());
            $this->_ApiLogger->crit('Create Order Invoice API - Exception Trace: '.$ex->getTraceAsString());

            return $ex->getMessage();
        }
        
        if (!empty($salesOrderFail)) {
            $this->_ApiLogger->crit('Create Order Invoice API - Failed Orders Invoices: ', $salesOrderFail);
        }
        
        $this->_ApiLogger->info('Create Order Invoice API - END');
        return 'Complete Successfully';
    }
    
     /**
     * @param int $orderId
     * @param bool $capture
     * @param array $items
     * @param bool $notify
     * @param bool $appendComment
     * @param \Magento\Sales\Api\Data\InvoiceCommentCreationInterface|null $comment
     * @param \Magento\Sales\Api\Data\InvoiceCreationArgumentsInterface|null $arguments
     * @return int
     * @throws \Magento\Sales\Api\Exception\DocumentValidationExceptionInterface
     * @throws \Magento\Sales\Api\Exception\CouldNotInvoiceExceptionInterface
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \DomainException
     */
    public function callInvoice(
        $orderId,
        $capture = true,
        array $items = [],
        $notify = true,
        $appendComment = false,
        InvoiceCommentCreationInterface $comment = null,
        InvoiceCreationArgumentsInterface $arguments = null
    ) {
        $connection = $this->resourceConnection->getConnection('sales');
        $order = $this->orderRepository->get($orderId);
        $invoice = $this->invoiceDocumentFactory->create(
            $order,
            $items,
            $comment,
            ($appendComment && $notify),
            $arguments
        );
        $errorMessages = $this->invoiceOrderValidator->validate(
            $order,
            $invoice,
            $capture,
            $items,
            $notify,
            $appendComment,
            $comment,
            $arguments
        );
        if ($errorMessages->hasMessages()) {
            $message = "Invoice Document Validation Error(s):\n" . implode("\n", $errorMessages->getMessages());
            $this->_ApiLogger->crit($message);
            return $message;
        }
        
        $connection->beginTransaction();
        try {
            $order = $this->paymentAdapter->pay($order, $invoice, $capture);
            $order->setState(
                $this->orderStateResolver->getStateForOrder($order, [OrderStateResolverInterface::IN_PROGRESS])
            );
            $order->setStatus($this->config->getStateDefaultStatus($order->getState()));
            $invoice->setState(\Magento\Sales\Model\Order\Invoice::STATE_PAID);
            $this->invoiceRepository->save($invoice);
            $this->orderRepository->save($order);
            $connection->commit();
            
        } catch (\Exception $ex) {
            $this->_ApiLogger->crit('Create Order Invoice API - Exception: ' . $ex->getMessage());
            $this->_ApiLogger->crit('Create Order Invoice API - Exception Trace: '.$ex->getTraceAsString());
            $connection->rollBack();
            return $ex->getMessage();
        }
        
        try {
            if ($notify) {
                if (!$appendComment) {
                    $comment = null;
                }
                $this->notifierInterface->notify($order, $invoice, $comment);
            }
        } catch (\Exception $ex) {
            $this->_ApiLogger->crit('Create Order Invoice API - Exception: ' . $ex->getMessage());
            $this->_ApiLogger->crit('Create Order Invoice API - Exception Trace: '.$ex->getTraceAsString());
            return $ex->getMessage();
        }

        return (int)$invoice->getEntityId();
    }
    
    /**
     * @author: Murad Dweikat
     * @desc: Make Pre-Orders as complete paid.
     * 
     * @param array $salesOrderIds
     */
    private function makePreOrderComplate($salesOrderIds, $productIds) {

        $preOrderCollection = $this->_objectManager->create('Webkul\Preorder\Model\Preorder')
                ->getCollection()
                ->addFieldToFilter('order_id',['in' => $salesOrderIds]);
        foreach ($preOrderCollection as $preOrder) {
            $preOrder->delete();
        }
        
        $this->makeOrderNotExported($salesOrderIds);
        $this->makeProductAvailable($productIds);
    }
    
    /**
     * @desc: Make Products as Available and not pre-order.
     * 
     * @param int[] $productIds
     */
    private function makeProductAvailable($productIds) {
        
        $stockRegistry = $this->_objectManager->create('\Magento\CatalogInventory\Model\StockRegistry');

        foreach ($productIds as $productId) {
            $product = $this->_objectManager->create('\Magento\Catalog\Model\Product')->load($productId);
            $stockStatus = $stockRegistry->getStockStatus($productId);
            $stockItem = $stockRegistry->getStockItem($productId);
            $stockStatus->setStockStatus(1);
            $stockItem->setIsInStock(1);
            $product->setWkPreorder(0);

            $product->save();
            $stockItem->save();
            $stockStatus->save();
        }
    }
    
    /**
     * @desc: Make Pre-Order as non exported after make invoice to allow Salesforce to pull it.
     * 
     * @param type $salesOrderIds
     */
    private function makeOrderNotExported($salesOrderIds) {
        
        $salesOrder = $this->_objectManager->create('Magento\Sales\Model\Order')
                    ->getCollection()
                    ->addFieldToFilter('entity_id',['in' => $salesOrderIds]);
        
        foreach ($salesOrder as $order) {
            $order->setSfExportedStatus(0);
            $order->save();
        }
    }
    
    /**
     * 
     * @desc: update dummy purchase link by new purchase link.
     * 
     * @param type $publicationNumber
     * @param type $releaseNumber
     */
    private function updatePurchaseLinks($publicationNumber, $releaseNumber) {
        
        $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = 'CALL endeavor_release_publication('.$publicationNumber .','.$releaseNumber.')';
        $result = $connection->query($sql);
        $row = $result->fetch();
        return $row;
    }
    
    /**
     * Send Email by Pre-Order status for support team.
     * 
     * @param type $orderSuccessRespone
     * @param type $orderFailRespone
     * @param type $sqlResualt
     */
    private function sendPreOrderStatus($publicationNumber, $releaseNumber, $orderSuccessRespone, $orderFailRespone, $updatedPurchaseResult) {
        
        $emailData = [];
        $emailData['success'] = json_encode($orderSuccessRespone);
        $emailData['fail'] = json_encode($orderFailRespone);
        $emailData['publicationNumber'] = $publicationNumber;
        $emailData['releaseNumber'] = $releaseNumber;
        if (!empty($updatedPurchaseResult)) {
            $emailData['updatedPurchaseResult'] = json_encode($updatedPurchaseResult['result']);
        } else {
            $emailData['updatedPurchaseResult'] = '';
        }
        
        // call send mail method from helper  
        $emailHelper = $this->_objectManager->get('Endeavor\SFIntegration\Helper\Email');
        
        $recipientEmailConfig = $this->scopeConfig->getValue(self::XML_PATH_RECIPIENT_EMAIL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $recipientEmailConfig = str_replace(' ', '', $recipientEmailConfig);
        $recipientEmails = explode(',', $recipientEmailConfig);
        
        /* Receiver Detail  */
        $receiverInfo = [
            'name' => 'Blue360Media Support',
            'email' => $recipientEmails
        ];
        
        $orderStatus = 'pre-order';
        
        $emailHelper->salesOrderApiSendMailMessage(
            $emailData, $receiverInfo, $orderStatus, true
        );
    }
    
    /**
     * Disable Old release Product.
     * 
     * @param type $oldReleaseProductIds
     */
    private function disableProducts($oldReleaseProductIds) {
        $storeId = $this->storeManager->getStore()->getId();
        foreach ($oldReleaseProductIds as $productId) {
            $product = $this->_objectManager->create('\Magento\Catalog\Model\Product')
                    ->setStoreId(0)
                    ->load($productId);
            $defualtStoreProduct = $this->_objectManager->create('\Magento\Catalog\Model\Product')
                    ->setStoreId($storeId)
                    ->load($productId);
            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
            $defualtStoreProduct->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);

            $product->save();
            $defualtStoreProduct->save();

        }
        
    }
    
}
