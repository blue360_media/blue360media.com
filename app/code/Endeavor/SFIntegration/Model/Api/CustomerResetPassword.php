<?php
/**
 * Copyright © 2018 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 18-01-2018
 * @desc send reset password for users.
 */

namespace Endeavor\SFIntegration\Model\Api;

class CustomerResetPassword implements \Endeavor\SFIntegration\Api\CustomerResetPasswordInterface
{
    /**
     * Logging instance
     * @var \Endeavor\SFIntegration\Logger\Logger
     */
    protected $_logger;
    
    /**
     * Customer Object
     *
     * @var \Magento\Customer\Model\CustomerFactory 
     */
    protected $_customerFactory;
    
    /**
     * 
     * @param \Magento\CatalogInventory\Model\StockRegistry $stock
     * @param \Magento\Catalog\Model\Product $product
     * @param \Endeavor\SFIntegration\Model\Result $result
     * @param \Endeavor\SFIntegration\Logger\Logger $logger
     */
    public function __construct(
        \Magento\Customer\Model\CustomerFactory $CustomerFactory,
        \Endeavor\SFIntegration\Logger\Logger $logger
    ) {
        $this->_customerFactory = $CustomerFactory;
        $this->_logger = $logger;
    }
    
    /**
     * @param string $customerEmail
     * @return mixed
     */
    public function execute($customerEmail) {
        try{
            $this->_logger->info('POST execute Reset Password API - Start');
            $request [] = ['request' => $customerEmail];
            $this->_logger->info('POST execute Reset Password API - Request', $request);

            $response = $this->sendResetPassword($customerEmail);

            return $response;
            
        } catch (\Exception $e) {
            $response = [];
            $response [] = ['status' => false, 'message' => $e->getMessage()];
            $request [] = ['request' => $customerEmail];
            $this->_logger->info('POST eBookAccess allowAssign API - Request:', $request);
            $this->_logger->info('POST eBookAccess allowAssign API - Response:', $response);
            $this->_logger->info('POST eBookAccess allowAssign API - End'.PHP_EOL);
            return $response;
        }
    }
    
    /**
     * @desc send Reset Password email.
     * @param String $customerEmail
     * @return Array $response
     */
    protected function sendResetPassword($customerEmail) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer = $this->getCustomerObject($customerEmail);
        $response = null;
        if (!empty($customer->getId())) {
            $sendResetPasswordHelper = $objectManager->get('Endeavor\SFIntegration\Helper\ResetPassword');
            $sendResetPasswordHelper->sendPasswordResetMail($customer);
            $response [] = ['status' => true, 'message' => 'reset password is sent successfully.'];
            $this->_logger->info('POST execute Reset Password API - Response', $response);
            $this->_logger->info('POST execute Reset Password API - End');
            
        } else {
            $response [] = ['status' => false, 'message' => 'customer not found.'];
            $this->_logger->info('POST execute Reset Password API - Response', $response);
            $this->_logger->info('POST execute Reset Password API - End');
            
        }
        return $response;
    }
    
    /**
     * @desc get Customer Object.
     * @param string $customerEmail Customer description.
     * @return Object $customer
     */
    protected function getCustomerObject ($customerEmail) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteId = $storeManager->getStore()->getWebsiteId();
        
        $customer = $objectManager->create('Magento\Customer\Model\Customer')
                ->setWebsiteId($websiteId)
                ->loadByEmail($customerEmail);
        
        return $customer;
    }
}