<?php
namespace Endeavor\SFIntegration\Model\Api;

class InventoryManagement implements \Endeavor\SFIntegration\Api\InventoryManagementInterface
{
    /**
     *
     * @var Magento\CatalogInventory\Model\StockRegistry 
     */
    protected $_stockRegistry;

    /**
     *
     * @var Endeavor\SFIntegration\Model\Result 
     */
    protected $result;

    /**
     * Logging instance
     * @var \Endeavor\SFIntegration\Logger\Logger
     */
    protected $_logger;

    /**
     * 
     * 
     */
    protected $_objectManager;
    
    /**
     * 
     * @param \Magento\CatalogInventory\Model\StockRegistry $stock
     * @param \Magento\Catalog\Model\Product $product
     * @param \Endeavor\SFIntegration\Model\Result $result
     */
    public function __construct(
    \Magento\CatalogInventory\Model\StockRegistry $stock,
    \Endeavor\SFIntegration\Model\Result $result,
    \Endeavor\SFIntegration\Logger\Logger $logger
    ) {
        $this->_stockRegistry = $stock;
        $this->result = $result;
        $this->_logger = $logger;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

    }
    
    /**
     * {@inheritdoc}
     */
    public function putInventory($items) {
        
        $this->_logger->info('Update Stock Inventory - Start');
        $this->_logger->info('Update Stock Inventory - Request:',$items);

        $ids =[];
        $failIds =[];
        
        foreach ($items as $item) {
            try {
                $product = $this->_objectManager->create('\Magento\Catalog\Model\Product')
                        ->loadByAttribute('sku', $item['sku']);
                if ($product) {
                    $stockItem = $this->_stockRegistry->getStockItemBySku($item['sku']);
                    $stockItem->setQty($item['qty']);

                    // handle B|BC|BL|M product 
                    if (isset($item['is_out_of_stock'])) {
                        $stockStatus = $this->_stockRegistry->getStockStatusBySku($item['sku']);
                        if (strtoupper($item['is_out_of_stock']) == 'Y') {
                            $stockStatus->setStockStatus(0);
                            $stockItem->setIsInStock(0);

                        } else {
                            $stockStatus->setStockStatus(1);
                            $stockItem->setIsInStock(1);

                        }
                        $stockStatus->save();

                    }

                    $stockItem->save();

                    $ids[] = $item['sku'];
                } else {
                    $failIds[] = $item['sku'];
                    
                }
            } catch (\Exception $e) {
                $failIds[] = $item['sku'];
                $this->_logger->crit('Update Stock Inventory API - Exception: '.$e->getMessage()
                        .', For Product: '.$item['sku']);
                $this->_logger->crit('Update Stock Inventory API - Exception Trace: '.$e->getTraceAsString());
                $this->result->setMsg($e->getMessage());
                
            }
        }
        
        $this->result->setSuccess($ids);
        $this->result->setFail($failIds);
        if (empty($this->result->getMsg())) {
            if (empty($failIds)) {
                $this->result->setMsg('The Stock Inventory updating has been successfully completed.');
                
            } else if (empty($ids)) {
                $this->result->setMsg('The Stock Inventory updating has failure.');
                
            } else {
                $this->result->setMsg('The Stock Inventory updating has been success but with some failure.');
                
            }
        }
        
        $this->_logger->info('Update Stock Inventory API - Response:',(array)$this->result);
        $this->_logger->info('Update Stock Inventory API - End'.PHP_EOL);
        
        return $this->result;
    }

}