<?php 
namespace Endeavor\SFIntegration\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var string
     */
    private static $connectionName = 'sales_order';
    private static $connectionNameGrid='sales_order_grid';
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->addSalesOrderCustomFields($setup);
        }
        
    }

    /**
     * Add meta title
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addSalesOrderCustomFields(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$connectionName),
            'sf_exported_status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                'default' => 0,
                'comment' => 'SF Exported Status'
            ]
        );
        
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$connectionName),
            'source_order',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'default' => 'MG',
                'length' => 2,
                'comment' => 'Sales Order Soruce'
            ]
        );
        
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$connectionName),
            'sf_order_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 35,
                'comment' => 'Salesforce Sales Order id'
            ]
        );
        
        $setup->getConnection()->addColumn(
                $setup->getTable(self::$connectionNameGrid),
                'sf_order_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 35,
                    'comment' => 'Salesforce Sales Order id'
                ]
            );
        return $this;
    }
}
