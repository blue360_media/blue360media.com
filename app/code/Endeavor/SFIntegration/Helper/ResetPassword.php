<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 27-12-2017.
 * @desc Helper to send reset password email for customer when changed his email.
 */

namespace Endeavor\SFIntegration\Helper;
use Magento\Customer\Model\AccountManagement;

class ResetPassword {
    
    /*
     * @var $_customerAccountManagement
     */
    protected $_customerAccountManagement;
    
    /**
     * @param \Magento\Customer\Api\AccountManagementInterface $customerAccountManagement
     */
    public function __construct(
        \Magento\Customer\Api\AccountManagementInterface $customerAccountManagement
    ) {
        $this->_customerAccountManagement = $customerAccountManagement;
    }
    
    /*
     * send Password Reset Mail
     * @param Object $customer
     */
    public function sendPasswordResetMail($customer) {
        $email = $customer->getEmail();
        try {
            $this->_customerAccountManagement->initiatePasswordReset(
                    $email, AccountManagement::EMAIL_RESET, $customer->getWebsiteId()
            );
        }
        catch (\Exception $exception) {
            echo "error: " . $exception->getMessage() . "\n";
        }
    }

}
