<?php
namespace Endeavor\SFIntegration\Helper;
 
/**
 * Custom Module Email helper
 */
class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Email template path
     * 
     * @var string
     */
    const XML_PATH_EMAIL_TEMPLATE_FIELD  = 'endwebapi/email/template';

    /**
     * Auto-Ship Order Email template path
     * 
     * @var string
     */
    const XML_PATH_AUTOSHIP_EMAIL_TEMPLATE_FIELD  = 'endwebapi/email/template_autoship';

    /**
     * Email template path
     * 
     * @var string
     */
    const XML_PATH_PREORDER_EMAIL_TEMPLATE_FIELD  = 'endwebapi/email/template_preorder';
    
    /**
     * Email template path
     * 
     * @var string
     */
    const XML_PATH_CONFIMATION_EMAIL_TEMPLATE_FIELD  = 'endwebapi/email/template_confirmation_shipping';

    /**
     * Email template path
     * 
     * @var string
     */
    const XML_PATH_CONFIMATION_FOR_OWNER_EMAIL_TEMPLATE_FIELD  = 'endwebapi/email/template_confirmation_all_orders';
    
    /**
     * Email sender path
     * 
     * @var string
     */
    const XML_PATH_EMAIL_SENDER_FIELD  = 'endwebapi/email/sender_email_identity';

    /**
     * Name sender path
     * 
     * @var string
     */
    const XML_PATH_SENDER_NAME_FIELD  = 'endwebapi/email/sender_name';
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
 
    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
 
    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;
 
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;
     
    /**
     * @var string
    */
    protected $temp_id;
 
    /**
     * 
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
        ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder; 
    }
 
    /**
     * Return store configuration value of your template field that which id you set for template
     *
     * @param string $path
     * @param int $storeId
     * @return mixed
     */
    protected function getConfigValue($path, $storeId)
    {
        return $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
 
    /**
     * Return store 
     *
     * @return Store
     */
    public function getStore()
    {
        return $this->_storeManager->getStore();
    }
 
    /**
     * Return template id according to store
     *
     * @return mixed
     */
    public function getTemplateId($xmlPath)
    {
        return $this->getConfigValue($xmlPath, $this->getStore()->getStoreId());
    }
 
    /**
     * [generateTemplate description]  with template file and tempaltes variables values                
     * @param  Mixed $emailTemplateVariables 
     * @param  Mixed $senderInfo             
     * @param  Mixed $receiverInfo           
     * @return void
     */
    public function generateTemplate($emailTemplateVariables,$senderInfo,$receiverInfo)
    {
        $template =  $this->_transportBuilder->setTemplateIdentifier($this->temp_id)
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->_storeManager->getStore()->getId(),
                    ]
                )
                ->setTemplateVars($emailTemplateVariables)
                ->setFrom($senderInfo)
                ->addTo($receiverInfo['email'],$receiverInfo['name']);
        return $this;        
    }
 
    /**
     * [sendInvoicedOrderEmail description]                  
     * @param  Mixed $emailTemplateVariables 
     * @param  Mixed $receiverInfo           
     * @return void
     */
    public function salesOrderApiSendMailMessage($emailTemplateVariables, $receiverInfo, $orderStatus)
    {
        try {
            $senderType = $this->getConfigValue(
                'endwebapi/email/sender_email_identity',
                $this->getStore()->getStoreId()
            );

            $emailPath = 'trans_email/ident_'.$senderType.'/email';

            /* Sender Detail  */
            $senderInfo = [
                'name' => $this->getConfigValue(self::XML_PATH_SENDER_NAME_FIELD, $this->getStore()->getStoreId()),
                'email' => $this->getConfigValue($emailPath, $this->getStore()->getStoreId())
            ];

            $templatePath = '';
            if (strtolower($orderStatus) == 'auto-ship') {
                $templatePath = self::XML_PATH_AUTOSHIP_EMAIL_TEMPLATE_FIELD;

            } else if (strtolower($orderStatus) == 'pre-order') {
                $templatePath = self::XML_PATH_PREORDER_EMAIL_TEMPLATE_FIELD;

            } else if (strtolower($orderStatus) == 'shipping_person_email') {
                $templatePath = self::XML_PATH_CONFIMATION_EMAIL_TEMPLATE_FIELD;

            } else if (strtolower($orderStatus) == 'owner_order_email'){
                $templatePath = self::XML_PATH_CONFIMATION_FOR_OWNER_EMAIL_TEMPLATE_FIELD;

            }else {
                $templatePath = self::XML_PATH_EMAIL_TEMPLATE_FIELD;
            }

            $this->temp_id = $this->getTemplateId($templatePath);
            $this->inlineTranslation->suspend();    
            $this->generateTemplate($emailTemplateVariables,$senderInfo,$receiverInfo);    
            $transport = $this->_transportBuilder->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();
            return true;
            
        } catch (\Exception $ex) {
             return false;
        }
    }
 
}