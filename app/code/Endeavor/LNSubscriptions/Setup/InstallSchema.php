<?php
namespace Endeavor\LNSubscriptions\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        if (!$installer->tableExists('endeavor_lnsubscriptions')) {
            $table = $installer->getConnection(
                )->newTable($installer->getTable('endeavor_lnsubscriptions')
                )->addColumn(
                    'subscriptions_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
                    'Entity ID'
                )->addColumn(
                    'customer_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true],
                    'Customer Id'
                )->addColumn(
                    'account_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true],
                    'Account Id'
                )->addColumn(
                    'publication_number',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'Publication Number'
                )->addColumn(
                    'isbn',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'ISBN Book'
                )->addColumn(
                    'product_desc',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    500,
                    ['nullable' => true],
                    'Product Description '
                )->addColumn(
                    'product_type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'product_type' 
                )->addColumn(
                    'subscription_start',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Subscriptions Created YY/MM'
                )->addColumn(
                    'subscription_end',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Subscriptions Deletion YY/MM'
                )->addColumn(
                    'service_flag',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Service Flag'
                )->addColumn(
                    'subscription_type',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Subscription Type'
                )->addColumn(
                    'status',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    [ 'nullable' => true],
                    'Status'
                )->addColumn(
                    'qty',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Subscriptions QTY'
                )->addIndex(
                    $installer->getIdxName('endeavor_lnsubscriptions', ['customer_id']),
                    ['customer_id']
                )->addIndex(
                    $installer->getIdxName('endeavor_lnsubscriptions', ['account_id']),
                    ['account_id']
                )->addForeignKey(
                    $installer->getFkName(
                        'endeavor_lnsubscriptions',
                        'customer_id',
                        'customer_entity',
                        'entity_id'
                    ),
                    'customer_id',
                    $installer->getTable('customer_entity'), 
                    'entity_id',
                    \Magento\Framework\Db\Ddl\Table::ACTION_SET_NULL
                )->addForeignKey(
                    $installer->getFkName(
                        'endeavor_lnsubscriptions',
                        'account_id',
                        'customer_entity',
                        'entity_id'
                    ),
                    'account_id',
                    $installer->getTable('customer_entity'), 
                    'entity_id',
                    \Magento\Framework\Db\Ddl\Table::ACTION_SET_NULL
                )->setComment('Endeavor lnsubscriptions');

            $installer->getConnection()->createTable($table);
        }
        
        
        $setup->endSetup();
    }
}
