<?php
namespace Endeavor\LNSubscriptions\Block\Adminhtml\Subscriptions;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'subscription_id';
        $this->_blockGroup = 'Endeavor_LNSubscriptions';
        $this->_controller = 'adminhtml_subscriptions';

        parent::_construct();

        $id= $this->getRequest()->getParam('id');

        $url = $this->getUrl('endeavor_lnsubscriptions/*/save', ['id'=>$id]);
        $urlBack = $this->getUrl('customer/index/edit', ['id'=>$id]);

        $this->buttonList->update('save', 'label', __('Save Subscription'));
        $this->buttonList->update('save', 'onclick','setLocation("' . $url . '")');
        $this->buttonList->update('back', 'onclick','setLocation("' . $urlBack . '")');
        $this->buttonList->remove('saveandcontinue');
        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

}
