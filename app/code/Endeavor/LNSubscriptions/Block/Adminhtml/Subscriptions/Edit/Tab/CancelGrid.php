<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\LNSubscriptions\Block\Adminhtml\Subscriptions\Edit\Tab;
 
class CancelGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        array $data = []
    ) {
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('cancel_tab_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

    /**
     * 
     * @return type
     */
    protected function _prepareCollection()
    {   
        $id= $this->getRequest()->getParam('id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerCollection = $objectManager->create('Magento\Customer\Model\Customer')->load($id);
        $role = $customerCollection->getCustomerRole();

        $collection = $objectManager->create('Endeavor\LNSubscriptions\Model\Subscriptions')->getCollection();
        if ($role == 60) {
            $collection->addFieldToFilter('status',(int)'1')->addFieldToFilter('customer_id',$id);
        } else {
            $collection->addFieldToFilter('status',(int)'1')->addFieldToFilter('account_id',$id);
        }
        $collection->setPageSize(40); 
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * {@inherent}
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'subscriptions_id',
            [
                'header' => __('Subscriptions Id'),
                'sortable' => false,
                'index' => 'subscriptions_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn(
            'publication_number',
            [
                'header' => __('Publication Number'),
                'sortable' => false,
                'index' => 'publication_number'
            ]
        );
        $this->addColumn(
            'isbn',
            [
                'header' => __('ISBN'),
                'sortable' => false,
                'index' => 'isbn'
            ]
        );
        $this->addColumn(
            'qty',
            [
                'header' => __('QTY'),
                'sortable' => false,
                'index' => 'qty'
            ]
        );
        $this->addColumn(
            'product_desc',
            [
                'header' => __('Product Description'),
                'sortable' => false,
                'index' => 'product_desc'
            ]
        );
        $this->addColumn(
            'product_type',
            [
                'header' => __('Product Type'),
                'sortable' => false,
                'index' => 'product_type'
            ]
        );
        $this->addColumn(
            'subscription_start',
            [
                'header' => __('Subscription Start'),
                'sortable' => false,
                'index' => 'subscription_start'
            ]
        );
        $this->addColumn(
            'subscription_end',
            [
                'header' => __('Subscription End'),
                'sortable' => false,
                'index' => 'subscription_end'
            ]
        );
        $this->addColumn(
            'service_flag',
            [
                'header' => __('Service Flag'),
                'sortable' => false,
                'index' => 'service_flag'
            ]
        );
        $this->addColumn(
            'subscription_type',
            [
                'header' => __('Subscription Type'),
                'sortable' => false,
                'index' => 'subscription_type'
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     *Return Grid URL for JS event handlers
     * 
     * @return String
     */
    public function getGridUrl()
    {
        return $this->getUrl('endeavor_lnsubscriptions/*/cancel', ['_current' => true]);
    }

    /**
    * Return row URL for JS event handlers
    *
    * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $item
    * @return string
    */
   public function getRowUrl($item)
   {
        $id= $this->getRequest()->getParam('id');
        return $this->getUrl('endeavor_lnsubscriptions/*/edit', ['subscriptions_id' => $item->getId(),'id'=>$id]);
   }


}