<?php

namespace Endeavor\LNSubscriptions\Block\Adminhtml\Subscriptions\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('subscriptions_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Subscription Settings'));
    }
    
    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->addTab(
            'hello',
            [
                'label' => __('Products'),
                'url' => $this->getUrl('endeavor_lnsubscriptions/subscriptions/view', ['_current' => true]),
                'class' => 'ajax'
            ]
        );
        return parent::_prepareLayout();
    }
}
