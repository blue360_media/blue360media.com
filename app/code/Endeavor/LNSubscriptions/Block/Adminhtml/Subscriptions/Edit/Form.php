<?php
namespace Endeavor\LNSubscriptions\Block\Adminhtml\Subscriptions\Edit;

/**
 * Adminhtml attachment edit form block
 *
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
     
    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
    }
 
    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('lnsubscriptions_form');
        $this->setTitle(__('Subscriptions Information'));
    }
    
    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('endeavor_lnsubscriptions_subscriptions');
       
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
 
        $form->setHtmlIdPrefix('endeavor_lnsubscriptions_');
 
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );
 
        if ($model->getId()) {
            $fieldset->addField('subscriptions_id', 'hidden', ['name' => 'subscriptions_id']);
        }
        $fieldset->addField(
            'publication_number',
            'text',
            ['name' => 'publication_number', 'label' => __('Publication Number'), 'title' => __('Publication Number'), 'required' => false]
        );
        $fieldset->addField(
            'isbn',
            'text',
            ['name' => 'isbn', 'label' => __('ISBN'), 'title' => __('ISBN'), 'required' => false]
        );
        $fieldset->addField(
            'qty',
            'text',
            ['name' => 'qty', 'label' => __('Subscriptions Quantity'), 'title' => __('Subscriptions Quantity'), 'required' => false]
        );
        $fieldset->addField(
            'product_desc',
            'text',
            ['name' => 'product_desc', 'label' => __('Product Description '), 'title' => __('Product Description '), 'required' => false]
        );
        $fieldset->addField(
            'product_type',
            'text',
            ['name' => 'product_type', 'label' => __('Product Type'), 'title' => __('Product Type'), 'required' => false]
        );
        $fieldset->addField(
            'subscription_start',
            'text',
            ['name' => 'subscription_start', 'label' => __('Subscriptions Created'), 'title' => __('Subscriptions Created'), 'required' => false]
        );
        
        $fieldset->addField(
            'subscription_end',
            'text',
            ['name' => 'subscription_end', 'label' => __('Subscriptions Deletion'), 'title' => __('Subscriptions Deletion'), 'required' => false]
        );
        $fieldset->addField(
            'service_flag',
            'text',
            ['name' => 'service_flag', 'label' => __('Service Flag'), 'title' => __('Service Flag'), 'required' => false]
        );
        $fieldset->addField(
            'subscription_type',
            'text',
            ['name' => 'subscription_type', 'label' => __('Subscription Type'), 'title' => __('Subscription Type'), 'required' => false]
        );
        $fieldset->addField(
            'status',
            'text',
            ['name' => 'status', 'label' => __('Status'), 'title' => __('Status'), 'required' => false]
        );
        $fieldset->addField(
            'id',
            'select',
            [
                'name' => 'id',
                'label' => __(''),
                'title' => __(''),
                'style' => 'visibility: hidden; position: absolute',
                'values' => $this->getAllOptions(),
                'disabled' => false
            ]
        );
        
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
 
        return parent::_prepareForm();
    }
    
    /**
     * Retrieve the current customer id
     * 
     * @return array
     */
    public function getAllOptions()
    {
        $item = array(
            'label' => $this->getRequest()->getParam('id'), 
            'value' => $this->getRequest()->getParam('id')
        );
        $org[] = $item;
        
        return $org;
    }
    
}
