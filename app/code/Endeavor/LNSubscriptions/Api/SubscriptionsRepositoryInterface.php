<?php
namespace Endeavor\LNSubscriptions\Api;

interface SubscriptionsRepositoryInterface
{


    /**
     * Save Subscriptions
     * @param \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface $subscriptions
     * @return \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface $subscriptions
    );

    /**
     * Retrieve Subscriptions
     * @param string $subscriptionsId
     * @return \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($subscriptionsId);

    /**
     * Retrieve Subscriptions matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Endeavor\LNSubscriptions\Api\Data\SubscriptionsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Subscriptions
     * @param \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface $subscriptions
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface $subscriptions
    );

    /**
     * Delete Subscriptions by ID
     * @param string $subscriptionsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($subscriptionsId);
}
