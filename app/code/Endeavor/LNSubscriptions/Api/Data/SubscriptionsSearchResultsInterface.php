<?php
namespace Endeavor\LNSubscriptions\Api\Data;

interface SubscriptionsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get Subscriptions list.
     * @return \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface[]
     */
    public function getItems();

    /**
     * Set cust_id list.
     * @param \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
