<?php
namespace Endeavor\LNSubscriptions\Api\Data;

interface SubscriptionsInterface
{
    /**
     * columns name
     * 
     * @var sting
     */
    const PUBLICATION_NUMBER = 'publication_number';
    const ISBN = 'isbn';
    const SUBSCRIPTION_START = 'subscription_start';
    const CUSTOMER_ID = 'customer_id';
    const SUBSCRIPTION_TYPE = 'subscription_type';
    const SERVICE_FLAG = 'service_flag';
    const STATUS = 'status';
    const SUBSCRIPTION_END = 'subscription_end';
    const ACCOUNT_ID = 'account_id';
    const PRODUCT_DESC = 'product_desc';
    const SUBSCRIPTIONS_ID = 'subscriptions_id';
    const PRODUCT_TYPE = 'product_type';
    const PRODUCT_QTY = 'qty';

    /**
     * Get subscriptions_id
     * @return string|null
     */
    public function getSubscriptionsId();

    /**
     * Set subscriptions_id
     * @param string $subscriptionsId
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setSubscriptionsId($subscriptionsId);

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customer_id
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setCustomerId($customer_id);
    
    
    /**
     * Get Qty
     * @return string|null
     */
    public function getQty();

    /**
     * Get account_id
     * @return string|null
     */
    public function getAccountId();

    /**
     * Set account_id
     * @param string $account_id
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setAccountId($account_id);

    /**
     * Get publication_number
     * @return string|null
     */
    public function getPublicationNumber();

    /**
     * Set publication_number
     * @param string $publication_number
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setPublicationNumber($publication_number);

    /**
     * Get isbn
     * @return string|null
     */
    public function getIsbn();

    /**
     * Set isbn
     * @param string $isbn
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setIsbn($isbn);

    /**
     * Get product_desc
     * @return string|null
     */
    public function getProductDesc();

    /**
     * Set product_desc
     * @param string $product_desc
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setProductDesc($product_desc);

    /**
     * Get product_type
     * @return string|null
     */
    public function getProductType();

    /**
     * Set product_type
     * @param string $product_type
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setProductType($product_type);

    /**
     * Get subscription_start
     * @return string|null
     */
    public function getSubscriptionStart();

    /**
     * Set subscription_start
     * @param string $subscription_start
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setSubscriptionStart($subscription_start);

    /**
     * Get subscription_end
     * @return string|null
     */
    public function getSubscriptionEnd();

    /**
     * Set subscription_end
     * @param string $subscription_end
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setSubscriptionEnd($subscription_end);

    /**
     * Get service_flag
     * @return string|null
     */
    public function getServiceFlag();

    /**
     * Set service_flag
     * @param string $service_flag
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setServiceFlag($service_flag);

    /**
     * Get subscription_type
     * @return string|null
     */
    public function getSubscriptionType();

    /**
     * Set subscription_type
     * @param string $subscription_type
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setSubscriptionType($subscription_type);

    
    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    
    /**
     * Set status
     * @param string $status
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setStatus($status);
    
    /**
     * Set Qty
     * @param string $qty
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setQty($qty);
}
