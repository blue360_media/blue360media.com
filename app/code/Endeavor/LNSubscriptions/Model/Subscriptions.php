<?php


namespace Endeavor\LNSubscriptions\Model;

use Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface;

class Subscriptions extends \Magento\Framework\Model\AbstractModel implements SubscriptionsInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\LNSubscriptions\Model\ResourceModel\Subscriptions');
    }

    /**
     * Get subscriptions_id
     * @return string
     */
    public function getSubscriptionsId()
    {
        return $this->getData(self::SUBSCRIPTIONS_ID);
    }

    /**
     * Set subscriptions_id
     * @param string $subscriptionsId
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setSubscriptionsId($subscriptionsId)
    {
        return $this->setData(self::SUBSCRIPTIONS_ID, $subscriptionsId);
    }

    /**
     * Get customer_id
     * @return string
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * Set customer_id
     * @param string $customer_id
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setCustomerId($customer_id)
    {
        return $this->setData(self::CUSTOMER_ID, $customer_id);
    }

    /**
     * Get account_id
     * @return string
     */
    public function getAccountId()
    {
        return $this->getData(self::ACCOUNT_ID);
    }

    /**
     * Set account_id
     * @param string $account_id
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setAccountId($account_id)
    {
        return $this->setData(self::ACCOUNT_ID, $account_id);
    }

    /**
     * Get publication_number
     * @return string
     */
    public function getPublicationNumber()
    {
        return $this->getData(self::PUBLICATION_NUMBER);
    }

    /**
     * Set publication_number
     * @param string $publication_number
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setPublicationNumber($publication_number)
    {
        return $this->setData(self::PUBLICATION_NUMBER, $publication_number);
    }

    /**
     * Get isbn
     * @return string
     */
    public function getIsbn()
    {
        return $this->getData(self::ISBN);
    }

    /**
     * Set isbn
     * @param string $isbn
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setIsbn($isbn)
    {
        return $this->setData(self::ISBN, $isbn);
    }

    /**
     * Get product_desc
     * @return string
     */
    public function getProductDesc()
    {
        return $this->getData(self::PRODUCT_DESC);
    }

    /**
     * Set product_desc
     * @param string $product_desc
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setProductDesc($product_desc)
    {
        return $this->setData(self::PRODUCT_DESC, $product_desc);
    }

    /**
     * Get product_type
     * @return string
     */
    public function getProductType()
    {
        return $this->getData(self::PRODUCT_TYPE);
    }

    /**
     * Set product_type
     * @param string $product_type
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setProductType($product_type)
    {
        return $this->setData(self::PRODUCT_TYPE, $product_type);
    }

    /**
     * Get subscription_start
     * @return string
     */
    public function getSubscriptionStart()
    {
        return $this->getData(self::SUBSCRIPTION_START);
    }

    /**
     * Set subscription_start
     * @param string $subscription_start
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setSubscriptionStart($subscription_start)
    {
        return $this->setData(self::SUBSCRIPTION_START, $subscription_start);
    }

    /**
     * Get subscription_end
     * @return string
     */
    public function getSubscriptionEnd()
    {
        return $this->getData(self::SUBSCRIPTION_END);
    }

    /**
     * Set subscription_end
     * @param string $subscription_end
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setSubscriptionEnd($subscription_end)
    {
        return $this->setData(self::SUBSCRIPTION_END, $subscription_end);
    }

    /**
     * Get service_flag
     * @return string
     */
    public function getServiceFlag()
    {
        return $this->getData(self::SERVICE_FLAG);
    }

    /**
     * Set service_flag
     * @param string $service_flag
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setServiceFlag($service_flag)
    {
        return $this->setData(self::SERVICE_FLAG, $service_flag);
    }

    /**
     * Get subscription_type
     * @return string
     */
    public function getSubscriptionType()
    {
        return $this->getData(self::SUBSCRIPTION_TYPE);
    }

    /**
     * Set subscription_type
     * @param string $subscription_type
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setSubscriptionType($subscription_type)
    {
        return $this->setData(self::SUBSCRIPTION_TYPE, $subscription_type);
    }

    /**
     * Get status
     * @return string
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * Set status
     * @param string $status
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }
    
    /**
     * Get Qty
     * @return string
     */
    public function getQty()
    {
        return $this->getData(self::PRODUCT_QTY);
    }

    /**
     * Set Qty
     * @param string $qty
     * @return Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface
     */
    public function setQty($qty)
    {
        return $this->setData(self::PRODUCT_QTY, $qty);
    }
}
