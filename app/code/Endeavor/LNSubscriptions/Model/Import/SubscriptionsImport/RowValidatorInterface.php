<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\LNSubscriptions\Model\Import\SubscriptionsImport;

interface RowValidatorInterface extends \Magento\Framework\Validator\ValidatorInterface
{
    /**
     * @var String Message For Invalid Subscription Values
     */
    const ERROR_INVALID_SUBID= 'InvalidValueSUBSCRIPTIONS_ID';
    
    /**
     * @var String Message For Invalid Subscription Id
     */
    const ERROR_SUBID_IS_EMPTY = 'EmptySUBSCRIPTIONS_ID';
       
    /**
     * Initialize validator
     *
     * @return $this
     */
    public function init($context);
}
