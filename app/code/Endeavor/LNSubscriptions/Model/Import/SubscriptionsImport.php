<?php
namespace Endeavor\LNSubscriptions\Model\Import;

use Endeavor\LNSubscriptions\Model\Import\SubscriptionsImport\RowValidatorInterface as ValidatorInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;

class SubscriptionsImport extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity
{
    /**
     * @var String Columns Name in database
     */
    const PUBLICATION_NUMBER = 'publication_number';
    const ISBN = 'isbn';
    const SUBSCRIPTION_START = 'subscription_start';
    const CUSTOMER_ID = 'customer_id';
    const SUBSCRIPTION_TYPE = 'subscription_type';
    const SERVICE_FLAG = 'service_flag';
    const STATUS = 'status';
    const SUBSCRIPTION_END = 'subscription_end';
    const ACCOUNT_ID = 'account_id';
    const PRODUCT_DESC = 'product_desc';
    const SUBSCRIPTIONS_ID = 'subscriptions_id';
    const PRODUCT_TYPE = 'product_type';
    const PRODUCT_QTY = 'qty';
	
    const TABLE_Entity = 'endeavor_lnsubscriptions';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = [
        ValidatorInterface::ERROR_SUBID_IS_EMPTY => 'Subscriptions Id is empty',
    ];

    /**
     * Permanent entity columns.
     *
     * @var string[]
     */
    protected $_permanentAttributes = [self::SUBSCRIPTIONS_ID];
     
    /**
     * If we should check column names
     *
     * @var bool
     */
    protected $needColumnCheck = true;
    
    /**
     *
     * @var \Magento\Customer\Model\GroupFactory 
     */
    protected $groupFactory;
    
    /**
     * Valid column names
     *
     * @array
     */
    protected $validColumnNames = [
        self::SUBSCRIPTIONS_ID,
        self::CUSTOMER_ID,
        self::ACCOUNT_ID,
        self::PUBLICATION_NUMBER,
        self::ISBN,
        self::PRODUCT_DESC,
        self::PRODUCT_TYPE,
        self::SUBSCRIPTION_START,
        self::SUBSCRIPTION_END,
        self::SERVICE_FLAG,
        self::SUBSCRIPTION_TYPE,
        self::STATUS,
        Self::PRODUCT_QTY,
    ];

    /**
     * Need to log in import history
     *
     * @var bool
     */
    protected $logInHistory = true;


    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $_connection;
    
    /**
     *
     * @var Magento\Framework\App\ResourceConnection 
     */
    protected $_resource;

    /**
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Magento\Customer\Model\GroupFactory $groupFactory
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->groupFactory = $groupFactory;
    }
    
    /**
     * 
     * @return @array
     */
    public function getValidColumnNames()
    {
        return $this->validColumnNames;
    }

    /**
     * Entity type code getter. table name
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return 'endeavor_lnsubscriptions';
    }

    /**
     * Row validation.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum)
    {
        $title = false;

        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
            if (!isset($rowData[self::SUBSCRIPTIONS_ID]) || empty($rowData[self::SUBSCRIPTIONS_ID])) {
                $this->addRowError(ValidatorInterface::ERROR_SUBID_IS_EMPTY, $rowNum);
                return false;
            }

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }


    /**
     * Create Advanced price data from raw data.
     *
     * @throws \Exception
     * @return bool Result of operation.
     */
    protected function _importData()
    {
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            $this->deleteEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }

        return true;
    }
    
    /**
     * Save newsletter subscriber
     *
     * @return $this
     */
    public function saveEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }
    
    /**
     * Replace newsletter subscriber
     *
     * @return $this
     */
    public function replaceEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }
    
    /**
     * Deletes newsletter subscriber data from raw data.
     *
     * @return $this
     */
    public function deleteEntity()
    {
        $listTitle = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);
                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowTtile = $rowData[self::SUBSCRIPTIONS_ID];
                    $listTitle[] = $rowTtile;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }
        if ($listTitle) {
            $this->deleteEntityFinish(array_unique($listTitle),self::TABLE_Entity);
        }
        return $this;
    }
    
    /**
     * Save and replace newsletter subscriber
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $listTitle = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError(ValidatorInterface::ERROR_SUBID_IS_EMPTY, $rowNum);
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                $rowTtile= $rowData[self::SUBSCRIPTIONS_ID];
                $listTitle[] = $rowTtile;
                $entityList[$rowTtile][] = [
                    self::SUBSCRIPTIONS_ID => $rowData[self::SUBSCRIPTIONS_ID],
                    self::CUSTOMER_ID => $rowData[self::CUSTOMER_ID],
                    self::ACCOUNT_ID => $rowData[self::ACCOUNT_ID],
                    self::PUBLICATION_NUMBER => $rowData[self::PUBLICATION_NUMBER],
                    self::ISBN => $rowData[self::ISBN],
                    self::PRODUCT_DESC => $rowData[self::PRODUCT_DESC],
                    self::PRODUCT_TYPE => $rowData[self::PRODUCT_TYPE],
                    self::SUBSCRIPTION_START => $rowData[self::SUBSCRIPTION_START],
                    self::SUBSCRIPTION_END => $rowData[self::SUBSCRIPTION_END],
                    self::SERVICE_FLAG => $rowData[self::SERVICE_FLAG],
                    self::SUBSCRIPTION_TYPE => $rowData[self::SUBSCRIPTION_TYPE],
                    self::STATUS => $rowData[self::STATUS],
                    self::PRODUCT_QTY => $rowData[self::PRODUCT_QTY],
                ];
            }            
            if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                if ($listTitle) {
                    if ($this->deleteEntityFinish(array_unique(  $listTitle), self::TABLE_Entity)) {
                        $this->saveEntityFinish($entityList, self::TABLE_Entity);
                    }
                }
            } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                $this->saveEntityFinish($entityList, self::TABLE_Entity);
            }
        }
        return $this;
    }
    
    /**
     * Save product prices.
     *
     * @param array $priceData
     * @param string $table
     * @return $this
     */
    protected function saveEntityFinish(array $entityData, $table)
    {
        if ($entityData) {
            $tableName = $this->_connection->getTableName($table);
            $entityIn = [];
            foreach ($entityData as $id => $entityRows) {
                    foreach ($entityRows as $row) {
                        $entityIn[] = $row;
                    }
            }
            if ($entityIn) {
                $this->_connection->insertOnDuplicate($tableName, $entityIn,[
                    self::SUBSCRIPTIONS_ID,
                    self::CUSTOMER_ID,
                    self::ACCOUNT_ID,
                    self::PUBLICATION_NUMBER,
                    self::ISBN,
                    self::PRODUCT_DESC,
                    self::PRODUCT_TYPE,
                    self::SUBSCRIPTION_START,
                    self::SUBSCRIPTION_END,
                    self::SERVICE_FLAG,
                    self::SUBSCRIPTION_TYPE,
                    self::STATUS,
                    self::PRODUCT_QTY,
                ]);
            }
        }
        return $this;
    }
    
    /**
     * Delete Subscription.
     * 
     * @param array $listTitle
     * @param type $table
     * @return boolean
     */
    protected function deleteEntityFinish(array $listTitle, $table)
    {
        if ($table && $listTitle) {
                try {
                    $this->countItemsDeleted += $this->_connection->delete(
                        $this->_connection->getTableName($table),
                        $this->_connection->quoteInto('subscriptions_id IN (?)', $listTitle)
                    );
                    return true;
                } catch (\Exception $e) {
                    return false;
                }

        } else {
            return false;
        }
    }
}
