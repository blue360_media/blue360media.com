<?php


namespace Endeavor\LNSubscriptions\Model;

use Endeavor\LNSubscriptions\Api\SubscriptionsRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Endeavor\LNSubscriptions\Model\ResourceModel\Subscriptions as ResourceSubscriptions;
use Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterfaceFactory;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SortOrder;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\NoSuchEntityException;
use Endeavor\LNSubscriptions\Api\Data\SubscriptionsSearchResultsInterfaceFactory;
use Endeavor\LNSubscriptions\Model\ResourceModel\Subscriptions\CollectionFactory as SubscriptionsCollectionFactory;

class SubscriptionsRepository implements SubscriptionsRepositoryInterface
{

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $SubscriptionsFactory;

    private $storeManager;

    protected $SubscriptionsCollectionFactory;

    protected $dataSubscriptionsFactory;

    protected $dataObjectHelper;

    protected $resource;


    /**
     * @param ResourceSubscriptions $resource
     * @param SubscriptionsFactory $subscriptionsFactory
     * @param SubscriptionsInterfaceFactory $dataSubscriptionsFactory
     * @param SubscriptionsCollectionFactory $subscriptionsCollectionFactory
     * @param SubscriptionsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceSubscriptions $resource,
        SubscriptionsFactory $subscriptionsFactory,
        SubscriptionsInterfaceFactory $dataSubscriptionsFactory,
        SubscriptionsCollectionFactory $subscriptionsCollectionFactory,
        SubscriptionsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->subscriptionsFactory = $subscriptionsFactory;
        $this->subscriptionsCollectionFactory = $subscriptionsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataSubscriptionsFactory = $dataSubscriptionsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface $subscriptions
    ) {
        /* if (empty($subscriptions->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $subscriptions->setStoreId($storeId);
        } */
        try {
            $this->resource->save($subscriptions);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the subscriptions: %1',
                $exception->getMessage()
            ));
        }
        return $subscriptions;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($subscriptionsId)
    {
        $subscriptions = $this->subscriptionsFactory->create();
        $subscriptions->load($subscriptionsId);
        if (!$subscriptions->getId()) {
            throw new NoSuchEntityException(__('Subscriptions with id "%1" does not exist.', $subscriptionsId));
        }
        return $subscriptions;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $collection = $this->subscriptionsCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $items = [];
        
        foreach ($collection as $subscriptionsModel) {
            $subscriptionsData = $this->dataSubscriptionsFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $subscriptionsData,
                $subscriptionsModel->getData(),
                'Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface'
            );
            $items[] = $this->dataObjectProcessor->buildOutputDataArray(
                $subscriptionsData,
                'Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface'
            );
        }
        $searchResults->setItems($items);
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Endeavor\LNSubscriptions\Api\Data\SubscriptionsInterface $subscriptions
    ) {
        try {
            $this->resource->delete($subscriptions);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Subscriptions: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($subscriptionsId)
    {
        return $this->delete($this->getById($subscriptionsId));
    }
}
