<?php


namespace Endeavor\LNSubscriptions\Model\ResourceModel\Subscriptions;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Endeavor\LNSubscriptions\Model\Subscriptions',
            'Endeavor\LNSubscriptions\Model\ResourceModel\Subscriptions'
        );
    }
}
