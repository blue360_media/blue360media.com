<?php


namespace Endeavor\LNSubscriptions\Model\ResourceModel;

class Subscriptions extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('endeavor_lnsubscriptions', 'subscriptions_id');
    }
}
