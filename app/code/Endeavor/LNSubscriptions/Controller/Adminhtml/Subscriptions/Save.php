<?php
namespace Endeavor\LNSubscriptions\Controller\Adminhtml\Subscriptions;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{
    /**
     *
     * @var Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    protected $dataPersistor;

    /**
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
                
        if ($data) {
            $id = $this->getRequest()->getParam('subscriptions_id');
            $model = $this->_objectManager->create('Endeavor\LNSubscriptions\Model\Subscriptions')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addError(__('This Subscriptions no longer exists.'));
                return $resultRedirect->setPath('customer/index/edit', ['id' => $this->getRequest()->getParam('id')]);
            }
        
            $model->setData($data);
        
            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved the Subscriptions.'));
                $this->dataPersistor->clear('endeavor_lnsubscriptions_subscriptions');
        
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('customer/index/edit', ['id' => $this->getRequest()->getParam('id')]);
                }
                return $resultRedirect->setPath('customer/index/edit', ['id' => $this->getRequest()->getParam('id')]);
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the Subscriptions.'));
            }
        
            $this->dataPersistor->set('endeavor_lnsubscriptions_subscriptions', $data);
            return $resultRedirect->setPath('*/*/edit', ['subscriptions_id' => $this->getRequest()->getParam('subscriptions_id')]);
        }
            return $resultRedirect->setPath('customer/index/edit', ['id' => $this->getRequest()->getParam('id')]);
    }
}
