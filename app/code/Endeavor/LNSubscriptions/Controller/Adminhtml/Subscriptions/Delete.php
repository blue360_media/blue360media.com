<?php
namespace Endeavor\LNSubscriptions\Controller\Adminhtml\Subscriptions;

class Delete extends \Endeavor\LNSubscriptions\Controller\Adminhtml\Subscriptions
{
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('subscriptions_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create('Endeavor\LNSubscriptions\Model\Subscriptions');
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccess(__('You deleted the Subscriptions.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['subscriptions_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can\'t find a Subscriptions to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
