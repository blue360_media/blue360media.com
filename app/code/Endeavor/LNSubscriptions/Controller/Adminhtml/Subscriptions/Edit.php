<?php
namespace Endeavor\LNSubscriptions\Controller\Adminhtml\Subscriptions;

class Edit extends \Endeavor\LNSubscriptions\Controller\Adminhtml\Subscriptions
{
    /**
     *
     * @var Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id= $this->getRequest()->getParam('subscriptions_id');

        $model = $this->_objectManager->create('Endeavor\LNSubscriptions\Model\Subscriptions');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This Subscriptions no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('endeavor_lnsubscriptions_subscriptions', $model);
        
        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Subscriptions') : __('New Subscriptions'),
            $id ? __('Edit Subscriptions') : __('New Subscriptions')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Subscriptionss'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Subscriptions'));
        return $resultPage;
    }
}
