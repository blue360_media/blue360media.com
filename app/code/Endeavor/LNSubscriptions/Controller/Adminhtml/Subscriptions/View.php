<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\LNSubscriptions\Controller\Adminhtml\Subscriptions;

class View extends \Magento\Customer\Controller\Adminhtml\Index{
    
    /**
     * View action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
