<?php
namespace Endeavor\LNSubscriptions\Controller\Adminhtml;

abstract class Subscriptions extends \Magento\Backend\App\Action
{
    /**
     *
     * @var \Magento\Framework\Registry $coreRegistry 
     */
    protected $_coreRegistry;
    
    /**
     * 
     * Menu Name
     * 
     *@var string
     */
    const ADMIN_RESOURCE = 'Endeavor_LNSubscriptions::Subscriptions';

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Endeavor'), __('Endeavor'))
            ->addBreadcrumb(__('Subscriptions'), __('Subscriptions'));
        return $resultPage;
    }
}
