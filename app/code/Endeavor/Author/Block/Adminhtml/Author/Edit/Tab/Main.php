<?php
namespace Endeavor\Author\Block\Adminhtml\Author\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;
    
    /**
    * @var \Magento\Catalog\Model\Category
    */
    protected $_subcategories;
    
    /**
    * @var \Magento\Directory\Model\ResourceModel\Country\CollectionFactory
    */
    protected $_countryCollectionFactory;
    
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Catalog\Model\Category $subcategories
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Catalog\Model\Category $subcategories,
        array $data = []
    ) {
       
        $this->_subcategories = $subcategories;
        $this->_countryCollectionFactory = $countryCollectionFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Endeavor\CustomerSegments\Model\ContCustomerSegmentsact */
        $model = $this->_coreRegistry->registry('author');
        
        $isElementDisabled = false;
        
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('authors_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Authors')]);

        if ($model->getId()) {
            $fieldset->addField('author_id', 'hidden', ['name' => 'author_id']);
        }
                
        $fieldset->addField(
            'image',
            'image',
            array(
                'name' => 'image',
                'label' => __('Author Image'),
                'title' => __('Author Image'),
                'required' => false
            )
        );

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Author Name'),
                'title' => __('Author Name'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'nickname',
            'text',
            [
                'name' => 'nickname',
                'label' => __('Author Nick Name'),
                'title' => __('Author Nick Name'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'email',
            'text',
            [
                'name' => 'email',
                'label' => __('Author Email'),
                'title' => __('Author Email'),
                'required' => false,
                'formElement'=>'email',
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'mobile',
            'text',
            [
                'name' => 'mobile',
                'label' => __('Author Mobile'),
                'title' => __('Author Mobile'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'country',
            'select',
            [
                'name' => 'country',
                'label' => __('Country'),
                'title' => __('Country'),
                'required' => false,
                'values' => $this->getCountries(),
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'state',
            'select',
            [
                'name' => 'state',
                'label' => __('State'),
                'title' => __('State'),
                'values' => array(
                    '' => '',
                    'Alabama'=> 'Alabama',
                    'Alaska' =>'Alaska',
                    'Arizona'=> 'Arizona',
                    'Arkansas' =>'Arkansas',
                    'California'=> 'California',
                    'Colorado' =>'Colorado',
                    'Connecticut'=> 'Connecticut',
                    'Delaware' =>'Delaware',
                    'Federal'=> 'Federal',
                    'Florida' =>'Florida',
                    'Georgia' => 'Georgia',
                    'Hawaii' => 'Hawaii',
                    'Idaho' => 'Idaho',
                    'Illinois' => 'Illinois',
                    'Indiana' => 'Indiana',
                    'Iowa' => 'Iowa',
                    'Kansas' => 'Kansas',
                    'Kentucky' => 'Kentucky',
                    'Louisiana' => 'Louisiana',
                    'Maine' => 'Maine',
                    'Maryland' => 'Maryland',
                    'Massachusetts' => 'Massachusetts',
                    'Michigan' => 'Michigan',
                    'Minnesota' => 'Minnesota',
                    'Mississippi' => 'Mississippi',
                    'Missouri' => 'Missouri',
                    'Montana' => 'Montana',
                    'National' => 'National',
                    'Nebraska' => 'Nebraska',
                    'Nevada' => 'Nevada',
                    'New Hampshire' => 'New Hampshire',
                    'New Jersey' => 'New Jersey',
                    'New Mexico' => 'New Mexico',
                    'New York' => 'New York',
                    'North Carolina' => 'North Carolina',
                    'North Dakota' => 'North Dakota',
                    'Ohio' => 'Ohio',
                    'Oklahoma' => 'Oklahoma',
                    'Oregon' => 'Oregon',
                    'Pennsylvania' => 'Pennsylvania',
                    'Puerto Rico' => 'Puerto Rico',
                    'Rhode Island' => 'Rhode Island',
                    'South Carolina' => 'South Carolina',
                    'South Dakota' => 'South Dakota',
                    'Tennessee' => 'Tennessee',
                    'Texas' => 'Texas',
                    'Utah' => 'Utah',
                    'Vermont' => 'Vermont',
                    'Virgin Islands' => 'Virgin Islands',
                    'Virginia' => 'Virginia',
                    'Washington' => 'Washington',
                    'West Virginia' => 'West Virginia',
                    'Wisconsin' => 'Wisconsin',
                    'Wyoming' =>'Wyoming'

                    ),
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'city',
            'text',
            [
                'name' => 'city',
                'label' => __('City'),
                'title' => __('City'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'zip',
            'text',
            [
                'name' => 'zip',
                'label' => __('ZIP'),
                'title' => __('ZIP'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'street',
            'text',
            [
                'name' => 'street',
                'label' => __('Street'),
                'title' => __('Street'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'about_author',
            'Endeavor\Author\Block\Adminhtml\Author\Edit\Editor\Editor',
            [
                'name' => 'about_author',
                'label' => __('About Author'),
                'title' => __('About Author'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Authors Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Authors Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    
    /**
     * Retrieve Country Collection
     *
     * @return array
     */
    public function getCountryCollection()
    {
        $collection = $this->_countryCollectionFactory->create()->loadByStore();
        return $collection;
    }
 
    /**
     * Retrieve list of top destinations countries
     *
     * @return array
     */
    protected function getTopDestinations()
    {
        $destinations = (string)$this->_scopeConfig->getValue(
            'general/country/destinations',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return !empty($destinations) ? explode(',', $destinations) : [];
    }

    /**
     * Retrieve list of countries in array option
     *
     * @return array
     */
    public function getCountries()
    {
        return $options = $this->getCountryCollection()
                ->setForegroundCountries($this->getTopDestinations())
                    ->toOptionArray();
    }
}
