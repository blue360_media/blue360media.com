<?php

namespace Endeavor\Author\Block\Adminhtml\Sales;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return void
     */
    protected function _construct() {
        
        $this->_objectId = 'publication_id';
        $this->_blockGroup = 'Endeavor_Author';
        $this->_controller = 'adminhtml_sales';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Sales Percent'));
        $this->buttonList->add(
                'saveandcontinue', [
            'label' => __('Save and Continue Edit'),
            'class' => 'save',
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                ],
            ]
                ], -100
        );

        $this->buttonList->update('delete', 'label', __('Delete Author'));
        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
        $this->buttonList->remove('back');
        $this->buttonList->remove('saveandcontinue');
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('author/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }
    
    /**
     * Get Publication Id of Current Product
     * 
     * @return int
     */
    public function getPublicationId()
    {
        return $this->getRequest()->getParam('publication_id');
    }
}
