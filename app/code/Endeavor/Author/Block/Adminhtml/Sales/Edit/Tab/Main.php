<?php
namespace Endeavor\Author\Block\Adminhtml\Sales\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;
    
    /**
    * @var \Magento\Catalog\Model\Category
    */
    protected $_subcategories;
    
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Catalog\Model\Category $subcategories,
        array $data = []
    ) {
       
        $this->_subcategories = $subcategories;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Endeavor\CustomerSegments\Model\ContCustomerSegmentsact */
        $model = $this->_coreRegistry->registry('author');
       // echo "<div style='padding: 10px 5px; background: lightgreen; color: red'> Please enter this identifier in the publication identifier field :". $this->getRequest()->getParam('publication_id')."</div>";
        $isElementDisabled = false;
        
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('authors_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Sales Percent')]);

        if ($model->getPublicationId()) {
            $fieldset->addField('publication_id', 'hidden', ['name' => 'publication_id']);
        }
        
        $fieldset->addField(
            'publication_id',
            'select',
            [
                'name' => 'publication_id',
                'label' => __('Publication Identifier'),
                'title' => __('Publication Identifier'),
                'required' => true,
                'style' => 'background-image: none; padding: 4px 10px',
                'values' => $this->getAllOptions(),
                'disabled' => $isElementDisabled
            ]
        );
                
        $fieldset->addField(
            'author_percent',
            'text',
            [
                'name' => 'author_percent',
                'label' => __('Sales Percent'),
                'title' => __('Sales Percent'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
        
       
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Author Sales Percent');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Author Sales Percent');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    
    /**
     * Assign the publication id to an array
     *
     * @return array
     */
    public function getAllOptions()
    {
        $item = array(
            'label' => $this->getRequest()->getParam('publication_id'), 
            'value' => $this->getRequest()->getParam('publication_id')
        );
        $org[] = $item;
        
        return $org;
    }
    
}
