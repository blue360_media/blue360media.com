<?php
namespace Endeavor\Author\Model\ResourceModel\ReportsAuthor;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Author\Model\ReportsAuthor', 'Endeavor\Author\Model\ResourceModel\ReportsAuthor');
    }
}
