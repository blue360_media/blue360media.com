<?php
namespace Endeavor\Author\Model\ResourceModel;


class Author extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('endeavor_author', 'author_id');
    }
}
