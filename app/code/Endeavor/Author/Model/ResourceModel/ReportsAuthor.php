<?php
namespace Endeavor\Author\Model\ResourceModel;


class ReportsAuthor extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('endeavor_author_reports', 'report_id');
    }
}
