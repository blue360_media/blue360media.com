<?php
namespace Endeavor\Author\Model\ResourceModel\Books;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'publication_id';

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Author\Model\Books', 'Endeavor\Author\Model\ResourceModel\Books');
    }
}
