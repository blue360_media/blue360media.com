<?php
namespace Endeavor\Author\Model\ResourceModel;


class Books extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('endeavor_author_books', 'entity_id');
        
    }
}
