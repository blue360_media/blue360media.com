<?php
namespace Endeavor\Author\Model\ResourceModel;


class SalesAuthor extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('endeavor_author_sales', 'percent_id');
    }
}
