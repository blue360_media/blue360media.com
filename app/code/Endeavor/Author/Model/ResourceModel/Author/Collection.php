<?php
namespace Endeavor\Author\Model\ResourceModel\Author;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Author\Model\Author', 'Endeavor\Author\Model\ResourceModel\Author');
    }
}
