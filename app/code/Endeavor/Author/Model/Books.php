<?php
namespace Endeavor\Author\Model;
use Magento\Framework\DataObject\IdentityInterface;

class Books extends \Magento\Framework\Model\AbstractModel implements IdentityInterface
{

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Author\Model\ResourceModel\Books');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
}

