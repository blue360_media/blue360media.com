<?php
namespace Endeavor\Author\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;
 
/**
 * Custom Attribute Renderer
 *
 * @author      Webkul Core Team <support@webkul.com>
 */
class Authors extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var OptionFactory
     */
    protected $optionFactory;
 
    /**
     * Get all Authors
     *
     * @return array
     */
    public function getAllOptions()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collect = $objectManager->create('Endeavor\Author\Model\Author')->getCollection();
        $org=[];
        foreach($collect as $value) {
            
            $item = array(
                'label' => $value->getName(), 
                'value' => $value->getAuthorId()
            );
            $org[] = $item;
        }
        return $org;
    }
}