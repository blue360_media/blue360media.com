<?php
namespace Endeavor\Author\Model;

class Author extends \Magento\Framework\Model\AbstractModel
{
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Endeavor\Author\Model\ResourceModel\Author $resource,
        \Endeavor\Author\Model\ResourceModel\Author\Collection $resourceCollection
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
      }
}

