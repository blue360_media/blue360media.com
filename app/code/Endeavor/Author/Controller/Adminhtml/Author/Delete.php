<?php

namespace Endeavor\Author\Controller\Adminhtml\Author;

/**
 * Delete Author.
 *
 * @author Nadeem Khleif
 */
class Delete extends \Magento\Backend\App\Action
{

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('author_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Endeavor\Author\Model\Author');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The Author has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['author_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find an author to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
