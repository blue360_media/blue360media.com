<?php

namespace Endeavor\Author\Controller\Adminhtml\Sales;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\TestFramework\ErrorLog\Logger;

/**
 * Save Sales Percent.
 *
 * @author Nadeem Khleif
 */
class Save extends \Magento\Backend\App\Action
{
    
    /**
     * @var \Endeavor\Author\Model\ResourceModel\Author\CollectionFactory
     */
    protected $_authorCollectionFactory;
      
    /**
    * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
    */
    protected $timezoneInterface;
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Registry
     * @param \Endeavor\Author\Model\ResourceModel\Author\CollectionFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        \Endeavor\Author\Model\ResourceModel\Author\CollectionFactory $authorCollectionFactory
    ) {
        $this->_contactCollectionFactory = $authorCollectionFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        $model = $this->_objectManager->create('Endeavor\Author\Model\SalesAuthor');
        $collect = $this->_objectManager->create('Endeavor\Author\Model\SalesAuthor')->getCollection();
        $model_book = $this->_objectManager->create('Endeavor\Author\Model\Books');
        $data['author_id'] = '';
        $data['book_id'] = $data['publication_id'];
        foreach ($model_book->getCollection() as $item) {
            if($item->getPublicationId() == $data['publication_id']) {
                $data['author_id'] = $item->getAuthorId();
                $model_book->load($item->getEntityId())->setSalesPercent($data['author_percent']);
                $model_book->save();
            }
        }
        if ($data) {
            if(count($collect) == 0){
            $model->setData($data);
            }
            foreach($collect as $item){
                if($item->getAuthorId() == $data['author_id'] && $item->getBookId() == $data['book_id']){
                    $model->load($item->getPercentId())->setData('author_percent',$data['author_percent']);
                    break;
                } else {
                    $model->setData($data);
                }
            }
            try {
                $model->save();
                
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('author/books/view',['author_id' => $data['author_id'], '_current' => true]);
                }
                return $resultRedirect->setPath('author/books/view', ['author_id' => $data['author_id'], '_current' => true]);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
             
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('author/books/view', ['author_id' => $data['author_id']]);
        }
        exit;
        return $resultRedirect->setPath('author/books/view');
    }
    
}
