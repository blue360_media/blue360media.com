<?php

namespace Endeavor\Author\Controller\Adminhtml\Books;

use Magento\Backend\App\Action;

/**
 * Author Books View.
 *
 * @author Nadeem Khleif
 */
class View extends \Magento\Backend\App\Action
{
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $_productCollectionFactory;
    
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    
    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    protected $_stockItemRepository;
    
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     * @param \Magento\Catalog\Model\ProductRepository
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
         Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        $this->_productRepository = $productRepository;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_stockItemRepository = $stockItemRepository;
        parent::__construct($context);
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        
        return $resultPage;
    }

    /**
     * Edit Author
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute() {
        $id = $this->getRequest()->getParam('author_id');
        $bookCollection = $this->_objectManager->get('Endeavor\Author\Model\Books')->getCollection();
            
        foreach($bookCollection as $item){
            $model_book = $this->_objectManager->create('Endeavor\Author\Model\Books');
            $model_book->load($item->getEntityId());
            $model_book->delete();
        } 
        $model = $this->_objectManager->create('Endeavor\Author\Model\Author');
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('author_name',array('in' => array((int)$id)));
        
        foreach ($collection as $product){
                        
            $model_book = $this->_objectManager->create('Endeavor\Author\Model\Books');
            $model_book->setData('publication_id',$product->getId());
            $model_book->setData('author_name',$this->getAuthor($id)->getName());
            $model_book->setData('author_id',$id);
            $model_book->setData('publication_name',$product->getName());
            $model_book->setData('sku',$product->getSku());
            $model_book->setData('publication_qty',$this->getStockItem($product->getId())->getQty());
            $model_book->setData('publication_price',$product->getFinalPrice());
            $model_book->setData('jurisdiction',$product->getResource()->getAttribute('jurisdiction')->getFrontend()->getValue($product));
            $model_book->setData('sales_percent',$this->getAuthorPercent($product->getId(), $id));
            $model_book->save();  
        }

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This author no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('author', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
                $id ? __('Edit Author') : __('New Author'), $id ? __('Edit Author') : __('Author Books')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Authors'));
        $resultPage->getConfig()->getTitle()
                ->prepend($model->getId() ? $model->getTitle() : __('Author Books'));

        return $resultPage;
    }
    
    /**
     * Get Author Record
     * 
     * @param int Author $Id
     * @return array of Author Information
     */   
    public function getAuthor($id)
    {
        $orderDatamodel= $this->_objectManager->get('Endeavor\Author\Model\Author');
        $author= $orderDatamodel->load($id);
        
        return $author;
    }
    
    /**
     * Return stock product object.
     * 
     * @param int product id.
     * @return object stock product.
     */
    public function getStockItem($productId)
    {
        return $this->_stockItemRepository->get($productId);
    }
    
    /**
     * Return the author percent for each publication.
     * 
     * @param int product id.
     * @param int author id.
     * @return float author sales percent.
     */
    public function getAuthorPercent($productId, $authorId)
    {
        $collect = $this->_objectManager->create('Endeavor\Author\Model\SalesAuthor')->getCollection();
        $salesPercent=0;
        foreach($collect as $item){
            if($item->getBookId() == $productId && $item->getAuthorId() == $authorId){
                $salesPercent= $item->getAuthorPercent();
            }
        }
        return $salesPercent;
    }
}
