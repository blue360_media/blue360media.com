<?php

namespace Endeavor\Author\Controller\Adminhtml\Statistics;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\TestFramework\ErrorLog\Logger;

/**
 * Show Author Reports.
 *
 * @author Nadeem Khleif
 */
class Save extends \Magento\Backend\App\Action
{
    
    /**
     * @var \Endeavor\Author\Model\ResourceModel\Author\CollectionFactory
     */
    protected $_authorCollectionFactory;
    
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRepositoryInterface;
    
    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;
    
    /**
     * \Magento\Backend\Helper\Js $jsHelper
     * @param Action\Context $context
     */
    public function __construct(
        Context $context,
        \Endeavor\Author\Model\ResourceModel\Author\CollectionFactory $authorCollectionFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
        
    ) {
        
        $this->_contactCollectionFactory = $authorCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->_customerRepositoryInterface = $customerRepositoryInterface;
        $this->_date = $date;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();

        $reportCollection = $this->_objectManager->get('Endeavor\Author\Model\ReportsAuthor')->getCollection();

        if(count($reportCollection) == 0){
            $this->saveReportCollection($data);
        } else {
            $this->deleteCollection();
            $this->saveReportCollection($data);
        }

        return $resultRedirect->setPath('*/*/view', ['author_id' => $this->getRequest()->getParam('author_id')]);
    }
    
    /**
     * Get Author Record
     * 
     * @param int Author $Id
     * @return array of Author Information
     */  
    public function getAuthor($id)
    {
        $orderDatamodel= $this->_objectManager->get('Endeavor\Author\Model\Author');
        $author= $orderDatamodel->load($id);
        
        return $author;
    }
    
    /**
     * Get Customer Name
     * 
     * @param int Customer $Id
     * @return string Customer Name
     */  
    public function getCustomer($id)
    {
        $customer= $this->_customerRepositoryInterface->getById($id);
        return $customer->getFirstname()." ".$customer->getlastname();
    }
    
    /**
     * Get Customer Group
     * 
     * @param int Customer $Id
     * @return string Customer Group
     */  
    public function getCustomerGroup($id)
    {
        $model = $this->_objectManager->create('Magento\Customer\Model\Group');
        $customer= $this->_customerRepositoryInterface->getById($id);
        return $model->load($customer->getGroupId())->getCode();
    }
    
    /**
     * Get Specific Product
     * 
     * @param int Product $Id
     * @return string Customer Group
     */
    public function getProductById($id)
    {
        return $this->_productRepository->getById($id);
    }
    
    /**
     * Save Report Information temporarily
     * 
     * @param array Report Info.
     * @return void
     */
    public function saveReportCollection($data)
    {
        $id = $data['author_id'];
        $author_name = $this->getAuthor($id)->getName();
        $orderItem = $this->_objectManager->get('Magento\Sales\Model\Order\Item')->getCollection();
        $orders = $this->_objectManager->get('Magento\Sales\Model\Order');
        $to_date='';
        
        if(empty($data['to_date'])){
            $to_date = $this->_date->gmtDate('Y-m-d');
        } else {
            $to_date = $data['to_date'];
        }
      
        foreach($orderItem as $item){
            $product= $this->getProductById($item->getProductId());
            $product_type= $product->getResource()->getAttribute('pub_type')->getFrontend()->getValue($product);
            $author= $product->getResource()->getAttribute('author_name')->getFrontend()->getValue($product);
            $jurisdiction= $product->getResource()->getAttribute('jurisdiction')->getFrontend()->getValue($product);
            if($author == $author_name){    
                if(strtotime($this->_date->gmtDate('Y-m-d',$item->getCreatedAt())) >= strtotime($data['from_date']) && strtotime($this->_date->gmtDate('Y-m-d',$item->getCreatedAt())) <= strtotime($to_date)){
                    $model = $this->_objectManager->create('Endeavor\Author\Model\ReportsAuthor');
                    $model->setData('author_name',$author_name);
                    $model->setData('publication_name',$item->getName().'- '.$product_type);
                    $model->setData('publication_qty',$item->getQtyOrdered());
                    $model->setData('total',$item->getRowTotal());
                    $model->setData('jurisdiction',$jurisdiction);
                    $model->setData('customer_name',$this->getCustomer($orders->load($item->getOrderId())->getCustomerId()));
                    $model->setData('customer_group',$this->getCustomerGroup($orders->load($item->getOrderId())->getCustomerId()));
                    $model->setData('author_percent',$this->getAuthorPercent($item->getRowTotal(), $item->getProductId(), $id));
                    $model->save();   
                }         
            }
        }
    }
    
    /**
     * Delete the report temporarily information.
     * 
     * @return void.
     */
    public function deleteCollection()
    {
        
        $reportCollection = $this->_objectManager->get('Endeavor\Author\Model\ReportsAuthor')->getCollection();
        
        foreach($reportCollection as $item){
            $model = $this->_objectManager->create('Endeavor\Author\Model\ReportsAuthor');
            $model->load($item->getReportId());
            $model->delete();
        }
    }
    
    /**
     * Return the author percent for each publication.
     * 
     * @param int product id.
     * @param int author id.
     * @return float author sales percent.
     */
    public function getAuthorPercent($total, $productId, $authorId)
    {
        $collect = $this->_objectManager->create('Endeavor\Author\Model\SalesAuthor')->getCollection();
        $salesPercent=0;
        foreach($collect as $item){
            if($item->getBookId() == $productId && $item->getAuthorId() == $authorId){
                $salesPercent= $item->getAuthorPercent();
            }
        }
        return (($total * $salesPercent) / 100);
    }
}
