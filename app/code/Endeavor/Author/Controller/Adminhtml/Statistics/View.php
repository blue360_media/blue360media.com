<?php

namespace Endeavor\Author\Controller\Adminhtml\Statistics;

use Magento\Backend\App\Action;

/**
 * View Author Reports.
 *
 * @author Nadeem Khleif
 */
class View extends \Magento\Backend\App\Action
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
         Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * Init actions
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }

    /**
     * Edit Author
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute() {
        $id = $this->getRequest()->getParam('author_id');
        $model = $this->_objectManager->create('Endeavor\Author\Model\Author');

        $reportCollection = $this->_objectManager->get('Endeavor\Author\Model\ReportsAuthor')->getCollection();

        foreach ($reportCollection as $item) {
            $model_report = $this->_objectManager->create('Endeavor\Author\Model\ReportsAuthor');
            $model_report->load($item->getReportId());
            if ($this->getAuthor($id)->getName() != $item->getAuthorName()) {
                $model_report->delete();
            }
        }

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This author no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $this->_coreRegistry->register('author', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
                $id ? __('Edit Author') : __('New Author'), $id ? __('Edit Author') : __('Author Reports')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Authors'));
        $resultPage->getConfig()->getTitle()
                ->prepend($model->getId() ? $model->getTitle() : __('Author Reports'));

        return $resultPage;
    }
    
    /**
     * Get Author Record
     * 
     * @param int Author $Id
     * @return array of Author Information
     */   
    public function getAuthor($id)
    {
        $orderDatamodel= $this->_objectManager->get('Endeavor\Author\Model\Author');
        $author= $orderDatamodel->load($id);
        
        return $author;
    }
}
