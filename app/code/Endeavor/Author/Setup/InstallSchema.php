<?php
namespace Endeavor\Author\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * install tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('endeavor_author')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('endeavor_author')
            )
            ->addColumn(
                'author_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Entity Id'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [
                    'nullable' => false,
                    'primary'  => false,
                ],
                'Author Name'
            )
            ->addColumn(
                'email',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,],
                'Author Email'
            )
            ->addColumn(
                'nickname',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,],
                'Author Nick Name'
            )
            ->addColumn(
                'mobile',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,],
                'Author Mobile'
            )
            ->addColumn(
                'nickname',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,],
                'Author Nick Name'
            )
            ->addColumn(
                'country',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,],
                'Author Country'
            )
            ->addColumn(
                'state',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,],
                'Author State'
            )
            ->addColumn(
                'city',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,],
                'Author City'
            )
            ->addColumn(
                'street',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,],
                'Author Street'
            )
            ->addColumn(
                'zip',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true,],
                'Author ZIP'
            )
            ->addColumn(
                'image',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '2M',
                ['nullable' => true,],
                'Author Image'
            )
            ->addColumn(
            'about_author',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [
                    'nullable' => true,
                ],
                'About Author'
            )
            ->setComment('Authors Table');
            $installer->getConnection()->createTable($table);
        }
//        if (!$installer->tableExists('endeavor_author_reports')) {
//            $table = $installer->getConnection()->newTable(
//                $installer->getTable('endeavor_author_reports')
//            )
//            ->addColumn(
//                'report_id',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
//                'Report Id'
//            )
//            ->addColumn(
//                'author_name',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                [
//                    'nullable' => false,
//                    'primary'  => false,
//                ],
//                'Author Name'
//            )
//            ->addColumn(
//                'publication_name',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                ['nullable' => false,],
//                'Publication Name'
//            )
//            ->addColumn(
//                'publication_qty',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                ['nullable' => false,],
//                'Publication Qty'
//            )
//            ->addColumn(
//                'customer_name',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                ['nullable' => false,],
//                'Customer Name'
//            )
//            ->addColumn(
//                'jurisdiction',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                ['nullable' => false,],
//                'Jurisdiction'
//            )
//            ->addColumn(
//                'customer_group',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                ['nullable' => false,],
//                'Customer Group'
//            )
//            ->addColumn(
//                'total',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                ['nullable' => false,],
//                'Total'
//            )
//            ->addColumn(
//                'author_percent',
//                \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
//                null,
//                ['nullable' => false,],
//                'Author Percent'
//            )
//            ->setComment('Authors Reports Table');
//            $installer->getConnection()->createTable($table);
//        }
//              if (!$installer->tableExists('endeavor_author_books')) {
//            $table = $installer->getConnection()->newTable(
//                $installer->getTable('endeavor_author_books')
//            )
//            ->addColumn(
//                'entity_id',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                [ 'identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true ],
//                'Entity Id'
//            )
//            ->addColumn(
//                'publication_id',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                [ 'nullable' => false , 'primary' => true],
//                'Publication Id'
//            )
//            ->addColumn(
//                'author_name',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                [
//                    'nullable' => false,
//                    'primary'  => false,
//                ],
//                'Author Name'
//            )
//            ->addColumn(
//                'author_id',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                [
//                    'nullable' => false
//                    
//                ],
//                'Author ID'
//            )
//            ->addColumn(
//                'publication_name',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                ['nullable' => false,],
//                'Publication Name'
//            )
//            ->addColumn(
//                'sku',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                ['nullable' => false,],
//                'Total'
//            )
//            ->addColumn(
//                'publication_qty',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                ['nullable' => false,],
//                'Publication Qty'
//            )
//            ->addColumn(
//                'publication_price',
//                \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
//                null,
//                ['nullable' => false,],
//                'Publication Price'
//            )
//            ->addColumn(
//                'jurisdiction',
//                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
//                255,
//                ['nullable' => false,],
//                'Jurisdiction'
//            )
//            ->addColumn(
//                'sales_percent',
//                \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
//                255,
//                ['nullable' => false,],
//                'Sales Percent'
//            )
//            ->setComment('Authors Books Table');
//            $installer->getConnection()->createTable($table);
//        }
//        if (!$installer->tableExists('endeavor_author_sales')) {
//            $table = $installer->getConnection()->newTable(
//                $installer->getTable('endeavor_author_sales')
//            )
//            ->addColumn(
//                'percent_id',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
//                'Percent Id'
//            )
//            ->addColumn(
//                'book_id',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                [ 'nullable' => true, 'unsigned' => true ],
//                'Book Id'
//            )
//            ->addColumn(
//                'author_id',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                [ 'nullable' => false, 'unsigned' => true ],
//                'Author Id'
//            )
//            ->addColumn(
//                'author_percent',
//                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
//                null,
//                ['nullable' => false,],
//                'Author Percent'
//            )->addForeignKey(
//                $installer->getFkName(
//                    'endeavor_author_sales',
//                    'author_id',
//                    'endeavor_author',
//                    'author_id'
//                ),
//                'author_id',
//                $installer->getTable('endeavor_author'), 
//                'author_id',
//                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//            )->addForeignKey(
//                $installer->getFkName(
//                    'endeavor_author_sales',
//                    'book_id',
//                    'catalog_product_entity',
//                    'entity_id'
//                ),
//                'book_id',
//                $installer->getTable('catalog_product_entity'), 
//                'entity_id',
//                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
//            )
//            ->setComment('Authors Books Sales Percent');
//            $installer->getConnection()->createTable($table);
//        }
        $installer->endSetup();
    }
}
