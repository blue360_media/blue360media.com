<?php

/**
 * Copyright © 2017 Endeavorpal, Inc. All rights reserved.
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 21-11-2017.
 * @desc in order to add promotions messages at catalog page with date check.
 */

namespace Endeavor\BookInformation\Block\Product;

class View extends \Magento\Framework\View\Element\Template 
{
    public function getBundleRules() {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $rules = $objectManager->get('Magento\CatalogRule\Model\Rule')->getCollection()->addIsActiveFilter(1)->setOrder('sort_order', 'DESC');
        $currentProduct = $objectManager->get('Magento\Framework\Registry')->registry('current_product');
        $productRules = array();
        $sortOrderFirstRule = 99999;
        $stopRulesProcessingFirstRule = false;
        $currentDate = date('Y-m-d');
        foreach ($rules as $rule) {
            $fromDate = $rule->getFromDate();
            $toDate = $rule->getToDate();
            if ($fromDate > $currentDate || $toDate < $currentDate) {
                continue;
            }
                
            $ruleproducts = $rule->getMatchingProductIds();
            if (isset($ruleproducts[$currentProduct->getId()]) && $ruleproducts[$currentProduct->getId()][1] == true) {
                $sortOrder = $rule->getSortOrder();
                $stopRulesProcessing = $rule->getStopRulesProcessing();
                if ($sortOrder < $sortOrderFirstRule) {
                    if ($stopRulesProcessing) {
                        unset($productRules);
                        $productRules[$rule->getRuleId()] = $rule->getDescription();
                    } else {
                        $productRules[$rule->getRuleId()] = $rule->getDescription();
                    }
                    $sortOrderFirstRule = $sortOrder;
                    $stopRulesProcessingFirstRule = $stopRulesProcessing;
                    $ruleFirstId = $rule->getRuleId();
                }
                if ($sortOrder == $sortOrderFirstRule) {
                    if (!$stopRulesProcessingFirstRule) {
                        $productRules[$rule->getRuleId()] = $rule->getDescription();
                    }
                }
            }
        }
        return $productRules;
    }

    public function getBundleItemsRules() {
        $productRules = array();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $rules = $objectManager->get('Magento\CatalogRule\Model\Rule')->getCollection()->addIsActiveFilter(1)->setOrder('sort_order', 'DESC');
        $currentProduct = $objectManager->get('Magento\Framework\Registry')->registry('current_product');
        if ($currentProduct->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
            $bundleItems = $currentProduct->getTypeInstance(true)->getSelectionsCollection(
                    $currentProduct->getTypeInstance(true)->getOptionsIds($currentProduct), $currentProduct);
            
            $sortOrderFirstRule = 99999;
            $stopRulesProcessingFirstRule = false;
            $currentDate = date('Y-m-d');
            foreach ($bundleItems as $item) {
                foreach ($rules as $rule) {
                    $fromDate = $rule->getFromDate();
                    $toDate = $rule->getToDate();
                    if ($fromDate > $currentDate || $toDate < $currentDate) {
                        continue;
                    }
                    $ruleproducts = $rule->getMatchingProductIds();
                    if (isset($ruleproducts[$item->getId()]) && $ruleproducts[$item->getId()][1] == true) {
                        $sortOrder = $rule->getSortOrder();
                        $stopRulesProcessing = $rule->getStopRulesProcessing();
                        if ($sortOrder < $sortOrderFirstRule) {
                            if ($stopRulesProcessing) {
                                unset($productRules);
                                $productRules[$rule->getRuleId()] = $rule->getDescription();
                            } else {
                                $productRules[$rule->getRuleId()] = $rule->getDescription();
                            }
                            $sortOrderFirstRule = $sortOrder;
                            $stopRulesProcessingFirstRule = $stopRulesProcessing;
                            $ruleFirstId = $rule->getRuleId();
                        }
                        if ($sortOrder == $sortOrderFirstRule) {
                            if (!$stopRulesProcessingFirstRule) {
                                $productRules[$rule->getRuleId()] = $rule->getDescription();
                            }
                        }
                    }
                }
            }
        }
        return $productRules;
    }

    public function getProductRules() {
        $rules = array();
        $bundleRules = $this->getBundleRules();
        $itemRules = $this->getBundleItemsRules();
        foreach ($bundleRules as $bundleRule) {
            if (!in_array($bundleRule, $rules)) {
                $rules[] = $bundleRule;
            }
        }
        foreach ($itemRules as $itemrule) {
            if (!in_array($itemrule, $rules)) {
                $rules[] = $itemrule;
            }
        }
        return $rules;
    }

}
