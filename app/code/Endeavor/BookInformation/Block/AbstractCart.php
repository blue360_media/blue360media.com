<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\BookInformation\Block;

/**
 * Description of AbstractCart
 *
 * @author Nadeem Khleif
 */
class AbstractCart {

    public function afterGetItemRenderer(\Magento\Checkout\Block\Cart\AbstractCart $subject, $result)
    {
        $result->setTemplate('Endeavor_BookInformation::cart/item/default.phtml');
        return $result;
    }
}
