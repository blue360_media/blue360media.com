<?php

namespace Endeavor\BookInformation\Block;
/**
 *@update:Aya Shaheen
 *@desc:Radio bundle, remove '+' from bundle items name : title + price
 *@date:6/12/2017
 *
 * @updated by: Amjad BaniMattar
 * @date: 12/14/2017
 * @desc: blue360/magento2#88, Updated in order to make product media type bold
**/
class Radio extends \Magento\Bundle\Block\Catalog\Product\View\Type\Bundle\Option\Radio
{
    protected $_template = 'Magento_Bundle::catalog/product/view/type/bundle/option/radio.phtml';
    
    public function getSelectionTitlePrice($selection, $includeContainer = true)
    {
        $productName = $this->escapeHtml($selection->getName());
        if (!empty($productName)) {
            $nameWithMediaType = explode('|', $productName);
            $productName = $nameWithMediaType[0];
          
            if (isset($nameWithMediaType[1])) {
                $productName .= '| <span class="media-type">' . $nameWithMediaType[1] . '</span>';
            }
        }
        $priceTitle = '<span class="product-name">' . $productName . '</span>';
        $priceTitle .= ' &nbsp; ' . ($includeContainer ? '<span class="price-notice">' : '') . ''
            . $this->renderPriceString($selection, $includeContainer) . ($includeContainer ? '</span>' : '');
        return $priceTitle;
    }

    
}
