<?php
/**
 * Copyright © 2017 EndeavorTechnology. All rights reserved.
 * @author: Mohammad Hamadenh
 * @email:  mhamadneh@endeavorpal.com
 * @date: 1/11/2017
 * @desc: 1. in order to show price with discounted price when you applied rules.
 *        2. in order to get promotions messages for each item.
 * 
 * @updated_By: Mohammad Hamadenh
 * @email:  mhamadneh@endeavorpal.com
 * @date: 20/12/2017
 * @desc: in order to add promotion messages only at the top of page when rule is applied to shipping.
 */
namespace Endeavor\BookInformation\Block\Checkout\Cart\Item;

class Renderer extends \Magento\Checkout\Block\Cart\Item\Renderer {
    
    /*
     * Get row total including discount for item
     *
     * @param  $rowTotal, $DiscountAmount
     * @return string
     *  
     */
    public function getItemRowTotalDiscount($rowTotal, $DiscountAmount) {
        // Do your custom stuff
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');
        $discountedPrice = $priceHelper->currency($rowTotal - $DiscountAmount, true, false);
        return ($discountedPrice);
    }
    /*
     * Get price including discount for item
     *
     * @param  $rowTotal, $DiscountAmount
     * @return string
     *  
     */
    public function getItemPriceDiscount($price, $discountAmount, $qty) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data');
        $discountedPrice = $priceHelper->currency($price - ($discountAmount / $qty), true, false);
        return($discountedPrice);
    }
    /*
     * Get Rule Messages for item
     *
     * @param  $rulesIds
     * @return Array
     *  
     */
    public function getRuleMessages($rulesIds){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $rulesMessages = array();
        $cartRules = explode(",", $rulesIds);
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $storeId = $storeManager->getStore()->getStoreId();
        foreach ($cartRules as $cartRule) {
            $cartRuleData = $objectManager->get('Magento\SalesRule\Model\Rule')->load($cartRule);
            if(($cartRuleData->getSimpleFreeShipping() == '')||($cartRuleData->getSimpleFreeShipping() == 0)){
                if($cartRuleData->getSimpleAction() != "cart_fixed") {
                    $rulesMessages[] = $objectManager->get('Magento\SalesRule\Model\ResourceModel\Rule')->getStoreLabel($cartRule, $storeId);
                }
            }
        }
        return $rulesMessages;
    }
}
