<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\BookInformation\Helper;

/**
 * Description of Data
 *
 * @author Nadeem Khleif
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper {
    
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepo
    ) {
        $this->_productRepo = $productRepo;
        parent::__construct($context);
    }


    /**
     * Load product from productId
     *
     * @param $id
     * @return $this
     */
    public function getProductById($id)
    {
        return $this->_productRepo->getById($id);
    }
}
