<?php
/**
 * Copyright © 2018 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 02/11/2018.
 * @desc Bulk Addresses for customers.
 */

namespace Endeavor\CustomerAddress\Controller\Customer;

use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Api\AccountManagementInterface;

class SaveBulk extends \Magento\Framework\App\Action\Action
{
    
    const FILE_HEADER = 0;
    const FILE_FIRST_NAME = 0;
    const FILE_LAST_NAME = 1;
    const FILE_EMAIL = 2;
    const FILE_COMPANY = 3;
    const FILE_PHONE_NUMBER = 4;
    const FILE_FAX = 5;
    const FILE_STREET_ADDRESS = 6;
    const FILE_CITY = 7;
    const FILE_STATE = 8;
    const FILE_ZIP_CODE = 9;
    
    /**
     * Customer Object
     *
     * @var \Magento\Customer\Model\CustomerFactory 
     */
    protected $_customerFactory;
    
    /**
     * Store website manager 
     *
     * @var \Magento\Store\Model\StoreManagerInterface 
     */
    protected $_storeManager;
    
    /**
     * @var AccountManagementInterface
     */
    protected $_customerAccountManagement;

    /**
     * @var Endeavor\Downloadable\Model\Import
     */
    private $_import;
    
    /**
     * PageFactory Data
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;
    
    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     * Core store config
     *
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $_addressFactory;
    
    /**
     * 
     * @param \Endeavor\Downloadable\Model\Import $import
     * @param \Magento\Downloadable\Model\Link\Purchased\ItemFactory $PurchasedItemFactory
     * @param \Magento\Downloadable\Model\LinkFactory $LinkFactory
     * @param \Magento\Downloadable\Model\Link\PurchasedFactory $PurchasedLinkFactory
     * @param \Magento\Customer\Model\CustomerFactory $CustomerFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param AccountManagementInterface $customerAccountManagement
     * @param \Endeavor\Downloadable\Model\RelatedItemsFactory $RelatedItems
     * @param Random $mathRandom
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Endeavor\Downloadable\Model\Import $import,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\CustomerFactory $CustomerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,   
        AccountManagementInterface $customerAccountManagement,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Framework\App\Action\Context $context
    ){
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
        $this->_import = $import;
        $this->_customerFactory = $CustomerFactory;
        $this->_storeManager = $storeManager;
        $this->_customerAccountManagement = $customerAccountManagement;   
        $this->_addressFactory = $addressFactory;
        
    }
    
    /**
     * Contact action
     *
     * @return void
     */
    public function execute() {
        try {
            if (isset($_FILES['import_file'])) {
                $fileAddressRows = $this->_import->importFromCsvFile($_FILES['import_file']);
                $post = $this->getRequest()->getPost();
                $customerId = $post['customerId'];

                $resultRedirect = $this->checkRequiredFields($fileAddressRows);

                if (empty($resultRedirect)) {

                    $resultRedirect = $this->validateFields($fileAddressRows);

                    if (empty($resultRedirect)) {

                        $this->createNewAddressToCustomer($fileAddressRows, $customerId);

                        $message = 'Address(es) Added successfully';
                        $this->messageManager->addSuccessMessage(__($message));
                        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                        $resultRedirect->setPath('customer/address');

                    }

                }

            } else {
                $message = 'Opps! Please check your file format.';
                $this->messageManager->addErrorMessage(__($message));
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('customer/address/new');

            }
            
        } catch (\Exception $e) {
            $message = 'Something went wrong while saving Addresses.';
            $this->messageManager->addErrorMessage(__($message));
            $this->messageManager->addException($e,__('Something went wrong while saving Addresses.'));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('customer/address/new');
            
        }
        
        return $resultRedirect;
        
    }
    
    /**
     * @desc validate row in csv file for addresses
     * @param Array $fileAddressRows
     * 
     * @return \Magento\Framework\Controller\ResultFactory $resultRedirect
     */
    protected function validateFields($fileAddressRows) {
        $validateErrors = [];
        $resultRedirect = null;
        foreach ($fileAddressRows as $rowAddressId => $rowAddress) {
            if ($rowAddressId != self::FILE_HEADER) {
                $phoneNumber = trim($rowAddress[self::FILE_PHONE_NUMBER]);
                if(!preg_match("/^[0-9]{10}$/", $phoneNumber)) {
                    $validateErrors ['Phone Number'][] = $rowAddressId;
                    
                }
                $zipCode = trim($rowAddress[self::FILE_ZIP_CODE]);
                if (!preg_match('#^\d{5}([\-]?\d{4})?$#',$zipCode)) {
                    $validateErrors ['Zip Code'][] = $rowAddressId;
                    
                }
                $state = trim($rowAddress[self::FILE_STATE]);
                $validatestate = $this->validateState($state);
                if (!$validatestate) {
                    $validateErrors ['State/Province Code'][] = $rowAddressId;
                    
                }
                $street = trim($rowAddress[self::FILE_STREET_ADDRESS]);
                if (strlen($street) > 80 ) {
                    $validateErrors ['Street Address'][] = $rowAddressId;
                    
                }
                $email = trim($rowAddress[self::FILE_EMAIL]);
                if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $email)) {
                    $validateErrors ['Email'][] = $rowAddressId;
                    
                }
                
            }
            
        }
        if (!empty($validateErrors)) {
            foreach ($validateErrors as $validateError => $rowAddressId) {
                if ($validateError == 'Street Address') {
                    $message = 'Opps! Maximum length allowed for '.$validateError.' is 80 in those rows: ';
                    foreach ($rowAddressId as $rowAddressIdItem) {
                        $message .= ($rowAddressIdItem+1).',';

                    }
                    
                } else {
                    $message = 'Opps! '.$validateError.' is not valid in those rows: ';
                    foreach ($rowAddressId as $rowAddressIdItem) {
                        $message .= ($rowAddressIdItem+1).',';

                    }
                    
                }
                $this->messageManager->addErrorMessage(__($message));

            }
            $message = 'And, Other Rows is not loaded ! ';
            $this->messageManager->addErrorMessage(__($message));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('customer/address/new');
            
        }
        return $resultRedirect;
        
    }
    
    /**
     * @desc validate state for each row in addresses
     * @param String $state
     * 
     * @return Bool $validState
     */
    protected function validateState($state) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $region = $objectManager->create('Magento\Directory\Model\ResourceModel\Region\Collection')
                    ->addFieldToFilter('code', ['eq' => $state])
                    ->addFieldToFilter('country_id', ['eq' => 'US'])
                    ->getFirstItem();
        
        $validState = true;
        if (empty($region->getId())) {
            $validState = false;
            
        }
        return $validState;
        
    }
    
    /**
     * @desc Check required fields in csv for addresses
     * @param Array $fileAddressRows
     * 
     * @return \Magento\Framework\Controller\ResultFactory $resultRedirect
     */
    protected function checkRequiredFields($fileAddressRows) {
        $requiredFields = [];
        $resultRedirect = null;
        foreach ($fileAddressRows as $rowAddressId => $rowAddress) {
            if ($rowAddressId != self::FILE_HEADER) {
                if (empty(trim($rowAddress[self::FILE_FIRST_NAME]))) {
                    $requiredFields ['First Name'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_LAST_NAME]))) {
                    $requiredFields ['Last Name'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_PHONE_NUMBER]))) {
                    $requiredFields ['Phone Number'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_STREET_ADDRESS]))) {
                    $requiredFields ['Street Address'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_CITY]))) {
                    $requiredFields ['City'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_STATE]))) {
                    $requiredFields ['State'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_ZIP_CODE]))) {
                    $requiredFields ['ZIP/POSTAL'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_EMAIL]))) {
                    $requiredFields ['Email'][] = $rowAddressId;
                
            }
            
        }
            
        }
        if (!empty($requiredFields)) {
            foreach ($requiredFields as $requiredField => $rowAddressId) {
                $message = 'Opps! '.$requiredField.' is Required in those rows: ';
                foreach ($rowAddressId as $rowAddressIdItem) {
                    $message .= ($rowAddressIdItem+1).',';

                }
                $this->messageManager->addErrorMessage(__($message));

            }
            $message = 'And, Other Rows is not loaded ! ';
            $this->messageManager->addErrorMessage(__($message));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('customer/address/new');

        }
        return $resultRedirect;
    }
    
    /**
     * @desc Create new address to customer.
     * @param Array $fileAddressRows
     * @param String $customerId
     */
    protected function createNewAddressToCustomer($fileAddressRows, $customerId) {
        
        foreach ($fileAddressRows as $fileAddressRowId => $fileAddressRowsItem) {
            if ($fileAddressRowId != self::FILE_HEADER) {
                $address = $this->_addressFactory->create();
                $firstName = trim($fileAddressRowsItem[self::FILE_FIRST_NAME]);
                $lastName = trim($fileAddressRowsItem[self::FILE_LAST_NAME]);
                $company = trim($fileAddressRowsItem[self::FILE_COMPANY]);
                $phoneNumber = trim($fileAddressRowsItem[self::FILE_PHONE_NUMBER]);
                $fax = trim($fileAddressRowsItem[self::FILE_FAX]);
                $streetAddress = trim($fileAddressRowsItem[self::FILE_STREET_ADDRESS]);
                $city = trim($fileAddressRowsItem[self::FILE_CITY]);
                $stateCode = trim($fileAddressRowsItem[self::FILE_STATE]);
                $regionId = $this->getRegionIdFromCode($stateCode);
                $regionName = $this->getRegionNameFromCode($stateCode);
                $zipCode = trim($fileAddressRowsItem[self::FILE_ZIP_CODE]);
                $email = trim($fileAddressRowsItem[self::FILE_EMAIL]);
                
                $address->setCustomerId($customerId)
                        ->setFirstname($firstName)
                        ->setLastname($lastName)
                        ->setCountryId('US')
                        ->setRegion($regionName)
                        ->setRegionId($regionId)
                        ->setPostcode($zipCode)
                        ->setCity($city)
                        ->setFax($fax)
                        ->setCompany($company)
                        ->setTelephone($phoneNumber)
                        ->setStreet($streetAddress)
                        ->setIsDefaultBilling('0')
                        ->setIsDefaultShipping('0')
                        ->setSaveInAddressBook('1');
                
                $address->save();
                
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $customerAddressRepository = $objectManager->get('Magento\Customer\Api\AddressRepositoryInterface');
                $customerAddressRep = $customerAddressRepository->getById($address->getId());
                $customerAddressRep->setCustomAttribute('shipping_address_email', $email);
                $customerAddressRepository->save($customerAddressRep);
                
                
            }
            
        }
        
    }
    
    /**
     * @desc get region name from code.
     * @param String $stateCode
     * 
     * @return String $regionName
     */
    protected function getRegionNameFromCode($stateCode) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $region = $objectManager->create('Magento\Directory\Model\ResourceModel\Region\Collection')
                    ->addFieldToFilter('code', ['eq' => $stateCode])
                    ->addFieldToFilter('country_id', ['eq' => 'US'])
                    ->getFirstItem();
        $regionName = $region->getDefaultName();
        return $regionName;
        
    }
    
    /**
     * @desc get region id from code.
     * @param String $stateCode
     * 
     * @return String $regionId
     */
    protected function getRegionIdFromCode($stateCode) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $region = $objectManager->create('Magento\Directory\Model\ResourceModel\Region\Collection')
                    ->addFieldToFilter('code', ['eq' => $stateCode])
                    ->addFieldToFilter('country_id', ['eq' => 'US'])
                    ->getFirstItem();
        $regionId = $region->getRegionId();
        return $regionId;
        
    }
    
}
