<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\CustomerAddress\Setup;

use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    /**
     * Init
     *
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(\Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory)
    {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();
        // insert attribute
        $customerSetup->addAttribute('customer_address', 'address_contact_name',  [
            'label' => 'Contact Name',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 1,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $contactName = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_contact_name');
        $contactName->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $contactName->save();
		
		
		// insert attribute
        $customerSetup->addAttribute('customer_address', 'address_attention_line',  [
            'label' => 'Attention Line',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 2,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $attentionLine = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_attention_line');
        $attentionLine->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $attentionLine->save();

		
	// insert attribute
        $customerSetup->addAttribute('customer_address', 'address_mailing_name',  [
            'label' => 'Mailing Address Name',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 123,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $mailingAddress = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_mailing_name');
        $mailingAddress->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $mailingAddress->save();
		
		
	// insert attribute
        $customerSetup->addAttribute('customer_address', 'address_contact_email',  [
            'label' => 'Contact e-mail Address',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 3,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $contactEmail = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_contact_email');
        $contactEmail->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $contactEmail->save();
		
        
        // insert attribute
        $customerSetup->addAttribute('customer_address', 'address_cc_service_email',  [
            'label' => 'Contact Customer Service e-mail',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 131,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $customerServiceEmail = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_cc_service_email');
        $customerServiceEmail->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $customerServiceEmail->save();
		
        
        // insert attribute
        $customerSetup->addAttribute('customer_address', 'address_work_product_email',  [
            'label' => 'Work Product e-mail',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 130,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $workProductEmail = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_work_product_email');
        $workProductEmail->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $workProductEmail->save();
        
        
        // insert attribute
        $customerSetup->addAttribute('customer_address', 'address_cs_mktg_email',  [
            'label' => 'Contact Sales Marketing e-mail',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 128,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $salesMarketingEmail = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_cs_mktg_email');
        $salesMarketingEmail->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $salesMarketingEmail->save();
        
        
        		
        // insert attribute
        $customerSetup->addAttribute('customer_address', 'address_notification_email',  [
            'label' => 'Notification e-mail',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 127,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $notificationEmail = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_notification_email');
        $notificationEmail->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $notificationEmail->save();
        
        // insert attribute
        $customerSetup->addAttribute('customer_address', 'address_contact_ap_email',  [
            'label' => 'Contact AP e-mail',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 126,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $contactAPEmail = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_contact_ap_email');
        $contactAPEmail->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $contactAPEmail->save();
        
        
        // insert attribute
        $customerSetup->addAttribute('customer_address', 'address_order_status_email',  [
            'label' => 'Order Status e-mail',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 125,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $orderStatusEmail = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_order_status_email');
        $orderStatusEmail->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $orderStatusEmail->save();
        
        $setup->endSetup();
        
        
        
        // insert attribute
        $customerSetup->addAttribute('customer_address', 'address_product_enti_email',  [
            'label' => 'Product Entitlement e-mail',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 124,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $productEntitlementEmail = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_product_enti_email');
        $productEntitlementEmail->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $productEntitlementEmail->save();
        // insert attribute
        $customerSetup->addAttribute('customer_address', 'address_alt_phone_number',  [
            'label' => 'Alternative Phone Number',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 121,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $alternativePhone = $customerSetup->getEavConfig()->getAttribute('customer_address', 'address_alt_phone_number');
        $alternativePhone->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $alternativePhone->save();
        
        
        
        
    }
}
