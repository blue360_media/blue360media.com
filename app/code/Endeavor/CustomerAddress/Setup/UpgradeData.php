<?php
namespace Endeavor\CustomerAddress\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
 
 
class UpgradeData implements UpgradeDataInterface {
 
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;
    
    /**
     * 
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
    }
    
    /**
     * 
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context ) {

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addCustomAttributes($setup);
            
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->addEmailAttributeToAddress($setup);
            
        }
        
    }
    /**
     * 
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function addEmailAttributeToAddress(ModuleDataSetupInterface $setup){
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY,
            'shipping_address_email',  [
                'label' => 'Shipping Email',
                'type' => 'varchar',
                'input' => 'text',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
                'position' => 10,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'system' => 0,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_used_in_grid' => false
            ]);

        $email = $customerSetup->getEavConfig()
            ->getAttribute(
                \Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY,
                'shipping_address_email'
            );
        $email->setData(
            'used_in_forms',
            ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address']
        );
        $email->save();
    }
    /**
     * 
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function addCustomAttributes(ModuleDataSetupInterface $setup){
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        
        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY,
            'b360_address_id',  [
                'label' => 'Blue360 Address Id',
                'type' => 'varchar',
                'input' => 'text',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
                'position' => 9,
                'visible' => true,
                'required' => false,
                'system' => 0,
                'is_filterable_in_grid' => false,
                'is_searchable_in_grid' => false,
                'is_visible_in_grid' => false,
                'is_used_in_grid' => false
            ]);

        $b360AddressId = $customerSetup->getEavConfig()
            ->getAttribute(
                \Magento\Customer\Model\Indexer\Address\AttributeProvider::ENTITY,
                'b360_address_id'
            );
        $b360AddressId->setData(
            'used_in_forms',
            ['adminhtml_customer_address']
        );
        $b360AddressId->save();
    }
}