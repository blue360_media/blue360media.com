<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\RelatedProducts\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\App\RequestInterface;

class Related extends \Magento\Ui\Component\Listing\Columns\Column
{
    /** @var \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet **/
    protected $attributeSet;

    /**
     * @var \Magento\Catalog\Model\Product\LinkFactory
     */
    protected $_linkFactory;

    
    /**
     * @var RequestInterface
     */
    protected $request;


    /**
     * Column name
     */
    const NAME = 'column.related';

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    protected $localeCurrency;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magento\Framework\Locale\CurrencyInterface $localeCurrency
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        RequestInterface $request,
        UiComponentFactory $uiComponentFactory,
        \Magento\Framework\Locale\CurrencyInterface $localeCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Eav\Api\AttributeSetRepositoryInterface $attributeSet,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->localeCurrency = $localeCurrency;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->attributeSet = $attributeSet;

    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $output = array();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $product_id = $this->request->getParam('current_product_id');
        $orderItems = $objectManager->create('Magento\Sales\Model\Order\Item')->getCollection();

        $productOrder = array();
        $relatedProducts = array();
        foreach ($orderItems->getItems() as $item) {
            if ($item->getProductId() == $product_id) {
                $productOrder[] = (int) $item->getOrderId();
            }
        }
        $orderItems->addFieldToFilter('order_id',$productOrder);
        foreach ($orderItems->getItems() as $item) {
            foreach ($productOrder as $orders) {
                if ($item->getOrderId() == $orders) {
                    if ($product_id != $item->getProductId()) {
                        $relatedProducts[] = $item->getProductId();
                    }
                }
            }
        }
        $productObj = $objectManager->create('Magento\Catalog\Model\Product');
        $productCollection = $productObj->getCollection()->addFieldToFilter('type_id','bundle');
        $store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();

        if (!empty($relatedProducts)) {
            $productCollection->addFieldToFilter('entity_id',$relatedProducts);
            foreach ($productCollection->getItems() as $item) {
                $newItem['entity_id'] = $item->getId();
                $productObj->load($item->getId());
                $newItem['attribute_set_id'] = $item->getAttributeSetId();
                $attributeSetRepository = $this->attributeSet->get($item->getAttributeSetId());
                $newItem['attribute_set_text'] = $attributeSetRepository->getAttributeSetName();
                $newItem['type_id'] = $item->getTypeId();
                $newItem['sku'] = $item->getSku();
                $newItem['thumbnail'] = $productObj->getThumbnail();

                $images = $productObj->getMediaGalleryImages() ;
                $image = $images->getFirstItem();
                $imageUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $productObj->getThumbnail();

                $newItem['thumbnail_alt'] =  $productObj->getName();
                $newItem['thumbnail_src'] = $imageUrl;
                $newItem['name'] = $productObj->getName();
                if ($item->getStatus()) {
                    $newItem['status_text'] = 'Enabled';
                    $newItem['status'] = $item->getStatus();
                } else {
                    $newItem['status_text'] = 'Disabled';
                    $newItem['status'] = $item->getStatus();
                }
                if ($item->getTypeId() != 'bundle') {
                    $newItem['price'] = '$'.number_format($productObj->getPrice(),2);
                } else {
                    $newItem['price'] = null;
                }
                $newItem['related']='Yes';
                array_unshift($output, $newItem);
            }
        }
        
        foreach ($dataSource['data']['items'] as & $item) {
            $added = false;
            $item['related']='Not';
            if($item['type_id'] == 'bundle'){
                foreach ($relatedProducts as $product) {
                    if ($item['entity_id'] == $product) {
                        $added = true;
                    }
                }
            
                if (!$added) {
                    array_push($output, $item);
                }
            }
        }
        
        if (!empty($output)) {
            $dataSource['data']['items'] = $output;
        }

        return $dataSource;
    }
}
