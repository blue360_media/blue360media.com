<?php
/**
 * Copyright © 2018 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Murad Dweikat <mdweikat@endeavorpal.com>
 * @date 2018/03/18
 * @desc override Login Button label
 */
namespace Endeavor\Theme\Block;

class AuthorizationLink extends \Magento\Customer\Block\Account\AuthorizationLink
{
    /**
     * 
     * @return string
     */
    public function getLabel()
    {
        return $this->isLoggedIn() ? __('Sign Out') : __('Sign In');
    }
}
