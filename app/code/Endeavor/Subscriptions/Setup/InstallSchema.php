<?php
namespace Endeavor\Subscriptions\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
      
        $table = $installer->getConnection(
            )->newTable($installer->getTable('endeavor_subscriptions')
            )->addColumn(
                'subscription_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                array('identity' => true, 'nullable' => false, 'primary' => true),
                'Subscription Id'
            )->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [
                    'nullable' => false,
                ],
                'Subscription Name'
            )->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                array('nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT),
                'Created At'
            )->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                array('nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE),
                'Updated At'
            )->addColumn(
                'terms_conditions',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [
                    'nullable' => false,
                ],
                'Terms and Conditions'
            )->setComment(
                'Subscriptions'
        );
        $setup->getConnection()->createTable($table);
        
        $table_endeavor_subscriptions = $setup->getConnection()->newTable($setup->getTable('endeavor_customer_subscriptions'));
        
        $table_endeavor_subscriptions->addColumn(
            'subscriptions_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
            'Entity ID'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            array('nullable' => true, 'unsigned' => true),
            'Customer Id'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            array('nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT),
            'Created At'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            array('nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE),
            'Updated At'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'account_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            array('nullable' => true, 'unsigned' => true),
            'Account Id'
        );
            
        $table_endeavor_subscriptions->addColumn(
            'quote_item_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            array('nullable' => true, 'unsigned' => true),
            'Quote Item Id'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            array('nullable' => true, 'unsigned' => true),
            'Order Id'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'product_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            array('nullable' => true, 'unsigned' => true),
            'Product Id'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'publication_number',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['default' => 'ISBN'],
            'Publication Number'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'isbn',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            array('nullable' => true),
            'ISBN Book'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'sku',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            array('nullable' => true),
            'SKU'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'product_name',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            500,
            array('nullable' => true),
            'Product Name '
        );

        $table_endeavor_subscriptions->addColumn(
            'media_type_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            array('nullable' => true),
            'Media Type Id'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'media_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            20,
            array('nullable' => true),
            'Media Type'
        );

        $table_endeavor_subscriptions->addColumn(
            'subscription_start',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            10,
            array('nullable' => true),
            'Subscriptions Created YY/MM'
        );
        
        $table_endeavor_subscriptions->addColumn(
            'subscription_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            11,
            [],
            'Subscription Id'
        );
     
        $table_endeavor_subscriptions->addColumn(
            'subscription_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            array('nullable' => true),
            'Subscription Type'
        );
       
        $table_endeavor_subscriptions->addColumn(
            'order_status',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            20,
            array('nullable' => true),
            'Order Status'
        );
        
	$table_endeavor_subscriptions->addColumn(
            'qty',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('nullable' => true),
            'Subscriptions QTY'
            )->addForeignKey(
                $installer->getFkName(
                    'endeavor_customer_subscriptions',
                    'customer_id',
                    'customer_entity',
                    'entity_id'
                ),
                'customer_id',
                $installer->getTable('customer_entity'), 
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(
                    'endeavor_customer_subscriptions',
                    'account_id',
                    'customer_entity',
                    'entity_id'
                ),
                'account_id',
                $installer->getTable('customer_entity'), 
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(
                    'endeavor_customer_subscriptions',
                    'product_id',
                    'catalog_product_entity',
                    'entity_id'
                ),
                'product_id',
                $installer->getTable('catalog_product_entity'), 
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(
                    'endeavor_customer_subscriptions',
                    'quote_item_id',
                    'quote_item',
                    'item_id'
                ),
                'quote_item_id',
                $installer->getTable('quote_item'), 
                'item_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(
                    'endeavor_customer_subscriptions',
                    'order_id',
                    'sales_order',
                    'entity_id'
                ),
                'order_id',
                $installer->getTable('sales_order'), 
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->addForeignKey(
                $installer->getFkName(
                    'endeavor_customer_subscriptions',
                    'subscription_id',
                    'endeavor_subscriptions',
                    'subscription_id'
                ),
                'subscription_id',
                $installer->getTable('endeavor_subscriptions'), 
                'subscription_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment(
                'Customer Subscriptions'
            );

        $setup->getConnection()->createTable($table_endeavor_subscriptions);
             
        $installer->endSetup();
    }
}
