<?php
namespace Endeavor\Subscriptions\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */    
    protected $currentCustomer;
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    public $_date;
    
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;
    
    /**
     * @var \Magento\Backend\Model\UrlInterface
     */
    protected $_backendUrl;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    protected $storeManager;
    
    /**
     * @var \Magento\Framework\Registry $registry
     */
    protected $_registry;

    /**
     * 
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->currentCustomer = $currentCustomer;
        $this->_date = $date;
        $this->_objectManager = $objectManager;
        $this->_backendUrl = $backendUrl;
        $this->storeManager = $storeManager;
        $this->_registry = $registry;

    }
    
    /**
     * Format the product name
     * 
     * @param string $name
     * @return string
     */
    public function getProductName($name)
    {
        $productName = explode(' ', $name);
        $dashed_product_name = implode('-',$productName);
        return $dashed_product_name;
    }
    
    /**
     * Retrieve the subscriptions records
     * 
     * @param int $productId
     * @return array
     */
    public function getSubscriptionsCollection($productId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $susbscriptionsModel = $objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');
        $collection = $susbscriptionsModel->getCollection();
        $collection->addFieldToFilter('customer_id',$customerSession->getCustomer()->getId());
        $collection->addFieldToFilter('publication_id',$productId);
        
        return $collection;
    }
    
}
