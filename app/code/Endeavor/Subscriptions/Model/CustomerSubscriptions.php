<?php
namespace Endeavor\Subscriptions\Model;

use Magento\Framework\DataObject\IdentityInterface;

class CustomerSubscriptions extends \Magento\Framework\Model\AbstractModel implements IdentityInterface
{
    /**
     * Model cache tag for clear cache in after save and after delete
     *
     * When you use true - all cache will be clean
     *
     * @var string|array|bool
     */
    const CACHE_TAG = 'endeavor_subscriptions';
    
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Subscriptions\Model\ResourceModel\CustomerSubscriptions');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    
}
