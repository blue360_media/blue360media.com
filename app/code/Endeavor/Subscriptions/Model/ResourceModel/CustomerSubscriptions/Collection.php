<?php
namespace Endeavor\Subscriptions\Model\ResourceModel\CustomerSubscriptions;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'subscriptions_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Subscriptions\Model\CustomerSubscriptions', 'Endeavor\Subscriptions\Model\ResourceModel\CustomerSubscriptions');
      
    }
}
