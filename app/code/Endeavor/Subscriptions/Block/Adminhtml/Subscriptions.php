<?php
namespace Endeavor\Subscriptions\Block\Adminhtml;

use Magento\Backend\Block\Widget\Context;

/**
 * Adminhtml customer segment content block
 */
class Subscriptions extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Block constructor
     *
     * @param Context $context
     * @param array $data
     * 
     * @return void
     */
    protected function _construct() {
        parent::__construct();
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
