<?php
namespace Endeavor\Subscriptions\Block\Adminhtml\Subscriptions\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;
    
    /**
    * @var \Magento\Catalog\Model\Category
    */
    protected $_subcategories;
    
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Catalog\Model\Category $subcategories,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, 
        array $data = []
    ) {
        $this->_subcategories = $subcategories;
        $this->_wysiwygConfig =$wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Endeavor\CustomerSegments\Model\ContCustomerSegmentsact */
        $model = $this->_coreRegistry->registry('subscription');
        
        $isElementDisabled = false;
        
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('subscriptions_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Subscriptions')]);

        if ($model->getId()) {
            $fieldset->addField('subscription_id', 'hidden', ['name' => 'subscription_id']);
        }
                       
        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Subscription Name'),
                'title' => __('Subscription Name'),
                'required' => true,
                'disabled' => $isElementDisabled
            ]
        );
                
        $fieldset->addField(
            'terms_conditions',
            'editor',
            [
                'name' => 'terms_conditions',
                'label' => __('Terms & Conditions'),
                'title' => __('Terms & Conditions'),
                'style' => 'height:10em',
                'required' => true,
                'config' => $this->_wysiwygConfig->getConfig()
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Subscription Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Subscription Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    
    /**
     * Get all customer groups added by admin
     *
     * @return array of customer groups ids and names
     */
    public function getCustomerGroups()
    {
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       $groups=$objectManager->create('\Magento\Customer\Model\ResourceModel\Group\Collection');
       $customer_groups = $groups->toOptionArray();
       return $customer_groups;
    }
}
