<?php
namespace Endeavor\Subscriptions\Block\Adminhtml\Subscriptions\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('subscriptions_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Subscription Settings'));
    }
}
