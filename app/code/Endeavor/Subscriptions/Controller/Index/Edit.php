<?php
namespace Endeavor\Subscriptions\Controller\Index;

/**
 * Edit Subscriptions in shopping cart
 *
 * @author Nadeem Khleif
 */
class Edit extends \Magento\Framework\App\Action\Action 
{    
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }
    
    /**
     * Edit Subscriptions in shopping cart
     *
     * @return String
     */
    public function execute()
    {
        $newSubscription = $this->getRequest()->getParam('subscription_id');
        $itemId = $this->getRequest()->getParam('item_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $subscriptionsCollection = $objectManager->create('Endeavor\Subscriptions\Model\Subscriptions')->load($newSubscription);
        $newSubscriptionName = $subscriptionsCollection->getName();
        $customerSubscriptions = $objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');
        //$customerSubscriptions->getCollection()->addFieldToFilter('quote_item_id',$itemId);
        
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
          foreach ($customerSubscriptions->getCollection() as $item) {
              if ($item->getQuoteItemId() == $itemId) {
                    $customerSubscriptions->load($item->getSubscriptionsId()
                        )->setSubscriptionType($newSubscriptionName)->save();
                    $customerSubscriptions->load($item->getSubscriptionsId()
                        )->setSubscriptionId($newSubscription)->save();
              }
          }           
        }
        return $result->setData($newSubscriptionName);
    }

}
