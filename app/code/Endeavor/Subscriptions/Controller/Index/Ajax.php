<?php
namespace Endeavor\Subscriptions\Controller\Index;

/**
 * Description of Ajax
 *
 * @author Nadeem Khleif
 */
class Ajax extends \Magento\Framework\App\Action\Action 
{
   /**
    * 
    * @param \Magento\Framework\App\Action\Context $context
    * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
    \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * 
     * @return string
     */
    public function execute()
    {
        $data = $this->getRequest()->getParam('subscription_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collect = $objectManager->create('Endeavor\Subscriptions\Model\Subscriptions')->load($data);
        $test='';
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $test.= $collect->getTermsConditions();
        }
        return $result->setData($test);
    }

}
