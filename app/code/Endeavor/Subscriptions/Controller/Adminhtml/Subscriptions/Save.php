<?php
namespace Endeavor\Subscriptions\Controller\Adminhtml\Subscriptions;

use Magento\Backend\App\Action\Context;

/**
 * Save Subscriptions after add new or edit.
 *
 * @author Nadeem Khleif
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $_jsHelper;
    
    /**
     * @var \Endeavor\CustomerSegments\Model\ResourceModel\CustomerSegments\CollectionFactory
     */
    protected $_organizationCollectionFactory;
    
    /**
     * 
     * @param Context $context
     * @param \Magento\Backend\Helper\Js $jsHelper
     * @param \Endeavor\Subscriptions\Model\ResourceModel\Subscriptions\CollectionFactory $organizationCollectionFactory
     */
    public function __construct(
        Context $context,
        \Magento\Backend\Helper\Js $jsHelper,
        \Endeavor\Subscriptions\Model\ResourceModel\Subscriptions\CollectionFactory $organizationCollectionFactory
    ) {
        $this->_jsHelper = $jsHelper;
        $this->_organizationCollectionFactory = $organizationCollectionFactory;
       
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            /** @var \Ashsmith\Blog\Model\Post $model */
            $model = $this->_objectManager->create('Endeavor\Subscriptions\Model\Subscriptions');

            $id = $this->getRequest()->getParam('subscription_id');
            if ($id) {
                $model->load($id);
            }
            $model->setData('name',$data['name']);
            $model->setData('terms_conditions',strip_tags($data['terms_conditions']));

            $this->_eventManager->dispatch(
                'subscriptions_prepare_save',
                ['subscriptions' => $model, 'request' => $this->getRequest()]
            );

            try {
                $model->save();
                $this->messageManager->addSuccess(__('You saved this Subscription.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['subscription_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the subscription.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['subscription_id' => $this->getRequest()->getParam('subscription_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
   
}
