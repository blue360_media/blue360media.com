<?php
namespace Endeavor\Subscriptions\Controller\Adminhtml\Subscriptions;

/**
 * Delete Subscriptions in adminhtml
 *
 * @author Nadeem Khleif
 */
class Delete extends \Magento\Backend\App\Action
{
    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('subscription_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Endeavor\Subscriptions\Model\Subscriptions');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The Subscription has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['subscription_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a subscription to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
