<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Subscriptions\Controller\Onepage;

class Success extends \Magento\Checkout\Controller\Onepage\Success
{
    /**
     * Order success action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $session = $this->getOnepage()->getCheckout();
        $resultPage = $this->resultPageFactory->create();
        $this->_eventManager->dispatch(
                'checkout_onepage_controller_success_action', ['order_ids' => [$session->getLastOrderId()]]
        );
        
        if (!$this->_objectManager->get('Magento\Checkout\Model\Session\SuccessValidator')->isValid()) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }
        $saleOrder = $this->_objectManager->create('\Magento\Sales\Model\Order')->load($session->getLastOrderId());
        $orderItems = $saleOrder->getAllItems();
        
        $subscriptionsCollection = $this->_objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions')->getCollection();
        $session->clearQuote();

        $customerSession = $this->_objectManager->get('Magento\Customer\Model\Session');
            
        foreach ($orderItems as $orderItem) {
            foreach ($subscriptionsCollection as $item) {
                if ($item->getQuoteItemId() == $orderItem->getQuoteItemId()) {
                    $this->_objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions'
                        )->load($item->getSubscriptionsId())->setCustomerId($customerSession->getCustomer()->getId())->save();
                    $this->_objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions'
                        )->load($item->getSubscriptionsId())->setOrderStatus('Complete')->save();
                    $this->_objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions'
                        )->load($item->getSubscriptionsId())->setOrderId($session->getLastOrderId())->save();
                }
            }
        }
        
        return $resultPage;
    }
}
