<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Subscriptions\Controller\Cart;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart as CustomerCart;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Add extends \Magento\Checkout\Controller\Cart\Add
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator
     * @param CustomerCart $cart
     * @param ProductRepositoryInterface $productRepository
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        CustomerCart $cart,
        ProductRepositoryInterface $productRepository
    ) {
        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart,
            $productRepository
        );
    }
    
    /**
     * Add product to shopping cart action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }
     
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $params = $this->getRequest()->getParams();

        $optionId = $params['optionId'];
        
        //$params['product'] = $params[$optionId];
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $product = parent::_initProduct();
        $cookieManager = $objectManager->get('Magento\Framework\Stdlib\CookieManagerInterface');
        if ($product->getTypeId() == 'bundle') {
                $selectionCollection = $product->getTypeInstance(true
                )->getSelectionsCollection(
                    $product->getTypeInstance(true)->getOptionsIds($product),
                    $product
                );
//           if (count($selectionCollection) == 1) {
//                $params['qty'] = $params[$params['product-id'].'-qty'];
//            } else {
//                $params['qty'] = $params['qty'];
//            }
        }
                        
        try {
            if (isset($params['qty'])) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface'
                        )->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $related = $this->getRequest()->getParam('related_product');
            
            /**
             * Check product availability
             */
            if (!$product) {
                return parent::goBack();
            }
            
            $this->cart->addProduct($product, $params);
            if (!empty($related)) {
                $this->cart->addProductsByIds(explode(',', $related));
            }
                        
            $this->cart->save();
            
            $flag = false;
            $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customerSession->getCustomer()->getId());
            $current_date = $objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->gmtDate('Y-m-d');
          
            $items = $this->cart->getQuote()->getAllItems();
            $model2 = $objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');
            
            //$this->cart->getQuote()->getAllItems()[$this->cart->getQuote()->getItemsCount() - 1]->setRedirectUrl($params['productUrl'])->save();
                        
            foreach ($model2->getCollection() as $item) {
                if ($item->getQuoteItemId() == $this->cart->getQuote()->getAllVisibleItems()[$this->cart->getQuote()->getItemsCount() - 1]->getId()) {
                    $model2->load($item->getSubscriptionsId())->setQty($item->getQty()+$params['qty'])->save();
                    $model2->load($item->getSubscriptionsId())->setCustomerId($customerSession->getCustomer()->getId())->save();
                    $flag = true;
                }
            }
            
            if (!$flag){
                if ($product->getTypeId() == 'bundle') {
                    $model2->setData('customer_id',$customerSession->getCustomer()->getId());
                    $model2->setData('account_id',$customer->getCustomerLnAccountId());
                    $model2->setData('quote_item_id',$this->cart->getQuote()->getAllVisibleItems()[$this->cart->getQuote()->getItemsCount() - 1]->getId());
                    $model2->setData('product_id',$product->getId());
                    $model2->setData('publication_number',$product->getData('publication_number'));
                    $model2->setData('isbn',$product->getData('isbn'));
                    $model2->setData('sku',$product->getSku());
                    $model2->setData('product_name',$product->getName());
                    $model2->setData('media_type_id',$product->getData('media_type'));
                    $model2->setData('media_type',$product->getResource()->getAttribute('media_type')->getFrontend()->getValue($product));
                    $model2->setData('subscription_start',$current_date);
                    $model2->setData('status',0);
                    $model2->setData('order_status','Incomplete');
                    $model2->setData('qty',$params['qty']);
                    $model2->save();
                    foreach ($items as $item) {
                        $model = $objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');
                        if ($item->getParentItemId() == $this->cart->getQuote()->getAllVisibleItems()[$this->cart->getQuote()->getItemsCount() - 1]->getId()) {
                            $model->setData('customer_id',$customerSession->getCustomer()->getId());
                            $model->setData('account_id',$customer->getCustomerLnAccountId());
                            $model->setData('quote_item_id',$item->getItemId());
                            $model->setData('product_id',$item->getProductId());
                            $model->setData('publication_number',$this->getProduct($item->getProductId())->getData('publication_number'));
                            $model->setData('isbn',$this->getProduct($item->getProductId())->getData('isbn'));
                            $model2->setData('sku',$item->getSku());
                            $model->setData('product_name', $item->getName());
                            $model->setData('media_type_id',$this->getProduct($item->getProductId())->getData('media_type'));
                            $model2->setData('media_type',$this->getProduct($item->getProductId())->getResource()->getAttribute('media_type')->getFrontend()->getValue($this->getProduct($item->getProductId())));
                            $model->setData('subscription_start',$current_date);
                            foreach ($selectionCollection as $proselection) {
                                if ($item->getName() == $proselection->getName()) {
                                    $subscriptions = $objectManager->create('Endeavor\Subscriptions\Model\Subscriptions')->load($cookieManager->getCookie($proselection->getProductId()));
                                    $subscriptionName = $subscriptions->getName();
                                    $model->setData('subscription_id',$cookieManager->getCookie($proselection->getProductId()));
                                    $model->setData('subscription_type',$subscriptionName);
                                }
                            }
                            $model->setData('status',0);
                            $model->setData('order_status','Incomplete');
                            $model->setData('qty',$item->getQty());
                            $model->save();
                        }
                    }
                } else {
                    $subscriptions = $objectManager->create('Endeavor\Subscriptions\Model\Subscriptions')->load($cookieManager->getCookie($product->getId()));
                    $subscriptionName = $subscriptions->getName();
                    $model = $objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');
                    $model->setData('customer_id',$customerSession->getCustomer()->getId());
                    $model->setData('account_id',$customer->getCustomerLnAccountId());
                    $model->setData('quote_item_id',$this->cart->getQuote()->getAllItems()[$this->cart->getQuote()->getItemsCount() - 1]->getId());
                    $model->setData('product_id',$product->getId());
                    $model->setData('publication_number',$product->getData('publication_number'));
                    $model->setData('isbn',$product->getData('isbn'));
                    $model->setData('sku',$product->getSku());
                    $model->setData('product_name',$product->getName());
                    $model->setData('media_type_id',$product->getData('media_type'));
                    $model->setData('media_type',$product->getResource()->getAttribute('media_type')->getFrontend()->getValue($product));
                    $model->setData('subscription_start',$current_date);
                    $model->setData('subscription_type',$subscriptionName);
                    $model->setData('subscription_id',$cookieManager->getCookie($product->getId()));
                    $model->setData('order_status','Incomplete');
                    $model->setData('qty',$params['qty']);
                    $model->save();
                } 
            }
            
            /**
             * @todo remove wishlist observer \Magento\Wishlist\Observer\AddToCart
             */
            $this->_eventManager->dispatch(
                'checkout_cart_add_product_complete',
                ['product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );

            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                if (!$this->cart->getQuote()->getHasError()) {
                    $message = __(
                        'You added %1 to your shopping cart.',
                        $product->getName()
                    );
                    
                    $this->messageManager->addSuccessMessage($message);
                }
                return $this->goBack(null, $product);
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($this->_checkoutSession->getUseNotice(true)) {
                $this->messageManager->addNotice(
                    $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($e->getMessage())
                );
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->messageManager->addError(
                        $this->_objectManager->get('Magento\Framework\Escaper')->escapeHtml($message)
                    );
                }
            }

            $url = $this->_checkoutSession->getRedirectUrl(true);

            if (!$url) {
                $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
                $url = $this->_redirect->getRedirectUrl($cartUrl);
            }

            return parent::goBack($url);

        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t add this item to your shopping cart right now.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            return parent::goBack($url);
        }
    }
    
    /**
     * Retrieve product by id.
     * 
     * @param int $id
     * @return object product
     */
    public function getProduct($id)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();         
        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository'); 
        
        return $productRepository->getById($id); 
    }
}
