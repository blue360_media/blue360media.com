<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Subscriptions\Controller\Cart;

class UpdateItemOptions extends \Magento\Checkout\Controller\Cart\UpdateItemOptions
{
    /**
     * Update product configuration for a cart item
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('id');
        $params = $this->getRequest()->getParams();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $cookieManager = $objectManager->get('Magento\Framework\Stdlib\CookieManagerInterface');
        $product_id = $params['product'];
        
        $product = $this->getProductItems($product_id);
        
        if (!isset($params['options'])) {
            $params['options'] = [];
        }
        try {
            if (isset($params['qty'])) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                    ['locale' => $this->_objectManager->get('Magento\Framework\Locale\ResolverInterface')->getLocale()]
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $quoteItem = $this->cart->getQuote()->getItemById($id);
            if (!$quoteItem) {
                throw new \Magento\Framework\Exception\LocalizedException(__('We can\'t find the quote item.'));
            }

            $item = $this->cart->updateItem($id, new \Magento\Framework\DataObject($params));
            if (is_string($item)) {
                throw new \Magento\Framework\Exception\LocalizedException(__($item));
            }
            if ($item->getHasError()) {
                throw new \Magento\Framework\Exception\LocalizedException(__($item->getMessage()));
            }

            $related = $this->getRequest()->getParam('related_product');
            if (!empty($related)) {
                $this->cart->addProductsByIds(explode(',', $related));
            }
            if ($product->getTypeId() == 'bundle'){
                $selectionCollection = $product->getTypeInstance(true
                    )->getSelectionsCollection(
                        $product->getTypeInstance(true)->getOptionsIds($product),
                        $product
                    );
            }
            $this->cart->save();
            
            $flag = 0;
            $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customerSession->getCustomer()->getId());
            $current_date = $objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->gmtDate('Y-m-d');
            $items = $this->cart->getQuote()->getAllItems();
            $customerSubscriptions = $objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');            
            foreach ($customerSubscriptions->getCollection() as $item) {
                if ($item->getQuoteItemId() == $this->cart->getQuote()->getAllVisibleItems()[$this->cart->getQuote()->getItemsCount() - 1]->getId()) {
                    $customerSubscriptions->load($item->getSubscriptionsId())->setQty($params['qty'])->save();
                    $flag = 1;
                }
            }
                        
            if ($flag==0) {
                if ($product->getTypeId() == 'bundle') {
                    $customerSubscriptions->setData('customer_id',$customerSession->getCustomer()->getId());
                    $customerSubscriptions->setData('account_id',$customer->getCustomerLnAccountId());
                    $customerSubscriptions->setData('quote_item_id',$this->cart->getQuote()->getAllVisibleItems()[$this->cart->getQuote()->getItemsCount() - 1]->getId());
                    $customerSubscriptions->setData('product_id',$product->getId());
                    $customerSubscriptions->setData('publication_number',$product->getData('publication_number'));
                    $customerSubscriptions->setData('isbn',$product->getData('isbn'));
                    $customerSubscriptions->setData('product_desc',$product->getDescription());
                    $customerSubscriptions->setData('product_type',$product->getData('media_type'));
                    $customerSubscriptions->setData('subscription_start',$current_date);
                    $customerSubscriptions->setData('status',0);
                    $customerSubscriptions->setData('order_status','Incomplete');
                    $customerSubscriptions->setData('qty',$params['qty']);
                    $customerSubscriptions->save();
                    foreach ($items as $item) {
                        $model = $objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');
                        if ($item->getParentItemId() == $this->cart->getQuote()->getAllVisibleItems()[$this->cart->getQuote()->getItemsCount() - 1]->getId()) {
                            $model->setData('customer_id',$customerSession->getCustomer()->getId());
                            $model->setData('account_id',$customer->getCustomerLnAccountId());
                            $model->setData('quote_item_id',$item->getItemId());
                            $model->setData('product_id',$item->getProductId());
                            $model->setData('publication_number',$this->getProductItems($item->getProductId())->getData('publication_number'));
                            $model->setData('isbn',$this->getProductItems($item->getProductId())->getData('isbn'));
                            $model->setData('product_desc',$this->getProductItems($item->getProductId())->getDescription());
                            $model->setData('product_type',$this->getProductItems($item->getProductId())->getData('media_type'));
                            $model->setData('subscription_start',$current_date);
                            foreach ($selectionCollection as $proselection) {
                                if ($item->getName() == $proselection->getName()) {
                                    $subscriptions = $objectManager->create('Endeavor\Subscriptions\Model\Subscriptions')->load($cookieManager->getCookie($proselection->getProductId()));
                                    $subscriptionName = $subscriptions->getName();
                                    $model->setData('subscription_id',$cookieManager->getCookie($proselection->getProductId()));
                                    $model->setData('subscription_type',$subscriptionName);
                                }
                            }
                            $model->setData('status',0);
                            $model->setData('order_status','Incomplete');
                            $model->setData('qty',$item->getQty());
                            $model->save();
                        }
                    }
                } else {
                    $subscriptions = $objectManager->create('Endeavor\Subscriptions\Model\Subscriptions')->load($cookieManager->getCookie($product->getId()));
                    $subscriptionName = $subscriptions->getName();
                    $model = $objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');
                    $model->setData('customer_id',$customerSession->getCustomer()->getId());
                    $model->setData('account_id',$customer->getCustomerLnAccountId());
                    $model->setData('quote_item_id',$this->cart->getQuote()->getAllItems()[$this->cart->getQuote()->getItemsCount() - 1]->getId());
                    $model->setData('product_id',$product->getId());
                    $model->setData('publication_number',$product->getData('publication_number'));
                    $model->setData('isbn',$product->getData('isbn'));
                    $model->setData('product_desc',$product->getDescription());
                    $model->setData('product_type',$product->getData('media_type'));
                    $model->setData('subscription_start',$current_date);
                    $model->setData('subscription_type',$subscriptionName);
                    $model->setData('subscription_id',$cookieManager->getCookie($product->getId()));
                    $model->setData('order_status','Incomplete');
                    $model->setData('qty',$params['qty']);
                    $model->save();
                } 
            }
            
            $this->_eventManager->dispatch(
                'checkout_cart_update_item_complete',
                ['item' => $item, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
            );
            if (!$this->_checkoutSession->getNoCartRedirect(true)) {
                if (!$this->cart->getQuote()->getHasError()) {
                    $message = __(
                        '%1 was updated in your shopping cart.',
                        $this->_objectManager->get('Magento\Framework\Escaper')
                            ->escapeHtml($product->getName())
                    );
                    $this->messageManager->addSuccess($message);
                }
                return $this->_goBack($this->_url->getUrl('checkout/cart'));
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            if ($this->_checkoutSession->getUseNotice(true)) {
                $this->messageManager->addNotice($e->getMessage());
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->messageManager->addError($message);
                }
            }

            $url = $this->_checkoutSession->getRedirectUrl(true);
            if ($url) {
                return $this->resultRedirectFactory->create()->setUrl($url);
            } else {
                $cartUrl = $this->_objectManager->get('Magento\Checkout\Helper\Cart')->getCartUrl();
                return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRedirectUrl($cartUrl));
            }
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t update the item right now.'));
            $this->_objectManager->get('Psr\Log\LoggerInterface')->critical($e);
            return $this->_goBack();
        }
        return $this->resultRedirectFactory->create()->setPath('*/*');
    }
    
    /**
     * Retrieve product by id.
     * 
     * @param int $id
     * @return object product
     */
    public function getProductItems($id)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();         
        $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository'); 
        
        return $productRepository->getById($id); 
    }
}
