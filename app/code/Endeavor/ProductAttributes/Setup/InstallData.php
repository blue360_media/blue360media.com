<?php

namespace Endeavor\ProductAttributes\Setup;

use Magento\Eav\Setup\EavSetup; 
use Magento\Eav\Setup\EavSetupFactory /* For Attribute create  */;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->eavSetupFactory = $eavSetupFactory; 
        $this->logger = $logger;
        /* assign object to class global variable for use in other class methods */
    }
 
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $this->logger->critical("Start Install Data.");
        $this->logger->debug('Installing data');
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
         
        /**
         * Add attributes to the eav/attribute
         */
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'update_date',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'datetime',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\Datetime',
                'frontend' => 'Magento\Eav\Model\Entity\Attribute\Frontend\Datetime',
                'label' => 'Update Date', 
                'input' => 'date',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'volumes_number',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Number of Volumes', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'publisher_name',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Publisher Name', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'edition',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Edition', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'pages_number',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Pages Number', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'isbn',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'ISBN', 
                'input' => 'text',
                'required' => true,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_grid' =>  true,
                'visible_in_grid' =>  true,
                'filterable_in_grid' =>  true,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
		$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'publication_number',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Publication Number', 
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'searchable' => true,
                'filterable' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_grid' =>  true,
                'visible_in_grid' =>  true,
                'filterable_in_grid' =>  true,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'grouping_number',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Grouping Number', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'new_business_list_price',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'New Business List Price', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => true,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'subscription_list_price',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Subscription List Price', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'subscription_type',
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Product Subscription Type', 
                'input' => 'select',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'option' => array ( 'value' => 
                    array(
                        'Release' => array('Release'),
                        'One Time Purchase' => array('One Time Purchase')
                        )
                )
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'subscription_price',
            [
                'group' => 'Attributes',
                'type' => 'decimal',
                'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Price',
                'frontend' => '',
                'label' => 'Last Release/Subscription Price', 
                'input' => 'price',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'warehouse_location',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'text',
                'backend' => '',
                'frontend' => '',
                'label' => 'Warehouse Location', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'media_type',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Media type', 
                'input' => 'multiselect',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'option' => array ( 'value' => 
                                array(
                                    'eBook' => array('eBook'),
                                    'HardCover' => array('Hard Cover'),
                                    'SoftCover' => array('Soft Cover'),
                                    'Looseleaf' => array('Looseleaf'),
                                    'Print' => array('Print'),
                                    'Combo' => array('Combo'),
                                    'CD' => array('CD'),
                                    'eBook + Book' => array('eBook + Book'),
                                    'Other' => array('Other'),
                                    )
                    )
            ]
        );
		$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            's_h_over_ride',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'S&H Over-ride', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'update_frequency',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Number of Annually Update', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'release_date',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'datetime',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\Datetime',
                'frontend' => '',
                'label' => 'Next Release Date', 
                'input' => 'date',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'last_release_date',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'datetime',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\Datetime',
                'frontend' => '',
                'label' => 'Last Release Date', 
                'input' => 'date',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
		$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'product_type_category',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Product Type Category', 
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => false,
                'required' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => true,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'product_sub_type_category',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Product Sub-Type Category', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
		$eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'jurisdiction',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Product Jurisdiction', 
                'input' => 'multiselect',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
				'option' => array ( 'value' => 
                        array(
                            'Alabama' => array('Alabama'),
                            'Alaska' => array('Alaska'),
                            'Arizona' => array('Arizona'),
                            'Arkansas' => array('Arkansas'),
                            'California' => array('California'),
                            'Colorado' => array('Colorado'),
                            'Connecticut' => array('Connecticut'),
                            'Delaware' => array('Delaware'),
                            'Federal' => array('Federal'),
                            'Florida' => array('Florida'),
                            'Georgia' => array('Georgia'),
                            'Hawaii' => array('Hawaii'),
                            'Idaho' => array('Idaho'),
                            'Illinois' => array('Illinois'),
                            'Indiana' => array('Indiana'),
                            'Iowa' => array('Iowa'),
                            'Kansas' => array('Kansas'),
                            'Kentucky' => array('Kentucky'),
                            'Louisiana' => array('Louisiana'),
                            'Maine' => array('Maine'),
                            'Maryland' => array('Maryland'),
                            'Massachusetts' => array('Massachusetts'),
                            'Michigan' => array('Michigan'),
                            'Minnesota' => array('Minnesota'),
                            'Mississippi' => array('Mississippi'),
                            'Missouri' => array('Missouri'),
                            'Montana' => array('Montana'),
                            'National' => array('National'),
                            'Nebraska' => array('Nebraska'),
                            'Nevada' => array('Nevada'),
                            'New Hampshire' => array('New Hampshire'),
                            'New Jersey' => array('New Jersey'),
                            'New Mexico' => array('New Mexico'),
                            'New York' => array('New York'),
                            'North Carolina' => array('North Carolina'),
                            'North Dakota' => array('North Dakota'),
                            'Ohio' => array('Ohio'),
                            'Oklahoma' => array('Oklahoma'),
                            'Oregon' => array('Oregon'),
                            'Pennsylvania' => array('Pennsylvania'),
                            'Puerto Rico' => array('Puerto Rico'),
                            'Rhode Island' => array('Rhode Island'),
                            'South Carolina' => array('South Carolina'),
                            'South Dakota' => array('South Dakota'),
                            'Tennessee' => array('Tennessee'),
                            'Texas' => array('Texas'),
                            'Utah' => array('Utah'),
                            'Vermont' => array('Vermont'),
                            'Virgin Islands' => array('Virgin Islands'),
                            'Virginia' => array('Virginia'),
                            'Washington' => array('Washington'),
                            'West Virginia' => array('West Virginia'),
                            'Wisconsin' => array('Wisconsin'),
                            'Wyoming' => array('Wyoming')
                            )
                    )
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'publication_cost',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Publication Cost', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'part_number',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Part Number', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'multipart_ui',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Multi Part Unique Identifiers', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false
            ]
        );
    }
}