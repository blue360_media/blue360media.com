<?php 
namespace Endeavor\ProductAttributes\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Eav\Setup\EavSetupFactory /* For Attribute create  */;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface {
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;
    
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->eavSetupFactory = $eavSetupFactory; 
        $this->logger = $logger;
        /* assign object to class global variable for use in other class methods */
    }
    /**
     * {@inheritdoc}
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->upgradeSortOrder($setup);
        }
        
    }

    /**
     * Add meta title
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function upgradeSortOrder(ModuleDataSetupInterface $setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'volumes_number',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Nº OF VOL.', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false,
                'sort_order' => 5
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'publisher_name',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'PUBLISHER', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false,
                'sort_order' => 1
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'edition',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'EDITION', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false,
                'sort_order' => 3
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'pages_number',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'PAGES Nº', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => false,
                'unique' => false,
                'sort_order' => 6
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'isbn',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'ISBN', 
                'input' => 'text',
                'required' => true,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => false,
                'default' => '',
                'searchable' => true,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => true,
                'used_in_grid' =>  true,
                'visible_in_grid' =>  true,
                'filterable_in_grid' =>  true,
                'used_in_product_listing' => false,
                'unique' => false,
                'sort_order' => 2
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'update_frequency',/* Custom Attribute Code */
            [
                'group' => 'Attributes',
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Number of Annually Update', 
                'input' => 'text',
                'required' => false,
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => true,
                'comparable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => false,
                'unique' => false,
                'sort_order' => 7
            ]
        );
        
    }
}
