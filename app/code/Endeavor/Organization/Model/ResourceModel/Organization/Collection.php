<?php
namespace Endeavor\Organization\Model\ResourceModel\Organization;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'order_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Organization\Model\Organization', 'Endeavor\Organization\Model\ResourceModel\Organization');
      
    }
}
