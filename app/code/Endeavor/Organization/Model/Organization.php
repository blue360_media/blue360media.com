<?php
namespace Endeavor\Organization\Model;

use Magento\Framework\DataObject\IdentityInterface;

class Organization extends \Magento\Framework\Model\AbstractModel implements IdentityInterface
{
    /**
     * @var string
     */
    const CACHE_TAG = 'endeavor_order_history';
    
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Organization\Model\ResourceModel\Organization');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    
}
