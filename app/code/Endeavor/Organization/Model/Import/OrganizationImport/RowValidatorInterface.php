<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Organization\Model\Import\OrganizationImport;

interface RowValidatorInterface extends \Magento\Framework\Validator\ValidatorInterface
{
    
    /**
     * Constants Used in Import Validation.
     */
    const ERROR_INVALID_ORDER_ID= 'InvalidValueORDER_ID';
    const ERROR_ORDER_ID_IS_EMPTY = 'EmptyORDER_ID';
    
    /**
     * Initialize validator
     *
     * @return $this
     */
    public function init($context);
}
