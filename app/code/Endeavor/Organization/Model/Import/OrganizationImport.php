<?php
namespace Endeavor\Organization\Model\Import;

use Endeavor\Organization\Model\Import\OrganizationImport\RowValidatorInterface as ValidatorInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;

class OrganizationImport extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity
{
    
    /**
     * Columns Names used in importer
     */
    const ORDER_ID = 'order_id';
    const CUSTOMER_ID = 'customer_id';
    const ACCOUNT_ID = 'account_id';
    const ORDER_DATE = 'order_date';
    const SHIP_DETAILS = 'ship_details';
    const BILL_DETAILS = 'bill_details';
    const TAX_EXEMPT_IND = 'tax_exempt_ind';
    const PO_REQ_IND = 'po_req_ind';
    const SEGMENT = 'segment';
    const PUBLICATION_NUMBER = 'publication_number';
    const ISBN = 'isbn';
    const TITLE_DESCRIPTION = 'title_description';
    const MEDIA = 'media';
    const ORIGINAL_INVOICE_NUMBER = 'original_invoice_number';
    const INVOICE_NUMBER = 'invoice_number';
    const SUBSCRIPTION_TYPE = 'subscription_type';
    const CREDIT_REASON = 'credit_reason';
    const RETURN_DATE = 'return_date';
    const MODIFIED_DURATION_MONTH = 'modified_duration_month';
    const QTY = 'qty';
    const INVOICE_PAID = 'invoice_paid';
    const LIST_VALUE = 'list_value';
    const UPKEEP = 'upkeep';
    const FREIGHT = 'freight';
    const COMBO = 'combo';
    const SALES_TYPE_CODE = 'sales_type_code';
    const SALES_CHANNEL_CODE = 'sales_channel_code';
    const TABLE_Entity = 'endeavor_order_history';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = [
        ValidatorInterface::ERROR_ORDER_ID_IS_EMPTY => 'Order Id is empty',
    ];
    
    /**
    *
    * @var Order_Id
    */
    protected $_permanentAttributes = [self::ORDER_ID];
    
    /**
    * If we should check column names
    *
    * @var bool
    */
    protected $needColumnCheck = true;
    
    /**
    *
    * @var \Magento\Customer\Model\GroupFactory
    */
    protected $groupFactory;
    
    /**
     * Valid column names
     *
     * @array
     */
    protected $validColumnNames = [
        self::ORDER_ID,
        self::CUSTOMER_ID,
        self::ACCOUNT_ID,
        self::ORDER_DATE,
        self::SHIP_DETAILS,
        self::BILL_DETAILS,
        self::TAX_EXEMPT_IND,
        self::PO_REQ_IND,
        self::SEGMENT,
        self::PUBLICATION_NUMBER,
        self::ISBN,
        self::TITLE_DESCRIPTION,
        self::MEDIA,
        self::ORIGINAL_INVOICE_NUMBER,
        self::INVOICE_NUMBER,
        self::SUBSCRIPTION_TYPE,
        self::CREDIT_REASON,
        self::RETURN_DATE,
        self::MODIFIED_DURATION_MONTH,
        self::QTY,
        self::INVOICE_PAID,
        self::LIST_VALUE,
        self::UPKEEP,
        self::FREIGHT,
        self::COMBO,
        self::SALES_TYPE_CODE,
        self::SALES_CHANNEL_CODE,
    ];

    /**
     * Need to log in import history
     *
     * @var bool
     */
    protected $logInHistory = true;

    /**
     *
     * @var validators
     */
    protected $_validators = [];

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_connection;
    
    /**
     *
     * @var \Magento\Framework\App\ResourceConnection 
     */
    protected $_resource;

    /**
     * 
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\ImportExport\Helper\Data $importExportData
     * @param \Magento\ImportExport\Model\ResourceModel\Import\Data $importData
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param ProcessingErrorAggregatorInterface $errorAggregator
     * @param \Magento\Customer\Model\GroupFactory $groupFactory
     */
    public function __construct(
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\ImportExport\Helper\Data $importExportData,
        \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
        \Magento\Framework\Stdlib\StringUtils $string,
        ProcessingErrorAggregatorInterface $errorAggregator,
        \Magento\Customer\Model\GroupFactory $groupFactory
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->groupFactory = $groupFactory;
    }
    
    /*
     * Check the names of columns
     * 
     * @return array
     */
    public function getValidColumnNames()
    {
        return $this->validColumnNames;
    }

    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return 'endeavor_order_history';
    }

    /**
     * Row validation.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum)
    {

        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }

        $this->_validatedRows[$rowNum] = true;
        if (!isset($rowData[self::ORDER_ID]) || empty($rowData[self::ORDER_ID])) {
            $this->addRowError(ValidatorInterface::ERROR_ORDER_ID_IS_EMPTY, $rowNum);
            return false;
        }

        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }

    /**
     * Create Advanced price data from raw data.
     *
     * @throws \Exception
     * @return bool Result of operation.
     */
    protected function _importData()
    {
        if (\Magento\ImportExport\Model\Import::BEHAVIOR_DELETE == $this->getBehavior()) {
            $this->deleteEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $this->getBehavior()) {
            $this->replaceEntity();
        } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $this->getBehavior()) {
            $this->saveEntity();
        }

        return true;
    }
    
    /**
     * Save newsletter subscriber
     *
     * @return $this
     */
    public function saveEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }
    
    /**
     * Replace newsletter subscriber
     *
     * @return $this
     */
    public function replaceEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }
    
    /**
     * Deletes newsletter subscriber data from raw data.
     *
     * @return $this
     */
    public function deleteEntity()
    {
        $listTitle = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            foreach ($bunch as $rowNum => $rowData) {
                $this->validateRow($rowData, $rowNum);
                if (!$this->getErrorAggregator()->isRowInvalid($rowNum)) {
                    $rowTtile = $rowData[self::ORDER_ID];
                    $listTitle[] = $rowTtile;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                }
            }
        }
        if ($listTitle) {
            $this->deleteEntityFinish(array_unique($listTitle),self::TABLE_Entity);
        }
        return $this;
    }
    
    /**
     * Save and replace newsletter subscriber
     *
     * @return $this
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function saveAndReplaceEntity()
    {
        $behavior = $this->getBehavior();
        $listTitle = [];
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $entityList = [];
            foreach ($bunch as $rowNum => $rowData) {
                if (!$this->validateRow($rowData, $rowNum)) {
                    $this->addRowError(ValidatorInterface::ERROR_ORDER_ID_IS_EMPTY, $rowNum);
                    continue;
                }
                if ($this->getErrorAggregator()->hasToBeTerminated()) {
                    $this->getErrorAggregator()->addRowToSkip($rowNum);
                    continue;
                }

                $rowTtile= $rowData[self::ORDER_ID];
                $listTitle[] = $rowTtile;
                $entityList[$rowTtile][] = [
                    self::ORDER_ID => $rowData[self::ORDER_ID],
                    self::CUSTOMER_ID => $rowData[self::CUSTOMER_ID],
                    self::ACCOUNT_ID => $rowData[self::ACCOUNT_ID],
                    self::ORDER_DATE => $rowData[self::ORDER_DATE],
                    self::SHIP_DETAILS => $rowData[self::SHIP_DETAILS],
                    self::BILL_DETAILS => $rowData[self::BILL_DETAILS],
                    self::TAX_EXEMPT_IND => $rowData[self::TAX_EXEMPT_IND],
                    self::PO_REQ_IND => $rowData[self::PO_REQ_IND],
                    self::SEGMENT => $rowData[self::SEGMENT],
                    self::PUBLICATION_NUMBER => $rowData[self::PUBLICATION_NUMBER],
                    self::ISBN => $rowData[self::ISBN],
                    self::TITLE_DESCRIPTION => $rowData[self::TITLE_DESCRIPTION],
                    self::MEDIA => $rowData[self::MEDIA],
                    self::ORIGINAL_INVOICE_NUMBER => $rowData[self::ORIGINAL_INVOICE_NUMBER],
                    self::INVOICE_NUMBER => $rowData[self::INVOICE_NUMBER],
                    self::SUBSCRIPTION_TYPE => $rowData[self::SUBSCRIPTION_TYPE],
                    self::CREDIT_REASON => $rowData[self::CREDIT_REASON],
                    self::RETURN_DATE => $rowData[self::RETURN_DATE],
                    self::MODIFIED_DURATION_MONTH => $rowData[self::MODIFIED_DURATION_MONTH],
                    self::QTY => $rowData[self::QTY],
                    self::INVOICE_PAID => $rowData[self::INVOICE_PAID],
                    self::LIST_VALUE => $rowData[self::LIST_VALUE],
                    self::UPKEEP => $rowData[self::UPKEEP],
                    self::FREIGHT => $rowData[self::FREIGHT],
                    self::COMBO => $rowData[self::COMBO],
                    self::SALES_TYPE_CODE => $rowData[self::SALES_TYPE_CODE],
                    self::SALES_CHANNEL_CODE => $rowData[self::SALES_CHANNEL_CODE],
                ];
            }            
            if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                if ($listTitle) {
                    if ($this->deleteEntityFinish(array_unique(  $listTitle), self::TABLE_Entity)) {
                        $this->saveEntityFinish($entityList, self::TABLE_Entity);
                    }
                }
            } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                $this->saveEntityFinish($entityList, self::TABLE_Entity);
            }
        }
        return $this;
    }
    
    /**
     * Save Organization.
     *
     * @param array $entityData
     * @param string $table
     * @return $this
     */
    protected function saveEntityFinish(array $entityData, $table)
    {
        if ($entityData) {
            $tableName = $this->_connection->getTableName($table);
            $entityIn = [];
            foreach ($entityData as $id => $entityRows) {
                    foreach ($entityRows as $row) {
                        $entityIn[] = $row;
                    }
            }
            if ($entityIn) {
                $this->_connection->insertOnDuplicate($tableName, $entityIn,[
                    self::ORDER_ID,
                    self::CUSTOMER_ID,
                    self::ACCOUNT_ID,
                    self::ORDER_DATE,
                    self::SHIP_DETAILS,
                    self::BILL_DETAILS,
                    self::TAX_EXEMPT_IND,
                    self::PO_REQ_IND,
                    self::SEGMENT,
                    self::PUBLICATION_NUMBER,
                    self::ISBN,
                    self::TITLE_DESCRIPTION,
                    self::MEDIA,
                    self::ORIGINAL_INVOICE_NUMBER,
                    self::INVOICE_NUMBER,
                    self::SUBSCRIPTION_TYPE,
                    self::CREDIT_REASON,
                    self::RETURN_DATE,
                    self::MODIFIED_DURATION_MONTH,
                    self::QTY,
                    self::INVOICE_PAID,
                    self::LIST_VALUE,
                    self::UPKEEP,
                    self::FREIGHT,
                    self::COMBO,
                    self::SALES_TYPE_CODE,
                    self::SALES_CHANNEL_CODE
                    ]);
            }
        }
        return $this;
    }
    
    /**
     * Delete Organization.
     *
     * @param array $listTitle
     * @param string $table
     * @return bool
     */
    protected function deleteEntityFinish(array $listTitle, $table)
    {
        if ($table && $listTitle) {
            try {
                $this->countItemsDeleted += $this->_connection->delete(
                    $this->_connection->getTableName($table),
                    $this->_connection->quoteInto('order_id IN (?)', $listTitle)
                );
                return true;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }
}
