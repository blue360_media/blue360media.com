<?php
namespace Endeavor\Organization\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
      
        $table = $installer->getConnection()->newTable(
            $installer->getTable('endeavor_order_history')) 
        ->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
            'Order Id'
        )
        ->addColumn(
            'customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true, 'unsigned' => true],
            'Customer Id'
        )
        ->addColumn(
            'account_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true, 'unsigned' => true],
            'Account Id'
        )
        ->addColumn(
            'order_date',
            \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
            null,
            ['nullable' => true],
            'Order Date'
        )
        ->addColumn(
            'ship_details',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Ship Details'
        )
        ->addColumn(
            'bill_details',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Bill Details'
        )
        ->addColumn(
            'tax_exempt_ind',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Tax Exempt Indicator'
        )
        ->addColumn(
            'po_req_ind',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'P.O. Required'
        )
        ->addColumn(
            'segment',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Segment'
        )
        ->addColumn(
            'publication_number',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Publication Number'
        )
        ->addColumn(
            'isbn',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            ['nullable' => true],
            'ISBN'
        )
        ->addColumn(
            'title_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            null,
            ['nullable' => true],
            'Publication Title'
        )
        ->addColumn(
            'media',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            ['nullable' => true],
            'Media'
        )
        ->addColumn(
            'original_invoice_number',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            ['nullable' => true],
            'Orginal Invoice Number'
        )
        ->addColumn(
            'invoice_number',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            ['nullable' => true],
            'Invoice Number'
        )
        ->addColumn(
            'subscription_type',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            ['nullable' => true],
            'Subscription Type'
        )
        ->addColumn(
            'credit_reason',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            500,
            ['nullable' => true],
            'Credit Reason'
        )
        ->addColumn(
            'return_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_DATE,
                null,
                [
                    'nullable' => true,
                ],
                'Return Date'
        )
        ->addColumn(
            'modified_duration_month',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Modified Duration Month'
        )
        ->addColumn(
            'qty',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => true],
            'Qty'
        )
        ->addColumn(
            'invoice_paid',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['nullable' => true],
            'Invoice Paid'
        )
        ->addColumn(
            'list_value',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['nullable' => true],
            'List Price'
        )
        ->addColumn(
            'upkeep',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            10,
            ['nullable' => true],
            'Upkeep'
        )
        ->addColumn(
            'freight',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['nullable' => true],
            'Freight'
        )
        ->addColumn(
            'combo',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            ['nullable' => true],
            'Combo'
        )
        ->addColumn(
            'sales_type_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            ['nullable' => true],
            'Sales Type Code'
        )
        ->addColumn(
            'sales_channel_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            50,
            ['nullable' => true],
            'Sales Channel Code'
        )
        ->addColumn(
            'discount_percentage',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['nullable' => true],
            'Discount Percentage'
        )
        ->addIndex(
            $installer->getIdxName('endeavor_order_history', ['customer_id']),
            ['customer_id']
        )
        ->addIndex(
            $installer->getIdxName('endeavor_order_history', ['account_id']),
            ['account_id']
        )
        ->addForeignKey(
            $installer->getFkName(
                'endeavor_order_history',
                'customer_id',
                'customer_entity',
                'entity_id'
            ),
            'customer_id',
            $installer->getTable('customer_entity'), 
            'entity_id',
            \Magento\Framework\Db\Ddl\Table::ACTION_SET_NULL
        )
        ->addForeignKey(
            $installer->getFkName(
                'endeavor_order_history',
                'account_id',
                'customer_entity',
                'entity_id'
            ),
            'account_id',
            $installer->getTable('customer_entity'), 
            'entity_id',
            \Magento\Framework\Db\Ddl\Table::ACTION_SET_NULL
        )
        ->setComment(
            'LexisNexis Order History'
        );
        
        $installer->getConnection()->createTable($table);
        
        $installer->endSetup();
    }
}
