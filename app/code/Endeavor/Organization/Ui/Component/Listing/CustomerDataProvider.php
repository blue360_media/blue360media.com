<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\Organization\Ui\Component\Listing;

/**
 * Description of CustomerDataProvider
 *
 * @author Nadeem Khleif
 */
class CustomerDataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\SearchResult
{
    
    /*
     * initiate the collection.
     * 
     * @return $this
     */
    protected function _initSelect()
    {
        parent::_initSelect();
        
        $this->getSelect()->group('main_table.entity_id'
                )->joinLeft(
                        ['secondTable' => $this->getTable('endeavor_order_history')],
                        'main_table.entity_id = secondTable.customer_id',
                        array(
                            'publication_number'=>'GROUP_CONCAT(DISTINCT secondTable.publication_number SEPARATOR "<br />")',
                            'subscription_type'=>'GROUP_CONCAT(DISTINCT secondTable.subscription_type ORDER BY secondTable.subscription_type SEPARATOR "<br />")'
                        )
                );
            
        return $this;
    }
}