<?php
namespace Endeavor\Organization\Block\Adminhtml\Organization\Edit\Tab;

class RelatedCustomers extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;
    
    /**
    * @var \Magento\Catalog\Model\Category
    */
    protected $_subcategories;
    
    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Catalog\Model\Category $subcategories
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Catalog\Model\Category $subcategories,
        array $data = []
    ) {
        $this->_subcategories = $subcategories;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    
    /**
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_coreRegistry->registry(RegistryConstants::CURRENT_CUSTOMER_ID);
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Related Accounts');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Related Accounts');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {   
    	
        $customerId = $this->getRequest()->getParam('id');
       
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);

         // show Tab By Role Id 
        $roleId = $customerObj->getCustomerRole();
           
        // role id for organization is 4 .
        if ($roleId == 60) {
            return true;
        } else {
            return false;
        }
        
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    
    /**
     * Tab class getter
     *
     * @return string
     */
    public function getTabClass()
    {
        return '';
    }
    
    /**
     * Return URL link to Tab content
     *
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('organization/organization/view', ['_current' => true]);
    }
    
     /**
     * {@inheritdoc}
     */
    public function getGridUrl()
    {
        return $this->getUrl('customer/*/view', ['_current' => true]);
    }
    
    /**
     * Tab should be loaded trough Ajax call
     *
     * @return bool
     */
    public function isAjaxLoaded()
    {
        return true;
    }
    
    /**
     * Get the ajax controller url.
     * 
     * @return type
     */
    public function getAjaxUrl()
    {
        return $this->getUrl('organization/organization/ajax');
    }
   
    /**
     * Get the current customer id
     * 
     * @return type
     */
    public function getCurrentCustomer()
    {
        return $this->getRequest()->getParam('id');
    }
}
