<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\Organization\Block\Adminhtml\Organization\Edit\Tab;
 
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;
    
    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->productFactory = $productFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('customer_tab_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

    /**
     * @return Grid
     */
    protected function _prepareCollection()
    {   
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collection = $objectManager->create('Magento\Customer\Model\Customer')->getCollection();
        $collection->addFieldToSelect(['entity_id', 'email', 'firstname', 'lastname']);
        $collection->addAttributeToSelect(['lexis_nexis_account_id', 'sales_rep', 'annual_account_spend']);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                'sortable' => false,
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        
        $this->addColumn(
            'lexis_nexis_account_id',
            [
                'header' => __('Account Id'),
                'sortable' => false,
                'index' => 'lexis_nexis_account_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'firstname',
            [
                'header' => __('First Name'),
                'sortable' => false,
                'index' => 'firstname'
            ]
        );
        
        $this->addColumn(
            'lastname',
            [
                'header' => __('Last Name'),
                'sortable' => false,
                'index' => 'lastname'
            ]
        );
        
        $this->addColumn(
            'email',
            [
                'header' => __('Email'),
                'sortable' => false,
                'index' => 'email'
            ]
        );
        
        $this->addColumn(
            'sales_rep',
            [
                'header' => __('Sales Representitve'),
                'sortable' => false,
                'index' => 'sales_rep'
            ]
        );
        
        $this->addColumn(
            'annual_account_spend',
            [
                'header' => __('Annual Account Spend'),
                'sortable' => false,
                'index' => 'annual_account_spend'
            ]
        );
        
        $this->addColumn('action', array(
            'header' => __('Action'),
            'width' => '100',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => __('View'),
                    'url' => array('base' => 'customer/index/edit'),
                    'field' => 'id'
                )

            ), 
            'filter' => false,
            'sortable' => false,
            'index' => 'stores',
            'is_system' => true,
        ));
        return parent::_prepareColumns();
    }
    
    /**
     * Get the Grid Url
     * 
     * @return string
     */
    public function getGridUrl()
    {
      return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * Get the main organizations view
     * 
     * @return string
     */
    public function getOrgUrl()
    {
        return $this->getUrl('organization/organization/index', ['_current' => true]);
    }
    
    /**
     * Get the main customers view
     * 
     * @return string
     */
    public function getNewCustomerUrl()
    {
        return $this->getUrl('customer/index/new');
    }
         
    
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    
}