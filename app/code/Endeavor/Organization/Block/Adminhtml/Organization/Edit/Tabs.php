<?php
namespace Endeavor\Organization\Block\Adminhtml\Organization\Edit;

/**
 * Admin page left menu
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    
    /**
     * Initiate the tab label and id.
     * 
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('organization_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('LexisNexis Order History Settings'));
    }
}
