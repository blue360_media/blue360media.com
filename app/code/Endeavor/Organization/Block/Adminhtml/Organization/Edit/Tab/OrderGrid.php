<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\Organization\Block\Adminhtml\Organization\Edit\Tab;
 
class OrderGrid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->productFactory = $productFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('order_tab_grid');
        $this->setDefaultSort('order_id');
        $this->setUseAjax(true);

    }

    /**
     * @return Grid
     */
    protected function _prepareCollection()
    {   
        $id = $this->getRequest()->getParam('id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collection = $objectManager->create('Magento\Customer\Model\Customer')->load($id);
        $role = $collection->getCustomerRole();
        if ($role == 60) {
            $orderCollection = $objectManager->create('Endeavor\Organization\Model\Organization')->getCollection();
            $orderCollection->addFieldToFilter('customer_id',$id);
            $this->setCollection($orderCollection);
        } else {
            $organizationCollection = $objectManager->create('Endeavor\Organization\Model\Organization')->getCollection(); 
            $organizationCollection->addFieldToFilter('account_id',$id);
            $this->setCollection($organizationCollection);
        }
        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'order_date',
            [
                'header' => __('Order Date'),
                'sortable' => false,
                'index' => 'order_date'
            ]
        );
        
        $this->addColumn(
            'ship_details',
            [
                'header' => __('Ship Details'),
                'sortable' => false,
                'index' => 'ship_details',
                'renderer' => 'Endeavor\Organization\Block\Adminhtml\Organization\Edit\Tab\Renderer\HtmlRenderer'
            ]
        );
        
        $this->addColumn(
            'bill_details',
            [
                'header' => __('Bill Details'),
                'sortable' => false,
                'index' => 'bill_details',
                'renderer' => 'Endeavor\Organization\Block\Adminhtml\Organization\Edit\Tab\Renderer\HtmlRenderer'
            ]
        );
        
        $this->addColumn(
            'tax_exempt_ind',
            [
                'header' => __('Tax Exempt Indicator'),
                'sortable' => false,
                'index' => 'tax_exempt_ind'
            ]
        );
        
        $this->addColumn(
            'po_req_ind',
            [
                'header' => __('P.O. Required'),
                'sortable' => false,
                'index' => 'po_req_ind'
            ]
        );
        
        $this->addColumn(
            'segment',
            [
                'header' => __('Segment'),
                'sortable' => false,
                'index' => 'segment'
            ]
        );
        
        $this->addColumn(
            'publication_number',
            [
                'header' => __('Publication Number'),
                'sortable' => false,
                'index' => 'publication_number'
            ]
        );
        
        $this->addColumn(
            'isbn',
            [
                'header' => __('ISBN'),
                'sortable' => false,
                'index' => 'isbn'
            ]
        );
        
        $this->addColumn(
            'title_description',
            [
                'header' => __('Title Description'),
                'sortable' => false,
                'index' => 'title_description'
            ]
        );
        
        $this->addColumn(
            'media',
            [
                'header' => __('Media'),
                'sortable' => false,
                'index' => 'media'
            ]
        );
        
        $this->addColumn(
            'original_invoice_number',
            [
                'header' => __('Orginal Invoice Number'),
                'sortable' => false,
                'index' => 'original_invoice_number'
            ]
        );
        
        $this->addColumn(
            'invoice_number',
            [
                'header' => __('Invoice Number'),
                'sortable' => false,
                'index' => 'invoice_number'
            ]
        );
        
        $this->addColumn(
            'subscription_type',
            [
                'header' => __('Subscription Type'),
                'sortable' => false,
                'index' => 'subscription_type'
            ]
        );
        
        $this->addColumn(
            'credit_reason',
            [
                'header' => __('Credit Reason'),
                'sortable' => false,
                'index' => 'credit_reason'
            ]
        );
        
        $this->addColumn(
            'return_date',
            [
                'header' => __('Return Date'),
                'sortable' => false,
                'index' => 'return_date'
            ]
        );
        
        $this->addColumn(
            'modified_duration_month',
            [
                'header' => __('Modified Duration Month'),
                'sortable' => false,
                'index' => 'modified_duration_month'
            ]
        );
        
        $this->addColumn(
            'qty',
            [
                'header' => __('Quantity'),
                'sortable' => false,
                'index' => 'qty'
            ]
        );
        
        $this->addColumn(
            'invoice_paid',
            [
                'header' => __('Invoice Paid'),
                'sortable' => false,
                'index' => 'invoice_paid'
            ]
        );
        
        $this->addColumn(
            'list_value',
            [
                'header' => __('List Price'),
                'sortable' => false,
                'index' => 'list_value'
            ]
        );
        
        $this->addColumn(
            'upkeep',
            [
                'header' => __('Upkeep'),
                'sortable' => false,
                'index' => 'upkeep'
            ]
        );
        
        $this->addColumn(
            'freight',
            [
                'header' => __('Freight'),
                'sortable' => false,
                'index' => 'freight'
            ]
        );
        
        $this->addColumn(
            'combo',
            [
                'header' => __('Combo'),
                'sortable' => false,
                'index' => 'combo'
            ]
        );
        
        $this->addColumn(
            'sales_type_code',
            [
                'header' => __('Sales Type Code'),
                'sortable' => false,
                'index' => 'sales_type_code'
            ]
        );
        
        $this->addColumn(
            'sales_channel_code',
            [
                'header' => __('Sales Channel Code'),
                'sortable' => false,
                'index' => 'sales_channel_code'
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * Get Grid Url.
     * 
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('organization/*/history', array('_current'=>true));
    }

    /**
     * Return row url for js event handlers
     *
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $item
     * @return string
     */
    public function getRowUrl($item)
    {
        return $this->getUrl('organization/*/edit', array('order_id'=>$item->getId(),'id'=>$this->getRequest()->getParam('id')));
    }
}