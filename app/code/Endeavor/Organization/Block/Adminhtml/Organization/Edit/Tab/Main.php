<?php 

namespace Endeavor\Organization\Block\Adminhtml\Organization\Edit\Tab;

class Main extends \Magento\Backend\Block\Widget\Form\Generic implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $store;
    
    /**
    * @var \Magento\Catalog\Model\Category
    */
    protected $_subcategories;
    
     /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    protected $_wysiwygConfig;
    
    /**
     *
     * @var Magento\Directory\Model\ResourceModel\Country\CollectionFactory 
     */
    protected $_countryCollectionFactory;

    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Catalog\Model\Category $subcategories
     * @param \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Catalog\Model\Category $subcategories,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig, 
        array $data = []
    ) {
        $this->_subcategories = $subcategories;
        $this->_wysiwygConfig = $wysiwygConfig;
        $this->_countryCollectionFactory = $countryCollectionFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        /* @var $model \Endeavor\CustomerSegments\Model\ContCustomerSegmentsact */
        $model = $this->_coreRegistry->registry('orders');
        
        $isElementDisabled = false;
        
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('organization_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('LexisNexis Order History')]);

        if ($model->getId()) {
            $fieldset->addField('order_id', 'hidden', ['name' => 'order_id']);
        }
        $dateFormat = $this->_localeDate->getDateFormat(
            \IntlDateFormatter::SHORT
        );
        
        $fieldset->addField(
            'id',
            'select',
            [
                'name' => 'id',
                'label' => __(''),
                'title' => __(''),
                'style' => 'visibility: hidden; position: absolute',
                'values' => $this->getAllOptions(),
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'order_date',
            'date',
            [
                'name' => 'order_date',
                'label' => __('Order Date'),
                'title' => __('Order Date'),
                'date_format' => $dateFormat,
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'ship_details',
            'editor',
            [
                'name' => 'ship_details',
                'label' => __('Ship Details'),
                'title' => __('Ship Details'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'config' => $this->_wysiwygConfig->getConfig()

            ]
        );

        $fieldset->addField(
            'bill_details',
            'editor',
            [
                'name' => 'bill_details',
                'label' => __('Bill Details'),
                'title' => __('Bill Details'),
                'required' => false,
                'disabled' => $isElementDisabled,
                'config' => $this->_wysiwygConfig->getConfig()
            ]
        );

        $fieldset->addField(
            'tax_exempt_ind',
            'text',
            [
                'name' => 'tax_exempt_ind',
                'label' => __('Tax Exempt Indicator'),
                'title' => __('Tax Exempt Indicator'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'po_req_ind',
            'text',
            [
                'name' => 'po_req_ind',
                'label' => __('Purchase Order Required'),
                'title' => __('Purchase Order Required'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'segment',
            'text',
            [
                'name' => 'segment',
                'label' => __('Segment'),
                'title' => __('Segment'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'publication_number',
            'text',
            [
                'name' => 'publication_number',
                'label' => __('Publication Number'),
                'title' => __('Publication Number'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );

        $fieldset->addField(
            'isbn',
            'text',
            [
                'name' => 'isbn',
                'label' => __('ISBN'),
                'title' => __('ISBN'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'title_description',
            'text',
            [
                'name' => 'title_description',
                'label' => __('Publication Title'),
                'title' => __('Publication Title'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'media',
            'text',
            [
                'name' => 'media',
                'label' => __('Media'),
                'title' => __('Media'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'original_invoice_number',
            'text',
            [
                'name' => 'original_invoice_number',
                'label' => __('Orginal Invoice Number'),
                'title' => __('Orginal Invoice Number'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'invoice_number',
            'text',
            [
                'name' => 'invoice_number',
                'label' => __('Invoice Number'),
                'title' => __('Invoice Number'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'subscription_type',
            'text',
            [
                'name' => 'subscription_type',
                'label' => __('Subscription Type'),
                'title' => __('Subscription Type'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'credit_reason',
            'text',
            [
                'name' => 'credit_reason',
                'label' => __('Credit Reason'),
                'title' => __('Credit Reason'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'return_date',
            'date',
            [
                'name' => 'return_date',
                'label' => __('Return Date'),
                'title' => __('Return Date'),
                'date_format' => $dateFormat,                
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
	$fieldset->addField(
            'modified_duration_month',
            'text',
            [
                'name' => 'modified_duration_month',
                'label' => __('Modified Duration Month'),
                'title' => __('Modified Duration Month'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'qty',
            'text',
            [
                'name' => 'qty',
                'label' => __('Qty'),
                'title' => __('Qty'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'invoice_paid',
            'text',
            [
                'name' => 'invoice_paid',
                'label' => __('Invoice Paid'),
                'title' => __('Invoice Paid'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
	$fieldset->addField(
            'list_value',
            'text',
            [
                'name' => 'list_value',
                'label' => __('List Price'),
                'title' => __('List Price'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'upkeep',
            'text',
            [
                'name' => 'upkeep',
                'label' => __('Upkeep'),
                'title' => __('Upkeep'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'freight',
            'text',
            [
                'name' => 'freight',
                'label' => __('Freight'),
                'title' => __('Freight'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
	$fieldset->addField(
            'combo',
            'text',
            [
                'name' => 'combo',
                'label' => __('Combo'),
                'title' => __('Combo'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'sales_type_code',
            'text',
            [
                'name' => 'sales_type_code',
                'label' => __('Sales Type Code'),
                'title' => __('Sales Type Code'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
        
        $fieldset->addField(
            'sales_channel_code',
            'text',
            [
                'name' => 'sales_channel_code',
                'label' => __('Sales Channel Code'),
                'title' => __('Sales Channel Code'),
                'required' => false,
                'disabled' => $isElementDisabled
            ]
        );
      		
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('LexisNexis Order History Information');
    }

    /**
     * Prepare title for tab
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('LexisNexis Order History Information');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
    
    /**
     * Retrieve the Current Customer ID
     * 
     * @return array
     */
    public function getAllOptions()
    {
        $item = array(
            'label' => $this->getRequest()->getParam('id'), 
            'value' => $this->getRequest()->getParam('id')
        );
        $org[] = $item;
        
        return $org;
    }
    
}
