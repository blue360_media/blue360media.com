<?php
namespace Endeavor\Organization\Block\Adminhtml\Organization;

class Edit extends \Magento\Backend\Block\Widget\Form\Container
{
    
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'organization_id';
        $this->_blockGroup = 'Endeavor_Organization';
        $this->_controller = 'adminhtml_organization';

        parent::_construct();

        $id= $this->getRequest()->getParam('id');
        $urlBack = $this->getUrl('customer/index/edit', ['id'=>$id]);
        
        $this->buttonList->update('save', 'label', __('Save Order History'));
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                    ],
                ]
            ],
            -100
        );

        $this->buttonList->update('delete', 'label', __('Delete Order History'));
        $this->buttonList->update('back', 'onclick','setLocation("' . $urlBack . '")');
        $this->buttonList->remove('reset');
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('organization/*/save', ['_current' => true, 'back' => 'edit', 'active_tab' => '']);
    }

}
