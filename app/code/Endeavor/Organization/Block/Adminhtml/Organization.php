<?php
namespace Endeavor\Organization\Block\Adminhtml;

/**
 * Adminhtml customer segment content block
 */
class Organization extends \Magento\Backend\Block\Widget\Grid\Container
{
    
    /**
     * 
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array $data
     */
    protected function _construct()
    {
        parent::__construct();
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}
