<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\Organization\Controller\Adminhtml\Organization;

/**
 * Description of View
 *
 * @author Nadeem Khleif
 */
class History extends \Magento\Customer\Controller\Adminhtml\Index
{
    
    /**
     * History action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
