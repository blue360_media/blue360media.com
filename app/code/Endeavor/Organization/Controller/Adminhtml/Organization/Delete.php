<?php
namespace Endeavor\Organization\Controller\Adminhtml\Organization;

class Delete extends \Magento\Backend\App\Action
{

    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('organization_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Endeavor\Organization\Model\Organization');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The Organization has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['organization_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a organization to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
