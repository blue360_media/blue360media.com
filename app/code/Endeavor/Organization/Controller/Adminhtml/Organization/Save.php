<?php
namespace Endeavor\Organization\Controller\Adminhtml\Organization;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Save extends \Magento\Backend\App\Action
{
    
    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $_jsHelper;
    
    /**
     * @var \Endeavor\Author\Model\ResourceModel\Author\CollectionFactory
     */
    protected $_authorCollectionFactory;
    
    /**
    * @var \Magento\Framework\Image\AdapterFactory
    */
    protected $adapterFactory;
    
    /**
    * @var \Magento\MediaStorage\Model\File\UploaderFactory
    */
    protected $uploader;
    
    /**
    * @var \Magento\Framework\Filesystem
    */
    protected $filesystem;
    
    /**
    * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
    */
    protected $timezoneInterface;

   
    /**
     * 
     * @param Context $context
     * @param \Magento\Backend\Helper\Js $jsHelper
     * @param \Magento\Framework\Image\AdapterFactory $adapterFactory
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $uploader
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Endeavor\Sales\Model\ResourceModel\Sales\CollectionFactory $authorCollectionFactory
     */
    public function __construct(
        Context $context,
        \Magento\Backend\Helper\Js $jsHelper,
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploader,
        \Magento\Framework\Filesystem $filesystem,
        \Endeavor\Sales\Model\ResourceModel\Sales\CollectionFactory $authorCollectionFactory
    ) {
        $this->_jsHelper = $jsHelper;
        $this->_contactCollectionFactory = $authorCollectionFactory;
        $this->adapterFactory = $adapterFactory;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        $model = $this->_objectManager->create('Endeavor\Organization\Model\Organization');
        
        if ($data) {
            $model->setData($data);
            try {
                $model->save();
                $this->messageManager->addSuccess(__('The Order has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('organization/*/edit', ['order_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('customer/index/edit', ['id' => $this->getRequest()->getParam('id')]);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the order.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('organization/*/edit', ['order_id' => $this->getRequest()->getParam('order_id')]);
        }
        return $resultRedirect->setPath('customer/index/edit', ['id' => $this->getRequest()->getParam('id')]);
    }
    
}
