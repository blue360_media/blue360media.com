<?php
namespace Endeavor\Organization\Controller\Adminhtml\Organization;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Ajax extends \Magento\Framework\App\Action\Action
{
    
    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }
    
    /**
     * Ajax action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {
        
        $mainSegment = $this->getRequest()->getParam('segment_id');
        $marketSegment = $this->getRequest()->getParam('market');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $secondaryCollection = $objectManager->create('Endeavor\CustomerAttributes\Model\SecondarySegment')->getCollection();
        if (!empty($marketSegment)) {
            $secondaryCollection->addFieldToFilter('parent_id',$marketSegment);
        } else {
            $secondaryCollection->addFieldToFilter('parent_id',$mainSegment);
        }
        $test ='';
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            foreach ($secondaryCollection as $item) {
               $test.='<option value="'.$item->getSegmentId().'">'.$item->getName().'</option>';
            }
           return $result->setData($test);
        }
    }
}
