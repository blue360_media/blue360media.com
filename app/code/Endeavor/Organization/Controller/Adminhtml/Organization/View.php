<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\Organization\Controller\Adminhtml\Organization;

/**
 * Description of View
 *
 * @author Nadeem Khleif
 */
class View extends \Magento\Customer\Controller\Adminhtml\Index{
    
    /**
     * View action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $this->_coreRegistry->register('customer_id',$id);
        $this->initCurrentCustomer();
        $resultLayout = $this->resultLayoutFactory->create();
        return $resultLayout;
    }
}
