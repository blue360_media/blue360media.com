<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Endeavor\MultipleShipping\Model\Quote\Address;

use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Api\Data\OrderAddressInterface;

/**
 * Class ToOrderAddress
 */
class ToOrderAddress extends \Magento\Quote\Model\Quote\Address\ToOrderAddress
{
    /**
     * @param Address $object
     * @param array $data
     * @return OrderAddressInterface
     */
    public function convert(Address $object, $data = [])
    {
        $orderAddress = $this->orderAddressRepository->create();

        $orderAddressData = $this->objectCopyService->getDataFromFieldset(
            'sales_convert_quote_address',
            'to_order_address',
            $object
        );

        $this->dataObjectHelper->populateWithArray(
            $orderAddress,
            array_merge($orderAddressData, $data),
            '\Magento\Sales\Api\Data\OrderAddressInterface'
        );

        if (isset($orderAddressData['shipping_email'])) {
            $orderAddress->setShippingEmail($orderAddressData['shipping_email']);

        }

        return $orderAddress;
    }
}
