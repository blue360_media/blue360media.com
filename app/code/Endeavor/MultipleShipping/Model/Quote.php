<?php
/**
 * Copyright © Endeavor Technology, Inc.
 */
namespace Endeavor\MultipleShipping\Model;

/**
 * Quote model
 * 
 */
class Quote extends \Magento\Quote\Model\Quote
{
    /**
     * Retrieve item model object by item identifier
     *
     * @param   int $itemId
     * @return  \Magento\Quote\Model\Quote\Item
     */
    public function getItemById($itemId)
    {
        /**
         * @update_by: Murad Dweikat
         * @update_date: 2018/03/04
         * @desc: handle error "Cart X does not contain item Y"
         */
        $item = $this->getItemsCollection()->addFieldToFilter('item_id', ['eq' => $itemId]);
        
        
        foreach ($item as $quoteItem ) {
            if ($itemId == $quoteItem->getId() ) {
                return $quoteItem;
            }
        }
        
        
        return false;
        
    }
}
