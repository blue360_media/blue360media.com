<?php
/**
 * Endeavor Technology, Inc.
 */
namespace Endeavor\MultipleShipping\Model\Checkout\Type;

use Magento\Framework\Exception\LocalizedException;

/**
 * Multishipping checkout model
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Multishipping extends \Magento\Multishipping\Model\Checkout\Type\Multishipping
{
    /**
     * Create orders per each quote address
     *
     * @return \Magento\Multishipping\Model\Checkout\Type\Multishipping
     * @throws \Exception
     */
    public function createOrders()
    {
        //Get Object Manager Instance
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $helper = $objectManager->create('Endeavor\MultipleShipping\Helper\Email');
        
        $orderIds = [];
        $this->_validate();
        $shippingAddresses = $this->getQuote()->getAllShippingAddresses();
        $orders = [];

        if ($this->getQuote()->hasVirtualItems()) {
            $shippingAddresses[] = $this->getQuote()->getBillingAddress();
        }

        try {
            foreach ($shippingAddresses as $address) {
                $order = $this->_prepareOrder($address);

                $orders[] = $order;
                $this->_eventManager->dispatch(
                    'checkout_type_multishipping_create_orders_single',
                    ['order' => $order, 'address' => $address, 'quote' => $this->getQuote()]
                );
            }
            
            $customer = $objectManager->create('Magento\Customer\Model\Customer');
            $websiteId = $this->_storeManager->getStore()->getWebsiteId();
            $customer->setWebsiteId($websiteId);
            $customer->load($order->getCustomerId()); 
            
            $ordersForSendEmail = [];
            foreach ($orders as $order) {
                $order->place();
                $order->save();
                if ($order->getCanSendNewEmailFlag()) {
                    $ordersForSendEmail[] = $order;
                    //$this->orderSender->send($order);
                    if ($customer->getIsSpecialMultiShippingFlow()) {
                        $this->sendConfirmationEmail($order);
                    }
                    
                }
                $orderIds[$order->getId()] = $order->getIncrementId();
            }
            
            /**************************************************************************
             * @author: Murad Dweikat                                                 *
             * @date: 2018/02/21                                                      *
             * @desc: send confirmation email for order owner by orders details.      *
             *                                                                        *
             ************* SEND CONFIRMATION EMAIL WITH ALL ORDER DETIALS *************/
            
            $order = $ordersForSendEmail[0];
            $customerName = $order->getCustomerName();
            $customerEmail = $order->getCustomerEmail();
            
            /* Receiver Detail  */
            $receiverInfo = [
                'name' => $customerName,
                'email' => $customerEmail
            ];

            $emailTemplateId = 'owner_order_email';
            
            $dataParams = [];
            $dataParams['customer_name'] = $customerName;
            $dataParams['orders'] = $ordersForSendEmail;

            $helper->sendEmailTemplate($emailTemplateId, $receiverInfo, $dataParams);
            
            /************  END **************/
            
            $this->_session->setOrderIds($orderIds);
            $this->_checkoutSession->setLastQuoteId($this->getQuote()->getId());

            $this->getQuote()->setIsActive(false);
            $this->quoteRepository->save($this->getQuote());

            $this->_eventManager->dispatch(
                'checkout_submit_all_after',
                ['orders' => $orders, 'quote' => $this->getQuote()]
            );

            return $this;
        } catch (\Exception $e) {
            $this->_eventManager->dispatch('checkout_multishipping_refund_all', ['orders' => $orders]);
            throw $e;
        }
    }
    
    /**
     * Add quote item to specific shipping address based on customer address id
     *
     * @param int $quoteItemId
     * @param array $data array('qty'=>$qty, 'address'=>$customerAddressId)
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \Magento\Multishipping\Model\Checkout\Type\Multishipping
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _addShippingItem($quoteItemId, $data)
    {

        $qty = isset($data['qty']) ? (int)$data['qty'] : 1;
        $addressId = isset($data['address']) ? $data['address'] : false;
        $quoteItem = $this->getQuote()->getItemById($quoteItemId);
        
        if ($addressId && $quoteItem) {
            if (!$this->isAddressIdApplicable($addressId)) {
                throw new LocalizedException(__('Please check shipping address information.'));
                
            }

            /**
             * Skip item processing if qty 0
             */
            if ($qty === 0) {
                return $this;
            }
            $quoteItem->setMultishippingQty((int)$quoteItem->getMultishippingQty() + $qty);
            $quoteItem->setQty($quoteItem->getMultishippingQty());
            try {
                $address = $this->addressRepository->getById($addressId);
            } catch (\Exception $e) {
            }
            if (isset($address)) {
                if (!($quoteAddress = $this->getQuote()->getShippingAddressByCustomerAddressId($address->getId()))) {
                    $quoteAddress = $this->_addressFactory->create()->importCustomerAddressData($address);
                    $this->getQuote()->addShippingAddress($quoteAddress);
                }

                $quoteAddress = $this->getQuote()->getShippingAddressByCustomerAddressId($address->getId());
                $quoteAddress->setCustomerAddressId($addressId);
                
                if ($address->getCustomAttribute('shipping_address_email')) {
                    $email = $address->getCustomAttribute('shipping_address_email')->getValue();
                    $quoteAddress->setShippingEmail($email);
                }
                $quoteAddressItem = $quoteAddress->getItemByQuoteItemId($quoteItemId);

                if ($quoteAddressItem) {
                    $quoteAddressItem->setQty((int)($quoteAddressItem->getQty() + $qty));
                } else {
                    $quoteAddress->addItem($quoteItem, $qty);
                }
                
                /**
                 * Require shipping rate recollect
                 */
                $quoteAddress->setCollectShippingRates((bool)$this->getCollectRatesFlag());
            }
        }
        return $this;
    }
    
    /**
     * Prepare order based on quote address
     * 
     * @param   \Magento\Quote\Model\Quote\Address $address
     * @return  \Magento\Sales\Model\Order
     * @throws  \Magento\Checkout\Exception
     */
    protected function _prepareOrder(\Magento\Quote\Model\Quote\Address $address)
    {
        $quote = $this->getQuote();
        $quote->unsReservedOrderId();
        $quote->reserveOrderId();
        $quote->collectTotals();

        $order = $this->quoteAddressToOrder->convert($address);
        $order->setQuote($quote);
        $order->setBillingAddress($this->quoteAddressToOrderAddress->convert($quote->getBillingAddress()));        

        if ($address->getAddressType() == 'billing') {
            $order->setIsVirtual(1);
        } else {
            $order->setShippingAddress($this->quoteAddressToOrderAddress->convert($address)); 
        }

        $order->setPayment($this->quotePaymentToOrderPayment->convert($quote->getPayment()));
        if ($this->priceCurrency->round($address->getGrandTotal()) == 0) {
            $order->getPayment()->setMethod('free');
        }
        
        foreach ($address->getAllItems() as $item) {
            $_quoteItem = $item->getQuoteItem();
            if (!$_quoteItem) {
                throw new \Magento\Checkout\Exception(
                    __('Item not found or already ordered')
                );
            }
            $item->setProductType(
                $_quoteItem->getProductType()
            )->setProductOptions(
                $_quoteItem->getProduct()->getTypeInstance()->getOrderOptions($_quoteItem->getProduct())
            );
            $orderItem = $this->quoteItemToOrderItem->convert($item);
            if ($item->getParentItem()) {
                $orderItem->setParentItem($order->getItemByQuoteItemId($item->getParentItem()->getId()));
            }
            $order->addItem($orderItem);
        }

        return $order;
    }
    
    /**
     * 
     * @param \Magento\Sales\Model\Order $order
     */
    public function sendConfirmationEmail($order) {
        
        //Get Object Manager Instance
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $helper = $objectManager->create('Endeavor\MultipleShipping\Helper\Email');
        $multiShippingHelper = $objectManager->create('Endeavor\MultipleShipping\Helper\Data');
        $shippingAddress = $objectManager->create('Magento\Sales\Model\Order\Address')
                ->load($order->getShippingAddressId());
        
        $shippingAddressEmail = $shippingAddress->getShippingEmail();
        $customerAddressId = $shippingAddress->getCustomerAddressId();
        if (empty($shippingAddressEmail) && !empty($customerAddressId)) {
            $shippingAddressRepository = $objectManager->get('Magento\Customer\Api\AddressRepositoryInterface');
            $shippingAddressRep = $shippingAddressRepository->getById($customerAddressId);
            $shippingAddressEmailAttribute = $shippingAddressRep->getCustomAttribute('shipping_address_email');
            if ($shippingAddressEmailAttribute != null) {
                $shippingAddressEmail = $shippingAddressEmailAttribute->getValue();

            }

        }
        /**
         * If Shipping Email not exist used customer email.
         */
        if (!$shippingAddressEmail) {
            $shippingAddressEmail = $order->getCustomerEmail();
            
        }
        
        /* Receiver Detail  */
        $receiverInfo = [
            'name' => $shippingAddress->getFirstname().' '.$shippingAddress->getLastname(),
            'email' => $shippingAddressEmail
        ];

        $emailTemplateId = 'shipping_person_email';

        $dataParams = [];
        $dataParams['order'] = $order;
        $dataParams['shipping_name'] = $shippingAddress->getFirstname().' '.$shippingAddress->getLastname();
        $dataParams['email'] = $shippingAddressEmail;
        $dataParams['formattedShippingAddress'] = $multiShippingHelper->getFormattedShippingAddress($order);
        $dataParams['isNotVirtual'] = !$order->getIsVirtual();
        $dataParams['shippingDescription'] = $order->getShippingDescription();

        $helper->sendEmailTemplate($emailTemplateId, $receiverInfo, $dataParams);
        

    }
    /**
     * Check if specified address ID belongs to customer.
     *
     * @param $addressId
     * @return bool
     */
    protected function isAddressIdApplicable($addressId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer = $objectManager->create('\Magento\Customer\Model\Session')->getCustomer();
        $applicableAddressIds = array_map(function($address) {
            /** @var \Magento\Customer\Api\Data\AddressInterface $address */
            return $address->getId();
        }, $customer->getAddresses());
        return !is_numeric($addressId) || in_array($addressId, $applicableAddressIds);
    }
}
