<?php
namespace Endeavor\MultipleShipping\Logger;

use Monolog\Logger;

/**
 * Description of Handler
 *
 * @author Murad Dweikat
 */
class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */ 
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/multi_shipping_bulk.log';
}
