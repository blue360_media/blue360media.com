<?php 
namespace Endeavor\MultipleShipping\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var string
     */
    private static $SALES_ORDER_ADDRESS_CONNECTION = 'sales_order_address';
    
    /**
     *
     * @var type 
     */
    private static $QUOTE_ADDRESS_CONNECTION = 'quote_address';
    
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addShippingEmailFields($setup);
        }
        
    }

    /**
     * Add meta title
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addShippingEmailFields(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$SALES_ORDER_ADDRESS_CONNECTION),
            'shipping_email',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'default' => null,
                'length' => 255,
                'comment' => 'Shipping Email'
            ]
        );
        
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$QUOTE_ADDRESS_CONNECTION),
            'shipping_email',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'default' => null,
                'length' => 255,
                'comment' => 'Shipping Email'
            ]
        );
        
        return $this;
    }
}
