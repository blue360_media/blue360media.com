<?php

namespace Endeavor\MultipleShipping\Helper;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\Math\Random;
use Magento\Framework\Stdlib\DateTime;

/**
 * Custom Module Assign Users helper
 */
class Assign
{
    /**
     *
     * @var type 
     */
    protected $_objectManager;
    
    /**
     * Downloadable Purchased Related Item object
     *
     * @var \Endeavor\Downloadable\Model\RelatedItemsFactory 
     */
    protected $_relatedItemsFactory;

    /**
     * Downloadable Purchased Item object
     *
     * @var \Magento\Downloadable\Model\Link\Purchased\ItemFactory 
     */
    protected $_purchasedItemFactory;

    /**
     * Downloadable Link object
     *
     * @var \Magento\Downloadable\Model\Link 
     */
    protected $_linkFactory;

    /**
     * Downloadable Purchased Link object
     *
     * @var \Magento\Downloadable\Model\Link\PurchasedFactory 
     */
    protected $_purchasedLinkFactory;

    /**
     * Customer Object
     *
     * @var \Magento\Customer\Model\CustomerFactory 
     */
    protected $_customerFactory;

    /**
     * Store website manager 
     *
     * @var \Magento\Store\Model\StoreManagerInterface 
     */
    protected $_storeManager;

    /**
     * @var AccountManagementInterface
     */
    protected $_customerAccountManagement;

    /**
     * @var Random
     */
    private $mathRandom;

    /**
     * @var Endeavor\Downloadable\Model\Import
     */
    private $import;

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Downloadable\Model\Link\PurchasedFactory
     */
    protected $_purchasedFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magento\Downloadable\Model\Link\Purchased\ItemFactory
     */
    protected $_itemFactory;

    /**
     * @var \Magento\Framework\DataObject\Copy
     */
    protected $_objectCopyService;

    /**
     * @var \Magento\Downloadable\Model\ResourceModel\Link\Purchased\Item\CollectionFactory
     */
    protected $_itemsFactory;

    /**
     * 
     * @param \Endeavor\Downloadable\Model\Import $import
     * @param \Magento\Downloadable\Model\Link\Purchased\ItemFactory $PurchasedItemFactory
     * @param \Magento\Downloadable\Model\LinkFactory $LinkFactory
     * @param \Magento\Downloadable\Model\Link\PurchasedFactory $PurchasedLinkFactory
     * @param \Magento\Customer\Model\CustomerFactory $CustomerFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Endeavor\MultipleShipping\Helper\AccountManagementInterface $customerAccountManagement
     * @param \Endeavor\Downloadable\Model\RelatedItemsFactory $RelatedItems
     * @param \Endeavor\MultipleShipping\Helper\Random $mathRandom
     * @param \Endeavor\SFIntegration\Logger\Logger $logger
     */
    public function __construct(
        \Endeavor\Downloadable\Model\Import $import,
        \Magento\Downloadable\Model\Link\Purchased\ItemFactory $PurchasedItemFactory,
        \Magento\Downloadable\Model\LinkFactory $LinkFactory,
        \Magento\Downloadable\Model\Link\PurchasedFactory $PurchasedLinkFactory,
        \Magento\Customer\Model\CustomerFactory $CustomerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        AccountManagementInterface $customerAccountManagement,
        \Endeavor\Downloadable\Model\RelatedItemsFactory $RelatedItems,
        Random $mathRandom,
        \Endeavor\SFIntegration\Logger\Logger $logger,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Downloadable\Model\Link\PurchasedFactory $purchasedFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Downloadable\Model\Link\Purchased\ItemFactory $itemFactory,
        \Magento\Downloadable\Model\ResourceModel\Link\Purchased\Item\CollectionFactory $itemsFactory,
        \Magento\Framework\DataObject\Copy $objectCopyService

    ) {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_logger = $logger;
        $this->import = $import;
        $this->_purchasedItemFactory = $PurchasedItemFactory;
        $this->_linkFactory = $LinkFactory;
        $this->_purchasedLinkFactory = $PurchasedLinkFactory;
        $this->_customerFactory = $CustomerFactory;
        $this->_storeManager = $storeManager;
        $this->_customerAccountManagement = $customerAccountManagement;
        $this->_relatedItemsFactory = $RelatedItems;
        $this->mathRandom = $mathRandom;
        $this->_scopeConfig = $scopeConfig;
        $this->_purchasedFactory = $purchasedFactory;
        $this->_productFactory = $productFactory;
        $this->_itemFactory = $itemFactory;
        $this->_itemsFactory = $itemsFactory;
        $this->_objectCopyService = $objectCopyService;

    }

    /**
     * 
     * @param String[] $orders
     * @return boolean|string
     */
    public function assignUsersInShippingAddress($orders) {
        $moveOrderToHelper = $this->_objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
        
        foreach ($orders as $orderId) {
            $salesOrder = $this->_objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
            $currentCustomer = $this->_customerFactory->create()->load($salesOrder->getCustomerId());
            $isSpecialMultiShippingFlow = $currentCustomer->getIsSpecialMultiShippingFlow();
            $allowAssign = false;
            if ($isSpecialMultiShippingFlow == 1) {
                $orderShippingAddress = $this->_objectManager->create('Magento\Sales\Model\Order\Address')
                        ->load($salesOrder->getShippingAddressId());            
                $customerAddressId = $orderShippingAddress->getCustomerAddressId();
                $shippingAddressEmail = $orderShippingAddress->getShippingEmail();
                $shippingAddress = null;
                if (empty($shippingAddressEmail) && !empty($customerAddressId)) {
                    $shippingAddressRepository = $this->_objectManager->get('Magento\Customer\Api\AddressRepositoryInterface');
                    $shippingAddressRep = $shippingAddressRepository->getById($customerAddressId);
                    $shippingAddressEmailAttribute = $shippingAddressRep->getCustomAttribute('shipping_address_email');
                    if ($shippingAddressEmailAttribute != null) {
                        $shippingAddressEmail = $shippingAddressEmailAttribute->getValue();
                        
                    }

                }
                if (!empty($shippingAddressEmail)) {
                    $shippingAddress = $salesOrder->getShippingAddress();
                    $allowAssign = true;
                }
            }
            $salesOrderItems = $this->_objectManager->create('\Magento\Sales\Model\Order\Item')
                    ->getCollection()
                    ->addFieldToFilter('order_id', ['eq' => $orderId])
                    ->addFieldToFilter('product_type', ['in' => ['downloadable', 'combo']]);
                        
            foreach ($salesOrderItems as $orderItem) { 
                
                /**
                 * Build Purchased Link for Order Item for order owner.
                 */
                $this->buildPurchasedLink($orderItem);
                
                
                /**
                 * Assign Downloadable|Combo for Email in Shipping Address.
                 * 
                 */
                if ($allowAssign) {
                    $part_number = $orderItem->getSku();
                    $orderFirstname = $salesOrder->getCustomerFirstname();
                    $orderLastname = $salesOrder->getCustomerLastname();
                    $username = $orderFirstname." ".$orderLastname;

                    $purchasedId = $this->getPurchasedIdForItem($orderId, $part_number);

                    $purchasedItems = $this->_purchasedItemFactory->create()->getCollection()
                            ->addFieldToFilter('purchased_id', $purchasedId);

                    $purchasedItem = $purchasedItems->getFirstItem();
                    $downloadableLink = $this->_linkFactory->create()->load($purchasedItem->getLinkId());
                    $purchasedLink = $this->_purchasedLinkFactory->create()->load($purchasedItem->getPurchasedId());
                    $customer = $this->_customerFactory->create();

                    $totalQuantityAssigned = 0;

                    $newPurchasedLink = $this->_purchasedLinkFactory->create();
                    $newPurchasedItem = $this->_purchasedItemFactory->create();
                    $relatedItems = $this->_relatedItemsFactory->create();
                    $relatedItemsCollection = $this->_relatedItemsFactory->create()->getCollection();
                    $tempPurchasedLink = $this->_purchasedLinkFactory->create()->getCollection();
                    $tempPurchasedItem = $this->_purchasedItemFactory->create();

                    $email = $shippingAddressEmail;
                    $firstname = $shippingAddress->getFirstname();
                    $lastname = $shippingAddress->getLastname();
                    $qty = $orderItem->getQtyOrdered();

                    $websiteId = $this->_storeManager->getWebsite()->getWebsiteId();
                    $customer->setWebsiteId($websiteId);
                    $customer->loadByEmail($email);

                    $customerId = $customer->getEntityId();
                    $dataPurchasedLink = $purchasedLink->getData();
                    unset($dataPurchasedLink['purchased_id']);
                    unset($dataPurchasedLink['created_at']);
                    unset($dataPurchasedLink['updated_at']);
                    $newPurchasedLink->setData($dataPurchasedLink);

                    $createPurchaseItems = false;
                    if (!empty($customerId)) {
                        $tempPurchasedLink->addFieldToFilter('order_id', ['eq' => $purchasedLink->getOrderId()])
                                ->addFieldToFilter('order_item_id', ['eq' => $purchasedLink->getOrderItemId()])
                                ->addFieldToFilter('customer_id', ['eq' => $customerId]);

                        if ($customer->getPasswordHash() === null) {
                            $newPasswordToken = $this->mathRandom->getUniqueHash();
                            $customer->setRpToken($newPasswordToken);
                            $customer->setRpTokenCreatedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT));
                            $customer->save();
                        }

                        if (!count($tempPurchasedLink->getItems())) {
                            $newPurchasedLink->setCustomerId($customerId);
                            $newPurchasedLink->save();

                            $createPurchaseItems = true;
                        }
                    } else {
                        $createPurchaseItems = true;
                        $customer->setEmail($email);
                        $customer->setFirstname($firstname);
                        $customer->setLastname($lastname);
                        $customer->setAddresses(null);
                        $storeId = $this->_storeManager->getWebsite($websiteId)->getDefaultStore()->getId();
                        $customer->setStoreId($storeId);

                        $storeName = $this->_storeManager->getStore($customer->getStoreId())->getName();
                        $customer->setCreatedIn($storeName);

                        $newPasswordToken = $this->mathRandom->getUniqueHash();
                        $customer->setRpToken($newPasswordToken);
                        $customer->setRpTokenCreatedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT));

                        $customer->save();

                        $lastCustomerId = $customer->getCollection()->getLastItem()->getEntityId();
                        $newPurchasedLink->setCustomerId($lastCustomerId);
                        $newPurchasedLink->save();

                        $newCustomerId = $customer->getEntityId();
                        $moveOrderToHelper->sendWelcomeMessage($newCustomerId);

                        $tempPurchasedLink->addFieldToFilter('order_id', ['eq' => $purchasedLink->getOrderId()])
                                ->addFieldToFilter('order_item_id', ['eq' => $purchasedLink->getOrderItemId()])
                                ->addFieldToFilter('customer_id', ['eq' => $customer->getId()]);

                        $response = [];
                        $response['notes'] = 'This email: ' . $email . ' is not registered in our website. So we sent a message with details of the account.';
                        $this->_logger->info('POST execute Assign Users API - notes', $response);

                    }

                    if ($createPurchaseItems) {
                        foreach ($purchasedItems as $pItem) {
                            $dataPurchasedItem = $pItem->getData();
                            unset($dataPurchasedItem['item_id']);
                            unset($dataPurchasedItem['created_at']);
                            unset($dataPurchasedItem['updated_at']);
                            $newPurchasedItem->setData($dataPurchasedItem);
                            $newPurchasedItem->setStatus('available');
                            $newPurchasedItem->setOrderItemId($purchasedLink->getOrderItemId());
                            $newPurchasedItem->setNumberOfDownloadsBought($downloadableLink->getNumberOfDownloads() * $qty);
                            $newPurchasedItem->setNumberOfDownloadsUsed(0);
                            $newPurchasedItem->setPurchasedId($newPurchasedLink->getId());
                            $linkHash = strtr(
                                    base64_encode(
                                            microtime() . $orderItem->getId() . $dataPurchasedItem['product_id']
                                    ), '+/=', '-_,'
                            );

                            $newPurchasedItem->setLinkHash($linkHash);
                            $newPurchasedItem->save();

                            $relatedItems = $this->_relatedItemsFactory->create();
                            $relatedItems->load($newPurchasedItem->getId(), 'related_item_id');
                            if (empty($relatedItems->getId())) {
                                $relatedItems = $this->_relatedItemsFactory->create();
                                $relatedItems->setItemId($pItem->getId());
                                $relatedItems->setRelatedItemId($newPurchasedItem->getId());
                                $relatedItems->setQty($qty);
                                $relatedItems->setCreatedBy($username);
                                $relatedItems->save();
                            } else {
                                $relatedItems->setQty($relatedItems->getQty() + $qty);
                                $relatedItems->save();
                            }
                        }
                    } else {
                        foreach ($tempPurchasedLink->getItems() as $newItems) {
                            $tempPurchasedItems = $this->_purchasedItemFactory->create()->getCollection()->addFieldToFilter('purchased_id', $newItems->getId());
                            foreach ($tempPurchasedItems as $tempPurchasedItem) {
                                $tempPurchasedItem->setNumberOfDownloadsBought($tempPurchasedItem->getNumberOfDownloadsBought() + $downloadableLink->getNumberOfDownloads() * $qty);
                                $tempPurchasedItem->save();

                                $relatedItems = $this->_relatedItemsFactory->create();
                                $relatedItems->load($tempPurchasedItem->getId(), 'related_item_id');

                                if (empty($relatedItems->getId())) {
                                    $relatedItems->setItemId($purchasedItem->getId());
                                    $relatedItems->setRelatedItemId($tempPurchasedItem->getId());
                                    $relatedItems->setQty($qty);
                                    $relatedItems->setCreatedBy($username);
                                    $relatedItems->save();
                                } else {
                                    $relatedItems->setQty($relatedItems->getQty() + $qty);
                                    $relatedItems->save();
                                }
                            }
                        }
                    }

                    $receiverInfo = ['name' => $customer->getFirstname() . ' ' . $customer->getLastname(),
                        'email' => $email];

                    $emailData = [];
                    $emailData['sender_name'] = $currentCustomer->getFirstname() . ' ' . $currentCustomer->getLastname();
                    $emailData['customer'] = $customer;
                    $emailData['product_name'] = $purchasedLink->getProductName();
                    $emailData['product_qty'] = $qty;

                    $totalQuantityAssigned += $qty;
                    $this->sendAssingUserMailMessage($emailData, $receiverInfo);
                    $this->_logger->info('POST execute Assign Users API - Done');

                    foreach ($purchasedItems as $pItem) {
                        $pItem->setNumberOfDownloadsUsed($pItem->getNumberOfDownloadsUsed() + $downloadableLink->getNumberOfDownloads() * $totalQuantityAssigned);
                        if (!$this->getRemainingDownloads($pItem)) {
                            $pItem->setStatus('expired');
                        }
                        $pItem->save();
                    }
                }
            }
        }
        
    }
    
    /*
     * @desc get Purchased Id For Item.
     * @param string $orderId
     * @param string $partNumber
     * 
     * @return string $purchasedId
     */
    protected function getPurchasedIdForItem($orderId, $partNumber) {

        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $purchaseLink = $this->_objectManager->get('\Magento\Downloadable\Model\Link\PurchasedFactory');
        $downloadablePurchasedLinks = $purchaseLink->create()->getCollection()->addFieldToFilter('order_id', $orderId);

        $purchasedId = null;
        if ($downloadablePurchasedLinks) {
            foreach ($downloadablePurchasedLinks as $downloadablePurchasedLink) {
                $productSku = $downloadablePurchasedLink->getProductSku();
                if ($productSku == $partNumber) {
                    $purchasedId = $downloadablePurchasedLink->getPurchasedId();
                    break;
                    
                }
                
            }
            
        }
        
        return $purchasedId;
        
    }
    
    /**
     * 
     * @param type $orderItem
     * @return $this
     */
    public function buildPurchasedLink($orderItem)
    {
        if (!$orderItem->getId()) {
            //order not saved in the database
            return $this;
        }
        
        if ($orderItem->getProductType() != \Endeavor\CustomProduct\Model\Product\Type::TYPE_COMBO) {
            return $this;
        }

        $product = $orderItem->getProduct();
        $purchasedLink = $this->_createPurchasedModel()->load($orderItem->getId(), 'order_item_id');
        if ($purchasedLink->getId()) {
            return $this;
        }
        if (!$product) {
            $product = $this->_createProductModel()->setStoreId(
                $orderItem->getOrder()->getStoreId()
            )->load(
                $orderItem->getProductId()
            );
        }
        
        if ($product->getTypeId() == \Endeavor\CustomProduct\Model\Product\Type::TYPE_COMBO) {
            $links = $product->getTypeInstance()->getLinks($product);
            if ($linkIds = $orderItem->getProductOptionByCode('links')) {
                
                $linkPurchased = $this->_createPurchasedModel();
                $this->_objectCopyService->copyFieldsetToTarget(
                    'downloadable_sales_copy_order',
                    'to_downloadable',
                    $orderItem->getOrder(),
                    $linkPurchased
                );
                $this->_objectCopyService->copyFieldsetToTarget(
                    'downloadable_sales_copy_order_item',
                    'to_downloadable',
                    $orderItem,
                    $linkPurchased
                );
                $linkSectionTitle = $product->getLinksTitle() ? $product
                    ->getLinksTitle() : $this
                    ->_scopeConfig
                    ->getValue(
                        \Magento\Downloadable\Model\Link::XML_PATH_LINKS_TITLE,
                        ScopeInterface::SCOPE_STORE
                    );
                $linkPurchased->setLinkSectionTitle($linkSectionTitle)->save();

                foreach ($linkIds as $linkId) {
                    if (isset($links[$linkId])) {
                        $linkPurchasedItem = $this->_createPurchasedItemModel()->setPurchasedId(
                            $linkPurchased->getId()
                        )->setOrderItemId(
                            $orderItem->getId()
                        );

                        $this->_objectCopyService->copyFieldsetToTarget(
                            'downloadable_sales_copy_link',
                            'to_purchased',
                            $links[$linkId],
                            $linkPurchasedItem
                        );
                        $linkHash = strtr(
                            base64_encode(
                                microtime() . $linkPurchased->getId() . $orderItem->getId() . $product->getId()
                            ),
                            '+/=',
                            '-_,'
                        );
                        $numberOfDownloads = $links[$linkId]->getNumberOfDownloads() * $orderItem->getQtyOrdered();
                        $linkPurchasedItem->setLinkHash(
                            $linkHash
                        )->setNumberOfDownloadsBought(
                            $numberOfDownloads
                        )->setStatus(
                            \Magento\Downloadable\Model\Link\Purchased\Item::LINK_STATUS_PENDING
                        )->setCreatedAt(
                            $orderItem->getCreatedAt()
                        )->setUpdatedAt(
                            $orderItem->getUpdatedAt()
                        )->save();
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @return \Magento\Downloadable\Model\Link\Purchased
     */
    protected function _createPurchasedModel()
    {
        return $this->_purchasedFactory->create();
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    protected function _createProductModel()
    {
        return $this->_productFactory->create();
    }

    /**
     * @return \Magento\Downloadable\Model\Link\Purchased\Item
     */
    protected function _createPurchasedItemModel()
    {
        return $this->_itemFactory->create();
    }

    /**
     * @return \Magento\Downloadable\Model\ResourceModel\Link\Purchased\Item\Collection
     */
    protected function _createItemsCollection()
    {
        return $this->_itemsFactory->create();
    }
    
        /**
     * Send email Message to notify assigned user by downloadable product 
     *
     * @param array $senderInfo
     * @param array $receiverInfo
     * @param array $emailData
     * @return void
     */
    public function sendAssingUserMailMessage($emailData,$receiverInfo)
    {
        /* call send mail method from helper or where you define it*/ 
        $this->_objectManager->get('Endeavor\Downloadable\Helper\Email')->assignedUserSendMailMessage(
              $emailData,
              $receiverInfo
        );
        
    }
    
    /**
     * Return number of remaining downloads
     *
     * @param Item $item
     * @return \Magento\Framework\Phrase|int
     */
    public function getRemainingDownloads($item) {
        $downloads = 0;
        if ($item->getNumberOfDownloadsBought()) {
            $downloads = $item->getNumberOfDownloadsBought() - $item->getNumberOfDownloadsUsed();
            
        }
        return $downloads;
        
    }

}
