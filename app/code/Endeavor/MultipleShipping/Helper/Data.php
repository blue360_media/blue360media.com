<?php
/**
 * Copyright © 2018 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 08/02/2018
 * @desc in order to add Helper for Estimate delivery date.
 */

namespace Endeavor\MultipleShipping\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {
    
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime 
     */
    protected $date;
    
    /**
     * @var \Magento\Sales\Model\Order\Address\Renderer
     */
    protected $addressRenderer;

    /**
     * @var \Magento\Payment\Helper\Data
     */
    protected $paymentHelper;
    
    /**
     * 
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Payment\Helper\Data $paymentHelper
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Payment\Helper\Data $paymentHelper
    ) {
        $this->addressRenderer = $addressRenderer;
        $this->paymentHelper = $paymentHelper;
        $this->date = $date;
    }
    
    /**
     * @desc get Item Status For Address.
     * @param Array $address address for shipping
     * @return Array status for address items
     */
    public function getItemStatusForAddress($address) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $currentDateTmie = $this->date->date('Y-m-d H:i:s','+5GMT');
        $preorderPreorderAvailability = $this->date->date('Y-m');
        $isPreorderOrdredItem = false;
        try {
            foreach ($address->getAllItems() as $item) {
                $addressProduct = $objectManager->create("Magento\Catalog\Model\Product")
                    ->load($item->getProductId());
                if ($addressProduct->getWkPreorder()) {
                    $isPreorderOrdredItem = true;
                    $addressProduct = $objectManager->create("Magento\Catalog\Model\Product")
                            ->load($item->getProductId());
                    $preorderPreorderAvailability = substr($addressProduct->getPreorderAvailability(),0,7);
                    break;
                }
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }
        
        $output = ['isPreOrder' => $isPreorderOrdredItem, 'estimateDeliveryDate' => $currentDateTmie,
                'availability' => $preorderPreorderAvailability];
        
        return $output;
    }
    
    /**
     * @desc get Estimate Date.
     * @param Array $address
     * @param string $shippingMethod
     * @return Array status for address items
     */
    public function getEstimateDate($address, $shippingMethod) {
        $output = $this->getItemStatusForAddress($address);
        $standard = $output['estimateDeliveryDate'];
		$ups = $output['estimateDeliveryDate'];
        $next2Day = $output['estimateDeliveryDate'];
        $nextDay = $output['estimateDeliveryDate'];
        if ($output['isPreOrder']) {
            $estimateDate = 'After 5 business days from shipping date';
        } else {
            // add another day if time after 3 PM
            $standardHours = $this->date->date('H', '+5GMT');
            if ($standardHours >= 15) {
                $standard = date('Y-m-j', strtotime("{$standard} +1 weekdays"));
				$ups = date('Y-m-j', strtotime("{$nextDay} +2 weekdays"));
                $next2Day = date('Y-m-j', strtotime("{$next2Day} +1 weekdays"));
                $nextDay = date('Y-m-j', strtotime("{$nextDay} +1 weekdays"));
                
            }
            $standard = date('Y-m-j', strtotime("{$standard} +4 weekdays"));
			$ups = date('Y-m-j', strtotime("{$nextDay} +2 weekdays"));
            $next2Day = date('Y-m-j', strtotime("{$next2Day} +2 weekdays"));
            $nextDay = date('Y-m-j', strtotime("{$nextDay} +1 weekdays"));

            $standardDate = date_format(date_create($standard), "D, F dS Y");
			$upsDate = date_format(date_create($ups), "D, F dS Y");
            $next2DayDate = date_format(date_create($next2Day), "D, F dS Y");
            $nextDayDate = date_format(date_create($nextDay), "D, F dS Y");

            if ($shippingMethod == 'Standard' || $shippingMethod == 'Free Shipping') {
                $estimateDate = $standardDate;
                
            } else if ($shippingMethod == 'UPS') {
                $estimateDate = $upsDate;
                
            }  else if ($shippingMethod == 'Next 2-Day') {
                $estimateDate = $next2DayDate;
                
            } else if ($shippingMethod == 'Next Day') {
                $estimateDate = $nextDayDate;
                
            } else {
                $estimateDate = 'Unknown';
                
            }
            
        }
        return $estimateDate;
        
    }
    
    /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedShippingAddress($order) {
        return $order->getIsVirtual() ? null : $this->addressRenderer->format($order->getShippingAddress(), 'html');
    }

    /**
     * @param Order $order
     * @return string|null
     */
    public function getFormattedBillingAddress($order) {
        return $this->addressRenderer->format($order->getBillingAddress(), 'html');
    }

    /**
     * Get payment info block as html
     *
     * @param Order $order
     * @return string
     */
    public function getPaymentHtml(\Magento\Sales\Model\Order $order) {
        return $this->paymentHelper->getInfoBlockHtml(
                        $order->getPayment(), $order->getStore()->getStoreId()
        );
    }

}