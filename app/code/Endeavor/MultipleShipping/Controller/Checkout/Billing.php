<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\MultipleShipping\Controller\Checkout;

use Magento\Multishipping\Model\Checkout\Type\Multishipping\State;
use Magento\Framework\App\ResponseInterface;

class Billing extends \Magento\Multishipping\Controller\Checkout\Billing
{
    /**
     * Multishipping checkout billing information page
     *
     * @return void|ResponseInterface
     */
    public function execute()
    {
        if (!$this->_validateBilling()) {
            return;
        }

        if (!$this->_validateMinimumAmount()) {
            return;
        }

        $addressesId = [];
        foreach ($this->_getCheckout()->getQuote()->getAllShippingAddresses() as $shippingAddress) {
            if ($shippingAddress->getDiscountDescription() == 'Free Shipping') {
                $addressesId[] = $shippingAddress->getAddressId();
            }

        }

        if (!empty($addressesId)) {

            $addressShippingRateCollection = $this->_objectManager->create('\Magento\Quote\Model\Quote\Address\Rate')->getCollection();
            $addressShippingRateCollection->addFieldToFilter('address_id', ['in' => $addressesId]);
            $addressShippingRateCollection->addFieldToFilter('method_title', 'Free Shipping');
            $addressShippingRateCollection->addFieldToFilter('price', 0);

            $shippingMethodCode = null;
            if (empty($addressShippingRateCollection->getFirstItem()->getData())) {
                $addressShippingRateCollection = $this->_objectManager->create('\Magento\Quote\Model\Quote\Address\Rate')->getCollection();
                $addressShippingRateCollection->addFieldToFilter('address_id', ['in' => $addressesId]);
                $addressShippingRateCollection->addFieldToFilter('method_title', 'Standard');

            }

            if (!empty($addressShippingRateCollection->getFirstItem()->getData())) {
                $shippingMethodCode = $addressShippingRateCollection->getFirstItem()->getCode();

            }

            foreach ($this->_getCheckout()->getQuote()->getAllShippingAddresses() as $shippingAddress) {
                if ($shippingAddress->getDiscountDescription() == 'Free Shipping') {
                    $shippingAddress->setShippingMethod($shippingMethodCode);
                    $shippingAddress->save();

                }

            }

        }
        
        
        if (!$this->_getState()->getCompleteStep(State::STEP_SHIPPING)) {
            return $this->_redirect('*/*/shipping');
        }

        $this->_getState()->setActiveStep(State::STEP_BILLING);
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
