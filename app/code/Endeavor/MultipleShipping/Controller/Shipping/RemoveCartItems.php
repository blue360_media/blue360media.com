<?php

namespace Endeavor\MultipleShipping\Controller\Shipping;

class RemoveCartItems extends \Magento\Framework\App\Action\Action {

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * Logging instance
     * @var \Endeavor\MultipleShipping\Logger\Logger
     */
    protected $_logger;

    /**
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepository
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Endeavor\MultipleShipping\Logger\Logger $logger
    ) {
        $this->cartRepository = $cartRepository;
        $this->checkoutSession = $checkoutSession;
        $this->_logger = $logger;

        parent::__construct($context);

    }
    
    /**
     * 
     */
    public function execute() {
        $this->_logger->crit('Start: RemoveCartItems:execute()');
        $_SESSION['isBulkOrder'] = 1;
        
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->checkoutSession->getQuote();
        $this->cartRepository->save($quote);
        
        if (isset($_SESSION['checkout']) && isset ($_SESSION['checkout']['quote_id_1'])) {
            foreach ($_SESSION['checkout'] as $key => $checkoutArray) {
                if ($key != 'quote_id_1') {
                    unset($_SESSION['checkout'][$key]);
                }
            }
        }
        
        try {
            $cart = $this->_objectManager->create('\Magento\Checkout\Model\Cart');
            $cart->getQuote()->removeAllItems();
            $cart->getQuote()->removeAllAddresses();
            $cart->getQuote()->setIsMultiShipping(false);
            $cart->getQuote()->collectTotals();
            $cart->truncate()->save();
            
        } catch (\Exception $ex) {
            $this->_logger->crit('Error: ' . $ex->getMessage());
            $this->_logger->crit('Trace: ' . $ex->getTraceAsString());

            $this->messageManager->addErrorMessage(__('opps!! Something went wrong Please try again after 5 seconds.'));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('bulkorder/shipping/index');
            
        }
        $this->_forward('savebulk');

        $this->_logger->crit('END: RemoveCartItems:execute()');
    }

}