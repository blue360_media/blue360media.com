<?php
/**
 * Copyright © 2018 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 02/11/2018.
 * @desc Bulk Addresses for customers.
 */

namespace Endeavor\MultipleShipping\Controller\Shipping;

use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Multishipping\Model\Checkout\Type\Multishipping\State;

class SaveBulk extends \Magento\Framework\App\Action\Action
{
    
    const FILE_HEADER_INDEX = 0;
    const FILE_FIRST_NAME = 0;
    const FILE_LAST_NAME = 1;
    const FILE_EMAIL = 2;
    const FILE_COMPANY = 11;
    const FILE_PHONE_NUMBER = 3;
    const FILE_FAX = 12;
    const FILE_STREET_ADDRESS = 4;
    const FILE_CITY = 5;
    const FILE_STATE = 6;
    const FILE_ZIP_CODE = 7;
    const FILE_SKU = 8;
    const FILE_QTY = 9;
    const FILE_SHIPPING_METHOD = 10;
    //const SHIPPNG_METHOD_STANDARD = 'Standard';
    const SHIPPNG_METHOD_UPS = 'UPS';
    const SHIPPING_METHOD_NEXT_DAY = 'Next Day';
    const SHIPPNG_METHOD_NEXT_2_DAY = 'Next 2-Day';
    const ADDRESSES_LIMIT = 30;
    
    /**
     * Logging instance
     * @var \Endeavor\MultipleShipping\Logger\Logger
     */
    protected $_logger;
    
    /**
     *
     * @var Int 
     */
    protected $_quoteWasMultiShipping = 0;
    
    /**
     * Customer Object
     *
     * @var \Magento\Customer\Model\CustomerFactory 
     */
    protected $_customerFactory;
    
    /**
     * Store website manager 
     *
     * @var \Magento\Store\Model\StoreManagerInterface 
     */
    protected $_storeManager;
    
    /**
     * @var AccountManagementInterface
     */
    protected $_customerAccountManagement;

    /**
     * @var Endeavor\Downloadable\Model\Import
     */
    private $_import;
    
    /**
     * PageFactory Data
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;
    
    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     * Core store config
     *
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $_addressFactory;
    
    /**
     * Customer Repository
     *
     * @var \Magento\Customer\Model\ResourceModel\CustomerRepository
     */
    protected $_customerRepository;
    
    /**
     * Product Factory
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $_cartRepositoryInterface;
    
    /**
     * Product Factory
     *
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $_cartManagementInterface;
    
    /**
     * FormKey
     *
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $_formKey;
    
    /**
     * Cart
     *
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;
    
    /**
     * Product Repository
     *
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $_productRepository;
    
    /**
     * @var \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\Matrixrate\CollectionFactory
     */
    protected $_matrixRateCollectionFactory;
    
    /**
     *
     * @var \Magento\Quote\Model\Quote\Address\Rate 
     */
    protected $_shippingRate;
    
    /**
     * 
     * @var \Magento\Multishipping\Model\Checkout\Type\Multishipping
     */
    protected $_multiShipping;
    
    /**
    * @var Magento\CatalogInventory\Model\StockRegistry 
    */
    protected $_stockRegistry;

    /**
     *
     * @var array 
     */
    protected $_stockItem;
    
    private $itemsBundleMap = [];

    /**
     * 
     * @param \Endeavor\Downloadable\Model\Import $import
     * @param \Magento\Downloadable\Model\Link\Purchased\ItemFactory $PurchasedItemFactory
     * @param \Magento\Downloadable\Model\LinkFactory $LinkFactory
     * @param \Magento\Downloadable\Model\Link\PurchasedFactory $PurchasedLinkFactory
     * @param \Magento\Customer\Model\CustomerFactory $CustomerFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param AccountManagementInterface $customerAccountManagement
     * @param \Endeavor\Downloadable\Model\RelatedItemsFactory $RelatedItems
     * @param Random $mathRandom
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Endeavor\Downloadable\Model\Import $import,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\CustomerFactory $CustomerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,   
        AccountManagementInterface $customerAccountManagement,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Catalog\Model\ProductFactory $ProductFactory,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\CatalogInventory\Model\StockRegistry $stock,
        \Magento\Quote\Model\Quote\Address\Rate $shippingRate,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\Matrixrate\CollectionFactory $MatrixRateCollectionFactory,
        \Magento\Multishipping\Model\Checkout\Type\Multishipping $multiShipping,
        \Endeavor\MultipleShipping\Logger\Logger $logger
    ){
        $this->_logger = $logger;
        $this->_multiShipping = $multiShipping;
        $this->_shippingRate = $shippingRate;
        $this->_matrixRateCollectionFactory = $MatrixRateCollectionFactory;
        $this->_productRepository = $productRepository;
        $this->_cart = $cart;
        $this->_formKey = $formKey;
        $this->_scopeConfig = $scopeConfig;
        $this->_import = $import;
        $this->_customerFactory = $CustomerFactory;
        $this->_storeManager = $storeManager;
        $this->_customerAccountManagement = $customerAccountManagement;   
        $this->_addressFactory = $addressFactory;
        $this->_customerRepository = $customerRepository;
        $this->_ProductFactory = $ProductFactory;
        $this->_cartRepositoryInterface = $cartRepositoryInterface;
        $this->_cartManagementInterface = $cartManagementInterface;
        $this->_stockRegistry = $stock;
        $this->_stockItem = [];
        
        parent::__construct($context);

    }
    
    /**
     * Contact action
     *
     * @return void
     */
    public function execute() {
        
        if (isset($_FILES['import_file'])) {
            $fileAddressRows = $this->_import->importFromCsvFile($_FILES['import_file']);
            $fileAddressRows = $this->trimData($fileAddressRows);
            $addressesCount = $this->addressesCount($fileAddressRows);
            if ($addressesCount > self::ADDRESSES_LIMIT) {
                $message = 'Maximum allowed shipment addresses per (csv file) is 30.';
                $this->messageManager->addErrorMessage(__($message));
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('bulkorder/shipping');
            } else {
                $customerId = $this->_getCheckout()->getCustomer()->getId();
                $this->_logger->addInfo('execute() customerId: ' .$customerId);
                $defaultShipping = [];
                if (!$this->_getCheckout()->getCustomerDefaultShippingAddress()) {
                    $defaultShipping [] = ['status' => false];
                    $message = 'Please Add Default Shipping Address before you can used this feature.';
                    $this->messageManager->addWarningMessage(__($message));
                    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultRedirect->setPath('customer/address/new/');

                }
                if (empty($defaultShipping)) {
                    $resultRedirect = $this->checkRequiredFields($fileAddressRows);

                    if (empty($resultRedirect)) {
                        $resultRedirect = $this->validateFields($fileAddressRows);

                        if (empty($resultRedirect)) {
                            try {
                                $_SESSION['isBulkOrder'] = 1;

                                $productNotFound = $this->createQuoteForProduct($fileAddressRows, $customerId);

                                if (empty($productNotFound)) {
                                    $shippingAddressesIds = $this->createNewAddressToCustomer($fileAddressRows, $customerId);
                                    
                                    $this->_logger->info('Imported File Data: ', $fileAddressRows);
                                    $shippingMethod = $this->prepareQuoteForMultiShipping($fileAddressRows, $shippingAddressesIds);

                                    if (empty($shippingMethod)) {
                                        $message = 'Bulk Order to multiple Address uploaded successfully';
                                        $this->messageManager->addSuccessMessage(__($message));
                                        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                                        $resultRedirect->setPath('multishipping/checkout/billing');

                                    } else {
                                        $message = 'opps!! Shipping Method is not correct in row(s): ';
                                        foreach ($shippingMethod as $shippingMethodItem) {
                                            $message .= $shippingMethodItem['row'] . ', ';
                                        }
                                        $message = $this->removeLastCommaFromMessage($message);
                                        $this->messageManager->addErrorMessage(__($message));
                                        $this->messageManager->addErrorMessage(__('Please try again with correct shipping method'));
                                        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                                        $resultRedirect->setPath('bulkorder/shipping');

                                    }

                                } else {
                                    $message = 'opps!! Product SKU is not valid in row(s): ';
                                    foreach ($productNotFound as $productNotFoundItem) {
                                        $message .= $productNotFoundItem['row'] . ', ';

                                    }
                                    $message = $this->removeLastCommaFromMessage($message);
                                    $this->messageManager->addErrorMessage(__($message));
                                    $this->messageManager->addErrorMessage(__('Please try again with correct SKU'));
                                    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                                    $resultRedirect->setPath('bulkorder/shipping');

                                }

                            } catch (\Exception $e) {
                                $this->_logger->crit(__('opps!! Something went wrong Please try again after 5 seconds.'));
                                $this->_logger->crit('Error: ' . $e->getMessage());
                                $this->_logger->crit('Trace: ' . $e->getTraceAsString());

                                $this->messageManager->addErrorMessage(__('opps!! Something went wrong Please try again after 5 seconds.'));
                                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                                $resultRedirect->setPath('bulkorder/shipping');

                            }
                        }
                    }
                }
            }
        } else {
            $message = 'Opps! Please check your file format.';
            $this->messageManager->addErrorMessage(__($message));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('bulkorder/shipping');

        }
            
        return $resultRedirect;
        
    }
    
    protected function prepareQuoteForMultiShipping($fileAddressRows, $shippingAddressesIds) {
        $this->_logger->info('Start -> prepareQuoteForMultiShipping()');
        $cart = $this->_objectManager->create('\Magento\Checkout\Model\Cart');

        $checkoutSessionQuote = $this->_getCheckoutSession()->getQuote();
        $this->_getCheckoutSession()->setCartWasUpdated(false);

        $checkoutSessionQuote->setIsMultiShipping(1);
        $checkoutSessionQuote->setIsMultiShipping(true);
                
        /**
         * FROM Magento\Multishipping\Controller\Checkout\Addresses
         */
        $this->_getState()->unsCompleteStep(State::STEP_SHIPPING);

        $this->_getState()->setActiveStep(State::STEP_SELECT_ADDRESSES);
        
        
        /**
         * FROM Magento\Multishipping\Controller\Checkout\AddressesPost.php
         */
        $this->_getCheckout()->setCollectRatesFlag(true);
        $this->_getState()->setActiveStep(State::STEP_SHIPPING);
        $this->_getState()->setCompleteStep(State::STEP_SELECT_ADDRESSES);
        
        $this->_logger->info('QuoteId: '.$cart->getQuote()->getId());
        
        $quoteItemShippingInfo = [];
        foreach ($shippingAddressesIds as $key => $shippingAddressesIdsItem) {
            $bundleSku = $shippingAddressesIdsItem['bundleSku'];
            $bundle = $this->_ProductFactory->create()->loadByAttribute('sku', $bundleSku);
            foreach ($cart->getQuote()->getAllItems() as $quoteItem) {
                if ($quoteItem->getProductId() == $bundle->getId()) {
                    $quoteItemShippingInfo[] = [
                        $quoteItem->getItemId() => [
                            'qty' => $shippingAddressesIdsItem['qty'], 
                            'address' => $shippingAddressesIdsItem['customerAddressId']
                            ]
                        ];

                }

            }
        }
        
        /**
        * Assign quote items to addresses and specify items qty
        *
        * array structure:
        * array(
        *      $quoteItemId => array(
        *          'qty'       => $qty,
        *          'address'   => $customerAddressId
        *      )
        * )
        *
        * @param array $info
        * @return \Magento\Multishipping\Model\Checkout\Type\Multishipping
        * @throws \Magento\Framework\Exception\LocalizedException
        * @SuppressWarnings(PHPMD.CyclomaticComplexity)
        * @SuppressWarnings(PHPMD.NPathComplexity)
        */
        $this->_logger->info('prepareQuoteForMultiShipping() -> quoteItemShippingInfo', $quoteItemShippingInfo);
        $this->_getCheckout()->setShippingItemsInformation($quoteItemShippingInfo);
        
        $shippingNotSet = [];
        foreach ($shippingAddressesIds as $key => $shippingAddressesIdsItem) {
            
            /*if (trim($shippingAddressesIdsItem['shippingMethod']) == self::SHIPPNG_METHOD_STANDARD) {
                continue;
            }*/

            if (trim($shippingAddressesIdsItem['shippingMethod']) == self::SHIPPNG_METHOD_UPS) {
                continue;
            }

            if (trim($shippingAddressesIdsItem['shippingMethod']) == self::SHIPPING_METHOD_NEXT_DAY) {
                continue;
            }
            if (trim($shippingAddressesIdsItem['shippingMethod']) == self::SHIPPNG_METHOD_NEXT_2_DAY) {
                continue;
            }
            $shippingNotSet [] = ['shippingMethod' => $shippingAddressesIdsItem['shippingMethod'], 'row' => ($key+1)];
            
        }
        if (empty($shippingNotSet)) {
            $this->setShippingMethodsToAddresses($fileAddressRows);
            $this->_getState()->setActiveStep(State::STEP_BILLING);
            $this->_getState()->setCompleteStep(State::STEP_SHIPPING);

        }
        $this->_logger->info('End -> prepareQuoteForMultiShipping()');
        
        return $shippingNotSet;
        
    }
    
    protected function getQuoteAddressIdFromCustomerAddressId($customerAddressId) {
        $this->_logger->info('End -> getQuoteAddressIdFromCustomerAddressId($customerAddressId = ' . $customerAddressId .')');
        $cart = $this->_objectManager->create('\Magento\Checkout\Model\Cart');
        $quote = $cart->getQuote();
        $quoteAddressId = 0;
        foreach ($quote->getAllShippingAddresses() as $quoteAddress) {
            if ($quoteAddress->getCustomerAddressId() == $customerAddressId) {
                $quoteAddressId = $quoteAddress->getAddressId();
                
            }
            
        }
        $this->_logger->info('End -> getQuoteAddressIdFromCustomerAddressId()');
        return $quoteAddressId;
        
    }
    
    protected function getRowTotalForAddress($addressId) {
        $cart = $this->_objectManager->create('\Magento\Checkout\Model\Cart');
        $quote = $cart->getQuote();
        $rowTotal = null;
        foreach ($quote->getAllShippingAddresses() as $quoteAddress) {
            if ($quoteAddress->getAddressId() == $addressId) {
                $rowTotal = $quoteAddress->getSubtotal();
            }
            
        }
        return $rowTotal;
        
    }
    
    protected function setShippingMethodsToAddresses($fileAddressRows) {
        $this->_logger->info('Start -> setShippingMethodsToAddresses()');
        $cart = $this->_objectManager->create('\Magento\Checkout\Model\Cart');
        $shippingMethods = [];        
        
        $quote = $cart->getQuote();
        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $quote->save();
                
        $isFirstAddress = true;

        foreach ($fileAddressRows as $key => $row) {
            if ($key != self::FILE_HEADER_INDEX ) {
                $stateCode = trim($row[self::FILE_STATE]);
                $regionId = $this->getRegionIdFromCode($stateCode);
                $shippingMethod = $row[self::FILE_SHIPPING_METHOD];
                
                $email = trim($row[self::FILE_EMAIL]);
                $quoteAddressId = 0;
                $shippingMethodCode = 'matrixrate_matrixrate_';
                
                foreach ($cart->getQuote()->getAllShippingAddresses() as $shippingAddress) {
                    $isCurrentShippingAdress = false;
                    if ($shippingAddress->getShippingEmail() == $email) {
                        $isCurrentShippingAdress = true;
                        
                    } else {
                        $customerAddressId = $shippingAddress->getCustomerAddressId();
                        if (!empty($customerAddressId)) {
                            $shippingAddressRepository = $this->_objectManager->create('Magento\Customer\Api\AddressRepositoryInterface');
                            $shippingAddressRep = $shippingAddressRepository->getById($customerAddressId);
                            $shippingAddressEmailAttribute = $shippingAddressRep->getCustomAttribute('shipping_address_email');
                            if ($shippingAddressEmailAttribute != null) {
                                $shippingAddressEmail = $shippingAddressEmailAttribute->getValue();
                                if ($shippingAddressEmail == $email) {
                                    $isCurrentShippingAdress = true;
                                    
                                }
                                
                            }

                        } else if ($key == 1 && $isFirstAddress) {
                            $isCurrentShippingAdress = true;
                            
                        }
                        
                    }

                    if ($isCurrentShippingAdress) {
                        $quoteAddressId = $shippingAddress->getId();
                        
                        if ($shippingAddress->getDiscountDescription() == 'Free Shipping') {
                            $shippingMethod = 'Standard';
                            
                        }
                        
                        $shippingRate = $this->getShippingRate($shippingMethod, $regionId);

                        $shippingMethodCode = $shippingRate->getCode();

                        $shippingAddress->setCollectShippingRates(true);
                        $shippingAddress->collectShippingRates();
                        $shippingAddress->setShippingMethod($shippingMethodCode);

                        $shippingAddress->addShippingRate($shippingRate);
                        $shippingAddress->save();
                        break;
                        
                    }
                    
                    $isFirstAddress = false;
                    
                }

                $shippingMethods[$quoteAddressId] = $shippingMethodCode;
              
            }
        }           
        $cart->getQuote()->save();
        
        $this->_eventManager->dispatch(
            'checkout_controller_multishipping_shipping_post',
            ['request' => $this->getRequest(), 'quote' => $this->_getCheckout()->getQuote()]
        );
        $this->_getCheckout()->setShippingMethods($shippingMethods);
        
        $addressShippingRateCollection = $this->_objectManager->create('\Magento\Quote\Model\Quote\Address\Rate')->getCollection();
        $addressShippingRateCollection->addFieldToFilter('address_id', ['in' => array_keys($shippingMethods)]);
        $addressShippingRateCollection->addFieldToFilter('price', 0);
        $addressShippingRateCollection->addFieldToFilter('method_title', 'Free Shipping');
        $addressShippingRateCollection->addFieldToFilter('carrier', 'matrixrate');
        
        $shippingMethodCode = null;
        if (!empty($addressShippingRateCollection->getFirstItem()->getData())) {
            $shippingMethodCode = $addressShippingRateCollection->getFirstItem()->getCode();
            
        }
        
        $addressesId = [];
        $addressCollection = $this->_objectManager->create('\Magento\Quote\Model\Quote\Address')->getCollection();
        $addressCollection->addFieldToFilter('quote_id', ['eq' => $cart->getQuote()->getId()]);
        $addressCollection->addFieldToFilter('address_type', ['eq' => 'shipping']);
        
        foreach ($addressCollection as $quoteAddress) {
            $addressesId[] = $quoteAddress->getAddressId();
            if ($shippingMethodCode && $quoteAddress->getShippingAmount() == 0) {
                $quoteAddress->setShippingMethod($shippingMethodCode);
                $quoteAddress->save();
                
            }
        }

        $addressShippingRateCollection = $this->_objectManager->create('\Magento\Quote\Model\Quote\Address\Rate')->getCollection();
        $addressShippingRateCollection->addFieldToFilter('address_id', ['in' => $addressesId]);
        foreach ($addressShippingRateCollection as $shippingAddressRate) {
            if ($shippingAddressRate->getMethod() == null && $shippingAddressRate->getCarrier() == null) {
                $shippingAddressRate->delete();

            }
            
        }
        
        $this->_logger->info('End -> setShippingMethodsToAddresses()');
        
    }
    
    protected function getShippingRate($shippingMethod, $regionId) {
        $shippingRateCollection = $this->_matrixRateCollectionFactory->create();
        $shippingRateCollection->addFieldToFilter('shipping_method',['eq' => $shippingMethod])
                ->addFieldToFilter('dest_region_id',['eq' => $regionId])
                ->addFieldToFilter('condition_to_value',['gteq' => 1])
                ->addFieldToFilter('condition_from_value',['lteq' => 0]);
        $pk = '';
        if ($shippingRateCollection->getSize()) {
            $shippingRateItem = $shippingRateCollection->getFirstItem();
            $pk = $shippingRateItem->getPk();

        } else {
            $shippingRateCollection = $this->_matrixRateCollectionFactory->create();
            $shippingRateCollection->addFieldToFilter('shipping_method',['eq' => $shippingMethod])
                    ->addFieldToFilter('dest_region_id',['eq' => '0'])
                    ->addFieldToFilter('condition_to_value',['gteq' => 1])
                    ->addFieldToFilter('condition_from_value',['lteq' => 0]);
            if($shippingRateCollection->getSize()){
                $shippingRateItem = $shippingRateCollection->getFirstItem();
                $pk = $shippingRateItem->getPk();
                
            }
            
        }
        
        $shippingCode = 'matrixrate_matrixrate_' . $pk;
        
        $shippingRate = $this->_objectManager->create('Magento\Quote\Model\Quote\Address\Rate');
        $shippingRate->setCode($shippingCode);
        
        return $shippingRate;

    }
        
    /**
     * @desc create quote for products in csv file
     * @param Array $fileAddressRows
     * @param String $customerId
     * 
     * @return \Magento\Framework\Controller\ResultFactory $resultRedirect
     */
    protected function createQuoteForProduct($fileAddressRows, $customerId) {
        $this->_logger->addRecord(\Monolog\Logger::INFO, 'Enter createQuoteForProduct(Array,'.$customerId.')');
        
        $cart = $this->_objectManager->create('\Magento\Checkout\Model\Cart');
        $cart->getQuote()->setItemsQty(0);
        $cart->saveQuote();
        
        $customerRepository = $this->_objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface');
        $customer = $customerRepository->getById($customerId);
        $billingAddressId = $customer->getDefaultBilling();
        $shippingAddressId = $customer->getDefaultShipping();
        
        $customerBillingAddress = $this->_objectManager->create('Magento\Customer\Model\Address')->load($billingAddressId);
        $customerShippingAddress = $this->_objectManager->create('Magento\Customer\Model\Address')->load($shippingAddressId);
                
        $billingAddressData = $customerBillingAddress->getData();
        $billingAddressData['customer_address_id'] = $billingAddressId; 
        $shippingAddressData = $customerShippingAddress->getData();

        $cart->getQuote()->getBillingAddress()->addData($billingAddressData);
        $cart->getQuote()->getShippingAddress()->addData($shippingAddressData);
            
        if ($cart->getQuote()->getIsMultiShipping()) {
            $this->_quoteWasMultiShipping = 1;
            
        } else {
            $cart->getQuote()->setIsMultiShipping(1);
            $this->_quoteWasMultiShipping = 0;
            
        }
       

        $this->_getCheckoutSession()->setCartWasUpdated(false);

        $checkoutSessionQuote = $this->_getCheckoutSession()->getQuote();
        $checkoutSessionQuote->setIsMultiShipping(1);

        $productNotFound = [];
        $productsList = [];
        foreach ($fileAddressRows as $fileAddressRowsIndex => $fileAddressRow) {
            if ($fileAddressRowsIndex != self::FILE_HEADER_INDEX) {
                $itemSku = trim($fileAddressRow[self::FILE_SKU]);
                $product = $this->_ProductFactory->create()->loadByAttribute('sku', $itemSku);
                if ($product->getStatus() != \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED) {
                    $product = null;
                    
                }
                if (empty($product)) {
                    $productNotFound[] = ['row' => $fileAddressRowsIndex, 'sku' => $itemSku];
                    
                } else {
                    $qty = $fileAddressRow[self::FILE_QTY];
                    if (isset($productsList[$itemSku])) {
                        $productsList[$itemSku]['qty'] += (int)$qty;
                        
                    } else {
                        $bundle = $this->getBundleIdFromItem($product->getId());
                        if ($bundle == null) {
                            $productNotFound[] = ['row' => $fileAddressRowsIndex, 'sku' => $itemSku];
                            
                        } else if ($bundle->getStatus() != \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED) {
                            $productNotFound[] = ['row' => $fileAddressRowsIndex, 'sku' => $itemSku];

                        } else {
                            $productsList[$itemSku]['qty'] = (int)$qty;
                            $productsList[$itemSku]['bundleSku'] = $bundle->getSku();
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        foreach ($productsList as $productSku => $data) {
            $this->addProductToCart($data['bundleSku'], $productSku, $data['qty']);
            
        }
        
        /**
         * To Handle add price to product in cart.
         */
        $quote = $cart->getQuote();
        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        $quote->save();
        
        $this->_logger->addRecord(\Monolog\Logger::INFO, 'Exit createQuoteForProduct');

        return $productNotFound;
    }
    
    /**
     * @desc get bundle from Item
     * @param String $itemId
     * 
     * @return \Magento\Catalog\Model\Product $bundle
     */
    protected function getBundleIdFromItem($itemId) {
        $bundle = null;
        if (array_key_exists($itemId, $this->itemsBundleMap)) {
            $bundle = $this->itemsBundleMap[$itemId];
            
        } else {
            
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $selection = $objectManager->create('Magento\Bundle\Model\Selection')
                    ->load($itemId, 'product_id');
            if (!empty($selection->getId())) {
                $bundle = $objectManager->create('Magento\Catalog\Model\Product')->load($selection->getParentProductId());
                
            }
            $this->itemsBundleMap[$itemId] = $bundle;
            
        }
        
        return $bundle;
        
    }
    
    /**
     * @desc add product to cart by bundle and item
     * @param String $bundleSku
     * @param String $itemSku
     * 
     * @return \Magento\Framework\Controller\ResultFactory $resultRedirect
     */
    protected function addProductToCart($bundleSku, $itemSku, $qty) {
        $bundleOptions = $this->_objectManager->create('\Magento\Bundle\Model\Option');
        
        $bundleItem = $this->_objectManager->create('\Magento\Catalog\Model\Product')->loadByAttribute('sku', $itemSku);
        $bundle = $this->_objectManager->create('\Magento\Catalog\Model\Product')->loadByAttribute('sku', $bundleSku);
        
        $bundleItemId = $bundleItem->getId();
        $bundleProductId = $bundle->getId();
        
        $bundleoption = $bundleOptions->getCollection()->addFieldToFilter('parent_id', $bundleProductId)->getFirstItem();
        $bundleOptionId = $bundleoption->getOptionId();

        $selectionCollection = $bundle->getTypeInstance(true)
                ->getSelectionsCollection($bundle->getTypeInstance(true)->getOptionsIds($bundle), $bundle);
        
        foreach ($selectionCollection as $selectionCollectionItem) {
            if ($selectionCollectionItem->getProductId() == $bundleItemId) {
                $bundleSelectionId = $selectionCollectionItem->getSelectionId();
                
            }
            
        }
        $bundleOption = array($bundleOptionId => $bundleSelectionId);
        $bundleOptionQty = array($bundleOptionId => "1");
                                
        $params = [
            'uenc' => null,
            'product' => $bundleProductId,
            'selected_configurable_option' => "",
            'related_product' => "",
            'form_key' => $this->_formKey->getFormKey(),
            'bundle_option' => $bundleOption,
            'optionId' => $bundleSelectionId,
            'bundle_option_qty' => $bundleOptionQty,
            'qty' => $qty
        ];
           
        $this->_logger->info('Add Product to Cart: ', $params);
        
        $cart = $this->_objectManager->create('\Magento\Checkout\Model\Cart');
        $cart->addProduct($bundle, $params);
        
        $this->_eventManager->dispatch(
            'checkout_cart_add_product_complete',
            ['product' => $bundle, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
        );
        
        return true;
        
    }

    /**
     * @desc filter exists row in csv file for addresses
     * @param Array $fileAddressRows
     * 
     * @return \Magento\Framework\Controller\ResultFactory $resultRedirect
     */
    protected function filterExistAddresses($fileAddressRowsItem, $customerId) {
        $customer = $this->_customerRepository->getById($customerId);
        $addresses = $customer->getAddresses();
        $addressId = null;
        foreach ($addresses as $address) {
            $firstName = trim($fileAddressRowsItem[self::FILE_FIRST_NAME]);
            $lastName = trim($fileAddressRowsItem[self::FILE_LAST_NAME]);
            $company = trim($fileAddressRowsItem[self::FILE_COMPANY]);
            $phoneNumber = trim($fileAddressRowsItem[self::FILE_PHONE_NUMBER]);
            $fax = trim($fileAddressRowsItem[self::FILE_FAX]);
            $streetAddress = trim($fileAddressRowsItem[self::FILE_STREET_ADDRESS]);
            $city = trim($fileAddressRowsItem[self::FILE_CITY]);
            $zipCode = trim($fileAddressRowsItem[self::FILE_ZIP_CODE]);
            $email = trim($fileAddressRowsItem[self::FILE_EMAIL]);
            $stateCode = trim($fileAddressRowsItem[self::FILE_STATE]);
            $regionId = $this->getRegionIdFromCode($stateCode);
            $currentStreet = $address->getStreet();
            
            if ($address->getFirstname() == $firstName
                && $address->getLastname() == $lastName
                && $address->getRegionId() == $regionId
                && $address->getPostcode() == $zipCode
                && $address->getCity() == $city
                && $address->getFax() == $fax
                && $address->getCompany() == $company
                && $address->getTelephone() == $phoneNumber
                && $currentStreet[0] == $streetAddress
                ) {
                    $addressId = $address->getId();
                }
                
        }
        return $addressId;
        
    }
    
    /**
     * @desc validate row in csv file for addresses
     * @param Array $fileAddressRows
     * 
     * @return \Magento\Framework\Controller\ResultFactory $resultRedirect
     */
    protected function validateFields($fileAddressRows) {
        $validateErrors = [];
        $resultRedirect = null;
        $preOrderHelper = $this->_objectManager->create('\Webkul\Preorder\Helper\Data');
        foreach ($fileAddressRows as $rowAddressId => $rowAddress) {
            if ($rowAddressId != self::FILE_HEADER_INDEX) {
                $phoneNumber = trim($rowAddress[self::FILE_PHONE_NUMBER]);
                if(!preg_match("/^[0-9]{10}$/", $phoneNumber)) {
                    $validateErrors ['Phone Number'][] = $rowAddressId;
                    
                }
                $zipCode = trim($rowAddress[self::FILE_ZIP_CODE]);
                if (!preg_match('#^\d{5}([\-]?\d{4})?$#',$zipCode)) {
                    $validateErrors ['Zip Code'][] = $rowAddressId;
                    
                }
                $state = trim($rowAddress[self::FILE_STATE]);
                $validatestate = $this->validateState($state);
                if (!$validatestate) {
                    $validateErrors ['State/Province Code'][] = $rowAddressId;
                    
                }
                $street = trim($rowAddress[self::FILE_STREET_ADDRESS]);
                if (strlen($street) > 80 ) {
                    $validateErrors ['Street Address'][] = $rowAddressId;
                    
                }
                $email = trim($rowAddress[self::FILE_EMAIL]);
                if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $email)) {
                    $validateErrors ['Email'][] = $rowAddressId;
                    
                }
                $itemSku = $rowAddress[self::FILE_SKU];
                $product = $this->_ProductFactory->create()->loadByAttribute('sku', $itemSku);
                if (empty($product)) {
                    $validateErrors ['Product SKU'][] = $rowAddressId;
                    
                } else if ($product->getStatus() != \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED) {
                    $validateErrors ['Product SKU'][] = $rowAddressId;
                    
                } else if ($product->getTypeId() == \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE) {
                    $validateErrors ['eBook'][] = $rowAddressId;
                    
                } else {
                    $bundle = $this->getBundleIdFromItem($product->getId());
                    if ($bundle != null) {
                        if ($preOrderHelper->isPreorder($bundle->getId())) {
                            $validateErrors ['preOrder'][] = $rowAddressId;

                        }
                    }
                    
                }
                
            }
            
        }
        if (!empty($validateErrors)) {
            foreach ($validateErrors as $validateError => $rowAddressId) {
                if ($validateError == 'Street Address') {
                    $message = 'Opps! Maximum length allowed for '.$validateError.' is 80 in row(s): ';
                    foreach ($rowAddressId as $rowAddressIdItem) {
                        $message .= ($rowAddressIdItem+1).', ';

                    }
                    
                } elseif ($validateError == 'eBook') {
                    $message = 'eBook only products cannot be processed through bulk orders. Please use regular checkout flow to items in row(s):  ';
                    foreach ($rowAddressId as $rowAddressIdItem) {
                        $message .= ($rowAddressIdItem+1).', ';

                    }
                    
                } else if ($validateError == 'preOrder') {
                    $message = 'pre-order cannot be processed through bulk orders. Please use regular checkout flow to items in row(s):  ';
                    foreach ($rowAddressId as $rowAddressIdItem) {
                        $message .= ($rowAddressIdItem+1).', ';

                    }
                    
                } else {
                    $message = 'Opps! '.$validateError.' is not valid in row(s): ';
                    foreach ($rowAddressId as $rowAddressIdItem) {
                        $message .= ($rowAddressIdItem+1).', ';

                    }
                    
                }
                $message = $this->removeLastCommaFromMessage($message);
                $this->messageManager->addErrorMessage(__($message));

            }
            $message = 'CSV is not loaded. Please clean based on the above directions and try it again.';
            $this->messageManager->addErrorMessage(__($message));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('bulkorder/shipping');
            
        }
        return $resultRedirect;
        
    }
    
    /**
     * @desc validate state for each row in addresses
     * @param String $state
     * 
     * @return Bool $validState
     */
    protected function validateState($state) {
        $region = $this->_objectManager->create('Magento\Directory\Model\ResourceModel\Region\Collection')
                    ->addFieldToFilter('code', ['eq' => $state])
                    ->addFieldToFilter('country_id', ['eq' => 'US'])
                    ->getFirstItem();
        
        $validState = true;
        if (empty($region->getId())) {
            $validState = false;
            
        }
        
        return $validState;
        
    }
    
    /**
     * @desc Check required fields in csv for addresses
     * @param Array $fileAddressRows
     * 
     * @return \Magento\Framework\Controller\ResultFactory $resultRedirect
     */
    protected function checkRequiredFields($fileAddressRows) {
        $requiredFields = [];
        $resultRedirect = null;
        foreach ($fileAddressRows as $rowAddressId => $rowAddress) {
            if ($rowAddressId != self::FILE_HEADER_INDEX) {
                if (empty(trim($rowAddress[self::FILE_FIRST_NAME]))) {
                    $requiredFields ['First Name'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_LAST_NAME]))) {
                    $requiredFields ['Last Name'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_PHONE_NUMBER]))) {
                    $requiredFields ['Phone Number'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_STREET_ADDRESS]))) {
                    $requiredFields ['Street Address'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_CITY]))) {
                    $requiredFields ['City'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_STATE]))) {
                    $requiredFields ['State'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_ZIP_CODE]))) {
                    $requiredFields ['ZIP/POSTAL'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_EMAIL]))) {
                    $requiredFields ['Email'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_SKU]))) {
                    $requiredFields ['SKU'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_QTY]))) {
                    $requiredFields ['QTY'][] = $rowAddressId;

                }
                if (empty(trim($rowAddress[self::FILE_SHIPPING_METHOD]))) {
                    $requiredFields ['Shipping Method'][] = $rowAddressId;

                }
                
            }
            
        }
        
        if (!empty($requiredFields)) {
            foreach ($requiredFields as $requiredField => $rowAddressId) {
                $message = 'Opps! '.$requiredField.' is required in row(s): ';
                foreach ($rowAddressId as $rowAddressIdItem) {
                    $message .= ($rowAddressIdItem+1).',';

                }
                $message = $this->removeLastCommaFromMessage($message);
                $this->messageManager->addErrorMessage(__($message));

            }
            $message = 'CSV is not loaded. Please clean based on the above directions and try it again.';
            $this->messageManager->addErrorMessage(__($message));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('bulkorder/shipping');

        }
        
        return $resultRedirect;
        
    }
    
    /**
     * @desc Create new address to customer
     * @param Array $fileAddressRows
     * @param String $customerId
     */
    protected function createNewAddressToCustomer($fileAddressRows, $customerId) {
        $this->_logger->addInfo('Start -> createNewAddressToCustomer( , ' . $customerId . ')');
        
        $shippingAddressesIdsInCsv = [];
        foreach ($fileAddressRows as $fileAddressRowId => $fileAddressRowsItem) {
            if ($fileAddressRowId != self::FILE_HEADER_INDEX) {
                
                $addressId = $this->filterExistAddresses($fileAddressRowsItem, $customerId);
                $customerAddressRepository = $this->_objectManager->create('Magento\Customer\Api\AddressRepositoryInterface');
                $email = trim($fileAddressRowsItem[self::FILE_EMAIL]);
                if ($addressId == null) {
                    $address = $this->_addressFactory->create();
                    $firstName = trim($fileAddressRowsItem[self::FILE_FIRST_NAME]);
                    $lastName = trim($fileAddressRowsItem[self::FILE_LAST_NAME]);
                    $company = trim($fileAddressRowsItem[self::FILE_COMPANY]);
                    $phoneNumber = trim($fileAddressRowsItem[self::FILE_PHONE_NUMBER]);
                    $fax = trim($fileAddressRowsItem[self::FILE_FAX]);
                    $streetAddress = trim($fileAddressRowsItem[self::FILE_STREET_ADDRESS]);
                    $city = trim($fileAddressRowsItem[self::FILE_CITY]);
                    $stateCode = trim($fileAddressRowsItem[self::FILE_STATE]);
                    $regionId = $this->getRegionIdFromCode($stateCode);
                    $regionName = $this->getRegionNameFromCode($stateCode);
                    $zipCode = trim($fileAddressRowsItem[self::FILE_ZIP_CODE]);
                    $qty = trim($fileAddressRowsItem[self::FILE_QTY]);
                    $shippingMethod = trim($fileAddressRowsItem[self::FILE_SHIPPING_METHOD]);
                    
                    $address->setCustomerId($customerId)
                            ->setFirstname($firstName)
                            ->setLastname($lastName)
                            ->setCountryId('US')
                            ->setRegion($regionName)
                            ->setRegionId($regionId)
                            ->setPostcode($zipCode)
                            ->setCity($city)
                            ->setFax($fax)
                            ->setCompany($company)
                            ->setTelephone($phoneNumber)
                            ->setStreet($streetAddress)
                            ->setIsDefaultBilling(0)
                            ->setIsDefaultShipping(0)
                            ->setSaveInAddressBook(0);

                    $address->save();

                    $customerAddressRep = $customerAddressRepository->getById($address->getId());
                    $customerAddressRep->setCustomAttribute('shipping_address_email', $email);
                    $customerAddressRepository->save($customerAddressRep);
                    $itemSku = trim($fileAddressRowsItem[self::FILE_SKU]);
                    $bundleItem = $this->_ProductFactory->create()->loadByAttribute('sku', $itemSku);
                    $bundle = $this->getBundleIdFromItem($bundleItem->getId());
                    
                    $shippingAddressesIdsInCsv [] = [
                        'customerAddressId' => $address->getId(), 
                        'regionId' => $regionId, 
                        'itemSku' => $itemSku,
                        'bundleSku' => $bundle->getSku(),
                        'qty' => $qty,
                        'shippingMethod' => $shippingMethod
                    ];
                    
                } else {
                    $stateCode = trim($fileAddressRowsItem[self::FILE_STATE]);
                    $regionId = $this->getRegionIdFromCode($stateCode);
                    $itemSku = trim($fileAddressRowsItem[self::FILE_SKU]);
                    $bundleItem = $this->_ProductFactory->create()->loadByAttribute('sku', $itemSku);
                    $bundle = $this->getBundleIdFromItem($bundleItem->getId());
                    $qty = trim($fileAddressRowsItem[self::FILE_QTY]);
                    $shippingMethod = trim($fileAddressRowsItem[self::FILE_SHIPPING_METHOD]);
                    
                    //Save Email to Address
                    $customerAddressRep = $customerAddressRepository->getById($addressId);
                    $customerAddressRep->setCustomAttribute('shipping_address_email', $email);
                    $customerAddressRepository->save($customerAddressRep);
                    
                    $shippingAddressesIdsInCsv [] = [
                        'customerAddressId' => $addressId, 
                        'regionId' => $regionId,
                        'itemSku' => $itemSku,
                        'bundleSku' => $bundle->getSku(),
                        'qty' => $qty,
                        'shippingMethod' => $shippingMethod
                        ];
                    
                }
                
            }
            
        }
        
        $this->_logger->addInfo('End -> getBundleIdFromItem()');
        return $shippingAddressesIdsInCsv;
        
    }
    
    /**
     * @desc get region name from code.
     * @param String $stateCode
     * 
     * @return String $regionName
     */
    protected function getRegionNameFromCode($stateCode) {
        $region = $this->_objectManager->create('Magento\Directory\Model\ResourceModel\Region\Collection')
                    ->addFieldToFilter('code', ['eq' => $stateCode])
                    ->addFieldToFilter('country_id', ['eq' => 'US'])
                    ->getFirstItem();
        $regionName = $region->getDefaultName();
        
        
        return $regionName;
        
    }
    
    /**
     * @desc get region id from code.
     * @param String $stateCode
     * 
     * @return String $regionId
     */
    protected function getRegionIdFromCode($stateCode) {
        $region = $this->_objectManager->create('Magento\Directory\Model\ResourceModel\Region\Collection')
                    ->addFieldToFilter('code', ['eq' => $stateCode])
                    ->addFieldToFilter('country_id', ['eq' => 'US'])
                    ->getFirstItem();
        $regionId = $region->getRegionId();
        return $regionId;
        
    }
    
    /**
     * Retrieve checkout model
     *
     * @return \Magento\Multishipping\Model\Checkout\Type\Multishipping
     */
    protected function _getCheckout() {
        return $this->_objectManager->create('Magento\Multishipping\Model\Checkout\Type\Multishipping');
    }

    /**
     * Retrieve checkout state model
     *
     * @return \Magento\Multishipping\Model\Checkout\Type\Multishipping\State
     */
    protected function _getState() {
        return $this->_objectManager->create('Magento\Multishipping\Model\Checkout\Type\Multishipping\State');
        
    }
    
    /**
     * Retrieve checkout session
     *
     * @return \Magento\Checkout\Model\Session
     */
    protected function _getCheckoutSession() {
        return $this->_objectManager->create('Magento\Checkout\Model\Session');
    }
    
    protected function trimData($data) {
        if ($data == null) {
            return null;
            
        }

        if (is_array($data)) {
            return array_map(array($this, 'trimData'), $data);
            
        } else {
            return trim($data);
            
        }
    }
    
    protected function removeLastCommaFromMessage($message) {
        if ($message == null) {
            $message = '';
            
        } else if (strlen($message) >= 2) {
            $message[strlen($message) - 2] = '';
            
        }
        return $message;
    }
    
    protected function addressesCount($fileAddressRows){
        $addressesCount = (count($fileAddressRows)-1);
        return $addressesCount;
        
    }
    
}
