<?php
/**
 *
 * Copyright © Endeavor Technology, Inc.
 */
namespace Endeavor\MultipleShipping\Observer\Multishipping;

class CheckoutControllerSuccessAction implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $orderIds = $observer->getData('order_ids');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        foreach ($orderIds as $orderId) {
            $saleOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
            $orderItems = $saleOrder->getAllItems();
            $increment_id = 0;

            foreach ($orderItems as $orderItem) {
                $increment_id = $increment_id + 1;
                $orderItem->setCustomIncrementId(str_pad($increment_id,6,'0',STR_PAD_LEFT));
                $orderItem->save();
            }
         
        }
        
        $AssignUser = $objectManager->get('Endeavor\MultipleShipping\Helper\Assign');
        
        /**
         * Assign user for eBook product for email in shipping address.
         */
        $AssignUser->assignUsersInShippingAddress($orderIds);
        
        
        
    }
}
