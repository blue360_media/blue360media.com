<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Endeavor\CustomerAttributes\Setup;

use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Endeavor\CustomerAttributes\Model\Segment;
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;
    private $segmentFactory;
    
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;
    
    /**
     * Init
     *
     * @param CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(\Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory, 
            \Endeavor\CustomerAttributes\Model\SegmentFactory $segmentFactory
)
    {
        $this->segmentFactory = $segmentFactory;
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        
        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_role',  [
            'label' => 'Role',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
            'position' => 3,
            'visible' => true,
            'required' => false,
            'system' => 0,
            'is_filterable_in_grid' => true,
            'is_searchable_in_grid' => true,
            'is_visible_in_grid' =>true,
            'is_used_in_grid' => true,
            'option' =>
                array (
                    'value' =>
                        array (
                            'Customer' =>  array('Customer'),
                            'Account' =>  array('Account')
                        )
                )
        ]);

        $role = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_role');
        $role->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $role->save();
		
        
        
        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_name',  [
            'label' => 'Name',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 4,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $name = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_name');
        $name->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $name->save();
    
        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_blue_customer_number',  [
            'label' => 'BlueMedia Customer Number',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 10,
            'visible' => true,
            'is_used_in_grid' => true,
            'is_visible_in_grid' => true,
            'is_filterable_in_grid' => true,
            'is_searchable_in_grid' => true,
            'required' => false,
            'system' => 0
        ]);
        
        

        $customerNumber = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_blue_customer_number');
        $customerNumber->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $customerNumber->save();

        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_blue_account_id',  [
            'label' => 'BlueMedia Account Id',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 11,
            'visible' => true,
            'is_used_in_grid' => true,
            'is_visible_in_grid' => true,
            'is_filterable_in_grid' => true,
            'is_searchable_in_grid' => true,
            'required' => false,
            'system' => 0
        ]);
        
        $accountNumber = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_blue_account_id');
        $accountNumber->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $accountNumber->save();

        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_website_address',  [
            'label' => 'Website Address',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 9,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $websiteAddress = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_website_address');
        $websiteAddress->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $websiteAddress->save();
        
        
        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_ln_organization_type',  [
            'label' => 'LexisNexis Organization Type',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
            'position' => 13,
            'visible' => true,
            'required' => false,
            'system' => 0,
            'option' =>
                array (
                    'value' =>
                        array (
                            'Association-Bar' =>  array('Association-Bar'),
                            'Banks, Savings & Loan' =>  array('Banks, Savings & Loan'),
                            'Bookstore serving a college/university' =>  array('Bookstore serving a college/university'),
                            'Bookstores NOT serving a college/univers' =>  array('Bookstores NOT serving a college/univers'),
                            'Business/Industry' =>  array('Business/Industry'),
                            'CFM-St/Local Govt' =>  array('CFM-St/Local Govt'),
                            'Charity/Non-Profit Organization' =>  array('Charity/Non-Profit Organization'),
                            'CJ USE ONLY - CJ EDUCATIONAL ACCOUNTS' =>  array('CJ USE ONLY - CJ EDUCATIONAL ACCOUNTS'),
                            'CJ USE ONLY - CJ LOCAL GOVERNMENT ACCOUN' =>  array('CJ USE ONLY - CJ LOCAL GOVERNMENT ACCOUN'),
                            'College/University' =>  array('College/University'),
                            'Corporation with Accounting, Tax or Fina' =>  array('Corporation with Accounting, Tax or Fina'),
                            'County/local Judges and courts' =>  array('County/local Judges and courts'),
                            'Education/school library' =>  array('Education/school library'),
                            'Educational Institution' =>  array('Educational Institution'),
                            'Federal Attorneys' =>  array('Federal Attorneys'),
                            'Federal Corrections' =>  array('Federal Corrections'),
                            'Federal Courts' =>  array('Federal Courts'),
                            'Federal Government' =>  array('Federal Government'),
                            'Foreign Distributor' =>  array('Foreign Distributor'),
                            'House Authors' =>  array('House Authors'),
                            'Individual/Student E-Commerce Purchaser' =>  array('Individual/Student E-Commerce Purchaser'),
                            'Insurance Agents & Brokers, CLU, Title C' =>  array('Insurance Agents & Brokers, CLU, Title C'),
                            'Internal Employee/Department' =>  array('Internal Employee/Department'),
                            'Large (or Major or national) accounting' =>  array('Large (or Major or national) accounting'),
                            'Large Law East, 51 - 99 Attys' =>  array('Large Law East, 51 - 99 Attys'),
                            'Large Law East,100+ Attys' =>  array('Large Law East,100+ Attys'),
                            'Large Law West, 100+ Attys' =>  array('Large Law West, 100+ Attys'),
                            'Large Law West, 51-99 Attys' =>  array('Large Law West, 51-99 Attys'),
                            'Law School' =>  array('Law School'),
                            'LL Strategic, 100+ Attys' =>  array('LL Strategic, 100+ Attys'),
                            'Medical - Doctors, Hospitals,Medical Ser' =>  array('Medical - Doctors, Hospitals,Medical Ser'),
                            'Prisoner' =>  array('Prisoner'),
                            'Private Citizen/Personal Account' =>  array('Private Citizen/Personal Account'),
                            'Public School' =>  array('Public School'),
                            'Puerto Rico and Virgin Island state acco' =>  array('Puerto Rico and Virgin Island state acco'),
                            'Real Estate' =>  array('Real Estate'),
                            'Small Law, 1 - 2 attorneys' =>  array('Small Law, 1 - 2 attorneys'),
                            'Small Law, 11 - 50 attorneys' =>  array('Small Law, 11 - 50 attorneys'),
                            'Small Law, 3 - 10 attorneys' =>  array('Small Law, 3 - 10 attorneys'),
                            'Small Law, Law Library' =>  array('Small Law, Law Library'),
                            'Small Law, Legal Aid' =>  array('Small Law, Legal Aid'),
                            'State and Local Attorneys' =>  array('State and Local Attorneys'),
                            'State and Local Corrections' =>  array('State and Local Corrections'),
                            'State and Local Courts Law Libraries' =>  array('State and Local Courts Law Libraries'),
                            'State Courts and judges' =>  array('State Courts and judges'),
                            'State/Local law Library' =>  array('State/Local law Library'),
                            'State/Local Public Library' =>  array('State/Local Public Library'),
                            'Trade Dealers' =>  array('Trade Dealers'),
                            'Unclassified' =>  array('Unclassified')
                        )
                )
        ]);

        $organizationType = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_ln_organization_type');
        $organizationType->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $organizationType->save();
        
        
        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_ln_segment',  [
            'label' => 'LexisNexis Segment',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
            'position' => 12,
            'visible' => true,
            'required' => false,
            'system' => 0,
            'option' =>
                array (
                    'value' =>
                        array (
                            'Asia Pacific' =>  array('Asia Pacific'),
                            'Canada' =>  array('Canada'),
                            'Corporate Legal' =>  array('Corporate Legal'),
                            'Federal - Corrections' =>  array('Federal - Corrections'),
                            'Federal Other' =>  array('Federal Other'),
                            'International' =>  array('International'),
                            'LE Law School Libraries' =>  array('LE Law School Libraries'),
                            'LE Law School Publishing' =>  array('LE Law School Publishing'),
                            'LE Oth_Academic' =>  array('LE Oth_Academic'),
                            'LL-East' =>  array('LL-East'),
                            'LL-Strategic' =>  array('LL-Strategic'),
                            'LL-West' =>  array('LL-West'),
                            'S&L - Corrections' =>  array('S&L - Corrections'),
                            'S&L Govt Other' =>  array('S&L Govt Other'),
                            'USLM Other' =>  array('USLM Other'),
                            'USLM Small Law' =>  array('USLM Small Law')
                        )
                )
        ]);

        $lnMarketSegment = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_ln_segment');
        $lnMarketSegment->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $lnMarketSegment->save();
        
        /*
         // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_market_segment',  [
            'label' => 'Market Segment',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
            'position' => 7,
            'visible' => true,
            'required' => false,
            'system' => 0,
            'option' =>
                    array (
                        'value' =>
                            array (
                                'State Agencies' =>  array('State Agencies'),
                                'Academic' =>  array('Academic'),
                                'Local Agencies' =>  array('Local Agencies'),
                                'Re-Sellers' =>  array('Re-Sellers'),
                                'Law Firms' =>  array('Law Firms'),
                                'Federal' =>  array('Federal'),
                                'Corporate' =>  array('Corporate'),
                                'Consumer' =>  array('Consumer')
                                )
                    )
        ]);
*/
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_market_segment',  [
            'label' => 'Market Segment',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Endeavor\CustomerAttributes\Model\Config\Source\Segments',
            'position' => 14,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);
        $marketSegment = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_market_segment');
        $marketSegment->setData(
            'used_in_forms',
            ['adminhtml_customer']//, 'customer_account_edit']
        );
        $marketSegment->save();

        		
        // insert attribute
        /*
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_secondary_market',  [
            'label' => 'Secondary Market',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
            'position' => 8,
            'visible' => true,
            'required' => false,
            'system' => 0,
            'option' =>
                    array (
                        'value' =>
                            array (
                                'State Police / Troopers - Detectives' =>  array('State Police / Troopers - Detectives'),
                                'Highway Patrol (Dept of Trans)' =>  array('Highway Patrol (Dept of Trans)'),
                                'Attorney General / Dept of Law' =>  array('Attorney General / Dept of Law'),
                                'Prosecutors' =>  array('Prosecutors')
                                
                                )
                    )
        ]);*/
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_secondary_market',  [
            'label' => 'Secondary Market',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Endeavor\CustomerAttributes\Model\Config\Source\SecondarySegments',
            'position' => 16,
            'user_defined' => true,
            'visible' => true,
            'required' => false,
            'system' => 0
            
        ]);

        $secondaryMarket = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_secondary_market');
        $secondaryMarket->setData(
            'used_in_forms',
            ['adminhtml_customer']//, 'customer_account_edit']
        );
        $secondaryMarket->save();
        
         // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_tax_exempt_status',  [
            'label' => 'Tax Exempt Status',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
            'position' => 19,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $taxExemptStatus = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_tax_exempt_status');
        $taxExemptStatus->setData(
            'used_in_forms',
            ['adminhtml_customer']//, 'customer_account_edit']
        );
        $taxExemptStatus->save();
		
        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_acct_level_discount',  [
            'label' => 'Account Level Discount',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 17,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $accountLevelDiscoun = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_acct_level_discount');
        $accountLevelDiscoun->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $accountLevelDiscoun->save();
        
        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_po_required',  [
            'label' => 'Purchase Order Required',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
            'position' => 19,
            'visible' => true,
            'required' => false,
            'system' => 0
        ]);

        $purchaseOrderRequired= $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_po_required');
        $purchaseOrderRequired->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $purchaseOrderRequired->save();
        
        // insert attribute
        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_credit_indicator',  [
            'label' => 'Credit Indicator',
            'type' => 'varchar',
            'input' => 'select',
            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
            'position' => 20,
            'visible' => true,
            'required' => false,
            'system' => 0,
            'option' =>
                array (
                    'value' =>
                        array (
                            'we need data' =>  array('We need data')
                        )
                )

        ]);

        $creditIndicator = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_credit_indicator');
        $creditIndicator->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );
        $creditIndicator->save();

        if (version_compare($context->getVersion(), '1.0.0')) {
                $setup->getConnection()->delete('endeavor_customer_secondary_segments');
                $setup->getConnection()->delete('endeavor_customer_segments');
                $secandarySegmentsData = 
                [
                    ['name'=>'State Police / Troopers - Detectives','parent_id'=>1],
                    ['name'=>'Highway Patrol (Dept of Trans)','parent_id'=>1],
                    ['name'=>'Attorney General / Dept of Law','parent_id'=>1],
                    ['name'=>'Prosecutors','parent_id'=>1],
                    ['name'=>'Bureau of Motor Vehicles (DMV)','parent_id'=>1],
                    ['name'=>'Law Libraries (Courts)','parent_id'=>1],
                    ['name'=>'Correction Facilities','parent_id'=>1],
                    ['name'=>'Dept. Fish & Game / Wildlife','parent_id'=>1],
                    ['name'=>'Fire Dept.','parent_id'=>1],
                    ['name'=>'State Courts','parent_id'=>1],
                    ['name'=>'Law School - Library','parent_id'=>2],
                    ['name'=>'Law School - Professor','parent_id'=>2],
                    ['name'=>'Law School - Students','parent_id'=>2],
                    ['name'=>'University - Deans','parent_id'=>2],
                    ['name'=>'University - Students','parent_id'=>2],
                    ['name'=>'University - Library','parent_id'=>2],
                    ['name'=>'University - Univ. Police','parent_id'=>2],
                    ['name'=>'High School - School Board - Teachers','parent_id'=>2],
                    ['name'=>'Driver’s Ed School','parent_id'=>2],
                    ['name'=>'School Resource Officers','parent_id'=>2],
                    ['name'=>'Private Police Academies','parent_id'=>2],
                    ['name'=>'Promotional Exam Providers','parent_id'=>2],
                    ['name'=>'Police Dept - Detectives','parent_id'=>3],
                    ['name'=>'Sheriff’s Office - Constables','parent_id'=>3],
                    ['name'=>'Sheriff’s Office - Officers','parent_id'=>3],
                    ['name'=>'Prosecutors','parent_id'=>3],
                    ['name'=>'Security Firms','parent_id'=>8],
                    ['name'=>'Insurance Co.','parent_id'=>8],
                    ['name'=>'Police Officers','parent_id'=>7],
                    ['name'=>'Students','parent_id'=>7],
                    ['name'=>'Wannabes','parent_id'=>7],
                    ['name'=>'Military - Police','parent_id'=>6],
                    ['name'=>'Military - Intelligence','parent_id'=>6],
                    ['name'=>'Military - JAG','parent_id'=>6],
                    ['name'=>'DNI - FBI','parent_id'=>6],
                    ['name'=>'DNI - CIA','parent_id'=>6],
                    ['name'=>'DNI - NSA','parent_id'=>6],
                    ['name'=>'DNI - Homeland Sec. - TSA','parent_id'=>6],
                    ['name'=>'DNI - Others','parent_id'=>6],
                    ['name'=>'DNI - US Marshals','parent_id'=>6],
                    ['name'=>'DNI - Secret Service','parent_id'=>6],
                    ['name'=>'ICE','parent_id'=>6],
                    ['name'=>'Federal Law Enforcement Training Center (FLETC)','parent_id'=>6],
                    ['name'=>'Forest Service','parent_id'=>6],
                    ['name'=>'BLM','parent_id'=>6],
                    ['name'=>'Fed. Courts','parent_id'=>6],
                    ['name'=>'Federal Corrections','parent_id'=>6],
                    ['name'=>'Large Law (50+) - Librarian','parent_id'=>5],
                    ['name'=>'Large Law (50+) - Attorneys (later on)','parent_id'=>5],
                    ['name'=>'Mid Law (21-49) - Librarian','parent_id'=>5],
                    ['name'=>'Mid Law (21-49) - Attorneys','parent_id'=>5],
                    ['name'=>'Small(3-20) - Speciality','parent_id'=>5],
                    ['name'=>'Small(3-20) - General','parent_id'=>5],
                    ['name'=>'Solo(1-2) - Speciality','parent_id'=>5],
                    ['name'=>'Solo(1-2) - General','parent_id'=>5],
                    ['name'=>'Uniform Stores','parent_id'=>4],
                    ['name'=>'Police Supply Stores','parent_id'=>4],
                    ['name'=>'Associations (Mrktg Channel)','parent_id'=>4],
                    ['name'=>'Bookstores (B&N, Amazon)','parent_id'=>4]
                ];

        $segmentsData = [
            ['name'=>'State Agencies'],
            ['name'=>'Academic'],
            ['name'=>'Local Agencies'],
            ['name'=>'Re-Sellers'],
            ['name'=>'Law Firms'],
            ['name'=>'Federal'],
            ['name'=>'Corporate'],
            ['name'=>'Consumer']
        ];

        foreach ($segmentsData as $item) 
        {
            $setup->getConnection()->insert('endeavor_customer_segments', $item);
        }
        
        foreach ($secandarySegmentsData as $item) 
        {
            $setup->getConnection()->insert('endeavor_customer_secondary_segments', $item);
        }
        
      }
        $setup->endSetup();
    }
}
