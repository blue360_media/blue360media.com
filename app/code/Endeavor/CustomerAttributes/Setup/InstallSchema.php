<?php

namespace Endeavor\CustomerAttributes\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        
        if (!$installer->tableExists('endeavor_customer_segments')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('endeavor_customer_segments'))
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
                    'Entity Id'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    255, 
                    ['nullable' => true],
                    'segment names'
                )
                ->setComment('Endeavor Customer Segments');
             $installer->getConnection()->createTable($table);
        }
        if (!$installer->tableExists('endeavor_customer_secondary_segments')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('endeavor_customer_secondary_segments'))
                ->addColumn(
                    'segment_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
                    'Segment Id'
                )
                ->addColumn(
                    'parent_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['nullable' => false, 'unsigned' => true],
                    'Parent Id'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT, 
                    255, 
                    ['nullable' => false], 
                    'Secondary Customer Segment Name'
                )
                ->addIndex(
                    $installer->getIdxName('endeavor_customer_secondary_segments', ['parent_id']),
                    ['parent_id']
                )
                ->setComment('Endeavor Customer Secondary Segments');

            $installer->getConnection()->createTable($table);
            
        }
        
        $installer->endSetup();
        
    }
}
