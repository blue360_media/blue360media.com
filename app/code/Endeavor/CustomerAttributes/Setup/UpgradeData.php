<?php
namespace Endeavor\CustomerAttributes\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
 
 
class UpgradeData implements UpgradeDataInterface {
  
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;
    
    /**
     * 
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
    }
    
    /**
     * 
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade( ModuleDataSetupInterface $setup, ModuleContextInterface $context ) {

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addCustomAttributes($setup);
            
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addMultiShippingAttribute($setup);
            
        }
        
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->changeMultiShippingAttribute($setup);
            
        }
        
    }
    
    /**
     * 
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function addCustomAttributes(ModuleDataSetupInterface $setup){
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();
        
        $marketSegment = $customerSetup->getEavConfig()
                ->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_market_segment');
        $marketSegment->setData(
            'used_in_forms',
            ['adminhtml_customer', 'customer_account_edit', 'customer_account_create']
        );
        $marketSegment->save();
        
        $secondaryMarket = $customerSetup->getEavConfig()
                ->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'customer_secondary_market');
        $secondaryMarket->setData(
            'used_in_forms',
            ['adminhtml_customer', 'customer_account_edit', 'customer_account_create']
        );
        $secondaryMarket->save();

    }
    
    /**
     * 
     * @update_By: Mohammad Hamadneh <mhamadneh@endeavorpal.com>
     * @date: 2018/03/13
     * @desc: Delete change attribute when customer edit his information.
     * 
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function changeMultiShippingAttribute(ModuleDataSetupInterface $setup) {
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();
        
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'is_special_multi_shipping_flow')
            ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'customer_account_create'
            ]]);
        
        $attribute->save();
        
    }
    
    /**
     * 
     * @author: Murad Dweikat
     * @date: 2018/02/19
     * @desc: Add Customer attribute called "is_special_multi_shipping_flow", to determine the customer is special
     *        multi shipping flow ( show price in confirmation email or not ).
     * 
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     */
    public function addMultiShippingAttribute(ModuleDataSetupInterface $setup){
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

        $setup->startSetup();

        $customerSetup->addAttribute(\Magento\Customer\Model\Customer::ENTITY, 'is_special_multi_shipping_flow', [
            'type' => 'int',
            'label' => 'Is Special Multi Shipping Flow',
            'input' => 'boolean',
            'source' => '',
            'required' => false,
            'visible' => true,
            'position' => 100,
            'system' => false,
            'backend' => '',
            'is_filterable_in_grid' => false,
            'is_searchable_in_grid' => false,
            'is_visible_in_grid' => false,
            'is_used_in_grid' => false
        ]);
        
        $attribute = $customerSetup->getEavConfig()->getAttribute('customer', 'is_special_multi_shipping_flow')
            ->addData(['used_in_forms' => [
                'adminhtml_customer',
                'adminhtml_checkout',
                'customer_account_create',
                'customer_account_edit'
            ]]);
        $attribute->save();
    }
    
}