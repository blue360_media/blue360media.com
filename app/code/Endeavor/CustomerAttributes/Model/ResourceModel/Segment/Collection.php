<?php
namespace Endeavor\CustomerAttributes\Model\ResourceModel\Segment;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\CustomerAttributes\Model\Segment', 'Endeavor\CustomerAttributes\Model\ResourceModel\Segment');
    }
}
