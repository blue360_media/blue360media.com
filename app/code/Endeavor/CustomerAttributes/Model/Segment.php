<?php
namespace Endeavor\CustomerAttributes\Model;

use Magento\Framework\DataObject\IdentityInterface;

class Segment extends \Magento\Framework\Model\AbstractModel implements IdentityInterface
{
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'endeavor_segment';

    /**
     * @var string
     */
    protected $_cacheTag = 'endeavor_segment';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'endeavor_segment';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\CustomerAttributes\Model\ResourceModel\Segment');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}
