<?php
namespace Endeavor\CustomerAttributes\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
 
/**
 * Custom Attribute Renderer
 *
 */
class Segments extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var OptionFactory
     */
    protected $optionFactory;
 
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collect = $objectManager->create('Endeavor\CustomerAttributes\Model\Segment')->getCollection();
        $initial = array('label'=>'Please Select','value'=>0);
        $sales=[];
        array_push($sales,$initial);        
        foreach($collect as $value) {
            $item = array(
                'label' => $value->getName(), 
                'value' =>  $value->getEntityId()
            );
            array_push($sales,$item);
        }
        return $sales;
    }
}