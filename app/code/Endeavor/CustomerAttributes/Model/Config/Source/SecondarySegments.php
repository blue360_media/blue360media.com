<?php
namespace Endeavor\CustomerAttributes\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
 
/**
 * Custom Attribute Renderer
 *
 */
class SecondarySegments extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var OptionFactory
     */
    protected $optionFactory;
 
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
       
        $initial = array('label'=>'Please Select','value'=>0);
        $secondarySegment=[];
        array_push($secondarySegment,$initial);        
        return $secondarySegment;
        
    }
}