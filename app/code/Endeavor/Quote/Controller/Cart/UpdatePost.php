<?php
/**
 *
 * Copyright © Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 04-23-2018.
 * @desc add update form for bundle selection in cart.
 */
namespace Endeavor\Quote\Controller\Cart;

class UpdatePost extends \Magento\Checkout\Controller\Cart\UpdatePost
{
    /**
     * Empty customer's shopping cart
     *
     * @return void
     */
    protected function _emptyShoppingCart()
    {
        try {
            $this->cart->truncate()->save();
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $this->messageManager->addError($exception->getMessage());
        } catch (\Exception $exception) {
            $this->messageManager->addException($exception, __('We can\'t update the shopping cart.'));
        }
    }
    protected function addProductToCart($bundleSku, $itemSku, $qty, $formKey) {
        $bundleOptions = $this->_objectManager->create('\Magento\Bundle\Model\Option');
        
        $bundleItem = $this->_objectManager->create('\Magento\Catalog\Model\Product')->loadByAttribute('sku', $itemSku);
        $bundle = $this->_objectManager->create('\Magento\Catalog\Model\Product')->loadByAttribute('sku', $bundleSku);
        
        $bundleItemId = $bundleItem->getId();
        $bundleProductId = $bundle->getId();
        
        $bundleoption = $bundleOptions->getCollection()->addFieldToFilter('parent_id', $bundleProductId)->getFirstItem();
        $bundleOptionId = $bundleoption->getOptionId();

        $selectionCollection = $bundle->getTypeInstance(true)
                ->getSelectionsCollection($bundle->getTypeInstance(true)->getOptionsIds($bundle), $bundle);
        
        foreach ($selectionCollection as $selectionCollectionItem) {
            if ($selectionCollectionItem->getProductId() == $bundleItemId) {
                $bundleSelectionId = $selectionCollectionItem->getSelectionId();
                
            }
            
        }
        $bundleOption = array($bundleOptionId => $bundleSelectionId);
        $bundleOptionQty = array($bundleOptionId => "1");
                                
        $params = [
            'uenc' => null,
            'product' => $bundleProductId,
            'selected_configurable_option' => "",
            'related_product' => "",
            'form_key' => $formKey,
            'bundle_option' => $bundleOption,
            'optionId' => $bundleSelectionId,
            'bundle_option_qty' => $bundleOptionQty,
            'qty' => $qty
        ];
           
        $cart = $this->_objectManager->create('\Magento\Checkout\Model\Cart');
        $cart->addProduct($bundle, $params);
        
        $this->_eventManager->dispatch(
            'checkout_cart_add_product_complete',
            ['product' => $bundle, 'request' => $this->getRequest(), 'response' => $this->getResponse()]
        );
        
        return true;
        
    }
    /**
     * Update customer's shopping cart
     *
     * @return void
     */
    protected function _updateShoppingCart()
    {
        try {
            $newItemsParam = $this->getRequest()->getParam('selection');
            $cartItemsBundle = $this->getRequest()->getParam('cart');
            $formKey = $this->getRequest()->getParam('form_key');
            $subscriptionParam = $this->getRequest()->getParam('subscriptions-item');
            $customerSubscriptions = $this->_objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');
            $subscriptions = [];
            if (!empty($subscriptionParam)) {
                foreach ($subscriptionParam as $subscriptionParamItem) {
                    $oldSubscriptionItem = $customerSubscriptions->load($subscriptionParamItem['subscriptionsId']);
                    $subscriptionId = $oldSubscriptionItem->getSubscriptionId();
                    $subscriptionType = $oldSubscriptionItem->getSubscriptionType();
                    $itemId = $oldSubscriptionItem->getProductId();
                    $oldSubscriptionBundle = $customerSubscriptions->load(($subscriptionParamItem['subscriptionsId']-1));
                    if (empty($subscriptions)) {
                    $subscriptions[] = ['sku' => $oldSubscriptionBundle->getSku(), 'productId' => $oldSubscriptionBundle->getProductId(), 'itemId' => $itemId, 'subscriptionId' => $subscriptionId, 'subscriptionType' => $subscriptionType, 'status' => 0];

                    } else {
                        foreach ($subscriptions as $subscriptionsItem) {
                            if (!in_array($itemId, $subscriptionsItem)) {
                                $subscriptions[] = ['sku' => $oldSubscriptionBundle->getSku(), 'productId' => $oldSubscriptionBundle->getProductId(), 'itemId' => $itemId, 'subscriptionId' => $subscriptionId, 'subscriptionType' => $subscriptionType, 'status' => 0];

                            }
                        }
                    }
                }
            }
            $this->cart->getQuote()->removeAllItems()->save();
            foreach ($cartItemsBundle as $itemKey => $cartItemsBundleItem) {
                $newItems = $newItemsParam[$itemKey];
                $newItemData = str_replace("selection", "", $newItems);
                $newItem = explode(",", $newItemData);
                $bundle = $this->_objectManager->create('\Magento\Catalog\Model\Product')->load($newItem[0]);
                $bundleItem = $this->_objectManager->create('\Magento\Catalog\Model\Product')->load($newItem[1]);
                $this->addProductToCart($bundle->getSku(), $bundleItem->getSku(), $cartItemsBundleItem['qty'], $formKey);
                
            }
            $this->cart->getQuote()->save();
            $newQuoteItems = $this->cart->getQuote()->getAllItems();
            $newQuoteItemAsArray = [];
            foreach ($newQuoteItems as $newQuoteItem) {
                $newQuoteItemAsArray [] = ['quoteItem' => $newQuoteItem, 'subscription' => 0];
                
            }
            if (!empty($subscriptions)) {
                foreach ($newQuoteItemAsArray as $quoteKey => $newQuoteItem) {
                    $quoteItemId = $newQuoteItem['quoteItem']->getItemId();
                    $quoteBundleItem = $this->_objectManager->get('Magento\Quote\Model\Quote\Item')->load($quoteItemId + 1);
                    $quoteBundleItemId = $quoteBundleItem->getProductId();
                    $quoteBundleId = $quoteBundleItem->getItemId();
                    foreach ($subscriptions as $key => $subscriptionParamItem) {
                        // Bundle Item doesn't change.
                        if (($newQuoteItem['quoteItem']->getProductId() == $subscriptionParamItem['productId']) && ($quoteBundleItemId == $subscriptionParamItem['itemId']) && $subscriptionParamItem['status'] != 1 && $newQuoteItem['subscription'] != 1) {
                            $this->addNewSubscription($newQuoteItem['quoteItem']->getProductId(), $quoteItemId, null, null, $newQuoteItem['quoteItem']->getQty());
                            $this->addNewSubscription($quoteBundleItem->getProductId(), $quoteBundleId, $subscriptionParamItem['subscriptionId'], $subscriptionParamItem['subscriptionType'], 1);
                            $subscriptions[$key]['status'] = 1;
                            $newQuoteItemAsArray[$quoteKey]['subscription'] = 1;
                        }
                    }
                }
                if (!empty($subscriptions)) {
                    foreach ($newQuoteItemAsArray as $newQuoteItem) {
                        $quoteItemId = $newQuoteItem['quoteItem']->getItemId();
                        $quoteBundleItem = $this->_objectManager->get('Magento\Quote\Model\Quote\Item')->load($quoteItemId + 1);
                        $quoteBundleItemId = $quoteBundleItem->getProductId();
                        $quoteBundleId = $quoteBundleItem->getItemId();
                        foreach ($subscriptions as $key => $subscriptionParamItem) {
                            // Bundle Item is changed.
                            if (($newQuoteItem['quoteItem']->getProductId() == $subscriptionParamItem['productId']) && $subscriptionParamItem['status'] != 1 && $newQuoteItem['subscription'] != 1) {
                                $this->addNewSubscription($newQuoteItem['quoteItem']->getProductId(), $quoteItemId, null, null, $newQuoteItem['quoteItem']->getQty());
                                $this->addNewSubscription($quoteBundleItem->getProductId(), $quoteBundleId, $subscriptionParamItem['subscriptionId'], $subscriptionParamItem['subscriptionType'], 1);
                                $subscriptions[$key]['status'] = 1;
                                $newQuoteItemAsArray[$quoteKey]['subscription'] = 1;
                            }
                        }
                    }
                }
            }
            $cartData = [];
            foreach ($newQuoteItems as $newQuoteItem) {
                if (!$newQuoteItem->getParentItemId()) {
                    $cartData[$newQuoteItem->getItemId()] = ['qty' => $newQuoteItem->getQty()];
                }
            }
            if (is_array($cartData)) {
                $filter = new \Zend_Filter_LocalizedToNormalized(
                        ['locale' => $this->_objectManager->get(
                            \Magento\Framework\Locale\ResolverInterface::class
                    )->getLocale()]
                );
                foreach ($cartData as $index => $data) {
                    if (isset($data['qty'])) {
                        $cartData[$index]['qty'] = $filter->filter(trim($data['qty']));
                    }
                }
                if (!$this->cart->getCustomerSession()->getCustomerId() && $this->cart->getQuote()->getCustomerId()) {
                    $this->cart->getQuote()->setCustomerId(null);
                }

                $cartData = $this->cart->suggestItemsQty($cartData);
                $this->cart->updateItems($cartData)->save();
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError(
                $this->_objectManager->get(\Magento\Framework\Escaper::class)->escapeHtml($e->getMessage())
            );
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t update the shopping cart.'));
            $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
        }
    }

    protected function addNewSubscription($productId, $quoteItemId, $subscriptionId, $subscriptionType, $qty) {
        $customerSession = $this->_objectManager->get('Magento\Customer\Model\Session');
        $customer = $this->_objectManager->create('Magento\Customer\Model\Customer')->load($customerSession->getCustomer()->getId());
        $product = $this->_objectManager->create('Magento\Catalog\Model\Product')->load($productId);
        $current_date = $this->_objectManager->create('\Magento\Framework\Stdlib\DateTime\DateTime')->gmtDate('Y-m-d');
        $model = $this->_objectManager->create('Endeavor\Subscriptions\Model\CustomerSubscriptions');
        $model->setData('customer_id',$customerSession->getCustomer()->getId());
        $model->setData('account_id',$customer->getCustomerLnAccountId());
        $model->setData('quote_item_id', $quoteItemId);
        $model->setData('product_id',$productId);
        $model->setData('publication_number',$product->getData('publication_number'));
        $model->setData('isbn',$product->getData('isbn'));
        $model->setData('sku',$product->getSku());
        $model->setData('product_name',$product->getName());
        $model->setData('media_type_id',$product->getData('media_type'));
        $model->setData('media_type',$product->getResource()->getAttribute('media_type')->getFrontend()->getValue($product));
        $model->setData('subscription_start',$current_date);
        $model->setData('subscription_type',$subscriptionType);
        $model->setData('subscription_id', $subscriptionId);
        $model->setData('order_status','Incomplete');
        $model->setData('qty', $qty);
        $model->save();
    }
    /**
     * Update shopping cart data action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if (!$this->_formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $updateAction = (string)$this->getRequest()->getParam('update_cart_action');

        switch ($updateAction) {
            case 'empty_cart':
                $this->_emptyShoppingCart();
                break;
            case 'update_qty':
                $this->_updateShoppingCart();
                break;
            default:
                $this->_updateShoppingCart();
        }

        return $this->_goBack();
    }
}
