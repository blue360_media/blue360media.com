<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Downloadable\Controller\Customer;

class Assign extends \Magento\Framework\App\Action\Action
{
    
    /**
     * Display downloadable links bought by customer
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout();
        if ($block = $this->_view->getLayout()->getBlock('downloadable_customer_products_list')) {
            $block->setRefererUrl($this->_redirect->getRefererUrl());
        }
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Assign Users'));
        $this->_view->renderLayout();
    }
    
}
