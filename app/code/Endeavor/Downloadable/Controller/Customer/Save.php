<?php
namespace Endeavor\Downloadable\Controller\Customer;

use Magento\Framework\Controller\ResultFactory;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Framework\Math\Random;
use Magento\Framework\Stdlib\DateTime;


class Save extends \Magento\Framework\App\Action\Action
{
     /**
     * Downloadable Purchased Related Item object
     *
     * @var \Endeavor\Downloadable\Model\RelatedItemsFactory 
     */
    protected $_relatedItemsFactory;

    /**
     * Downloadable Purchased Item object
     *
     * @var \Magento\Downloadable\Model\Link\Purchased\ItemFactory 
     */
    protected $_purchasedItemFactory;

     /**
     * Downloadable Link object
     *
     * @var \Magento\Downloadable\Model\Link 
     */
    protected $_linkFactory;

    /**
     * Downloadable Purchased Link object
     *
     * @var \Magento\Downloadable\Model\Link\PurchasedFactory 
     */
    protected $_purchasedLinkFactory;

     /**
     * Customer Object
     *
     * @var \Magento\Customer\Model\CustomerFactory 
     */
    protected $_customerFactory;

         /**
     * Store website manager 
     *
     * @var \Magento\Store\Model\StoreManagerInterface 
     */
    protected $_storeManager;

    /**
     * @var AccountManagementInterface
     */
    protected $_customerAccountManagement;

    /**
     * @var Random
     */
    private $mathRandom;

    /**
     * @var Endeavor\Downloadable\Model\Import
     */
    private $import;

    /**
     * 
     * @param \Endeavor\Downloadable\Model\Import $import
     * @param \Magento\Downloadable\Model\Link\Purchased\ItemFactory $purchasedItemFactory
     * @param \Magento\Downloadable\Model\LinkFactory $linkFactory
     * @param \Magento\Downloadable\Model\Link\PurchasedFactory $purchasedLinkFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param AccountManagementInterface $customerAccountManagement
     * @param \Endeavor\Downloadable\Model\RelatedItemsFactory $relatedItems
     * @param Random $mathRandom
     * @param \Magento\Framework\App\Action\Context $context
     */
    public function __construct(
        \Endeavor\Downloadable\Model\Import $import,
        \Magento\Downloadable\Model\Link\Purchased\ItemFactory $purchasedItemFactory,
        \Magento\Downloadable\Model\LinkFactory $linkFactory,
        \Magento\Downloadable\Model\Link\PurchasedFactory $purchasedLinkFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,   
        AccountManagementInterface $customerAccountManagement,
        \Endeavor\Downloadable\Model\RelatedItemsFactory $relatedItems,
        Random $mathRandom,
        \Magento\Framework\App\Action\Context $context
    ){
        $this->import = $import;
        $this->_purchasedItemFactory = $purchasedItemFactory;
        $this->_linkFactory = $linkFactory;
        $this->_purchasedLinkFactory = $purchasedLinkFactory;
        $this->_customerFactory = $customerFactory;
        $this->_storeManager = $storeManager;
        $this->_customerAccountManagement = $customerAccountManagement;
        $this->_relatedItemsFactory = $relatedItems;
        $this->mathRandom = $mathRandom;
        
        parent::__construct($context);
        
    }
    
    /*
     * @updated_By: Mohammad Hamadneh <mhamadneh@endeavorpal.com>
     * @date: 10-01-2018.
     * @desc: in order to validate email and data before eBook access assignment to other customer, and minus qty when it full success.
     */
    /**
     * Contact action
     *
     * @return void
     */
    public function execute()
    {
        if(isset($_FILES['import_file'])){
            $dataRows = $this->import->importFromCsvFile($_FILES['import_file']);
        }
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $post = $this->getRequest()->getPost();
        if ($post) {
            $itemId = $post['itemId']; 
            $purchasedItem = $this->_purchasedItemFactory->create()->load($itemId);
            $downloadableLink = $this->_linkFactory->create()->load($purchasedItem->getLinkId());
            $purchasedLink = $this->_purchasedLinkFactory->create()->load($purchasedItem->getPurchasedId());
            $relatedItemsCollection = $this->_relatedItemsFactory->create()->getCollection();
            $customer = $this->_customerFactory->create();
            $currentCustomer = $this->_customerFactory->create();
          
            $productId = $purchasedItem->getProductId();
            $orderItem = $objectManager->create('Magento\Sales\Model\Order\Item')->load($purchasedItem->getOrderItemId());
            
            $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderItem->getOrderId());
            
            $currentCustomerId = $purchasedLink->getCustomerId();
            $orderCustomerId = $order->getCustomerId();
            $currentCustomer->load($currentCustomerId);
            
            $totalAssignUser = 0;
            foreach ($order->getAllItems() as $item) {
                if (($item->getProductId() == $productId) /*&& ($currentCustomerId == $orderCustomerId)*/ ) {
                    if ($downloadableLink->getNumberOfDownloads() != 0) {
                        $totalAssignUser =  intval($this->getRemainingDownloads($purchasedItem) / $downloadableLink->getNumberOfDownloads());
                        
                    } else {
                        $this->messageManager->addWarningMessage('You don\'t have enough quantity.');
                        // Redirect to your form page (or anywhere you want...)
                        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                        $resultRedirect->setPath('downloadable/customer/products');
                        return $resultRedirect;
                        
                    }
                    
                }
                
            }
            
            // Number of Assign User enter
            $assignUserFilled = 0;
            $totalQuantity = 0;
            for ($i = 1; $i <= $totalAssignUser; $i++) {
                if (!empty($post['email' . $i])) {
                    $assignUserFilled++;
                    if (!empty($post['quantity-' . $i])) {
                        $totalQuantity += $post['quantity-'. $i];
                        
                    } else {
                        $post['quantity-'. $i] = 1;
                        $totalQuantity += $post['quantity-'. $i];
                        
                    }
                    
                }
                
            }
            
            // Index of Assin user in form.
            $assignUserFilledIndexes = [];
            for ($i = 1; $i <= $totalAssignUser; $i++) {
                if (!empty($post['email' . $i])) {
                    $assignUserFilledIndexes[] = $i;
                    
                }
                
            }

            if ($totalQuantity > $this->getRemainingDownloads($purchasedItem)) {
                $this->messageManager->addWarningMessage('You don\'t have enough quantity.');
                // Redirect to your form page (or anywhere you want...)
                $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultRedirect->setPath('downloadable/customer/products');
                return $resultRedirect;
                
            }
            
            $total = 0;

            try {
                for ($i = 1; $i<=$totalAssignUser; $i++) {
                    if (in_array($i,$assignUserFilledIndexes)) {
                        $newPurchasedLink = $this->_purchasedLinkFactory->create();
                        $newPurchasedItem = $this->_purchasedItemFactory->create();
                        $relatedItems = $this->_relatedItemsFactory->create();
                        $relatedItemsCollection = $this->_relatedItemsFactory->create()->getCollection();
                        $tempPurchasedLink = $this->_purchasedLinkFactory->create()->getCollection();
                        $tempPurchasedItem = $this->_purchasedItemFactory->create();

                        $email = trim($post['email'.$i]);
                        $firstname = $post['firstname-'.$i];
                        $lastname = $post['lastname-'.$i];
                        $qty = $post['quantity-'. $i];
                        
                        $websiteId = $this->_storeManager->getWebsite()->getWebsiteId();
                        $customer->setWebsiteId($websiteId);
                        $customer->loadByEmail($email);

                        $customerId = $customer->getEntityId();

                        $dataPurchasedLink = $purchasedLink->getData();
                        unset($dataPurchasedLink['purchased_id']);
                        unset($dataPurchasedLink['created_at']);
                        unset($dataPurchasedLink['updated_at']);
                        $newPurchasedLink->setData($dataPurchasedLink);

                        if (!empty($customerId)) {
                            
                            $tempPurchasedLink->addFieldToFilter('order_id',['eq' => $purchasedLink->getOrderId()])
                                ->addFieldToFilter('order_item_id',['eq' => $purchasedLink->getOrderItemId()])
                                ->addFieldToFilter('customer_id',['eq' => $customerId]);
                            
                            if ($customer->getPasswordHash() === null) {
                                $newPasswordToken = $this->mathRandom->getUniqueHash();
                                $customer->setRpToken($newPasswordToken);
                                $customer->setRpTokenCreatedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT));
                                $customer->save();
                            }
                            
                            if (!count($tempPurchasedLink->getItems())) {
                                $newPurchasedLink->setCustomerId($customerId);
                                $newPurchasedLink->save();   

                            }

                        } else {
                            $customer->setEmail($email);
                            $customer->setFirstname($firstname);
                            $customer->setLastname($lastname);
                            $customer->setAddresses(null);
                            $storeId = $this->_storeManager->getWebsite($websiteId)->getDefaultStore()->getId();
                            $customer->setStoreId($storeId);

                            $storeName = $this->_storeManager->getStore($customer->getStoreId())->getName();
                            $customer->setCreatedIn($storeName);

                            $newPasswordToken = $this->mathRandom->getUniqueHash();
                            $customer->setRpToken($newPasswordToken);
                            $customer->setRpTokenCreatedAt((new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT));

                            $customer->save();

                            $lastCustomerId = $customer->getCollection()->getLastItem()->getEntityId();
                            $newPurchasedLink->setCustomerId($lastCustomerId);
                            $newPurchasedLink->save();
                            
                            $newCustomerId = $customer->getEntityId();
                            $moveOrderToHelper = $objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
                            $moveOrderToHelper->sendWelcomeMessage($newCustomerId);
                            
                            $tempPurchasedLink->addFieldToFilter('order_id',['eq' => $purchasedLink->getOrderId()])
                                ->addFieldToFilter('order_item_id',['eq' => $purchasedLink->getOrderItemId()])
                                ->addFieldToFilter('customer_id',['eq' => $customer->getEmail()]);

                            // Display the succes form validation message
                            $this->messageManager->addWarningMessage('This is email: '.$email.' is not registered in our website. So we sent a message with details of the account.');
                            
                        }

                        if (!count($tempPurchasedLink->getItems())) {

                            $dataPurchasedItem = $purchasedItem->getData();
                            unset($dataPurchasedItem['item_id']);
                            unset($dataPurchasedItem['created_at']);
                            unset($dataPurchasedItem['updated_at']);
                            $newPurchasedItem->setData($dataPurchasedItem);
                            $newPurchasedItem->setStatus('available');
                            $newPurchasedItem->setOrderItemId($purchasedLink->getOrderItemId());
                            $newPurchasedItem->setNumberOfDownloadsBought($downloadableLink->getNumberOfDownloads() * $qty);
                            $newPurchasedItem->setNumberOfDownloadsUsed(0);
                            $purchasedLinkCollection = $this->_purchasedLinkFactory->create()->getCollection(); 
                            $newPurchasedItem->setPurchasedId($purchasedLinkCollection->getLastItem()->getId());
                            $linkHash = strtr(
                                base64_encode(
                                    microtime() . $tempPurchasedLink->getLastItem()->getId() 
                                        . $orderItem->getId() . $dataPurchasedItem['product_id']
                                ),
                                '+/=',
                                '-_,'
                            );

                            $newPurchasedItem->setLinkHash($linkHash);
                            $newPurchasedItem = $newPurchasedItem->save();
                            
                        } else {
                            $tempPurchasedItem->load($tempPurchasedLink->getLastItem()->getId(),'purchased_id');
                            $tempPurchasedItem->setNumberOfDownloadsBought($tempPurchasedItem->getNumberOfDownloadsBought() + $downloadableLink->getNumberOfDownloads() * $qty);
                            $newPurchasedItem = $tempPurchasedItem->save();
                            
                        }

                        $relatedItemsCollection->addFieldToFilter('item_id',['eq' => $purchasedItem->getId()])
                            ->addFieldToFilter('related_item_id',['eq' => $newPurchasedItem->getId()]);

                        if (!count($relatedItemsCollection->getItems())) {
                            $relatedItems->setItemId($purchasedItem->getId());
                            $orderFirstname = $order->getCustomerFirstname();
                            $orderLastname = $order->getCustomerLastname();
                            $orderCustomerName = $orderFirstname." ".$orderLastname;
                            $relatedItems->setCreatedBy($orderCustomerName);
                            $relatedItems->setRelatedItemId($newPurchasedItem->getId());
                            /* 
                             * Murad Dweikat
                             * @desc: in order to assign order quantity, instead of qty of downloadable.
                             * $relatedItems->setQty($downloadableLink->getNumberOfDownloads() * $qty);
                            */
                            $relatedItems->setQty($qty);
                            $relatedItems->save();
                            
                        } else {
                            $relatedItems->load($relatedItemsCollection->getLastItem()->getId());
                            $relatedItems->setQty($relatedItems->getQty() + $qty);
                            $relatedItems->save();
                            
                        }

                        /* Receiver Detail  */
                        $receiverInfo = ['name' => $customer->getFirstname().' '.$customer->getLastname(),'email' => $email];

                        $emailData = [];
                        $emailData['sender_name'] = $currentCustomer->getFirstname().' '.$currentCustomer->getLastname();
                        $emailData['customer'] = $customer;
                        $emailData['product_name'] = $purchasedLink->getProductName();
                        $emailData['product_qty'] = (int)$qty;

                        $this->sendAssingUserMailMessage($emailData,$receiverInfo);
                        $total += $qty;
                        
                    }
                    
                }
                
            } catch (\Exception $e) {
                $this->messageManager->addWarningMessage('error have been occures: '.$e->getMessage());
                
            } finally {
                $purchasedItem->setNumberOfDownloadsUsed($purchasedItem->getNumberOfDownloadsUsed() + $downloadableLink->getNumberOfDownloads() * $total);
                if (!$this->getRemainingDownloads($purchasedItem)) {
                    $purchasedItem->setStatus('expired');
                    
                }
                $purchasedItem->save();
                
            }
            
            // Display the succes form validation message
            $this->messageManager->addSuccess('Assign user completely successfully.');

            // Redirect to your form page (or anywhere you want...)
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setPath('downloadable/customer/products');

            return $resultRedirect;
            
        }
        // Render the page 
        $this->_view->loadLayout();
        $this->_view->renderLayout();
     
    }
    
    /**
     * Return number of remaining downloads
     *
     * @param Item $item
     * @return \Magento\Framework\Phrase|int
     */
    public function getRemainingDownloads($item)
    {
        if ($item->getNumberOfDownloadsBought()) {
            $downloads = $item->getNumberOfDownloadsBought() - $item->getNumberOfDownloadsUsed();
            return $downloads;
            
        }
        return 0;
        
    }
    
    /**
     * Send email Message to notify assigned user by downloadable product 
     *
     * @param array $senderInfo
     * @param array $receiverInfo
     * @param array $emailData
     * @return void
     */
    public function sendAssingUserMailMessage($emailData,$receiverInfo)
    {
        /* call send mail method from helper or where you define it*/ 
        $this->_objectManager->get('Endeavor\Downloadable\Helper\Email')->assignedUserSendMailMessage(
              $emailData,
              $receiverInfo
        );
        
    }
       
    
}
