<?php 
namespace Endeavor\Downloadable\Controller\Adminhtml\Order;

use Magento\Framework\Controller\ResultFactory;

class ImportExport extends \Endeavor\Downloadable\Controller\Adminhtml\Order
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Endeavor_Downloadable::import_export';

    /**
     * Import and export Page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->setActiveMenu('Endeavor_Downloadable::system_order');
//        $resultPage->addContent(
//            $resultPage->getLayout()->createBlock('Magento\TaxImportExport\Block\Adminhtml\Rate\ImportExportHeader')
//        );
        $resultPage->addContent(
            $resultPage->getLayout()->createBlock('Endeavor\Downloadable\Block\Adminhtml\Order\ImportExport')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Sales Order'));
        $resultPage->getConfig()->getTitle()->prepend(__('Export Sales Order'));
        return $resultPage;
    }
}