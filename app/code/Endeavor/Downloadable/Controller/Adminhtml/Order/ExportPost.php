<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Downloadable\Controller\Adminhtml\Order;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;

class ExportPost extends \Endeavor\Downloadable\Controller\Adminhtml\Order
{
    /**
     * Export action from import/export tax
     *
     * @return ResponseInterface
     */
    public function execute()
    {
        
        //Get Object Manager Instance
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $post = $this->getRequest()->getPost();
        $export_from = $post['export_from'];
        $export_to = $post['export_to'];
        
        $fromDate = date('Y-m-d H:i:s', strtotime($export_from));
        $toDate = date('Y-m-d H:i:s', strtotime($export_to.' + 1 day -1 sec'));


        $content = $objectManager->get('Endeavor\Downloadable\Helper\Orders')
                ->getFilteredOrders($fromDate,$toDate);

        $csv_file = 'SalesOrder_'.date('Y-m-d_His');
        return $this->fileFactory->create($csv_file.'.csv', $content, DirectoryList::VAR_DIR);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Endeavor_Downloadable::import_export')
            || $this->_authorization->isAllowed('Magento_Sales::sales');

    }
    
}
