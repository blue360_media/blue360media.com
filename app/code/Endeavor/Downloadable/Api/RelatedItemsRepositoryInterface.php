<?php
namespace Endeavor\Downloadable\Api;

interface RelatedItemsRepositoryInterface
{
    /**
     * Save RelatedItems
     * @param \Endeavor\Downloadable\Api\Data\RelatedItemsInterface $relatedItems
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Endeavor\Downloadable\Api\Data\RelatedItemsInterface $relatedItems
    );

    /**
     * Retrieve RelatedItems
     * @param string $entityId
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($entityId);

    /**
     * Retrieve RelatedItems matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete RelatedItems
     * @param \Endeavor\Downloadable\Api\Data\RelatedItemsInterface $relatedItems
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Endeavor\Downloadable\Api\Data\RelatedItemsInterface $relatedItems
    );

    /**
     * Delete RelatedItems by ID
     * @param string $entityId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($entityId);
}
