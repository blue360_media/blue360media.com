<?php
namespace Endeavor\Downloadable\Api\Data;

interface RelatedItemsInterface
{
    /**
     * Columns name in db table
     * 
     * @var string
     */
    const ENTITY_ID = 'entity_id';
    const ITEM_ID = 'item_id';
    const RELATED_ITEM_ID = 'related_item_id';
    const QTY = 'qty';
    const CREATED_BY = 'created_by';


    /**
     * Get entity_id
     * @return int|null
     */
    public function getEntityId();

    /**
     * Set entity_id
     * @param int $entityId
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setEntityId($entityId);

    /**
     * Get item_id
     * @return int|null
     */
    public function getItemId();

    /**
     * Set item_id
     * @param int $item_id
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setItemId($item_id);

    /**
     * Get related_item_id
     * @return int|null
     */
    public function getRelatedItemId();

    /**
     * Set related_item_id
     * @param int $related_item_id
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setRelatedItemId($related_item_id);

    /**
     * Get qty
     * @return int|null
     */
    public function getQty();

    /**
     * Set qty
     * @param int $qty
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setQty($qty);
    
    /**
     * Get created_by
     * @return text|null
     */
    public function getCreatedBy();

    /**
     * Set created_by
     * @param text $createdBy
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setCreatedBy($createdBy);

    
}
