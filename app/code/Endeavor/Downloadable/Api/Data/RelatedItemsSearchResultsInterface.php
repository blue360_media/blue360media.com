<?php
namespace Endeavor\Downloadable\Api\Data;

interface RelatedItemsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get RelatedItems list.
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface[]
     */
    public function getItems();

    /**
     * Set item_id list.
     * @param \Endeavor\Downloadable\Api\Data\RelatedItemsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
