<?php
namespace Endeavor\Downloadable\Block\Customer\Products;

class ListProducts extends \Magento\Downloadable\Block\Customer\Products\ListProducts
{
    /**
     * Return form action url of assign user
     *
     * @param Item $item
     * @return string
     */
    public function getAssignUserUrl($item) {
        return $this->getUrl('downloadable/customer/assign', ['id' => $item->getId()]);
        
    }
    
    /**
     * Return form action url of save assign user
     *
     * @param Item $item
     * @return string
     */
    public function getFormAction() {
        return $this->getUrl('downloadable/customer/save');
        
    }

    public function getReaderUrl($hash) {
        return $this->getUrl('downloadable/reader/index', ['hash' => $hash]);

    }

    public function getHash($item) {
        return $item->getLinkHash();
        
    }

    /**
     * Return quantity of Assign User
     *
     * @param int $purchasedItemId
     * @return int
     */
    public function getTotalAssignUser($purchasedItemId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $purchasedItem = $objectManager->create(
            'Magento\Downloadable\Model\Link\Purchased\Item')->load($purchasedItemId,'item_id');
        
        $orderItem = $objectManager->create('Magento\Sales\Model\Order\Item')->load($purchasedItem->getOrderItemId());
        $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderItem->getOrderID());
        $orderCustomerId = $order->getCustomerId();
        $currentCustomerId = $customerSession->getCustomer()->getId();
        $productId = $purchasedItem->getProductId();

        $downloadableLink = $objectManager->create(
            'Magento\Downloadable\Model\Link')->load($purchasedItem->getLinkId());
                
        $qty = 0;
        foreach($order->getAllItems() as $item){    
            if (($item->getProductId() == $productId) /*&&($currentCustomerId == $orderCustomerId)*/) {
                if ($downloadableLink->getNumberOfDownloads() != 0) {
                    $qty =  intval($purchasedItem->getNumberOfDownloadsBought() 
                            / $downloadableLink->getNumberOfDownloads());
                } else {
                    $qty =  intval($purchasedItem->getNumberOfDownloadsBought());
                }
                
            }
            
        }
        
        return $qty;
        
    }
    
    /**
     * Return quantity of Remaining Assign User
     *
     * @param int $purchasedItemId
     * @return int
     */
    public function getRemainingQtyById($purchasedItemId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $purchasedItem = $objectManager->create('Magento\Downloadable\Model\Link\Purchased\Item')->load($purchasedItemId,'item_id');
        
        $orderItem = $objectManager->create('Magento\Sales\Model\Order\Item')->load($purchasedItem->getOrderItemId());
        $order = $objectManager->create('Magento\Sales\Model\Order')->load($orderItem->getOrderId());
        $orderCustomerId = $order->getCustomerId();
        $currentCustomerId = $customerSession->getCustomer()->getId();

        $productId = $purchasedItem->getProductId();

        $downloadableLink = $objectManager->create('Magento\Downloadable\Model\Link')->load($purchasedItem->getLinkId());
        
        $qty = 0;
        foreach ($order->getAllItems() as $item) {    
            if ((is_int($this->getRemainingDownloads($purchasedItem))) && $downloadableLink->getNumberOfDownloads() != 0) {
                if (($item->getProductId() == $productId) /*&& ($currentCustomerId == $orderCustomerId)*/) {
                    $qty =  intval($this->getRemainingDownloads($purchasedItem) / $downloadableLink->getNumberOfDownloads());
                    
                }
                
            }
            
        }
        return $qty;
    }
    
    /**
     * Return Related Customers Email
     *
     * @return array
     */
    public function getCustomersEmail()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $currentCustomerId = $customerSession->getCustomer()->getId();
        $customersEmail = [];
        
        if (!empty($currentCustomerId)) {
            $collection = $objectManager->create('Magento\Customer\Model\Customer')->getCollection();
            $collection->addAttributeToSelect(['entity_id', 'email', 'firstname', 'lastname']);
            $collection->addAttributeToFilter('customer_parent_account', $currentCustomerId);
            $customersEmail = $collection->getItems();
            
        }
        
        return $customersEmail;
        
    }
    
    /**
     * Return Purchased Link
     *
     * @param int $purchasedLinkId
     * @return \Magento\Downloadable\Model\Link\Purchased
     */
    public function getPurchasedLinkById($purchasedLinkId)
    {
        $purchasedItem = $this->_itemsFactory->create()->addFieldToFilter('item_id',['in' => $purchasedLinkId]);
        $purchased = $this->_linksFactory->create()->addFieldToFilter('purchased_id', $purchasedItem->getLastItem()->getPurchasedId());        
                
        return $purchased->getLastItem();
        
    }
    
    /**
     * Return Purchased Link Item
     *
     * @param int $purchasedItemId
     * @return \Magento\Downloadable\Model\Link\Purchased\Item
     */
    public function getPurchasedLinkItemById($purchasedItemId)
    {
        $purchasedItem = $this->_itemsFactory->create()->addFieldToFilter('item_id',['in' => $purchasedItemId]);
        return $purchasedItem->getLastItem();
        
    }
    
     /**
     * Return Role for Current Customer
     *
     * @return string|null
     */
    public function getCurrentCustomerRole()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($customerSession->getId());
        $role = $customer->getCustomerRole();
        $attributeModel = $objectManager->create('Magento\Customer\Model\Attribute')->load('customer_role','attribute_code');
        $options = $attributeModel->getSource()->getAllOptions();
        
        $roleName = '';
        foreach ($options as $item) {
            if($item['value'] == $role ){
                $roleName = $item['label'];
            }
        }
        
        return $roleName;
        
    }
    
    /**
     * Return Customer Assigned Users
     *
     * @param int $purchasedItemId
     * @return array
     */
    public function getAssignedUsers($purchasedItemId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $relatedItems = $objectManager->create('Endeavor\Downloadable\Model\RelatedItems')->getCollection();
        $relatedItemsIds = array();
        $assignedUser = [];
        $i = 0;
        foreach ($relatedItems->getItems() as $item) {
            if ($item->getItemId() == $purchasedItemId) {
                $relatedItemsIds[$item->getRelatedItemId()] = $item->getQty();
                
            }
            
        }
        
        if (!empty($relatedItemsIds)) {
            foreach ($relatedItemsIds as $id => $qty) {
                $purchasedItem = $objectManager->create('Magento\Downloadable\Model\Link\Purchased\Item')->load($id);
                $purchased = $objectManager->create('Magento\Downloadable\Model\Link\Purchased')->load($purchasedItem->getPurchasedId());        

                $customerModel = $objectManager->create('Magento\Customer\Model\Customer')->load($purchased->getCustomerId());

                $assignedUser[$i]['email'] = $customerModel->getEmail();
                $assignedUser[$i]['firstname'] = $customerModel->getFirstname();
                $assignedUser[$i]['lastname'] = $customerModel->getLastname();
                $assignedUser[$i]['qty'] = $qty;
                $i++;
                
            }

        }
        
        return $assignedUser;
        
    }
    
}