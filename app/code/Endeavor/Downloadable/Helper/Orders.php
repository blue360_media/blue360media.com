<?php
namespace Endeavor\Downloadable\Helper;
 
/**
 * Custom Module Email helper
 */
class Orders extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * 
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }
    
     /**
     * Return all filtered order 
     *
     * @return content
     */
    public function getFilteredOrders($fromDate,$toDate,$type = 'export')
    {        
        //Get Object Manager Instance
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $orderItems = [];
        
        /** start csv content and set template */
        $headers = new \Magento\Framework\DataObject(
            [
                'increment_id' => __('Sale Order PO'),
                'custom_increment_id' => __('Reference Number'),
                'shipping_description' => __('Shipping Delivery Method'),
                'status' => __('Complete Delivery (Y/N)'),
                'sku' => __('Material to Order'),
                'qty' => __('Quantity'),
                'firstname' => __('Recipient Name 1'),
                'lastname' => __('Recipient Name 2'),
                'recipient_3' => __('Recipient Name 3'),
                'street' => __('Recipient Address'),
                'city' => __('Recipient City'),
                'region' => __('Recipient State'),
                'postcode' => __('Recipient Zip'),
                'country_id' => __('Recipient Country'),
                'bill_name_1' => __('Trans Bill-To Name 1'),
                'bill_name_2' => __('Trans Bill-To Name 2'),
                'bill_street' => __('Trans Bill-To Street'),
                'bill_city' => __('Trans Bill-To City'),
                'bill_state' => __('Trans Bill-To State'),
                'bill_zip' => __('Trans Bill-To Zip'),
                'bill_country' => __('Trans Bill-To Country'),
                'bill_acct_num' => __('Trans Bill-To Carrier Acct Num'),
                'telephone' => __('Recipient Phone'),
            ]
        );
        $template = '"{{increment_id}}","{{custom_increment_id}}","{{shipping_description}}","{{status}}","{{sku}}"' .
            ',"{{qty}}","{{firstname}}","{{lastname}}","{{recipient_3}}"'
                . ',"{{street}}","{{city}}","{{region}}","{{postcode}}","{{country_id}}","{{bill_name_1}}"'
                . ',"{{bill_name_2}}","{{bill_street}}","{{bill_city}}","{{bill_state}}","{{bill_zip}}","{{bill_country}}","{{bill_acct_num}}","{{telephone}}"';
        $content = $headers->toString($template);

        $content .= "\n";

        $collection = $objectManager->create('Magento\Sales\Model\Order\Item')
            ->getCollection()
            ->addAttributeToFilter('product_type', array('neq' => 'downloadable'))
            ->addFieldToSelect(
                [
                    'item_id',
                    'order_id',
                    'parent_item_id',
                    'product_type',
                    'product_id',
                    'sku',
                    'qty_ordered',
                    'custom_increment_id'
                ]
            )
            ->addFieldToFilter('updated_at', ['lteq' => $toDate])
            ->addFieldToFilter('updated_at', ['gteq' => $fromDate]);

        foreach ($collection->getItems() as $salesOrderItem) {
            $orderItem = $salesOrderItem;
            
            $orderItem->addData(['qty' => intval($orderItem->getQtyOrdered())]);
            $order = $objectManager->create('Magento\Sales\Model\Order')
                    ->load($orderItem->getOrderId());
            
            if ($order->getSourceOrder() == 'SF' 
                    || empty($orderItem->getParentItemId())
                    ||$order->getStatus() == 'canceled') {
                continue;
            }
            
            $productItem = $objectManager->create('Magento\Catalog\Model\Product')
                    ->load($orderItem->getProductId());
            
            $multipartUi = $productItem->getMultipartUi();
            $multipartUiArray = array_map('trim',explode(",",$multipartUi));

            if (!empty($multipartUi)) {
                foreach ($multipartUiArray as $sku) {
                        $lineItem = $this->getLineItem($orderItem,$sku);
                        $content .= $lineItem->toString($template) . "\n";
                }
            } else if ($orderItem->getProductType() == 'combo') {
                $parent = $objectManager->create('Magento\Sales\Model\Order\Item')
                        ->load($orderItem->getParentItemId());
                
                $parentId = $parent->getProductId();
                if (isset($parentId)) {
                    $product = $objectManager->create('Magento\Catalog\Model\Product')->load($parentId);
                    $collection = $product->getTypeInstance(true)
                       ->getSelectionsCollection($product->getTypeInstance(true)->getOptionsIds($product), $product);

                    $productIds = array();
                    foreach ($collection as $item) {
                        $productIds[] = $item->getId();
                    }

                    $printProduct = $objectManager->create('Magento\Catalog\Model\Product')
                        ->getCollection()
                        ->addAttributeToFilter('name', array('like' => '%print%'))
                        ->addFieldToFilter('entity_id',$productIds);

                    $printProductSku = $printProduct->getLastItem()->getSku();
                    $lineItem = $this->getLineItem($orderItem);
                    if (isset($printProductSku)) {
                        $lineItem = $this->getLineItem($orderItem,$printProductSku);
                    }
                }
                $content .= $lineItem->toString($template) . "\n";
            } else {
                $lineItem = $this->getLineItem($orderItem);
                $content .= $lineItem->toString($template) . "\n";
            }
        }
        
        return $content;
    }
    
    /**
     * 
     * @param Magento\Sales\Model\Order\Item $orderItem
     * @param string $productSku
     * @return Magento\Sales\Model\Order\Item
     */
    protected  function getLineItem($orderItem,$productSku = null)
    {
        //Get Object Manager Instance
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $order = $objectManager->create('Magento\Sales\Model\Order')
                    ->load($orderItem->getOrderId());
        
        $customer = $objectManager->create('Magento\Customer\Model\Customer')
                ->load($order->getCustomerId());
        
        $customerKeys =['email' => '','firstname' => '','lastname' => '','customer_blue_account_id' => '','customer_blue_customer_number' => ''];
        $customerArray = array_intersect_key($customer->getData(), $customerKeys);
        
        if ($order->getShippingDescription() == 'Shipping Method - Next Day') {
            $order->setShippingDescription('91');

        } else if ($order->getShippingDescription() == 'Shipping Method - Next 2-Day') {
            $order->setShippingDescription('92');
		} else if ($order->getShippingDescription() == 'Shipping Method - UPS') {
            $order->setShippingDescription('UG');			
        } else {
            $order->setShippingDescription('UG');

        }

        $orderAddress = $objectManager->create('Magento\Sales\Model\Order\Address')
                ->load($order->getShippingAddressId());
                
        $orderAddressKeys = ['postcode' => '', 'shipping_description' => '', 'street' => '', 'city' => '',
            'telephone' => '', 'region_id' => '', 'country_id' => '', 'firstname' => '', 'lastname' => ''];
        $orderAddressArray = array_intersect_key($orderAddress->getData(), $orderAddressKeys);
        
        $orderKeys =['status' => '', 'shipping_description' => '', 'customer_id' => '', 'shipping_address_id' => '',
            'increment_id' => ''];
        $orderArray = array_intersect_key($order->getData(), $orderKeys);
        
        if ($orderArray['status'] == 'complete') {
            $orderArray['status'] = "Y" ;
        } else {
            $orderArray['status'] = "N" ;
        }
        
        $orderItem->addData($orderArray);
        $orderItem->addData($orderAddressArray);
        $orderItem->addData($customerArray);
        
        $orderRegion = $objectManager->create('Magento\Directory\Model\Region')
                ->load($orderItem->getRegionId());

        $orderItem->setRegion($orderRegion->getCode());


        if(!empty($productSku)){
            $orderItem->setSku($productSku);
        }

        return $orderItem;
    }
    
    
}