<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Downloadable\Model;

class Import
{
    /**
     * 
     * @param type $file
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function importFromCsvFile($file)
    {
        if (!isset($file['tmp_name'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $csvProcessor = $objectManager->create('Magento\Framework\File\Csv');
        
        $importProductRawData = $csvProcessor->getData($file['tmp_name']);

//        foreach ($importProductRawData as $dataRow) {
//            \Zend_Debug::dump($dataRow);
////            var_dump($dataRow); // $data[$i] is an array with your csv columns as values.
//        }
        return $importProductRawData;
    }
    
}
