<?php


namespace Endeavor\Downloadable\Model\ResourceModel\RelatedItems;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Endeavor\Downloadable\Model\RelatedItems',
            'Endeavor\Downloadable\Model\ResourceModel\RelatedItems'
        );
    }
}
