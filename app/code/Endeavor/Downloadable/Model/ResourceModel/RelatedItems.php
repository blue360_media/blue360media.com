<?php


namespace Endeavor\Downloadable\Model\ResourceModel;

class RelatedItems extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('endeavor_relateditems', 'entity_id');
    }
}
