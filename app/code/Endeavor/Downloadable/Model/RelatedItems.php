<?php


namespace Endeavor\Downloadable\Model;

use Endeavor\Downloadable\Api\Data\RelatedItemsInterface;

class RelatedItems extends \Magento\Framework\Model\AbstractModel implements RelatedItemsInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Downloadable\Model\ResourceModel\RelatedItems');
    }

    /**
     * Get entity_id
     * @return string
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * Set entity_id
     * @param string $entityId
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get item_id
     * @return string
     */
    public function getItemId()
    {
        return $this->getData(self::ITEM_ID);
    }

    /**
     * Set item_id
     * @param string $item_id
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setItemId($item_id)
    {
        return $this->setData(self::ITEM_ID, $item_id);
    }

    /**
     * Get related_item_id
     * @return string
     */
    public function getRelatedItemId()
    {
        return $this->getData(self::RELATED_ITEM_ID);
    }

    /**
     * Set related_item_id
     * @param string $related_item_id
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setRelatedItemId($related_item_id)
    {
        return $this->setData(self::RELATED_ITEM_ID, $related_item_id);
    }
    
    /**
     * Get qty
     * @return int
     */
    public function getQty()
    {
        return $this->getData(self::QTY);
    }

    /**
     * Set qty
     * @param int $qty
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setQty($qty)
    {
        return $this->setData(self::QTY, $qty);
    }
    
    /**
     * Get created_by
     * @return text
     */
    public function getCreatedBy()
    {
        return $this->getData(self::CREATED_BY);
    }

    /**
     * Set created_by
     * @param text $createdBy
     * @return \Endeavor\Downloadable\Api\Data\RelatedItemsInterface
     */
    public function setCreatedBy($createdBy)
    {
        return $this->setData(self::CREATED_BY, $createdBy);
    }
}
