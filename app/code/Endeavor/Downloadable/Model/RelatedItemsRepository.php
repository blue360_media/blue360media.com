<?php
namespace Endeavor\Downloadable\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Api\DataObjectHelper;
use Endeavor\Downloadable\Api\RelatedItemsRepositoryInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Endeavor\Downloadable\Model\ResourceModel\RelatedItems\CollectionFactory as RelatedItemsCollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SortOrder;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Endeavor\Downloadable\Api\Data\RelatedItemsSearchResultsInterfaceFactory;
use Endeavor\Downloadable\Api\Data\RelatedItemsInterfaceFactory;
use Endeavor\Downloadable\Model\ResourceModel\RelatedItems as ResourceRelatedItems;

class RelatedItemsRepository implements RelatedItemsRepositoryInterface
{
    /**
     *
     * @var Magento\Store\Model\StoreManagerInterface 
     */
    private $storeManager;

    /**
     *
     * @var Endeavor\Downloadable\Model\ResourceModel\RelatedItems\CollectionFactory 
     */
    protected $relatedItemsFactory;

    /**
     *
     * @var Endeavor\Downloadable\Api\Data\RelatedItemsInterfaceFactory 
     */
    protected $dataRelatedItemsFactory;

    /**
     *
     * @var DataObjectProcessor 
     */
    protected $dataObjectProcessor;

    /**
     *
     * @var RelatedItemsCollectionFactory 
     */
    protected $relatedItemsCollectionFactory;

    /**
     *
     * @var Endeavor\Downloadable\Api\Data\RelatedItemsSearchResultsInterfaceFactory 
     */
    protected $searchResultsFactory;

    /**
     *
     * @var ResourceRelatedItems 
     */
    protected $resource;

    /**
     *
     * @var DataObjectHelper 
     */
    protected $dataObjectHelper;


    /**
     * @param ResourceRelatedItems $resource
     * @param RelatedItemsFactory $relatedItemsFactory
     * @param RelatedItemsInterfaceFactory $dataRelatedItemsFactory
     * @param RelatedItemsCollectionFactory $relatedItemsCollectionFactory
     * @param RelatedItemsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceRelatedItems $resource,
        RelatedItemsFactory $relatedItemsFactory,
        RelatedItemsInterfaceFactory $dataRelatedItemsFactory,
        RelatedItemsCollectionFactory $relatedItemsCollectionFactory,
        RelatedItemsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->relatedItemsFactory = $relatedItemsFactory;
        $this->relatedItemsCollectionFactory = $relatedItemsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataRelatedItemsFactory = $dataRelatedItemsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Endeavor\Downloadable\Api\Data\RelatedItemsInterface $relatedItems
    ) {
        try {
            $relatedItems->getResource()->save($relatedItems);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the relatedItems: %1',
                $exception->getMessage()
            ));
        }
        return $relatedItems;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($entityId)
    {
        $relatedItems = $this->relatedItemsFactory->create();
        $relatedItems->getResource()->load($relatedItems, $entityId);
        if (!$relatedItems->getId()) {
            throw new NoSuchEntityException(__('RelatedItems with id "%1" does not exist.', $entityId));
        }
        return $relatedItems;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->relatedItemsCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Endeavor\Downloadable\Api\Data\RelatedItemsInterface $relatedItems
    ) {
        try {
            $relatedItems->getResource()->delete($relatedItems);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the RelatedItems: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($entityId)
    {
        return $this->delete($this->getById($entityId));
    }
}
