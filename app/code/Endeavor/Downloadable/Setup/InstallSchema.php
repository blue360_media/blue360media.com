<?php
namespace Endeavor\Downloadable\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        $table_endeavor_relateditems = $setup->getConnection(
                )->newTable($setup->getTable('endeavor_relateditems'));
        
        $table_endeavor_relateditems->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
            'Entity ID'
        );

        $table_endeavor_relateditems->addColumn(
            'item_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['unsigned' => true],
            'item_id'
        );
        
        $table_endeavor_relateditems->addColumn(
            'related_item_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['unsigned' => true],
            'related_item_id'
        );
        
        $table_endeavor_relateditems->addForeignKey(
            $installer->getFkName(
                'endeavor_relateditems',
                'item_id',
                'downloadable_link_purchased_item',
                'item_id'
            ),
            'item_id',
            $installer->getTable('downloadable_link_purchased_item'), 
            'item_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
        
        $setup->getConnection()->createTable($table_endeavor_relateditems);

        $setup->endSetup();
    }
}
