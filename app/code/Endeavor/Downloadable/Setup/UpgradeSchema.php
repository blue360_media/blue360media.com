<?php 
namespace Endeavor\Downloadable\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var string
     */
    private static $CONNECTIONNAME = 'endeavor_relateditems';
    
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addQtyField($setup);
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->addUserField($setup);
        }
    }
    
    /**
     * Add meta title
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addUserField(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$CONNECTIONNAME),
            'created_by',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'length' => 100,
                'nullable' => false,
                'default' => 'Admin',
                'comment' => 'Created By'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$CONNECTIONNAME),
            'created_at',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                'nullable' => false,
                'length' => null,
                'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,
                'comment' => 'Created At'
            ]                
        );
        return $this;
    }
    
    /**
     * Add meta title
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addQtyField(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$CONNECTIONNAME),
            'qty',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'length' => 10,
                'unsigned' => true,
                'comment' => 'Item Qty'
            ]
        );
        return $this;
    }
}
