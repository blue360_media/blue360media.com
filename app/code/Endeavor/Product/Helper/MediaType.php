<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 03-25-2018.
 * @desc get Media type attribute for product.
 * 
*/

namespace Endeavor\Product\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class MediaType extends AbstractHelper {
    
    /**
     * @param string $productId
     * 
     * @return string $mediaType
     */
    public function getMediaTypeForBundleSelection($productId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
        
        $attributeInfo = $product->getResource()->getAttribute('media_type');
        $attributeId = $attributeInfo->getAttributeId();
        /**
         * Fetch particular option's name and value of the attribute
         */
        if ($product->getCustomAttribute('media_type')) {
            $optionId = $product->getCustomAttribute('media_type')->getValue();
            $attributeOptionSingle = $objectManager->create('\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection')
                                                ->setPositionOrder('asc')
                                                ->setAttributeFilter($attributeId)
                                                ->setIdFilter($optionId)
                                                ->setStoreFilter(0)
                                                ->load()
                                                ->getFirstItem();
            $mediaType = $attributeOptionSingle->getValue();
        } else {
            $mediaType = 'B';
            
        }
        
        return $mediaType;
        
    }
    
}
