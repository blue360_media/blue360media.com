<?php
/**
 * Copyright © 2018 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 04-29-2018.
 * @desc in order to get if product has rule or not.
 * 
*/

namespace Endeavor\Product\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class CatalogRuleHelper extends AbstractHelper {
    
    /**
     * @method getIsMatchingRule
     * @desc in order to get if product has rule or not.
     * @param string $productId
     * 
     * @return boolean $isMatchingAnyRule
     */
    public function getIsMatchingRule($productId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $website = $objectManager->create('\Magento\Store\Model\StoreManagerInterface')->getWebsite();
        $rules = $objectManager->create('\Magento\CatalogRule\Model\rule')->getCollection()
            ->addWebsiteFilter($website) //filter rules for current site
            ->addIsActiveFilter(1); //filter active rules
        
        $isMatchingAnyRule = false;
        foreach ($rules as $rule) {
            $affectedProductIds = $rule->getMatchingProductIds();
            if (isset($affectedProductIds[$productId])) {
                if ($affectedProductIds[$productId][1]) {
                    $isMatchingAnyRule = true;
                    
                }
                
            }
            
        }
        return $isMatchingAnyRule;
    }
    
}
