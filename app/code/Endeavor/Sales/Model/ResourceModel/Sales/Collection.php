<?php
namespace Endeavor\Sales\Model\ResourceModel\Sales;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Endeavor\Sales\Model\Sales',
            'Endeavor\Sales\Model\ResourceModel\Sales'
        );
    }
}
