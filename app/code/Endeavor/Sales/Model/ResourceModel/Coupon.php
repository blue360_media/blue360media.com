<?php

namespace Endeavor\Sales\Model\ResourceModel;

/**
 * Description of Coupon
 *
 * @author mhamadneh
 */
class Coupon extends \Magento\SalesRule\Model\ResourceModel\Coupon {

    protected function _construct() {
        $this->_init('salesrule_coupon', 'coupon_id');
    }

}
