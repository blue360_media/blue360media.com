<?php
namespace Endeavor\Sales\Model;

use Endeavor\Sales\Api\Data\SalesInterface;

class Sales extends \Magento\Framework\Model\AbstractModel implements SalesInterface
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Sales\Model\ResourceModel\Sales');
    }

    /**
     * Get sales_id
     * @return string
     */
    public function getSalesId()
    {
        return $this->getData(self::SALES_ID);
    }

    /**
     * Set sales_id
     * @param string $salesId
     * @return Endeavor\Sales\Api\Data\SalesInterface
     */
    public function setSalesId($salesId)
    {
        return $this->setData(self::SALES_ID, $salesId);
    }

    /**
     * Get code
     * @return string
     */
    public function getCode()
    {
        return $this->getData(self::CODE);
    }

    /**
     * Set code
     * @param string $code
     * @return Endeavor\Sales\Api\Data\SalesInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Get firstname
     * @return string
     */
    public function getFirstname()
    {
        return $this->getData(self::FIRSTNAME);
    }

    /**
     * Set firstname
     * @param string $firstname
     * @return Endeavor\Sales\Api\Data\SalesInterface
     */
    public function setFirstname($firstname)
    {
        return $this->setData(self::FIRSTNAME, $firstname);
    }

    /**
     * Get lastname
     * @return string
     */
    public function getLastname()
    {
        return $this->getData(self::LASTNAME);
    }

    /**
     * Set lastname
     * @param string $lastname
     * @return Endeavor\Sales\Api\Data\SalesInterface
     */
    public function setLastname($lastname)
    {
        return $this->setData(self::LASTNAME, $lastname);
    }
}
