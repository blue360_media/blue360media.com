<?php
namespace Endeavor\Sales\Model\Config\Source;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
 
/**
 * Custom Attribute Renderer
 *
 */
class SalesRep extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @var OptionFactory
     */
    protected $optionFactory;
 
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collect = $objectManager->create('Endeavor\Sales\Model\Sales')->getCollection();
        $initial = array('label'=>'Please Select','value'=>0);
        $sales=[];
        array_push($sales,$initial);
        foreach ($collect as $value) {
            $label = $value->getFirstName();
            if (!empty($value->getLastName())) {
                $label = $label .' '. $value->getLastName();
            }
            $label = $label .'-'.$value->getCode();
            $item = array(
                'label' => $label, 
                'value' =>  $value->getSalesId()
            );
            array_push($sales,$item);
        }
        return $sales;
    }
}