<?php
namespace Endeavor\Sales\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Endeavor\Sales\Model\ResourceModel\Sales\CollectionFactory as SalesCollectionFactory;
use Endeavor\Sales\Api\Data\SalesInterfaceFactory;
use Magento\Framework\Reflection\DataObjectProcessor;
use Endeavor\Sales\Api\Data\SalesSearchResultsInterfaceFactory;
use Magento\Framework\Api\SortOrder;
use Endeavor\Sales\Model\ResourceModel\Sales as ResourceSales;
use Magento\Store\Model\StoreManagerInterface;
use Endeavor\Sales\Api\SalesRepositoryInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\NoSuchEntityException;

class SalesRepository implements SalesRepositoryInterface
{
    /**
     *
     * @var SalesSearchResultsInterfaceFactory 
     */
    protected $searchResultsFactory;

    /**
     *
     * @var DataObjectProcessor 
     */
    protected $dataObjectProcessor;

    /**
     *
     * @var SalesCollectionFactory 
     */
    protected $SalesCollectionFactory;
    
    /**
     *
     * @var SalesInterfaceFactory 
     */
    protected $dataSalesFactory;
    
    /**
     *
     * @var StoreManagerInterface 
     */
    private $storeManager;
    
    /**
     *
     * @var SalesFactory 
     */
    protected $SalesFactory;

    /**
     *
     * @var DataObjectHelper 
     */
    protected $dataObjectHelper;

    /**
     *
     * @var ResourceSales 
     */
    protected $resource;


    /**
     * @param ResourceSales $resource
     * @param SalesFactory $salesFactory
     * @param SalesInterfaceFactory $dataSalesFactory
     * @param SalesCollectionFactory $salesCollectionFactory
     * @param SalesSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceSales $resource,
        SalesFactory $salesFactory,
        SalesInterfaceFactory $dataSalesFactory,
        SalesCollectionFactory $salesCollectionFactory,
        SalesSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->salesFactory = $salesFactory;
        $this->salesCollectionFactory = $salesCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataSalesFactory = $dataSalesFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Endeavor\Sales\Api\Data\SalesInterface $sales
    ) {
        try {
            $this->resource->save($sales);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the sales: %1',
                $exception->getMessage()
            ));
        }
        return $sales;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($salesId)
    {
        $sales = $this->salesFactory->create();
        $sales->load($salesId);
        if (!$sales->getId()) {
            throw new NoSuchEntityException(__('Sales with id "%1" does not exist.', $salesId));
        }
        return $sales;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $collection = $this->salesCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $items = [];
        
        foreach ($collection as $salesModel) {
            $salesData = $this->dataSalesFactory->create();
            $this->dataObjectHelper->populateWithArray(
                $salesData,
                $salesModel->getData(),
                'Endeavor\Sales\Api\Data\SalesInterface'
            );
            $items[] = $this->dataObjectProcessor->buildOutputDataArray(
                $salesData,
                'Endeavor\Sales\Api\Data\SalesInterface'
            );
        }
        $searchResults->setItems($items);
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Endeavor\Sales\Api\Data\SalesInterface $sales
    ) {
        try {
            $this->resource->delete($sales);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Sales: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($salesId)
    {
        return $this->delete($this->getById($salesId));
    }
}
