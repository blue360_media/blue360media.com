<?php
namespace Endeavor\Sales\Controller\Adminhtml\Sales;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('sales_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create('Endeavor\Sales\Model\Sales');
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The sales representative has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['sales_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find an sales representative to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
