<?php
namespace Endeavor\Sales\Controller\Adminhtml\Sales;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $_jsHelper;
    
    /**
     * @var \Endeavor\Author\Model\ResourceModel\Author\CollectionFactory
     */
    protected $_authorCollectionFactory;
    
    /**
    * @var \Magento\Framework\Image\AdapterFactory
    */
    protected $adapterFactory;
    
    /**
    * @var \Magento\MediaStorage\Model\File\UploaderFactory
    */
    protected $uploader;
    
    /**
    * @var \Magento\Framework\Filesystem
    */
    protected $filesystem;
    
    /**
    * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
    */
    protected $timezoneInterface;

    /**
     * \Magento\Backend\Helper\Js $jsHelper
     * @param Action\Context $context
     */
    public function __construct(
        Context $context,
        \Magento\Backend\Helper\Js $jsHelper,
        \Magento\Framework\Image\AdapterFactory $adapterFactory,
        \Magento\MediaStorage\Model\File\UploaderFactory $uploader,
        \Magento\Framework\Filesystem $filesystem,
        \Endeavor\Sales\Model\ResourceModel\Sales\CollectionFactory $authorCollectionFactory
    ) {
        $this->_jsHelper = $jsHelper;
        $this->_contactCollectionFactory = $authorCollectionFactory;
        $this->adapterFactory = $adapterFactory;
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute() {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        $model = $this->_objectManager->create('Endeavor\Sales\Model\Sales');
        $salesCollection = $model->getCollection();
        $salesCollection->addFieldToFilter('code',$data['code']);
        
        if (count($salesCollection) !=0 ) {
             $this->messageManager->addError(__('The Code you entered already exist.'));
             return $resultRedirect->setPath('sales/sales/index');
        }
        
        if ($data) {

            $model->setData($data);
            try {
                $model->save();
                $this->messageManager->addSuccess(__('The Sales Representative has been saved.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['sales_id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the sales representative.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['sales_id' => $this->getRequest()->getParam('sales_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
    
}
