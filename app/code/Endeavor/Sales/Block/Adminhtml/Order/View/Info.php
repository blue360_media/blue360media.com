<?php

/**
 * Copyright © 2017 Endeavorpal, Inc. All rights reserved.
 * By: Mohammad Hamadneh.
 * Email: mhamadneh@endeavorpal.com
 */

namespace Endeavor\Sales\Block\Adminhtml\Order\View;

class Info extends \Magento\Backend\Block\Template {

    /**
     * Returns Order Data to show on frontend
     *
     * @return Object
     */
    public function getSFOrder() {
        $orderId = $this->getRequest()->getParam('order_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->get('Magento\Sales\Model\Order')->load($orderId);
        return $order;
    }

}
