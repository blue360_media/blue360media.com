<?php
namespace Endeavor\Sales\Setup;

use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
 
/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    
    /**
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;
    
    /**
     * @var AttributeSetFactory
     */
    private $attributeSetFactory;
    
    /**
     *
     * @var Magento\Framework\App\Request\Http 
     */
    protected $request;
    
    /**
     * 
     * @param CustomerSetupFactory $customerSetupFactory
     * @param \Magento\Framework\App\Request\Http $request
     * @param AttributeSetFactory $attributeSetFactory
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        \Magento\Framework\App\Request\Http $request,
        AttributeSetFactory $attributeSetFactory
    ) {
        $this->customerSetupFactory = $customerSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->request = $request;
    }
 
    
    /**
     * {@inheritdoc}
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        
        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
      
        $customerSetup->addAttribute(Customer::ENTITY, 'customer_sales_rep', [
            'type' => 'varchar',
            'label' => 'Sales Representative',
            'input' => 'text',
            'backend' => '',
            'source' => '',
            'required' => false,
            'textInput' => '',
            'visible' => true,
            'is_used_in_grid' => true,
            'is_visible_in_grid' => true,
            'is_filterable_in_grid' => true,
            'is_searchable_in_grid' => true,
            'user_defined' => true,
            'sort_order' => 1000,
            'position' => 1,
            'system' => 0,
        ]);
        
         $attribute3 = $customerSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'customer_sales_rep'
            )->addData([
            'attribute_set_id' => $attributeSetId,
            'attribute_group_id' => $attributeGroupId,
            'used_in_forms' => ['adminhtml_customer'],
            ]);
         
        $attribute3->save();
        
         
    }
}