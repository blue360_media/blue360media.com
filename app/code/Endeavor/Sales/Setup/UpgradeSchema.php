<?php

namespace Endeavor\Sales\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $tableName = $setup->getTable('salesrule_coupon');
        $keyName="SALESRULE_COUPON_CODE";
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
              if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();
                $connection->dropIndex($tableName, $keyName);
            }
        }


        $setup->endSetup();

    }
}