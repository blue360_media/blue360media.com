<?php
namespace Endeavor\Sales\Api\Data;

interface SalesSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get Sales list.
     * @return \Endeavor\Sales\Api\Data\SalesInterface[]
     */
    public function getItems();

    /**
     * Set code list.
     * @param \Endeavor\Sales\Api\Data\SalesInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
    
}
