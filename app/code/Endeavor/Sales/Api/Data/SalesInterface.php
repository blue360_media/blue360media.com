<?php
namespace Endeavor\Sales\Api\Data;

interface SalesInterface
{
    /**
     * @var string columns name
     */
    const FIRSTNAME = 'firstname';
    const LASTNAME = 'lastname';
    const SALES_ID = 'sales_id';
    const CODE = 'code';


    /**
     * Get sales_id
     * @return string|null
     */
    public function getSalesId();

    /**
     * Set sales_id
     * @param string $salesId
     * @return Endeavor\Sales\Api\Data\SalesInterface
     */
    public function setSalesId($salesId);

    /**
     * Get code
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     * @param string $code
     * @return Endeavor\Sales\Api\Data\SalesInterface
     */
    public function setCode($code);

    /**
     * Get firstname
     * @return string|null
     */
    public function getFirstname();

    /**
     * Set firstname
     * @param string $firstname
     * @return Endeavor\Sales\Api\Data\SalesInterface
     */
    public function setFirstname($firstname);

    /**
     * Get lastname
     * @return string|null
     */
    public function getLastname();

    /**
     * Set lastname
     * @param string $lastname
     * @return Endeavor\Sales\Api\Data\SalesInterface
     */
    public function setLastname($lastname);
}
