<?php
namespace Endeavor\Sales\Api;

interface SalesRepositoryInterface
{
    /**
     * Save Sales
     * @param \Endeavor\Sales\Api\Data\SalesInterface $sales
     * @return \Endeavor\Sales\Api\Data\SalesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Endeavor\Sales\Api\Data\SalesInterface $sales
    );

    /**
     * Retrieve Sales
     * @param string $salesId
     * @return \Endeavor\Sales\Api\Data\SalesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($salesId);

    /**
     * Retrieve Sales matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Endeavor\Sales\Api\Data\SalesSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Sales
     * @param \Endeavor\Sales\Api\Data\SalesInterface $sales
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Endeavor\Sales\Api\Data\SalesInterface $sales
    );

    /**
     * Delete Sales by ID
     * @param string $salesId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($salesId);
}
