<?php
namespace Endeavor\PaymentComplete\Logger;

class Logger extends \Monolog\Logger
{    
    /**
     * @var string
     */
    public $name = 'PayPal_Transaction';

}
