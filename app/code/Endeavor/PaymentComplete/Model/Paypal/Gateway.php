<?php
namespace Endeavor\PaymentComplete\Model\Paypal;

/**
 * @abstract Capture Payment amount and make Specific Order as Complete payment.
 *
 * @author Murad Dweikat
 * @date: 2017/11/29
 */
class Gateway {
    
    /**
     * Complete Payment Order Status
     */
    const DO_DIRECT_PAYMENT = 'DoDirectPayment';
    
    const SET_EXPRESS_CHECKOUT = 'SetExpressCheckout';

    const GET_EXPRESS_CHECKOUT_DETAILS = 'GetExpressCheckoutDetails';

    const DO_EXPRESS_CHECKOUT_PAYMENT = 'DoExpressCheckoutPayment';
    
    const PAYMENT_COMPLETE = 'complete_payment';

    /**
     * GetExpressCheckoutDetails request map
     *
     * @var string[]
     */
    protected $_getExpressCheckoutDetailsRequest = ['TOKEN', 'SUBJECT'];
    
    /**
     * Required fields in the response
     *
     * @var array
     */
    protected $_requiredResponseParams = [self::DO_DIRECT_PAYMENT => ['ACK', 'CORRELATIONID', 'AMT']];

    /**
     *
     * @var type 
     */
    protected $_messageManager;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $formKey;
    
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**
     *
     * @var Magento\Paypal\Model\Api\Nvp
     */
    protected $api;

    /**
     *
     * @var Magento\Framework\App\Action\Context 
     */
    protected $_context;
    
    /**
     * Config instance
     *
     * @var PaypalConfig
     */
    protected $_config;

     /**
     * Api Model Type
     *
     * @var string
     */
    protected $_apiType = \Magento\Paypal\Model\Api\Nvp::class;
    
    /**
     * @var \Magento\Paypal\Model\Api\Type\Factory
     */
    protected $_apiTypeFactory;
    
    /**
     * @var \Magento\Framework\HTTP\Adapter\CurlFactory
     */
    protected $_curlFactory;
    
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     * @var \Magento\Paypal\Model\CertFactory
     */
    protected $_certFactory;
    
    /**
     * API call HTTP headers
     *
     * @var array
     */
    protected $_headers = [];
    
    /**
     * Warning codes recollected after each API call
     *
     * @var array
     */
    protected $_callWarnings = [];

    /**
     * Global private to public interface map
     * @var array
     */
    protected $_globalMap = [];

    /**
     * Filter callbacks for exporting $this data to API call
     *
     * @var array
     */
    protected $_exportToRequestFilters = [];
    
    /**
     * Filter callback for preparing internal amounts to NVP request
     *
     * @var array
     */
    protected $_importFromRequestFilters = [
        'REDIRECTREQUIRED' => '_filterToBool',
        'SUCCESSPAGEREDIRECTREQUESTED' => '_filterToBool',
        'PAYMENTSTATUS' => '_filterPaymentStatusFromNvpToInfo',
    ];
    
    /**
     * Map for billing address import/export
     *
     * @var array
     */
    protected $_billingAddressMap = [
        'BUSINESS' => 'company',
        'NOTETEXT' => 'customer_notes',
        'EMAIL' => 'email',
        'FIRSTNAME' => 'firstname',
        'LASTNAME' => 'lastname',
        'MIDDLENAME' => 'middlename',
        'SALUTATION' => 'prefix',
        'SUFFIX' => 'suffix',
        'COUNTRYCODE' => 'country_id', // iso-3166 two-character code
        'STATE' => 'region',
        'CITY' => 'city',
        'STREET' => 'street',
        'STREET2' => 'street2',
        'ZIP' => 'postcode',
        'PHONENUM' => 'telephone',
    ];
    
    /**
     * SetExpressCheckout request map
     *
     * @var string[]
     */
    protected $_setExpressCheckoutRequest = [
        'PAYMENTACTION',
        'AMT',
        'CURRENCYCODE',
        'RETURNURL',
        'CANCELURL',
        'INVNUM',
        'SOLUTIONTYPE',
        'NOSHIPPING',
        'GIROPAYCANCELURL',
        'GIROPAYSUCCESSURL',
        'BANKTXNPENDINGURL',
        'PAGESTYLE',
        'HDRIMG',
        'HDRBORDERCOLOR',
        'HDRBACKCOLOR',
        'PAYFLOWCOLOR',
        'LOCALECODE',
        'BILLINGTYPE',
        'SUBJECT',
        'ITEMAMT',
        'SHIPPINGAMT',
        'TAXAMT',
        'REQBILLINGADDRESS',
        'USERSELECTEDFUNDINGSOURCE',
    ];
    
        /**
     * DoExpressCheckoutPayment request map
     *
     * @var string[]
     */
    protected $_doExpressCheckoutPaymentRequest = [
        'TOKEN',
        'PAYERID',
        'PAYMENTACTION',
        'AMT',
        'CURRENCYCODE',
        'IPADDRESS',
        'BUTTONSOURCE',
        'NOTIFYURL',
        'RETURNFMFDETAILS',
        'SUBJECT',
        'ITEMAMT',
        'SHIPPINGAMT',
        'TAXAMT',
    ];
    
    /**
     * Payment information response specifically to be collected after some requests
     * @var string[]
     */
    protected $_paymentInformationResponse = [
        'PAYERID',
        'PAYERSTATUS',
        'CORRELATIONID',
        'ADDRESSID',
        'ADDRESSSTATUS',
        'PAYMENTSTATUS',
        'PENDINGREASON',
        'PROTECTIONELIGIBILITY',
        'EMAIL',
        'SHIPPINGOPTIONNAME',
        'TAXID',
        'TAXIDTYPE',
    ];
    
    /**
     * Whether to return raw response information after each call
     *
     * @var bool
     */
    protected $_rawResponseNeeded = false;

    /**
     * Error codes recollected after each API call
     *
     * @var array
     */
    protected $_callErrors = [];

    /**
     * @var \Magento\Paypal\Model\Api\ProcessableExceptionFactory
     */
    protected $_processableExceptionFactory;
    
    /**
     *
     * @var Magento\Customer\Model\Session 
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;
    
    /**
     * @var \Magento\Framework\App\ObjectManager
     */
    protected $_objectManager;
    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Paypal\Model\Api\Nvp $paypalApi
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Paypal\Model\Api\Nvp $paypalApi,
        \Magento\Paypal\Model\Api\Type\Factory $apiTypeFactory,
        \Magento\Paypal\Model\Config $config,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Magento\Paypal\Model\Api\ProcessableExceptionFactory $processableExceptionFactory,
        \Magento\Paypal\Model\CertFactory $certFactory,
            
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->formKey = $formKey;
        $this->scopeConfig = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
        $this->api = $paypalApi;
        $this->_context = $context;
        $this->_url = $context->getUrl();
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->_messageManager = $this->_context->getMessageManager();
        $this->_apiTypeFactory = $apiTypeFactory;
        $this->_config = $config;
        $this->_curlFactory = $curlFactory;
        $this->_storeManager = $storeManager;
        $this->_certFactory = $certFactory;
        $this->_processableExceptionFactory = $processableExceptionFactory;
        $this->customerSession = $customerSession;
    }
    
    /**
     * SetExpressCheckout call
     *
     * TODO: put together style and giropay settings
     *
     * @return void
     * @link https://cms.paypal.com/us/cgi-bin/?&cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_SetExpressCheckout
     */
    public function callSetExpressCheckout($data)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $sfOrderId = $data['order'];
        $sfItems = $data['items'];
        $sfRemainingAmount = $data['amountDue'];
        $sfInvoice = $data['invoice'];
        
        $itemsName = '';
        $i = 0;
        foreach ($sfItems as $key => $value) {
            if ($i >= count($sfItems)) {
                $itemsName .= $value['name'];
            } else {
                $itemsName .= $value['name'].', ';
            }
            $i++;
        }
        
        $itemsDesc = 'Items: '.$itemsName;
        
        $params = ['sf_order_id' => $sfOrderId, 'invoiceNumber' => $data['invoice'],'status' => '1',
                    'amountDue' => $data['amountDue'],'grandTotal' => $data['grandTotal'],
                    'amountPaid' => $data['amountPaid'], '_current' => true];
        $returnUrl = $this->_url->getUrl('order/paypal/complete', $params);
        $cancelUrl = $this->_url->getUrl('outstandinginvoice/index/index', ['status' => '0']);
        
        $request = [
            'PAYMENTACTION' => 'Sale',
            'AMT' => $sfRemainingAmount,
            'CURRENCYCODE' => 'USD',
            'RETURNURL' => $returnUrl,
            'CANCELURL' => $cancelUrl,
            'INVNUM' => $sfInvoice,
            'SOLUTIONTYPE' => 'Mark',
            'NOSHIPPING' => '1',
            'GIROPAYCANCELURL' => $this->_url->getUrl('paypal/express/cancel'),
            'GIROPAYSUCCESSURL' => $this->_url->getUrl('checkout/onepage/success'),
            'BANKTXNPENDINGURL' => $this->_url->getUrl('checkout/onepage/success'),
            'BILLINGTYPE' => 'MerchantInitiatedBilling',
            'ITEMAMT' => $sfRemainingAmount,
            'L_NUMBER0' => NULL,
            'L_NAME0' => 'Outstanding Invoice Payment for Order #'.$sfOrderId,
            'L_QTY0' => '1',
            'L_AMT0' => $sfRemainingAmount,
            'L_DESC0' => $itemsDesc
        ];
        
        $requestKeys = [
            'METHOD' => 'SetExpressCheckout',
            'VERSION' => '72.0',
            'USER' => $this->getPayPalConfigValue('api_username'),
            'PWD' => $this->getPayPalConfigValue('api_password'),
            'SIGNATURE' => $this->getPayPalConfigValue('api_signature'),
            'BUTTONSOURCE' => 'Magento_Cart_Community'
        ];

        $request = array_merge($request, $requestKeys);
        $response = $this->call(self::SET_EXPRESS_CHECKOUT, $request);

        $param = [];
        if (isset($response['TOKEN'])) {
            $param = ['cmd' => '_express-checkout', 'token' => $response['TOKEN']];
        }
        
        $requestUrl = $this->getPaypalUrl($param);
        $response['request_url'] = $requestUrl ;
        
        Return $response;
    }
    
    /**
     * GetExpressCheckoutDetails call
     *
     * @return void
     * @link https://cms.paypal.com/us/cgi-bin/?&cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_GetExpressCheckoutDetails
     */
    public function callGetExpressCheckoutDetails($token)
    {
        $this->_prepareExpressCheckoutCallRequest($this->_getExpressCheckoutDetailsRequest);
        $request = $this->_exportToRequest($this->_getExpressCheckoutDetailsRequest);
        $request = $token;
        
        $request['ADDROVERRIDE'] = 0;
//        $request['REQBILLINGADDRESS'] = '1';
        $requestKeys = [
            'METHOD' => 'GetExpressCheckoutDetails',
            'VERSION' => '72.0',
            'USER' => $this->getPayPalConfigValue('api_username'),
            'PWD' => $this->getPayPalConfigValue('api_password'),
            'SIGNATURE' => $this->getPayPalConfigValue('api_signature'),
            'BUTTONSOURCE' => 'Magento_Cart_Community'
        ];

        $request = array_merge($request, $requestKeys);
        $response = $this->call(self::GET_EXPRESS_CHECKOUT_DETAILS, $request);
        
        return $response;
    }
    
    /**
     * DoExpressCheckout call
     *
     * @return void
     * @link https://cms.paypal.com/us/cgi-bin/?&cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoExpressCheckoutPayment
     */
    public function callDoExpressCheckoutPayment($requestData)
    {
        $request = $requestData;
        $request['ADDROVERRIDE'] = 0;
        $request['PAYMENTACTION'] = 'Sale';
        
        $requestKeys = [
            'METHOD' => 'GetExpressCheckoutDetails',
            'VERSION' => '72.0',
            'USER' => $this->getPayPalConfigValue('api_username'),
            'PWD' => $this->getPayPalConfigValue('api_password'),
            'SIGNATURE' => $this->getPayPalConfigValue('api_signature'),
            'BUTTONSOURCE' => 'Magento_Cart_Community'
        ];

        $request = array_merge($request, $requestKeys);
        $response = $this->call(self::DO_EXPRESS_CHECKOUT_PAYMENT, $request);
        
        return $response;
        
    }
    
    /**
     * PayPal web URL generic getter
     *
     * @param array $params
     * @return string
     */
    public function getPaypalUrl(array $params = [])
    {
        return sprintf(
            'https://www.%spaypal.com/cgi-bin/webscr%s',
            $this->getPayPalConfigValue('sandbox_flag') ? 'sandbox.' : '',
            $params ? '?' . http_build_query($params) : ''
        );
    }
    
    /**
     * @return \Magento\Paypal\Model\Api\Nvp
     */
    public function _getApi()
    {
        if (null === $this->api) {
            $this->api = $this->_apiTypeFactory->create($this->_apiType)->setConfigObject($this->_config);
        }
        return $this->api;
    }
    
    /**
     * Do the API call
     *
     * @param string $methodName
     * @param array $request
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function call($methodName, array $request)
    {
        $request = $this->_addMethodToRequest($methodName, $request);
        $eachCallRequest = ["VERSION","USER","PWD","SIGNATURE","BUTTONSOURCE"];
        $logger = $this->_objectManager->create('Psr\Log\LoggerInterface');

        if ($this->getUseCertAuthentication()) {
            $key = array_search('SIGNATURE', $eachCallRequest);
            if ($key) {
                unset($eachCallRequest[$key]);
            }
        }
        $request = $this->_exportToRequest($eachCallRequest, $request);
        $debugData = ['url' => $this->getApiEndpoint(), $methodName => $request];
        
        try {
            $http = $this->_curlFactory->create();
            $config = ['timeout' => 60, 'verifypeer' => $this->_config->getValue('verifyPeer')];
            // TODO: handle use proxy.
            if (false && $this->getUseProxy()) {
                $config['proxy'] = $this->getProxyHost() . ':' . $this->getProxyPort();
            }
            if ($this->getUseCertAuthentication()) {
                $config['ssl_cert'] = $this->getApiCertificate();
            }
            $http->setConfig($config);
            $http->write(
                \Zend_Http_Client::POST,
                $this->getApiEndpoint(),
                '1.1',
                $this->_headers,
                $this->_buildQuery($request)
            );
            $response = $http->read();
        } catch (\Exception $e) {
            $debugData['http_error'] = ['error' => $e->getMessage(), 'code' => $e->getCode()];
            $logger->debug($debugData);
            throw $e;
        }
        
        $response = preg_split('/^\r?$/m', $response, 2);
        $response = trim($response[1]);
        $response = $this->_deformatNVP($response);

        $debugData['response'] = $response;
        $response = $this->_postProcessResponse($response);
        
        // handle transport error
        if ($http->getErrno()) {

            $logger->critical(
                new \Exception(
                    sprintf('PayPal NVP CURL connection error #%s: %s', $http->getErrno(), $http->getError())
                )
            );
            $http->close();

            throw new \Magento\Framework\Exception\LocalizedException(
                __('Payment Gateway is unreachable at the moment. Please use another payment option.')
            );
        }

        // cUrl resource must be closed after checking it for errors
        $http->close();

        if (!$this->_validateResponse($methodName, $response)) {
            $logger->critical(new \Exception(__('PayPal response hasn\'t required fields.')));
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Something went wrong while processing your order.')
            );
        }

        $this->_callErrors = [];
        if ($this->_isCallSuccessful($response)) {
            return $response;
        }
        
        $this->_handleCallErrors($response);

        return $response;
    }
    
    /**
     * Add method to request array
     *
     * @param string $methodName
     * @param array $request
     * @return array
     */
    public function _addMethodToRequest($methodName, $request)
    {
        $request['METHOD'] = $methodName;
        return $request;
    }
    
    /**
     * Check whether API certificate authentication should be used
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getUseCertAuthentication()
    {
        $value =  $this->scopeConfig->getValue(
            'paypal/wpp/apiAuthentication',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return (bool)$value;
    }
    
        /**
     * API endpoint getter
     *
     * @return string
     */
    public function getApiEndpoint()
    {
        $url = $this->getUseCertAuthentication() ? 'https://api%s.paypal.com/nvp' : 'https://api-3t%s.paypal.com/nvp';
        return sprintf($url, $this->getPayPalConfigValue('sandbox_flag') ? '.sandbox' : '');
    }
        
     /**
     * API endpoint getter
     *
     * @return string
     */
    public function getPayPalConfigValue($key)
    {
        $path = 'paypal/wpp/'.$key;
        $value =  $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return $value;
    }
    
    /**
     * Payment API authentication methods source getter
     *
     * @return array
     */
    public function getApiAuthenticationMethods()
    {
        return ['0' => __('API Signature'), '1' => __('API Certificate')];
    }

    /**
     * Api certificate getter
     *
     * @return string
     */
    public function getApiCertificate()
    {
        $websiteId = $this->_storeManager->getStore($this->_storeManager->getStore()->getStoreId())->getWebsiteId();
        return $this->_certFactory->create()->loadByWebsite($websiteId, false)->getCertPath();
    }
    
    /**
     * Return Paypal Api proxy status based on config data
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getUseProxy() {
        return $this->_getDataOrConfig('use_proxy', false);
    }
    
    /**
     * Unified getter that looks in data or falls back to config
     *
     * @param string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function _getDataOrConfig($key, $default = null)
    {
        if ($this->hasData($key)) {
            return $this->getData($key);
        }
        return $this->_config->getValue($key) ? $this->_config->getValue($key) : $default;
    }
    
    /**
     * Build query string from request
     *
     * @param array $request
     * @return string
     */
    public function _buildQuery($request) {
        return http_build_query($request);
    }
    
    /**
     * Parse an NVP response string into an associative array
     * @param string $nvpstr
     * @return array
     */
    public function _deformatNVP($nvpstr) {
        $intial = 0;
        $nvpArray = [];

        $nvpstr = strpos($nvpstr, "\r\n\r\n") !== false ? substr($nvpstr, strpos($nvpstr, "\r\n\r\n") + 4) : $nvpstr;

        while (strlen($nvpstr)) {
            //postion of Key
            $keypos = strpos($nvpstr, '=');
            //position of value
            $valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen($nvpstr);

            /*getting the Key and Value values and storing in a Associative Array*/
            $keyval = substr($nvpstr, $intial, $keypos);
            $valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);
            //decoding the respose
            $nvpArray[urldecode($keyval)] = urldecode($valval);
            $nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
        }
        return $nvpArray;
    }
    
    /**
     * Export $this public data to private request array
     *
     * @param array $privateRequestMap
     * @param array $request
     * @return array
     */
    public function &_exportToRequest(array $privateRequestMap, array $request = [])
    {
        $map = [];
        foreach ($privateRequestMap as $key) {
            if (isset($this->_globalMap[$key])) {
                $map[$this->_globalMap[$key]] = $key;
            }
        }
        $result = \Magento\Framework\DataObject\Mapper::accumulateByMap([$this, 'getDataUsingMethod'], $request, $map);
        foreach ($privateRequestMap as $key) {
            if (isset($this->_exportToRequestFilters[$key]) && isset($result[$key])) {
                $callback = $this->_exportToRequestFilters[$key];
                $privateKey = $result[$key];
                $publicKey = $map[$this->_globalMap[$key]];
                $result[$key] = call_user_func([$this, $callback], $privateKey, $publicKey);
            }
        }
        return $result;
    }
    
        /**
     * Check the EC request against unilateral payments mode and remove the SUBJECT if needed
     *
     * @param &array $requestFields
     * @return void
     */
    public function _prepareExpressCheckoutCallRequest(&$requestFields)
    {
        if (!$this->_config->shouldUseUnilateralPayments()) {
            $key = array_search('SUBJECT', $requestFields);
            if ($key) {
                unset($requestFields[$key]);
            }
        }
    }
    
        /**
     * Additional response processing.
     * Hack to cut off length from API type response params.
     *
     * @param array $response
     * @return array
     */
    public function _postProcessResponse($response)
    {
        foreach ($response as $key => $value) {
            $pos = strpos($key, '[');

            if ($pos === false) {
                continue;
            }

            unset($response[$key]);

            if ($pos !== 0) {
                $modifiedKey = substr($key, 0, $pos);
                $response[$modifiedKey] = $value;
            }
        }

        return $response;
    }
    
    /**
     * Validate response array.
     *
     * @param string $method
     * @param array $response
     * @return bool
     */
    public function _validateResponse($method, $response)
    {
        if (isset($this->_requiredResponseParams[$method])) {
            foreach ($this->_requiredResponseParams[$method] as $param) {
                if (!isset($response[$param])) {
                    return false;
                }
            }
        }
        return true;
    }
    
    /**
     * Catch success calls and collect warnings
     *
     * @param array $response
     * @return bool success flag
     */
    public function _isCallSuccessful($response)
    {
        if (!isset($response['ACK'])) {
            return false;
        }

        $ack = strtoupper($response['ACK']);
        $this->_callWarnings = [];
        if ($ack == 'SUCCESS' || $ack == 'SUCCESSWITHWARNING') {
            // collect warnings
            if ($ack == 'SUCCESSWITHWARNING') {
                for ($i = 0; isset($response["L_ERRORCODE{$i}"]); $i++) {
                    $this->_callWarnings[] = $response["L_ERRORCODE{$i}"];
                }
            }
            return true;
        }
        return false;
    }
    
        /**
     * Import $this public data from a private response array
     *
     * @param array $privateResponseMap
     * @param array $response
     * @return void
     */
    public function _importFromResponse(array $privateResponseMap, array $response)
    {
        $map = [];
        foreach ($privateResponseMap as $key) {
            if (isset($this->_globalMap[$key])) {
                $map[$key] = $this->_globalMap[$key];
            }
            if (isset($response[$key]) && isset($this->_importFromRequestFilters[$key])) {
                $callback = $this->_importFromRequestFilters[$key];
                $response[$key] = call_user_func([$this, $callback], $response[$key], $key, $map[$key]);
            }
        }
        \Magento\Framework\DataObject\Mapper::accumulateByMap($response, [$this, 'setDataUsingMethod'], $map);
    }
    
    /**
     * Create billing and shipping addresses basing on response data
     *
     * @param array $data
     * @return void
     */
    public function _exportAddressses($data)
    {
        $address = new \Magento\Framework\DataObject();
        \Magento\Framework\DataObject\Mapper::accumulateByMap($data, $address, $this->_billingAddressMap);
        $address->setExportedKeys(array_values($this->_billingAddressMap));
        $this->_applyStreetAndRegionWorkarounds($address);
        $this->setExportedBillingAddress($address);
        // assume there is shipping address if there is at least one field specific to shipping
        if (isset($data['SHIPTONAME'])) {
            $shippingAddress = clone $address;
            \Magento\Framework\DataObject\Mapper::accumulateByMap($data, $shippingAddress, $this->_shippingAddressMap);
            $this->_applyStreetAndRegionWorkarounds($shippingAddress);
            // PayPal doesn't provide detailed shipping name fields, so the name will be overwritten
            $shippingAddress->addData(['firstname'  => $data['SHIPTONAME']]);
            $this->setExportedShippingAddress($shippingAddress);
        }
    }
    
    /**
     * Handle logical errors
     *
     * @param array $response
     * @return void
     * @throws \Magento\Paypal\Model\Api\ProcessableException|\Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function _handleCallErrors($response)
    {
        $logger = $this->_objectManager->create('Psr\Log\LoggerInterface');

        $errors = $this->_extractErrorsFromResponse($response);
        if (empty($errors)) {
            return;
        }

        $errorMessages = [];
        foreach ($errors as $error) {
            $errorMessages[] = $error['message'];
            $this->_callErrors[] = $error['code'];
        }
        $errorMessages = implode(' ', $errorMessages);

        $exceptionLogMessage = sprintf(
            'PayPal NVP gateway errors: %s Correlation ID: %s. Version: %s.',
            $errorMessages,
            isset($response['CORRELATIONID']) ? $response['CORRELATIONID'] : '',
            isset($response['VERSION']) ? $response['VERSION'] : ''
        );
        $logger->critical($exceptionLogMessage);

        $exceptionPhrase = __('PayPal gateway has rejected request. %1', $errorMessages);

        /** @var \Magento\Framework\Exception\LocalizedException $exception */
        $firstError = $errors[0]['code'];
        $exception = $this->_processableExceptionFactory->create(
                ['phrase' => $exceptionPhrase, 'code' => $firstError]
            );

        throw $exception;
    }
    
    /**
     * Extract errors from PayPal's response and return them in array
     *
     * @param array $response
     * @return array
     */
    public function _extractErrorsFromResponse($response)
    {
        $errors = [];

        for ($i = 0; isset($response["L_ERRORCODE{$i}"]); $i++) {
            $errorCode = $response["L_ERRORCODE{$i}"];
            $errorMessage = $this->_formatErrorMessage(
                $errorCode,
                $response["L_SHORTMESSAGE{$i}"],
                isset($response["L_LONGMESSAGE{$i}"]) ? $response["L_LONGMESSAGE{$i}"] : null
            );
            $errors[] = [
                'code' => $errorCode,
                'message' => $errorMessage,
            ];
        }

        return $errors;
    }
    
    /**
     * Format error message from error code, short error message and long error message
     *
     * @param string $errorCode
     * @param string $shortErrorMessage
     * @param string $longErrorMessage
     * @return string
     */
    public function _formatErrorMessage($errorCode, $shortErrorMessage, $longErrorMessage)
    {
        $longErrorMessage  = preg_replace('/\.$/', '', $longErrorMessage);
        $shortErrorMessage = preg_replace('/\.$/', '', $shortErrorMessage);

        return $longErrorMessage ? sprintf('%s (#%s: %s).', $longErrorMessage, $errorCode, $shortErrorMessage)
            : sprintf('#%s: %s.', $errorCode, $shortErrorMessage);
    }
    
    /**
     * Check whether PayPal error can be processed
     *
     * @param int $errorCode
     * @return bool
     */
    public function _isProcessableError($errorCode)
    {
        $processableErrorsList = $this->getProcessableErrors();

        if (!$processableErrorsList || !is_array($processableErrorsList)) {
            return false;
        }

        return in_array($errorCode, $processableErrorsList);
    }
}
