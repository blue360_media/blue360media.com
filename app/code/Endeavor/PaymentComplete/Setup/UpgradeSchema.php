<?php 
namespace Endeavor\PaymentComplete\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var string
     */
    private static $connectionName = 'sales_order';
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addSalesOrderCustomFields($setup);
        }
        
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->updateLastPaidAmount($setup);
        }
        
    }

    /**
     * Add Last Paid Amount.
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function addSalesOrderCustomFields(SchemaSetupInterface $setup) {
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$connectionName),
            'last_paid_amount',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'nullable' => false,
                'length' => '12,4',
                'default' => '0.0000',
                'comment' => 'Last Paid Amount'
            ]
        );
        
        $setup->getConnection()->addColumn(
            $setup->getTable(self::$connectionName),
            'sf_grand_total',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'nullable' => false,
                'length' => '12,4',
                'default' => '0.0000',
                'comment' => 'Salesforce Grand Total'
            ]
        );

        return $this;
    }
    
    /**
     * Add Last Paid Amount.
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    protected function updateLastPaidAmount(SchemaSetupInterface $setup) {
        $setup->getConnection()->modifyColumn(
            $setup->getTable(self::$connectionName),
            'last_paid_amount',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'nullable' => true,
                'length' => '12,4',
                'default' => null,
                'comment' => 'Last Paid Amount'
            ]
        );                
    }
}
