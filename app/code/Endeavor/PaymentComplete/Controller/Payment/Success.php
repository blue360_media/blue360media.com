<?php
namespace Endeavor\PaymentComplete\Controller\Payment;

/**
 * @abstract Capture Payment amount and make Specific Order as Complete payment.
 *
 * @author Murad Dweikat
 * @date: 2017/11/29
 */
class Success extends \Magento\Framework\App\Action\Action {
    
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
        
    /**
     *
     * @var Magento\Framework\App\Action\Context 
     */
    protected $_context;
    
    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_context = $context;

        parent::__construct($context);
    }
    
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {        
        $resultPage = $this->resultPageFactory->create();        
        return $resultPage;
    }
    
}
