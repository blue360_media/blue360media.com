<?php
namespace Endeavor\PaymentComplete\Controller\Payment;

use Magento\Framework\Controller\ResultFactory;

/**
 * @abstract Capture Payment amount and make Specific Order as Complete payment.
 *
 * @author Murad Dweikat
 * @date: 2017/11/29
 */
class Complete extends \Magento\Framework\App\Action\Action {
    
    /**
     * Complete Payment Order Status
     */
    const PAYMENT_COMPLETE = 'complete_payment';
    
    /**
     *
     * @var type 
     */
    protected $_messageManager;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $formKey;
    
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**
     *
     * @var Magento\Customer\Model\Session 
     */
    protected $customerSession;

    /**
     *
     * @var Magento\Framework\App\Action\Context 
     */
    protected $_context;
	
	protected $logger;
    
    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Customer\Model\Session $customerSession,
		\Psr\Log\LoggerInterface $logger,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->formKey = $formKey;
        $this->scopeConfig = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
        $this->customerSession = $customerSession;
		$this->logger = $logger;
        $this->_context = $context;
        $this->_messageManager = $this->_context->getMessageManager();

        parent::__construct($context);
    }
    
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $postValues = $this->getRequest()->getPostValue();
        $isSuccess = false;
        if (empty($postValues['email'])) {
            $errorMessage = __('Please Enter Your email to send the invoice information.');
            $this->_messageManager->addErrorMessage(__($errorMessage));
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultPage->setPath('order/payment/index');
            return $resultPage;
        } 
        
        if (!empty($postValues)) {
            $sfOrderId = (isset($postValues['order'])) ? $postValues['order'] : '';
            try {
                
                $totalAmount = $postValues['amount'];
                $grandTotal = $postValues['grandTotal'];
                $cardNumber = $postValues['cardnumber'];
                $email = $postValues['email'];
                $expirationDate = $postValues['year'].'-'.$postValues['month'];
                $cardCode = $postValues['cvv'];
                $billToPhoneNumber = $postValues['telephone'];
                $billToCountry = $postValues['country_id'];
                $billToZip = $postValues['postcode'];
                $billRegionId = $postValues['region_id'];
                $billToCity = $postValues['city'];
                $address = $postValues['street'];
                $billToAddress = $address[1].' '.$address[2];
                $name = explode(' ', $postValues['owner']);
                $billToFirstName = $name[0];
                $billToLastName = (isset($name[1])) ? $name[1] : '';
                
                $salesOrder = $this->_objectManager->create('\Magento\Sales\Model\Order')
                        ->load($sfOrderId, 'sf_order_id');

                $countryRegion = $this->_objectManager->create('Magento\Directory\Model\Region')
                        ->load($billRegionId);
                $billToState = $countryRegion->getDefaultName();
                
                if ($salesOrder && ($salesOrder->getStatus() == 'complete_payment')) {
                    $errorMessage = 'The Order #'.$sfOrderId.' is already paid.';
                    $this->_messageManager->addErrorMessage(__($errorMessage));
                    $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultPage->setPath('order/payment/index');

                }
                $customerId = $this->customerSession->getCustomer()->getId();

                if ($salesOrder->getCustomerId() == $customerId) {
                    $orderId = $salesOrder->getId();
                    $orderUrl = $this->_url->getUrl('sales/order/view',['order_id' => $orderId]);
                    $this->getRequest()->setParams(['orderUrl' => $orderUrl, 'isOrderVisable' => true]);

                } else {
                    $this->getRequest()->setParams(['orderUrl' => '', 'isOrderVisable' => false]);

                }
                $payment = $salesOrder->getPayment();
                
                if ($payment) {

                    $payment->setMethod('authnetcim');

                    /** @var \ParadoxLabs\Authnetcim\Model\Gateway $gateway */
                    $gateway = $this->_objectManager->create('\ParadoxLabs\Authnetcim\Model\Gateway');

                    $testConfig = $this->scopeConfig->getValue('payment/authnetcim/test', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    $parameters['test_mode'] = ($testConfig == '1')? true : false;
                    $login = $this->scopeConfig->getValue('payment/authnetcim/login', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                    $trans_key = $this->scopeConfig->getValue('payment/authnetcim/trans_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
					
					
					//$parameters['test_mode'] = true;
                    //$login = "49rDsw9AN";
                    //$trans_key = "3tW85g6Kd9236JpK";

                    $parameters['login'] = $login;
                    $parameters['password'] = $trans_key;
                    $gateway->init($parameters);

                    $gateway->setParameter('billToFirstName', $billToFirstName);
                    $gateway->setParameter('billToLastName', $billToLastName);
                    $gateway->setParameter('billToAddress', $billToAddress);
                    $gateway->setParameter('billToCity', $billToCity);
                    $gateway->setParameter('billToState', $billToState);
                    $gateway->setParameter('billToZip', $billToZip);
                    $gateway->setParameter('billToCountry', $billToCountry);
                    $gateway->setParameter('billToPhoneNumber', $billToPhoneNumber);
                    $gateway->setParameter('cardNumber', $cardNumber);
                    $gateway->setParameter('expirationDate', $expirationDate);
                    $gateway->setParameter('cardCode', $cardCode);
                    $gateway->setParameter('email', $email);
                    $gateway->setTransactionId('');

                    $errorMessage = '';
                    try {
                        $result = $gateway->capture($payment, $totalAmount, null);
						$this->logger->info("shbt capture");
						$this->logger->info(print_r($result->getData(), true));
                    } catch (\Exception $ex) {
                        $errorMessage = $ex->getMessage();
                        $this->getRequest()->setParams(['status' => 0, 'message' => $errorMessage]);
                    }

                    if(!empty($result)) {
						$this->logger->info("shbt transaction_id");
						
                        $information = $payment->getData('additional_information');
						if(!is_array($information)){
							$information = unserialize($information);
						}						
						$this->logger->info($result->getData('transaction_id'));
						$this->logger->info(print_r($information, true));
						
                        $information['transaction_id'] = $result->getData('transaction_id');
						
                        $information['card_type'] = $result->getData('card_type');
                        $information = $payment->setData('additional_information', $information);
                        $last4Cc = substr($result->getData('acc_number'), -4);
                        $payment->setCcLast4($last4Cc);
                        $payment->setCcOwner(implode(' ',$name));
						$this->logger->info("shbt done");
						$this->logger->info($last4Cc);
                    }

                    if (isset($result['is_error']) && !$result['is_error']) {
                        try {
                            $previousStatus = $salesOrder->getStatus();
							
							$this->logger->info("shbt status");
							$this->logger->info($previousStatus);
                            //TODO: handle make partial paid payment. ($totalAmount + $prevPaidAmount).
                            $salesOrder->setTotalPaid($grandTotal);
                            $salesOrder->setBaseTotalPaid($grandTotal);
                            $salesOrder->setStatus(self::PAYMENT_COMPLETE);
                            $salesOrder->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
                            $salesOrder->setLastPaidAmount($totalAmount);
                            $salesOrder->setSfGrandTotal($grandTotal);
                            
                            $billingAddressData = [
                                "region_id" => $billRegionId,
                                "region" => $billToState,
                                "postcode" => $billToZip,
                                "street" => $address,
                                "city" => $billToCity,
                                "email" => $email,
                                "telephone" => $billToPhoneNumber,
                                "firstname" => $billToFirstName,
                                "lastname" => $billToLastName
                            ];
                            $salesOrder->getBillingAddress()->addData($billingAddressData);
                            $customerEmail = $salesOrder->getCustomerEmail();
                            $customerFirstname = $salesOrder->getCustomerFirstname();
                            $customerLastname = $salesOrder->getCustomerLastname();
							
							$this->logger->info("shbt order");
							$this->logger->info($customerFirstname);
							
							$this->logger->info("shbt order");
							$this->logger->info($customerEmail);

                            $salesOrder->setCustomerEmail($email);
                            $salesOrder->setCustomerFirstname($billToFirstName);
                            $salesOrder->setCustomerLastname($billToLastName);
							
							$this->logger->info("shbt order");
							$this->logger->info($billToFirstName);
							
                            $salesOrder->save();
                            
                            $invoiceService = $this->_objectManager->create('\Magento\Sales\Model\Service\InvoiceService');
                            $invoice = $invoiceService->prepareInvoice($salesOrder);

                            $invoice->setCustomerNoteNotify('1');
                            $invoice->setState('2');
                            $invoice->save();
                            
                            $invoiceComment = $this->_objectManager->create('Magento\Sales\Api\Data\InvoiceCommentInterface');

                            $comment = 'Your total paid order: '.$salesOrder->getTotalPaid()."<br>"
                                    .'And you paid: '.$result['amount'];
                            $invoiceComment->setComment($comment);
                            $invoiceComment->setIsVisibleOnFront(0);
                            $invoiceComment->setIsCustomerNotified(1);
                            $invoiceComment->setEntityName('invoice');
                            $invoiceComment->setParentId($invoice->getId());
                            $invoiceComment->save();

                            $invoice->setComments($invoiceComment);
                            $invoice->save();

                            $invoiceService->notify($invoice->getId());
                            
                            $statusHistory = $this->_objectManager->create('Magento\Sales\Api\Data\OrderStatusHistoryInterface');
                            $comment = 'Captured amount of $'.$result['amount'].' online. Transaction ID: "'
                                    .$result->getData('transaction_id').'" <br> Previous Status: '.$previousStatus;
                            $statusHistory->setComment($comment);
                            $statusHistory->setStatus(self::PAYMENT_COMPLETE);
                            $statusHistory->setIsVisibleOnFront(0);
                            $statusHistory->setIsCustomerNotified(0);
                            $statusHistory->setEntityName('order');
                            $statusHistory->setParentId($salesOrder->getId());
                            $statusHistory->save();
                            
                            /**
                             * @author Murad Dweikat <mdweikat@endeavorpal.com>
                             * @desc: update customer and order info. if fill valid email and not exist before.
                             */
                            if (strpos($customerEmail, 'dummy') !== false) {
                                $CustomerModel = $this->_objectManager->create('Magento\Customer\Model\Customer');
                                $storeManager = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface'); 
                                $storeId = $storeManager->getStore()->getStoreId();
                                $CustomerModel->setWebsiteId($storeId);
                                $CustomerModel->loadByEmail($email);
                                
                                if (!$CustomerModel->getId()) {
                                    $CustomerModel = $this->_objectManager->create('Magento\Customer\Model\Customer');
                                    $CustomerModel->setWebsiteId($storeId);
                                    $CustomerModel->load($salesOrder->getCustomerId());
                                    $CustomerModel->setEmail($email);
                                    $CustomerModel->save();
                                }
                                
                            } else {
                                $salesOrder->setCustomerEmail($customerEmail);
                                $salesOrder->setCustomerFirstname($customerFirstname);
                                $salesOrder->setCustomerLastname($customerLastname);
                                $salesOrder->save();

                            }
                            foreach ($salesOrder->getItems() as $orderItem) {
                                $orderItem->setQtyInvoiced($orderItem->getQtyOrdered());
                                $orderItem->save();
                            }
                            
                            $isSuccess = true;
                            
                        } catch (\Exception $ex) {
                            $errorMessage = __('Somthing wrong! Please try within 24 hour.');
                            $this->_messageManager->addErrorMessage(__($errorMessage));
                            $subject = 'Outstanding Invoice Payment by Authorize.net Issue';
                            $this->sendErrorEmailMessage($sfOrderId, $subject, $ex->getMessage().' Trace:'.$ex->getTraceAsString());
                            
                        }
                        
                        $this->getRequest()->setParams(['status' => 1, 'message' => __('Your Payment Success Completely.')]);
                    } else {
                        $this->_messageManager->addErrorMessage($errorMessage);
                        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                        $resultPage->setPath('order/payment/index');

                    }

                } else {
                    $errorMessage = __('Somthing wrong! Please try within 24 hour.');
                    $this->_messageManager->addErrorMessage(__($errorMessage));
                    $subject = 'Outstanding Invoice Payment by Authorize.net Issue';
                    $this->sendErrorEmailMessage($sfOrderId, $subject, __('Can\'t Create Payment for the order.'));

                    $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultPage->setPath('outstandinginvoice/index/index');
                }
            } catch (\Exception $ex) {
                $errorMessage = __('Somthing wrong! Please try within 24 hour.');
                $this->_messageManager->addErrorMessage(__($errorMessage));
                $subject = 'Outstanding Invoice Payment by Authorize.net Issue';
                $this->sendErrorEmailMessage($sfOrderId, $subject, $ex->getMessage().' Trace:'.$ex->getTraceAsString());

                $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultPage->setPath('outstandinginvoice/index/index');
            }
        }
        if (empty($resultPage) && $isSuccess) {
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $sfOrderId = (isset($postValues['order'])) ? $postValues['order'] : '';
            $queryParams = [
                'sf_order_id' => $sfOrderId,
                'status' => 1
            ];
            $resultPage->setPath('order/payment/success', ['_query' => $queryParams]);
        } else {
            $errorMessage = 'Somthing wrong! Please try within 24 hour.';
            $this->_messageManager->addErrorMessage(__($errorMessage));
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultPage->setPath('outstandinginvoice/index/index');
        }
        
        return $resultPage;
    }
    
    /**
     * Send Email by Pre-Order status for support team.
     * 
     * @param type $orderNumber
     * @param type $subject
     * @param type $message
     */
    private function sendErrorEmailMessage($orderNumber, $subject , $message) {
        
        $emailData = [];
        $emailData['sf_order_id'] = $orderNumber;
        $emailData['subject'] = $subject;
        $emailData['message'] = $message;
        
        // call send mail method from helper  
        $emailHelper = $this->_objectManager->get('Endeavor\PaymentComplete\Helper\Email');
                
        $emailHelper->sendErrorEmailMessage(
            $emailData
        );
    }
    
}
