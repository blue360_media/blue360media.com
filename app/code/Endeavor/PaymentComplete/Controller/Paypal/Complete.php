<?php
namespace Endeavor\PaymentComplete\Controller\Paypal;

use Magento\Framework\Controller\ResultFactory;

/**
 * @abstract Capture Payment amount and make Specific Order as Complete payment.
 *
 * @author Murad Dweikat
 * @date: 2017/11/29
 * 
 * @updated_by: Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date: 17-1-2018.
 * @desc:  in order to send email when paypal is used in the outstanding invoice.
 */
class Complete extends \Magento\Framework\App\Action\Action {
    
    /*
     * 
     */
    const PAYMENT_COMPLETE = 'complete_payment';
    
    /**
     *
     * @var type 
     */
    protected $_messageManager;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $formKey;
    
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**
     *
     * @var Endeavor\PaymentComplete\Model\Paypal\Gateway
     */
    protected $api;

    /**
     *
     * @var Magento\Framework\App\Action\Context 
     */
    protected $_context;
        
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     *
     * @var Magento\Customer\Model\Session 
     */
    protected $customerSession;

    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Paypal\Model\Api\Nvp $paypalApi
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Endeavor\PaymentComplete\Model\Paypal\Gateway $paypalApi,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->formKey = $formKey;
        $this->scopeConfig = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
        $this->api = $paypalApi;
        $this->_context = $context;
        $this->_messageManager = $this->_context->getMessageManager();
        $this->_storeManager = $storeManager;
        $this->customerSession = $customerSession;

        parent::__construct($context);
    }
    
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $logger = $this->_objectManager->create('Endeavor\PaymentComplete\Logger\Logger');
        $resultPage = null;
        $params = $this->getRequest()->getParams();

        $token = ['TOKEN' => $params['token']];        
        $payerId = $params['PayerID'];
        $sfOrderId = $params['sf_order_id'];
        $status = $params['status'];
        $totalAmount = $params['amountDue'];
        $grandTotal = $params['grandTotal'];
        $amountPaid = $params['amountPaid'];
        $invoiceNumber = $params['invoiceNumber'];
        
        $logger->info('CallGetExpressCheckoutDetails - Request', $token);
        
        // Check Payment information on paypal.
        $getExpressResponseToken = $this->api->callGetExpressCheckoutDetails($token);

        $logger->info('CallGetExpressCheckoutDetails - Response', $getExpressResponseToken);

        $isSameAmount = true;
        $resultPage = null;
        if ($getExpressResponseToken['PAYMENTREQUEST_0_AMT'] < $totalAmount) {
            $isSameAmount = false;
            $errorMessage = 'Somthing wrong! You try to pay less than Amount Due, Please make full payment.';
            $this->_messageManager->addErrorMessage($errorMessage); 
            
            $subject = 'Outstanding Invoice Page issue';
            $this->sendErrorEmailMessage($sfOrderId, $subject, $errorMessage.'<br>PAYMENTREQUEST_0_AMT: '
                    .$getExpressResponseToken['PAYMENTREQUEST_0_AMT'].', and Amount Due: '.$totalAmount);
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultPage->setPath('outstandinginvoice/index/index');
            
        } else {
            $salesOrder = $this->_objectManager->create('\Magento\Sales\Model\Order')
                        ->load($sfOrderId, 'sf_order_id');
            $doExpressResponseToken = '';
            if ($salesOrder && $salesOrder->getId()) {

                $logger->info('callDoExpressCheckoutPayment - Request', $getExpressResponseToken);

                // send request to capture amount by using payment info. from previous method.
                $doExpressResponseToken = $this->api->callDoExpressCheckoutPayment($getExpressResponseToken);

                $logger->info('callDoExpressCheckoutPayment - Response', $doExpressResponseToken);

                if (!$this->api->_isCallSuccessful($doExpressResponseToken)) {
                    $errors = $this->api->_extractErrorsFromResponse($doExpressResponseToken);
                    $errorsMsg = 'Failed to pay by PayPal:<br> Error Details:<br>';
                    foreach ($errors as $error) {
                        $errorsMsg .= $error['code'].': '.$error['message'].', ';
                        $this->_messageManager->addErrorMessage($error['code'].': '.$error['message']);
                    }
                    $subject = 'Outstanding Invoice Payment by PayPal Issue';
                    $this->sendErrorEmailMessage($sfOrderId, $subject, $errorsMsg);
                    $this->api->_handleCallErrors($doExpressResponseToken);

                    $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultPage->setPath('outstandinginvoice/index/index');
                    return $resultPage;

                }
            } else {
                $errorMessage = 'The Order not available at this time! Please try within 24 hour.';
                $this->_messageManager->addErrorMessage($errorMessage); 
                $this->sendErrorEmailMessage($sfOrderId, 'Outstanding Invoice Payment by PayPal Issue','You try to pay for order not found.');
                $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultPage->setPath('outstandinginvoice/index/index');

            }

            try {
                $payment = $salesOrder->getPayment();

                if ($payment && $isSameAmount) {

                    $payment->setMethod('paypal_express');
                    try {
                        // send email containing the warning message if exist.
                        if (!empty($this->api->_callWarnings)) {
                            $subject = 'Outstandign Invoice PayPal Warning Message';
                            $this->sendErrorEmailMessage($sfOrderId, $subject, json_encode($this->api->_callWarnings));
                        }

                        $previousStatus = $salesOrder->getStatus();
                        $information = $payment->getData('additional_information');
						
						$logger->info('information - payment information start', $information);
						if(!isset($information['method_title']) || !is_array($information) ) {
							$information = array();
							$information['method_title'] = '';
						}
						
                        $information['method_title'] = 'PayPal Express Checkout';
                        $information['transaction_id'] = $doExpressResponseToken['PAYMENTINFO_0_TRANSACTIONID'] ?? '';
                        $information['paypal_express_checkout_shipping_overridden'] = 1;
                        $information['paypal_correlation_id'] = $doExpressResponseToken['CORRELATIONID'] ?? '';
                        $information['paypal_payer_id'] = $getExpressResponseToken['PAYERID'] ?? '';
                        $information['paypal_payer_email'] = $getExpressResponseToken['EMAIL'] ?? '';
                        $information['paypal_payer_status'] = $getExpressResponseToken['PAYERSTATUS'] ?? '';
                        $information['paypal_address_status'] = $getExpressResponseToken['ADDRESSSTATUS'] ?? '';
                        $information['paypal_express_checkout_payer_id'] = $getExpressResponseToken['PAYERID'] ?? '';
                        $information['paypal_express_checkout_token'] = $token['TOKEN'] ?? '';
                        $information['paypal_payment_status'] = $doExpressResponseToken['PAYMENTINFO_0_PAYMENTSTATUS'] ?? '';
                        $payment->setData('additional_information', $information);
                        $payment->setAmountOrdered($amountPaid + $totalAmount);
                        $payment->setBaseAmountOrdered($amountPaid + $totalAmount);
                        $payment->setAmountPaid($amountPaid + $totalAmount);
                        $payment->setBaseAmountPaid($amountPaid + $totalAmount);
                        $payment->setLastTransId($doExpressResponseToken['PAYMENTINFO_0_TRANSACTIONID']);
						
						$logger->info('information - payment information end', $information);

                        $salesOrder->setTotalPaid($amountPaid + $totalAmount);
                        $salesOrder->setBaseTotalPaid($amountPaid + $totalAmount);
                        $salesOrder->setStatus(self::PAYMENT_COMPLETE);
                        $salesOrder->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
                        $salesOrder->setLastPaidAmount($totalAmount);
                        $salesOrder->setSfGrandTotal($grandTotal);

                        $email = $information['paypal_payer_email'];
                        $salesOrder->setCustomerEmail($email);
                        $salesOrder->save();

                        $invoiceService = $this->_objectManager->create('\Magento\Sales\Model\Service\InvoiceService');
                        $invoice = $invoiceService->prepareInvoice($salesOrder);

                        $invoice->setCustomerNoteNotify('1');
                        $invoice->setState('2');
                        $invoice->save();

                        $invoiceComment = $this->_objectManager->create('Magento\Sales\Api\Data\InvoiceCommentInterface');

                        $comment = 'Your total paid order: '.$salesOrder->getTotalPaid()."<br>"
                                .'And you paid: '.$totalAmount;
                        $invoiceComment->setComment($comment);
                        $invoiceComment->setIsVisibleOnFront(0);
                        $invoiceComment->setIsCustomerNotified(0);
                        $invoiceComment->setEntityName('invoice');
                        $invoiceComment->setParentId($invoice->getId());
                        $invoiceComment->save();

                        $invoice->setComments($invoiceComment);
                        $invoice->save();

                        $invoiceService->notify($invoice->getId());

                        $statusHistory = $this->_objectManager->create('Magento\Sales\Api\Data\OrderStatusHistoryInterface');
                        $comment = 'Captured amount of $'.$totalAmount.' online. Transaction ID: "'
                                .$doExpressResponseToken['PAYMENTINFO_0_TRANSACTIONID'].'" <br> Previous Status: '.$previousStatus;
                        $statusHistory->setComment($comment);
                        $statusHistory->setStatus(self::PAYMENT_COMPLETE);
                        $statusHistory->setIsVisibleOnFront(0);
                        $statusHistory->setIsCustomerNotified(0);
                        $statusHistory->setEntityName('order');
                        $statusHistory->setParentId($salesOrder->getId());
                        $statusHistory->save();

                        foreach ($salesOrder->getItems() as $orderItem) {
                            $orderItem->setQtyInvoiced($orderItem->getQtyOrdered());
                            $orderItem->save();
                        }

                    } catch (\Exception $ex) {
                        $errorMessage = 'Somthing wrong! Please try within 24 hour.';
                        $this->_messageManager->addErrorMessage(__($errorMessage));
                        $subject = 'Outstanding Invoice Payment by PayPal Issue';
                        $this->sendErrorEmailMessage($sfOrderId, $subject, $ex->getMessage().' Trace:'.$ex->getTraceAsString());
                        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                        $resultPage->setPath('outstandinginvoice/index/index');
                    }
                } else {
                    $errorMessage = 'Somthing wrong! Please try within 24 hour.';
                    $this->_messageManager->addErrorMessage(__($errorMessage));
                    $subject = 'Outstanding Invoice Payment by PayPal Issue';
                    $this->sendErrorEmailMessage($sfOrderId, $subject, __('Can\'t Create Payment for the order.'));
                    $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultPage->setPath('outstandinginvoice/index/index');
                }

            } catch (\Exception $ex) {
                $errorMessage = 'Somthing wrong! Please try within 24 hour.';
                $this->_messageManager->addErrorMessage(__($errorMessage));
                $subject = 'Outstanding Invoice Payment by PayPal Issue';

                $this->sendErrorEmailMessage($sfOrderId, $subject, $ex->getMessage().' Trace:'.$ex->getTraceAsString());

                $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultPage->setPath('outstandinginvoice/index/index');
            }

            if (empty($resultPage)) {
                $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $queryParams = [
                    'sf_order_id' => $this->getRequest()->getParam('sf_order_id'),
                    'invoiceNumber' => $this->getRequest()->getParam('invoiceNumber'),
                    'status' => 1
                ];
                $resultPage->setPath('order/payment/success', ['_query' => $queryParams]);
            }

            $logger->info('PayPal Transaction - End'.PHP_EOL);
        }
        return $resultPage;

    }
    
    /**
     * Send Email about error happened in outstanding payment for support team.
     * 
     * @param String $orderNumber
     * @param String $subject
     * @param String $message
     */
    private function sendErrorEmailMessage($orderNumber, $subject, $message) {
        
        $emailData = [];
        $emailData['sf_order_id'] = $orderNumber;
        $emailData['subject'] = $subject;
        $emailData['message'] = $message;
        
        // call send mail method from helper  
        $emailHelper = $this->_objectManager->get('Endeavor\PaymentComplete\Helper\Email');
                
        $emailHelper->sendErrorEmailMessage(
            $emailData
        );
    }
}
