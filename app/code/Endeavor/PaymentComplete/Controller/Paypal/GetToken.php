<?php
namespace Endeavor\PaymentComplete\Controller\Paypal;

use Magento\Framework\Controller\ResultFactory;

/**
 * @abstract Capture Payment amount and make Specific Order as Complete payment.
 *
 * @author Murad Dweikat
 * @date: 2017/11/29
 */
class GetToken extends \Magento\Framework\App\Action\Action {
    
    /**
     *
     * @var type 
     */
    protected $_messageManager;

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $formKey;
    
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**
     *
     * @var Endeavor\PaymentComplete\Model\Paypal\Gateway
     */
    protected $api;

    /**
     *
     * @var Magento\Framework\App\Action\Context 
     */
    protected $_context;
        
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
    
    /**
     *
     * @var Magento\Customer\Model\Session 
     */
    protected $customerSession;

    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Magento\Paypal\Model\Api\Nvp $paypalApi
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Endeavor\PaymentComplete\Model\Paypal\Gateway $paypalApi,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->formKey = $formKey;
        $this->scopeConfig = $scopeConfig;
        $this->resultPageFactory = $resultPageFactory;
        $this->api = $paypalApi;
        $this->_context = $context;
        $this->_messageManager = $this->_context->getMessageManager();
        $this->_storeManager = $storeManager;
        $this->customerSession = $customerSession;

        parent::__construct($context);
    }
    
    /**
     * execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $logger = $this->_objectManager->create('Endeavor\PaymentComplete\Logger\Logger');
        $postValues = $this->getRequest()->getPostValue();
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        
        $sfOrderId = $postValues['order'];
        $logger->info('PayPal Transaction - START');
        $logger->info('Salesforce Order Id: '.$sfOrderId);

        $logger->info('callSetExpressCheckout: Request:', $postValues);

        $tokenSEC = $this->callSetExpressCheckout($postValues);

        $logger->info('callSetExpressCheckout: Response:', $tokenSEC);
        
        // Redirct to PayPal if token valid.
        if ($this->api->_isCallSuccessful($tokenSEC)) {
            header('Location: '.$tokenSEC['request_url']);
            exit;
        }

        $resultPage->setPath('outstandinginvoice/index/index');
        
        return $resultPage;

    }
    
   /**
     * SetExpressCheckout call
     *
     * TODO: put together style and giropay settings
     *
     * @return void
     * @link https://cms.paypal.com/us/cgi-bin/?&cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_SetExpressCheckout
     */
    public function callSetExpressCheckout($postValues)
    {        
        return $this->api->callSetExpressCheckout($postValues);
    }
    
}
