<?php
namespace Endeavor\CheckoutStep\Controller\Account;

/**
 * @copyright (c) 2017, Endeavor Technology, Inc.
 * @author Murad Dweikat
 * 
 */
class Create extends \Magento\Checkout\Controller\Account\Create
{
    /**
     * Execute request
     *
     * @throws AlreadyExistsException
     * @throws NoSuchEntityException
     * @throws \Exception
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->_objectManager->get(\Magento\Framework\Controller\Result\JsonFactory::class)->create();

        if ($this->customerSession->isLoggedIn()) {
            return $resultJson->setData(
                [
                    'errors' => true,
                    'message' => __('Customer is already registered')
                ]
            );
        }
        $orderId = $this->checkoutSession->getLastOrderId();
        if (!$orderId) {
            return $resultJson->setData(
                [
                    'errors' => true,
                    'message' => __('Your session has expired')
                ]
            );
        }
        try {
            $customerService = $this->orderCustomerService->create($orderId);
            $customerId = $customerService->getId();
            /**
             * @author Murad Dweikat
             */
            $salesOrder = $this->_objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
            $quote = $this->_objectManager->create('\Magento\Quote\Model\Quote')->load($salesOrder->getQuoteId());
            $segment = explode('|',$quote->getGuestSegment());
            
            // check if two segment attribute saved.
            if (count($segment) == 2) {
                $marketSegment = $segment[0];
                $secondarySegment = $segment[1];
                
                $customer = $this->_objectManager->create('\Magento\Customer\Model\Customer')->load($customerId);
                $customerData = $customer->getDataModel();
                $customerData->setCustomAttribute('customer_market_segment', $marketSegment);
                $customerData->setCustomAttribute('customer_secondary_market', $secondarySegment);
                $customer->updateData($customerData);
                $customer->save();
            }
            
            return $resultJson->setData(
                [
                    'errors' => false,
                    'message' => __('A letter with further instructions will be sent to your email.')
                ]
            );
        } catch (\Exception $e) {
            $this->messageManager->addException($e, $e->getMessage());
            throw $e;
        }
    }
}