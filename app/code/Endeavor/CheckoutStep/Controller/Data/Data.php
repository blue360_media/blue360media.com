<?php
namespace Endeavor\CheckoutStep\Controller\Data;

/**
 * Description of Data
 *
 * @author mdwei
 */
class Data extends \Magento\Framework\App\Action\Action {
    
    /**
     *
     * @var Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->customerSession = $customerSession;

        parent::__construct($context);
    }
    
    
    /**
     * Ajax action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $segmentValue = $this->getRequest()->getParam('segment');

        $segmentOptions = '<option value="">Please Select ...</option>';
        $secondarySegmentOptions = '<option value="">Please Select ...</option>';
        
        $result = $this->resultJsonFactory->create();

        $marketSegment = 'customer_market_segment';
        $secondaryMarket = 'customer_secondary_market';
        
        $customer = $this->customerSession->getCustomer();
        
        $customerData = $customer->getDataModel();
        $marketSegmentOption = $customerData->getCustomAttribute($marketSegment);
        $marketSegmentValue = (isset($marketSegmentOption)) ? $marketSegmentOption->getValue() : ''; 
        $secondaryMarketOption = $customerData->getCustomAttribute($secondaryMarket); 
        $secondaryMarketValue = (isset($secondaryMarketOption)) ? $secondaryMarketOption->getValue() : ''; 

        $segmentCollecttion = $objectManager->create('Endeavor\CustomerAttributes\Model\Segment')
                ->getCollection(); 

        if ($this->getRequest()->isAjax()) {

            foreach ($segmentCollecttion as $value) {
                if (isset($marketSegmentValue) && $value->getEntityId() == $marketSegmentValue) {
                    $segmentOptions.= '<option value="'.$value->getEntityId().'" selected>'.$value->getName().'</option>';
                } else {
                    $segmentOptions.= '<option value="'.$value->getEntityId().'">'.$value->getName().'</option>';
                }
            }
        }
        
        $secondarySegmentCollecttion = $objectManager->create('Endeavor\CustomerAttributes\Model\SecondarySegment')
                ->getCollection();  
        if (isset($segmentValue) && $segmentValue != null) {
            $secondarySegmentCollecttion->addFieldToFilter('parent_id',['eq' => $segmentValue]);
        } else {
            $secondarySegmentCollecttion->addFieldToFilter('parent_id',['eq' => $marketSegmentValue]);
        }

        if ($this->getRequest()->isAjax()) {
            foreach ($secondarySegmentCollecttion as $value) {
                if (isset($secondaryMarketValue) &&  $value->getSegmentId() == $secondaryMarketValue){
                    $secondarySegmentOptions.= '<option value="'.$value->getSegmentId().'" selected>'.$value->getName().'</option>';
                } else {
                    $secondarySegmentOptions.= '<option value="'.$value->getSegmentId().'">'.$value->getName().'</option>';
                }
            }
        }
        
        
        $segOptions = [] ;
        $segOptions['segmentMarket'] = $segmentOptions;
        $segOptions['secondarySegmentMarket'] = $secondarySegmentOptions;
        
        return $result->setData($segOptions);

    }
}
