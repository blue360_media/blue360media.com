<?php
namespace Endeavor\CheckoutStep\Controller\Data;

/**
 * Description of Data
 *
 * @author mdwei
 */
class Save extends \Magento\Framework\App\Action\Action {
    
    /**
     *
     * @var Magento\Customer\Model\Session
     */
    protected $customerSession;
    
    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }
    
    /**
     * Save Customer Segment Type
     * Ajax action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute() {
        
        $segmentType = $this->getRequest()->getParam('segmentType');
        $secondaryMarket = $this->getRequest()->getParam('secondaryMarket');
        $customerId = $this->getRequest()->getParam('customerId');
        if (!empty($customerId)) {
            $customer = $this->_objectManager->create('\Magento\Customer\Model\Customer')->load($customerId);
            $customerData = $customer->getDataModel();
            $customerData->setCustomAttribute('customer_market_segment', $segmentType);
            $customerData->setCustomAttribute('customer_secondary_market', $secondaryMarket);
            $customer->updateData($customerData);
            $customer->save();
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
        $quote = $cart->getQuote();
        $quote->setGuestSegment($segmentType.'|'.$secondaryMarket);
        $quote->save();
        
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            return $result->setData('Customer attribute saved successfully.');
        }

    }
}
