<?php
/**
 * Copyright � 2017 Endeavor Technology, Inc. All rights reserved.
 * 
 * @author Murad Dweikat
 */
namespace Endeavor\CheckoutStep\Block;

class SegmentList extends \Magento\Framework\View\Element\Template implements \Magento\Framework\DataObject\IdentityInterface
{
    /**
     *
     * @var Endeavor\CustomerAttributes\Model\ResourceModel\Segment\CollectionFactory 
     */
    protected $_segmentCollection;

    /**
     *
     * @var Endeavor\CustomerAttributes\Model\ResourceModel\SecondarySegment\CollectionFactory 
     */
    protected $_secondarySegmentCollection;

    
    /**
     * 
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Endeavor\CustomerAttributes\Model\ResourceModel\Segment\CollectionFactory $segmentCollection
     * @param \Endeavor\CustomerAttributes\Model\ResourceModel\SecondarySegment\CollectionFactory $secondarySegmentCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Endeavor\CustomerAttributes\Model\ResourceModel\Segment\CollectionFactory $segmentCollection,
        \Endeavor\CustomerAttributes\Model\ResourceModel\SecondarySegment\CollectionFactory $secondarySegmentCollection,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_segmentCollection = $segmentCollection;
        $this->_secondarySegmentCollection = $secondarySegmentCollection;
    }

    /**
     * 
     * @return Endeavor\CustomerAttributes\Model\ResourceModel\Segment\Collection
     */
    public function getMarketSegments()
    {
        if (!$this->hasData('segments')) {
            $segments = $this->_segmentCollection->create();
            
            $this->setData('segments', $segments);
        }
        return $this->getData('segments');
    }
    
    /**
     * 
     * @return Endeavor\CustomerAttributes\Model\ResourceModel\SecondarySegment\Collection
     */
    public function getSecondarySegments($selectedSegmentValue)
    {
        if (!$this->hasData('secondarySegments')) {
            $secondarySegments = $this->_secondarySegmentCollection->create()
                    ->addFieldToFilter('parent_id',['eq' => $selectedSegmentValue]);
            $this->setData('secondarySegments', $secondarySegments);
        }
        return $this->getData('secondarySegments');
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Endeavor\CustomerAttributes\Model\Segment::CACHE_TAG . '_' . 'list',
            \Endeavor\CustomerAttributes\Model\SecondarySegment::CACHE_TAG . '_' . 'list'];
    }
       
}
