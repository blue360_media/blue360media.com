<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * By: Mohammad hamadneh
 * email:mhamadneh-t@endeavorpal.com
 */
namespace Endeavor\Map\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action 
{
    /**
     *
     * @var Magento\Framework\View\Result\PageFactory 
     */
    protected $_pageFactory;

    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @return type
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }
    /**
     * 
     * @return void
     */
    public function execute() {
        return $this->_pageFactory->create();
    }

}
