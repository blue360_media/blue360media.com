<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * By: Mohammad hamadneh
 * email:mhamadneh-t@endeavorpal.com
 */
namespace Endeavor\Map\Block;

use Magento\Framework\View\Element\Template;

class Main extends Template 
{
    /**
     *
     * @var Magento\Catalog\Model\CategoryFactory 
     */
    protected $_categoryFactory;
    
    /**
     *
     * @var Magento\Catalog\Model\ResourceModel\Category\CollectionFactory 
     */
    protected $_categoryCollectionFactory;
    
    /**
     *
     * @var Magento\Catalog\Helper\Category 
     */
    protected $_categoryHelper;
    
    /**
     * 
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Helper\Category $categoryHelper,
        array $data = []
    ) {
        $this->_categoryFactory = $categoryFactory;
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        $this->_categoryHelper = $categoryHelper;
        parent::__construct($context, $data);
    }
    
    /**
     * 
     * @param type $categoryId
     * @return type
     */
    public function getCategory($categoryId) {
        $category = $this->_categoryFactory->create();
        $category->load($categoryId);
        return $category;
    }

    /**
     * 
     * @param type $categoryId
     * @return type
     */
    public function getCategoryProducts($categoryId) {
        $products = $this->getCategory($categoryId)->getProductCollection();
        $products->addAttributeToSelect('*');
        return $products;
    }

    /**
     * 
     */
    public function getCategoryCollection(
        $isActive = true,
        $level = false,
        $sortBy = false,
        $pageSize = false
    ) {
        $collection = $this->_categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');

        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }

        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }

        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }

        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize);
        }

        return $collection;
    }

    public function getChildren($categoryId = false) {
        if ($this->_category) {
            return $this->_category->getChildren();
        } else {
            return $this->getCategory($categoryId)->getChildren();
        }
    }

    public function getMapCategoryId() {
        $categories = $this->getCategoryCollection();
        foreach ($categories as $category) {
            if ("Jurisdictions" == $category->getName()) {
                $categoryId = $category->getId();
            }
        }
        return $categoryId;
    }

    public function getStatesAsArray() {
        $states = $this->getCategory($this->getMapCategoryId());
        $childrenCategories = $states->getChildrenCategories();
        foreach ($childrenCategories as $child) {
            $subChild[] = $child;
        }
        return $subChild;
    }

    public function getStateUrl($stateName) {
        $states = $this->getStatesAsArray();
        foreach ($states as $state) {
            if ($state->getName() == $stateName) {
                $url = $state->getUrl();
                return $url;
            }
        }
    }

}
