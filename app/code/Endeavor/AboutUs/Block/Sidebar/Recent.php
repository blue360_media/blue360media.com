<?php
namespace Endeavor\AboutUs\Block\Sidebar;

/**
 * AboutUs sidebar categories block
 */
class Recent extends \Endeavor\AboutUs\Block\Post\PostList\AbstractList
{
    /**
     * @trait Endeavor\AboutUs\Block\Sidebar\Widget
     */
    use Widget;

    /**
     * @var string
     */
    protected $_widgetKey = 'recent_posts';

    /**
     * @return $this
     */
    public function _construct()
    {
        $this->setPageSize(
            (int) $this->_scopeConfig->getValue(
                'mfaboutus/sidebar/'.$this->_widgetKey.'/posts_per_page',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        );
        return parent::_construct();
    }

    /**
     * Retrieve block identities
     * @return array
     */
    public function getIdentities()
    {
        return [\Magento\Cms\Model\Block::CACHE_TAG . '_aboutus_recent_posts_widget'  ];
    }

}
