<?php
namespace Endeavor\AboutUs\Block\Sidebar;

/**
 * AboutUs sidebar rss
 */
class Rss extends \Magento\Framework\View\Element\Template
{
    /**
     * @trait Endeavor\AboutUs\Block\Sidebar\Widget
     */
    use Widget;

    /**
     * @var string
     */
    protected $_widgetKey = 'rss_feed';

    /**
     * Available months
     * @var array
     */
    protected $_months;

    /**
     * Retrieve aboutus identities
     * @return array
     */
    public function getIdentities()
    {
        return [\Magento\Cms\Model\Block::CACHE_TAG . '_aboutus_rss_widget'  ];
    }

}
