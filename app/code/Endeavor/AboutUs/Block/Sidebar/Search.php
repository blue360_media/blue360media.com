<?php
namespace Endeavor\AboutUs\Block\Sidebar;

/**
 * AboutUs sidebar categories block
 */
class Search extends \Magento\Framework\View\Element\Template
{
    /**
     * @trait Endeavor\AboutUs\Block\Sidebar\Widget
     */
    use Widget;

    /**
     * @var \Endeavor\AboutUs\Model\Url
     */
    protected $_url;

    /**
     * @var string
     */
    protected $_widgetKey = 'search';

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Endeavor\AboutUs\Model\Url $url
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Endeavor\AboutUs\Model\Url $url,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_url = $url;
    }

    /**
     * Retrieve query
     * @return string
     */
    public function getQuery()
    {
        return urldecode($this->getRequest()->getParam('q', ''));
    }

    /**
     * Retrieve serch form action url
     * @return string
     */
    public function getFormUrl()
    {
        return $this->_url->getUrl('', \Endeavor\AboutUs\Model\Url::CONTROLLER_SEARCH);
    }

}
