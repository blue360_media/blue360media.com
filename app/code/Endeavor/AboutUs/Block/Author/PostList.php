<?php
 

namespace Endeavor\AboutUs\Block\Author;

use Magento\Store\Model\ScopeInterface;

/**
 * AboutUs author posts list
 */
class PostList extends \Endeavor\AboutUs\Block\Post\PostList
{
    /**
     * Prepare posts collection
     *
     * @return void
     */
    protected function _preparePostCollection()
    {
        parent::_preparePostCollection();
        if ($author = $this->getAuthor()) {
            $this->_postCollection->addAuthorFilter($author);
        }   
    }

    /**
     * Retrieve author instance
     *
     * @return \Endeavor\AboutUs\Model\Author
     */
    public function getAuthor()
    {
        return $this->_coreRegistry->registry('current_aboutus_author');
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        if ($author = $this->getAuthor()) {
            $this->_addBreadcrumbs($author);
            $this->pageConfig->addBodyClass('aboutus-author-' . $author->getIdentifier());
            $this->pageConfig->getTitle()->set($author->getTitle());
            $this->pageConfig->addRemotePageAsset(
                $author->getAuthorUrl(),
                'canonical',
                ['attributes' => ['rel' => 'canonical']]
            );
        }

        return parent::_prepareLayout();
    }

    /**
     * Prepare breadcrumbs
     *
     * @param \Endeavor\AboutUs\Model\Author $author
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function _addBreadcrumbs($author)
    {
        if ($this->_scopeConfig->getValue('web/default/show_cms_breadcrumbs', ScopeInterface::SCOPE_STORE)
            && ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs'))
        ) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                    'label' => __('Home'),
                    'title' => __('Go to Home Page'),
                    'link' => $this->_storeManager->getStore()->getBaseUrl()
                ]
            );

            $breadcrumbsBlock->addCrumb(
                'aboutus',
                [
                    'label' => __('About Us'),
                    'title' => __('Go to About Us Home Page'),
                    'link' => $this->_url->getBaseUrl()
                ]
            );

            $breadcrumbsBlock->addCrumb('aboutus_author',[
                'label' => $author->getTitle(),
                'title' => $author->getTitle()
            ]);
        }
    }
}
