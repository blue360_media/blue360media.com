<?php
namespace Endeavor\AboutUs\Model\ResourceModel\Tag;

/**
 * AboutUs tag collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Constructor
     * Configures collection
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Endeavor\AboutUs\Model\Tag', 'Endeavor\AboutUs\Model\ResourceModel\Tag');
    }

}
