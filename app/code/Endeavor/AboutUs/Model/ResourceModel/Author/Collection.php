<?php
namespace Endeavor\AboutUs\Model\ResourceModel\Author;

/**
 * AboutUs author collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Constructor
     * Configures collection
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Endeavor\AboutUs\Model\Author', 'Endeavor\AboutUs\Model\ResourceModel\Author');
    }
}
