<?php
namespace Endeavor\AboutUs\Model;

/**
 * Category management model
 */
class CategoryManagement extends AbstractManagement
{
    /**
     * @var \Endeavor\AboutUs\Model\CategoryFactory
     */
    protected $_itemFactory;

    /**
     * Initialize dependencies.
     *
     * @param \Endeavor\AboutUs\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Endeavor\AboutUs\Model\CategoryFactory $categoryFactory
    ) {
        $this->_itemFactory = $categoryFactory;
    }

}
