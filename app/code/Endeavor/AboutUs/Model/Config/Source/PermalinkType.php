<?php
namespace Endeavor\AboutUs\Model\Config\Source;

use Endeavor\AboutUs\Model\Url;

/**
 * Used in creating options for permalink config value selection
 */
class PermalinkType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => Url::PERMALINK_TYPE_DEFAULT, 'label' => __('Default: mystore.com/{aboutus_route}/{post_route}/post-title/')],
            ['value' => Url::PERMALINK_TYPE_SHORT, 'label' => __('Short: mystore.com/{aboutus_route}/post-title/')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $array = [];
        foreach($this->toOptionArray() as $item) {
            $array[$item['value']] = $item['label'];
        }
        return $array;
    }
}
