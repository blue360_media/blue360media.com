<?php
namespace Endeavor\AboutUs\Model\Config\Source;

/**
 * Lazy load options
 */
class LazyLoad implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var int options value
     */
    const DISABLED = 0;
    
    /**
     * @var int options value
     */
    const ENABLED_WITH_AUTO_TRIGER = 1;
    
    /**
     * @var int options value
     */
    const ENABLED_WITHOUT_AUTO_TRIGER = 2;

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::DISABLED, 'label' => __('No')],
            ['value' => self::ENABLED_WITH_AUTO_TRIGER, 'label' => __('Yes (With auto trigger)')],
            ['value' => self::ENABLED_WITHOUT_AUTO_TRIGER, 'label' => __('Yes (Without auto trigger)')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $array = [];
        foreach($this->toOptionArray() as $item) {
            $array[$item['value']] = $item['label'];
        }
        return $array;
    }
}
