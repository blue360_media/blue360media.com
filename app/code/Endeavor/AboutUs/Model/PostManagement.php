<?php
namespace Endeavor\AboutUs\Model;

/**
 * Post management model
 */
class PostManagement extends AbstractManagement
{
    /**
     * @var \Endeavor\AboutUs\Model\PostFactory
     */
    protected $_itemFactory;

    /**
     * Initialize dependencies.
     *
     * @param \Endeavor\AboutUs\Model\PostFactory $postFactory
     */
    public function __construct(
        \Endeavor\AboutUs\Model\PostFactory $postFactory
    ) {
        $this->_itemFactory = $postFactory;
    }

}
