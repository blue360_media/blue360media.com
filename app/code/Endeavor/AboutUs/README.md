# [Endeavor](http://endeavor.com/) AboutUs Extension for Magento 2

AboutUs extension allows you to create a full-fledged aboutus on your [Magento 2](http://magento.com/) Store.

## Features
  * Unlimited aboutus posts and categories
  * Post tags are available
  * Multi Stores Support
  * SEO friendly permalinks
  * Facebook,Disqus and Google+ comments on post page
  * Related Products and Related Post
  * Available posts search
  * Lazy load on posts list
  * Author information and posts by author
  * Recent Posts, Archive, Categories, Search From, Tag Cloud sidebar widgets
  * Import Posts and Categories form WordPress and AW AboutUs extension for M1
  * Recent Posts Widget
  * Sitemap XML
  * AboutUs Rss Feed
  * Open Graph (OG) meta tags
  * REST API
## Requirements
  * Magento Community Edition 2.x or Magento Enterprise Edition 2.x

## Installation Method 1 - Installing via composer
  * Open command line
  * Using command "cd" navigate to your magento2 root directory
  * Run command: composer require endeavor/module-aboutus

  

## Installation Method 2 - Installing using archive
  * Extract files
  * In your Magento 2 root directory create folder app/code/Endeavor/AboutUs
  * Copy files and folders from archive to that folder
  * In command line, using "cd", navigate to your Magento 2 root directory
  * Run commands:
```
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
```

## Support
If you have any issues, please [contact us](mailto:support@endeavor.com)
then if you still need help, open a bug report in GitHub's

Please do not use Magento Connect's Reviews or (especially) the Q&A for support.
There isn't a way for us to reply to reviews and the Q&A moderation is very slow.

## Donate to us
All Endeavor extension are absolutely free and licensed under the Open Software License version 3.0. We want to create more awesome features for you and bring up new releases as fast as we can. We hope for your support.

## License
The code is licensed under [Open Software License ("OSL") v. 3.0](http://opensource.org/licenses/osl-3.0.php).
