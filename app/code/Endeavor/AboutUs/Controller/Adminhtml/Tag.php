<?php
namespace Endeavor\AboutUs\Controller\Adminhtml;

/**
 * Admin aboutus tag edit controller
 */
class Tag extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey = 'aboutus_tag_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey = 'Endeavor_AboutUs::post';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass = 'Endeavor\AboutUs\Model\Tag';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu = 'Endeavor_AboutUs::post';
}
