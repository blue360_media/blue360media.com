<?php
namespace Endeavor\AboutUs\Controller\Adminhtml\Post\Upload;

use Endeavor\AboutUs\Controller\Adminhtml\Upload\Image\Action;

/**
 * AboutUs featured image upload controller
 */
class FeaturedImg extends Action
{
    /**
     * File key
     *
     * @var string
     */
    protected $_fileKey = 'featured_img';

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Endeavor_AboutUs::post');
    }

}
