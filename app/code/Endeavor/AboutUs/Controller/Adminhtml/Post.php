<?php
namespace Endeavor\AboutUs\Controller\Adminhtml;

/**
 * Admin aboutus post edit controller
 */
class Post extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey = 'aboutus_post_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey = 'Endeavor_AboutUs::post';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass = 'Endeavor\AboutUs\Model\Post';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu = 'Endeavor_AboutUs::post';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField = 'is_active';

}
