<?php
namespace Endeavor\AboutUs\Controller\Adminhtml;

/**
 * Admin aboutus category edit controller
 */
class Category extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey = 'aboutus_category_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey = 'Endeavor_AboutUs::category';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass = 'Endeavor\AboutUs\Model\Category';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu = 'Endeavor_AboutUs::category';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField = 'is_active';
}