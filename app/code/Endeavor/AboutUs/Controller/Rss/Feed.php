<?php
namespace Endeavor\AboutUs\Controller\Rss;

/**
 * AboutUs rss feed view
 */
class Feed extends \Magento\Framework\App\Action\Action
{
    /**
     * View aboutus rss feed action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->getResponse()
            ->setHeader('Content-type', 'text/xml; charset=UTF-8')
            ->setBody(
                $this->_view->getLayout()->getBlock('aboutus.rss.feed')->toHtml()
            );
    }

}
