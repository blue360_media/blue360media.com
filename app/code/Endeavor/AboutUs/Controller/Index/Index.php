<?php
namespace Endeavor\AboutUs\Controller\Index;

/**
 * AboutUs home page view
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * View aboutus homepage action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

}
