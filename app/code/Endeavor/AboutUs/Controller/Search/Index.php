<?php
namespace Endeavor\AboutUs\Controller\Search;

/**
 * AboutUs search results view
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * View aboutus search results action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

}
