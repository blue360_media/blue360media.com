<?php
namespace Endeavor\AboutUs\Controller\Author;

use \Magento\Store\Model\ScopeInterface;

/**
 * AboutUs author posts view
 */
class View extends \Magento\Framework\App\Action\Action
{
    /**
     * View aboutus author action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $config = $this->_objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');

        $enabled = (int) $config->getValue('mfaboutus/author/enabled',
            ScopeInterface::SCOPE_STORE);
        $pageEnabled = (int) $config->getValue('mfaboutus/author/page_enabled',
            ScopeInterface::SCOPE_STORE);

        if (!$enabled || !$pageEnabled) {
            $this->_forward('index', 'noroute', 'cms');
            return;
        }

        $author = $this->_initAuthor();
        if (!$author) {
            $this->_forward('index', 'noroute', 'cms');
            return;
        }

        $this->_objectManager->get('\Magento\Framework\Registry')->register('current_aboutus_author', $author);

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

    /**
     * Init author
     *
     * @return \Endeavor\AboutUs\Model\Author || false
     */
    protected function _initAuthor()
    {
        $id = $this->getRequest()->getParam('id');

        $author = $this->_objectManager->create('Endeavor\AboutUs\Model\Author')->load($id);

        if (!$author->getId()) {
            return false;
        }

        return $author;
    }

}
