<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 30-11-2017.
 * @desc in order to build checkout page for OutstandingInvoices.
 */

namespace Endeavor\OutstandingInvoice\Block;
 
class OutstandingInvoice extends \Magento\Framework\View\Element\Template
 {

    /**
     * Return form action url for OutstandingInvoice page.
     *
     * @param Item $item
     * @return string
     */
    public function getFormAction() {
        return $this->getUrl('order/payment/index', ['_secure' => true]);
    }

}
