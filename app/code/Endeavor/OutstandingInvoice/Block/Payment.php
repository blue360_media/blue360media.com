<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 30-11-2017.
 * @desc 1- in order to build payment page.
 *       2- updated to get order line items Name, Edition, imageUrl.
 *       3- updated to get order only image, other data from sales force.
 */

namespace Endeavor\OutstandingInvoice\Block;
 
class Payment extends \Magento\Framework\View\Element\Template
{

    /**
     * Return form action url for checkout page.
     *
     * @param Item $item
     * @return string
     */
    public function getSaveUrl() {
        return $this->getUrl('order/payment/complete');
    }
    
    /**
     * Return form action url for checkout page.
     *
     * @param Item $item
     * @return string
     */
    public function getPayPalUrl() {
        return $this->getUrl('order/paypal/gettoken');
    }

    /**
     * Return Order line item Image.
     *
     * @param SKU $productSKU
     * @return string
     */
    public function getLineItemImage($productSKU) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productId = $objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($productSKU);
        $product = $objectManager->get('Magento\Catalog\Model\Product')->load($productId);
        $image = $product->getImage();
        return $image;
    }

}
