<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 30-11-2017.
 * @desc in order to build OutstandingInvoice page.
 */
 
namespace Endeavor\OutstandingInvoice\Controller\Index;
 
use Magento\Framework\App\Action\Context;
 
class Index extends \Magento\Framework\App\Action\Action
 {
    
    /**
     * PageFactory Data
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    
    /**
     * Page Action
     * Retrieve OutstandingInvoice Page
     *
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute() {
        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
    }

}
