<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 30-11-2017.
 * @desc 1- in order to build checkout page.
 *       2- update to increase read timeout for request.
 *       3- update to pass tax, shipping, amountpaid, orderitems, discount and order items to frontend.
 *       4- update to make order number more smarter, validate e-commerce and salesforce order number.
 * 
 * @updated_By Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 26-12-2017.
 * @desc update to change order number to be more smarter depending on e-commerce order number.
 */
 
namespace Endeavor\OutstandingInvoice\Controller\Payment;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Zend\Http\Client;

class Index extends \Magento\Framework\App\Action\Action
{
    
    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     * Keys for pass through variables in OutstandingInvoice/OutstandingInvoice_payment
     * Uses additional_information as storage
     */
    const XML_GENERATE_SALESFORCE_ACCESS_TOKEN_URL = 'api/salesforce/access_token_url';
    const XML_GET_SALES_ORDER_DETAILS = 'api/salesforce/sales_order_details_url';
    const XML_GENERATE_SALESFORCE_ACCESS_TOKEN_CLIENT_SECRET = 'api/salesforce/access_token_client_secret';
    const XML_GENERATE_SALESFORCE_ACCESS_TOKEN_CLIENT_ID = 'api/salesforce/access_token_client_id';
    const XML_GENERATE_SALESFORCE_ACCESS_TOKEN_REFRESH_TOKEN = 'api/salesforce/access_token_refresh_token';
    const XML_GENERATE_SALESFORCE_ACCESS_TOKEN_GRANT_TYPE = 'api/salesforce/access_token_grant_type';
    const XML_READ_TIME_OUT = 'api/salesforce/read_time_out';

    /**
     * PageFactory Data
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;
    
    /**
     * accessToken from salesforce Data
     * 
     */
    private $_accessToken = null;

    /**
     * messageManager Data
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;
    
    /**
     * @param Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
        $this->_messageManager = $context->getMessageManager();
    }
    
    /**
     * Page Action
     * Retrieve Checkout Page
     *
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute() {
        
        $postValues = $this->getRequest()->getPostValue();
        
        $orderNumber = $postValues['order'] ?? null;
        $invoiceNumber = $postValues['invoice'] ?? null;

        if (isset($postValues['order']) || isset($postValues['invoice'])) {
            $orderNumber = trim($postValues['order']);
            $invoiceNumber = trim($postValues['invoice']);
            
            // validate salesforce order or e-commerce order.
            $orderNumber = $this->getSFOrderNumOrMagentoOrderNum($orderNumber);
            
            $this->getSalesforceToken();

            if (is_null($this->getAccessToken()) == false) {
                
                $resultPage = $this->getOrderAmountToPayFromSalesforce($orderNumber, $invoiceNumber, $this->getAccessToken());

                // we got a response from Salesforce create a result page
                // and load the checkout page
                if (empty($resultPage)) {
                    $resultPage = $this->_resultPageFactory->create();
                }
            } else {
                $message = 'Opps! We are sorry something went wrong, Please try again in 5 minuties.';
                $this->_messageManager->addErrorMessage(__($message));
                $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                $resultPage->setUrl($this->_redirect->getRefererUrl());
            }
        } else {
            $message = 'Opps! direct access is not allowed! please fill in Order or Invoice Number below.';
            $this->_messageManager->addErrorMessage(__($message));
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultPage->setPath('outstandinginvoice/index/index');
            
        }

        return $resultPage;
    }
    /**
     * @param accessToken $accessToken
     * 
     */
    protected function setAccessToken($accessToken){
        $this->_accessToken = $accessToken;
    }
    
    /**
     * @return accessToken Return accessToken.
     * 
     */
    protected function getAccessToken(){
        return $this->_accessToken;
    }
    
    /**
     * @return accessToken Return access token from salesforce.
     * 
     */
    protected function getSalesforceToken() {
        $client = new Client();
        $uri = $this->_scopeConfig->getValue(self::XML_GENERATE_SALESFORCE_ACCESS_TOKEN_URL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $timeout = $this->_scopeConfig->getValue(self::XML_READ_TIME_OUT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $tokenGrantType = $this->_scopeConfig->getValue(self::XML_GENERATE_SALESFORCE_ACCESS_TOKEN_GRANT_TYPE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $tokenClientSecret = $this->_scopeConfig->getValue(self::XML_GENERATE_SALESFORCE_ACCESS_TOKEN_CLIENT_SECRET, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $tokenClientId = $this->_scopeConfig->getValue(self::XML_GENERATE_SALESFORCE_ACCESS_TOKEN_CLIENT_ID, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $tokenRefreshToken = $this->_scopeConfig->getValue(self::XML_GENERATE_SALESFORCE_ACCESS_TOKEN_REFRESH_TOKEN, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $client->setUri($uri);
        $client->setMethod('POST');
        $client->setOptions(array('timeout' => $timeout));
        $client->setParameterPost(array(
            'grant_type' => $tokenGrantType,
            'client_secret' => $tokenClientSecret,
            'client_id' => $tokenClientId,
            'refresh_token' => $tokenRefreshToken
        ));

        $response = $client->send();
		$responseArray = json_decode($response->getBody(), true);
		//echo "<pre>"; print_r($responseArray);exit;

        if ($response->isSuccess()) {
            // the POST was successful
            $responseArray = json_decode($response->getBody(), true);
            if (isset($responseArray['access_token'])) {
                $accessToken = $responseArray['access_token'];
                $this->setAccessToken($accessToken);                
            }          
            
        } 
    } // end of getSalesforceToken
    
    /**
     * get order/invoice data from Salesforce.
     * Retrieve Checkout OutstandingInvoice Page
     *
     * @return \Magento\Framework\View\Result\PageFactory
     */
    protected function getOrderAmountToPayFromSalesforce($orderNumber, $invoiceNumber, $accessToken) {
        $orderFound = false;
        $orderPaid = false;
        $resultRedirect = null;
        $message = '';
        if (!empty($orderNumber)) {
            $salesOrder = $this->getOrder($orderNumber);
            if ($salesOrder->getId()) {
                $orderFound = true;
                $orderPaid = $this->checkMagentoOrderComplete($orderNumber);
            }
        }
        if (!$orderPaid) {
            $response = $this->getOrderFromSalesForce($orderNumber, $invoiceNumber, $accessToken, $orderFound);
            if ($response->isSuccess()) {
                // the POST was successful
                $responseArray = json_decode($response->getBody(), true);

                if ($responseArray['isSuccess']) {
                    //All went great collect the response data for checkout page.
                    $orderNumberParam = $responseArray['orderNumber'];
                    $this->getRequest()->setParams(['orderNumber' => $orderNumberParam]);
                    if (empty($orderNumber) && !empty($invoiceNumber)) {
                        $orderPaid = $this->checkMagentoOrderComplete($orderNumberParam);
                    }
                    
                    $amountDue = $responseArray['amountDue'];
                    $this->getRequest()->setParams(['amountDue' => $amountDue]);

                    $invoiceNumberParam = $responseArray['invoiceNumber'];
                    $this->getRequest()->setParams(['invoiceNumber' => $invoiceNumberParam]);

                    $grandTotal = $responseArray['grandTotal'];
                    $this->getRequest()->setParams(['grandTotal' => $grandTotal]);

                    $tax = $responseArray['tax'];
                    $this->getRequest()->setParams(['tax' => $tax]);

                    $shipping = $responseArray['shipping'];
                    $this->getRequest()->setParams(['shipping' => $shipping]);

                    $discount = $responseArray['discount'];
                    $this->getRequest()->setParams(['discount' => $discount]);

                    $amountPaid = $responseArray['amountPaid'];
                    $this->getRequest()->setParams(['amountPaid' => $amountPaid]);

                    $orderItems = $responseArray['orderItems'];
                    $this->getRequest()->setParams(['orderItems' => $orderItems]);


                } else {
                    // check the message response and dispaly to client
                    // send the client back to the Billing Statment page
                    $message = $responseArray['message'];

                }
            } else {
                $message = 'Please try again after 25 hours';
                //Please try again
            }
        }
        if ($orderPaid) {
            $message = 'The Order ' . $orderNumber . ' is already paid.';
        }
        
        if (!empty($message)) {
            $this->_messageManager->addErrorMessage(__($message));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            
        }
        return $resultRedirect;
    } // end of getOrderAmountToPayFromSalesforce
    
    /**
     * Check if order in magento and order is paid.
     * Retrieve Checkout OutstandingInvoice Page
     *
     * @return \Magento\Framework\View\Result\PageFactory
     */
    protected function checkMagentoOrderComplete($orderNumber) {
        $orderCompleted = false;
        $salesOrder = $this->getOrder($orderNumber);
        if ($salesOrder && ($salesOrder->getBaseGrandTotal() == $salesOrder->getTotalPaid())) {
            $orderCompleted = true;
        }

        return $orderCompleted;
    }
    
    /**
     * get order if is found in magento
     *
     * @return \Magento\Sales\Model\Order
     */
    protected function getOrder($orderNumber) {
        $salesOrder = $this->_objectManager->create('\Magento\Sales\Model\Order')
                ->load($orderNumber, 'sf_order_id');
        
        return $salesOrder;
    }
    
    /**
     * get response data from salesforce
     *
     * @return array of response data
     */
    protected function getOrderFromSalesForce($orderNumber, $invoiceNumber, $accessToken, $orderFound) {
        $salesOrderDetailsURL = $this->_scopeConfig->getValue(self::XML_GET_SALES_ORDER_DETAILS, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $timeout = $this->_scopeConfig->getValue(self::XML_READ_TIME_OUT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $json = json_encode(['requestOrder' => ['orderNumber' => $orderNumber, 'invoiceNumber'=>$invoiceNumber, 'isOrderInMagento' => $orderFound]]);
        $client = new Client();
        $client->setUri($salesOrderDetailsURL);
        $client->setMethod('POST');
        $client->setHeaders(['authorization' => 'Bearer ' . $accessToken, 'content-type' => 'application/json']);
        $client->setRawBody($json, 'application/json');
        $client->setOptions(array('timeout' => $timeout));
        $response = $client->send();
        
        return $response;
    }
    /*
     * Validate e-commerce or salesforce order number.
     * @param   $orderNumber string
     * @return String   correct salesforce order number
     */
    protected function getSFOrderNumOrMagentoOrderNum($orderNumber) {

        // Order starting with 0 and doesn't contain '/'.
        $eCommerceOrderStart = substr($orderNumber,0,1);
        if ($eCommerceOrderStart == 0 && !strpos($orderNumber, '/')) {
            $eCommerceOrderNumber = str_pad($orderNumber, 9, '0', false);
            // get sfOrder from e-commerce Order.
            $orderNumber = $this->getSfOrderFromEcommerceOrder($eCommerceOrderNumber);
            return $orderNumber;
        }
        
        // Order contains 1700000231/0000002135 or viceversa.
        if (strpos($orderNumber, '/') !== false) {
            $sfOrderAndMagento = explode('/', $orderNumber);
            $sfOrderAndMagento[0] = trim($sfOrderAndMagento[0]);
            $sfOrderAndMagento[1] = trim($sfOrderAndMagento[1]);
            
            if (substr($sfOrderAndMagento[0],0,1) == 0) {
                return $sfOrderAndMagento[1];
            }
            
            if (substr($sfOrderAndMagento[1],0,1) == 0) {
                return $sfOrderAndMagento[0];
            }
        }
        // return sfOrderNumber
        return $orderNumber;
    }// end of getSFOrderNumOrMagentoOrderNum
    
    /*
     * get salesforce order number from e-commerce order number.
     * @param   $eCommerceOrderNumber string
     * @return salesforce order number
     */
    protected function getSfOrderFromEcommerceOrder($eCommerceOrderNumber){
        $salesOrder = $this->_objectManager->create('\Magento\Sales\Model\Order')
                ->load($eCommerceOrderNumber, 'increment_id');
        $orderNumber = $salesOrder->getSfOrderId();
        return $orderNumber;
    }
}
