<?php
namespace Endeavor\CustomProduct\Model\Product;

class Price extends \Magento\Catalog\Model\Product\Type\Price
{
    const TYPE_ID = 'combo';

    /**
     * {@inheritdoc}
     */
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
    {
        // method intentionally empty
    }
}