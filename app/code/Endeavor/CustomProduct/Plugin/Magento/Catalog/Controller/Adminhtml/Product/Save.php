<?php

namespace Endeavor\CustomProduct\Plugin\Magento\Catalog\Controller\Adminhtml\Product;

use Magento\Downloadable\Api\Data\SampleInterfaceFactory as SampleFactory;
use Magento\Downloadable\Api\Data\LinkInterfaceFactory as LinkFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;

class Save
{
    /**
     * Email template path
     * 
     * @var string
     */
    const XML_PATH_EMAIL_TEMPLATE_FIELD  = 'assignuser/email/template';

    /**
     * Email sender path
     * 
     * @var string
     */
    const XML_PATH_EMAIL_SENDER_FIELD  = 'assignuser/email/sender_email_identity';

    /**
     * Name sender path
     * 
     * @var string
     */
    const XML_PATH_SENDER_NAME_FIELD  = 'assignuser/email/sender_name';

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var SampleFactory
     */
    private $sampleFactory;

    /**
     * @var LinkFactory
     */
    private $linkFactory;

    /**
     * @var \Magento\Downloadable\Model\Sample\Builder
     */
    private $sampleBuilder;

    /**
     * @var \Magento\Downloadable\Model\Link\Builder
     */
    private $linkBuilder;

    /**
     *
     * @var type 
     */
    private $productRepository;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    
    /**
     * @var  \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $_transportBuilder;
    
    /**
     *
     * @var type 
     */
    protected $_storeManager;    

    /**
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager   
    ) {
        $this->request = $request;
        $this->scopeConfig = $scopeConfig;
        $this->productRepository = $productRepository;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;        

    }

    public function afterExecute(
    \Magento\Catalog\Controller\Adminhtml\Product\Save $subject, $result
    ) {
        if ($downloadable = $this->request->getPost('combo')) {

            if (isset($downloadable['link']) && is_array($downloadable['link'])) {
                foreach ($downloadable['link'] as $linkData) {
                    $params = $this->request->getParams();
                    $sku = $params['product']['sku'];
                    $id = $this->productRepository->get($sku)->getId();

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $uploaderFile = $objectManager->create('Magento\MediaStorage\Model\File\UploaderFactory');
                    $filesystem = $objectManager->create('Magento\Framework\Filesystem');
                    $mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                    $DownloadableLinkModel = $objectManager->create('Magento\Downloadable\Model\Link');

                    if ((isset($linkData['is_delete']) 
                            && isset($linkData['link_id']) 
                            && $linkData['is_delete'])
                    ) {
                        $DownloadableLinkModel->load($linkData['link_id'])->delete();
                    } else {
                        $this->getLinkBuilder()->setData(
                                $linkData
                        )->build(
                                $this->getLinkFactory()->create()
                        );

                        if (!isset($linkData['link_id'])) {
                            $DownloadableLinkModel->setData('product_id', $id);
                        } else {
                            $DownloadableLinkModel = $objectManager->create('Magento\Downloadable\Model\Link')->load($linkData['link_id']);
                        }

                        $DownloadableLinkModel->setData('sort_order', $linkData['sort_order']);
                        if ((isset($linkData['is_unlimited']) && $linkData['is_unlimited'])) {
                            $DownloadableLinkModel->setData('number_of_downloads','0');
                        }else{
                            $DownloadableLinkModel->setData('number_of_downloads', $linkData['number_of_downloads']);
                        }
                        $DownloadableLinkModel->setData('is_shareable', $linkData['is_shareable']);
                        if ($linkData['type'] == 'file') {
                            $DownloadableLinkModel->setData('link_file', $linkData['file'][0]['file']);
                            $DownloadableLinkModel->setData('link_url', null);
                        }else{
                            $DownloadableLinkModel->setData('link_file',null);
                            $DownloadableLinkModel->setData('link_url', $linkData['link_url']);
                        }

                        $DownloadableLinkModel->setData('sample_type', $linkData['sample']['type']);
                        
                        if ($linkData['sample']['type'] == 'file') {
                            if (isset($linkData['sample']['file'])) {
                                $DownloadableLinkModel->setData('sample_file', $linkData['sample']['file'][0]['file']);
                                $DownloadableLinkModel->setData('sample_url',null);
                            }
                        }else{
                            if (isset($linkData['sample']['url'])) {
                                $DownloadableLinkModel->setData('sample_file', null);
                                $DownloadableLinkModel->setData('sample_url', $linkData['sample']['url']);
                            }
                        }
                        
                        $DownloadableLinkModel->setData('link_type', $linkData['type']);
                        $DownloadableLinkModel->setData('title', $linkData['title']);
                        $DownloadableLinkModel->setData('price', $linkData['price']);
                        $DownloadableLinkModel->save();
                    }
                }
            }
            if (isset($downloadable['sample']) && is_array($downloadable['sample'])) {
                foreach ($downloadable['sample'] as $SampleData) {
                    $params = $this->request->getParams();
                    $sku = $params['product']['sku'];
                    $id = $this->productRepository->get($sku)->getId();

                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $uploaderFile = $objectManager->create('Magento\MediaStorage\Model\File\UploaderFactory');
                    $filesystem = $objectManager->create('Magento\Framework\Filesystem');
                    $mediaDirectory = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                    $DownloadableSampleModel = $objectManager->create('Magento\Downloadable\Model\Sample');

                    if ((isset($SampleData['is_delete']) && $SampleData['is_delete'])) {
                        $DownloadableSampleModel->load($SampleData['sample_id'])->delete();
                    } else {
                        $this->getSampleBuilder()->setData(
                                $SampleData
                        )->build(
                                $this->getSampleFactory()->create()
                        );

                        if (!isset($SampleData['sample_id'])) {
                            $DownloadableSampleModel->setData('product_id', $id);
                        } else {
                            $DownloadableSampleModel = $objectManager->create('Magento\Downloadable\Model\Sample')->load($SampleData['sample_id']);
                        }
                        
                        $DownloadableSampleModel->setData('sort_order', $SampleData['sort_order']);
                        if ($SampleData['type'] == 'file') {
                            if (isset($SampleData['file'])) {
                                $DownloadableSampleModel->setData('sample_file', $SampleData['file'][0]['file']);
                                $DownloadableSampleModel->setData('sample_url',null);
                            }
                        }else{
                                $DownloadableSampleModel->setData('sample_url', $SampleData['sample_url']);
                                $DownloadableSampleModel->setData('sample_file',null);
                            }
                        $DownloadableSampleModel->setData('sample_type', $SampleData['type']);
                        $DownloadableSampleModel->setData('title', $SampleData['title']);
                        $DownloadableSampleModel->save();
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get LinkBuilder instance
     *
     * @deprecated
     * @return \Magento\Downloadable\Model\Link\Builder
     */
    private function getLinkBuilder() {
        if (!$this->linkBuilder) {
            $this->linkBuilder = ObjectManager::getInstance()->get(\Magento\Downloadable\Model\Link\Builder::class);
        }

        return $this->linkBuilder;
    }

    /**
     * Get SampleBuilder instance
     *
     * @deprecated
     * @return \Magento\Downloadable\Model\Sample\Builder
     */
    private function getSampleBuilder() {
        if (!$this->sampleBuilder) {
            $this->sampleBuilder = ObjectManager::getInstance()->get(
                    \Magento\Downloadable\Model\Sample\Builder::class
            );
        }

        return $this->sampleBuilder;
    }

    /**
     * Get LinkFactory instance
     *
     * @deprecated
     * @return LinkFactory
     */
    private function getLinkFactory() {
        if (!$this->linkFactory) {
            $this->linkFactory = ObjectManager::getInstance()->get(LinkFactory::class);
        }

        return $this->linkFactory;
    }

    /**
     * Get Sample Factory
     *
     * @deprecated
     * @return SampleFactory
     */
    private function getSampleFactory() {
        if (!$this->sampleFactory) {
            $this->sampleFactory = ObjectManager::getInstance()->get(SampleFactory::class);
        }

        return $this->sampleFactory;
    }    
}
