<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\CustomProduct\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

class IsAllowedGuestCheckoutObserver extends \Magento\Downloadable\Observer\IsAllowedGuestCheckoutObserver
{
    /**
     *  Xml path to disable checkout
     */
    const XML_PATH_DISABLE_GUEST_CHECKOUT = 'catalog/downloadable/disable_guest_checkout';
//    const XML_PATH_DISABLE_GUEST_CHECKOUT_FOR_COMBO = 'catalog/combo/disable_guest_checkout';

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;
    
    /**
     * Helper for PreOrder
     *
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_preorderHelper = $preorderHelper;
    }


    /**
     * Edited By: Mohammad Hamadneh.
     * @date: 14-11-2017.
     * @desc: in order to prevent guest to checkout preOrder.
     * Check is allowed guest checkout if quote contain downloadable product(s)
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $helper = $this->_preorderHelper;
        $store = $observer->getEvent()->getStore();
        $result = $observer->getEvent()->getResult();

        $result->setIsAllowed(true);

        // Enable Guest Checkout
        if (!$this->_scopeConfig->isSetFlag(
            self::XML_PATH_DISABLE_GUEST_CHECKOUT,
            ScopeInterface::SCOPE_STORE,
            $store
        )) {
            return $this;
        }

        /* @var $quote \Magento\Quote\Model\Quote */
        $quote = $observer->getEvent()->getQuote();
        
        // for Disable Guest Checkout
        /*
        * Edited By: Mohammad Hamadneh.
        * @date: 14-11-2017.
        * @desc: in order to prevent guest to checkout preOrder.
        */
        foreach ($quote->getAllItems() as $item) {
            if ($helper->isPreorder($item->getProductId())) {
                $result->setIsAllowed(false);
                break;
            }
            if (($product = $item->getProduct())
                && ($product->getTypeId() == \Magento\Downloadable\Model\Product\Type::TYPE_DOWNLOADABLE 
                        || $product->getTypeId() == \Endeavor\CustomProduct\Model\Product\Type::TYPE_COMBO
            )) {
                $result->setIsAllowed(false);
                break;
            }
        }

        return $this;
    }
}
