<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\CustomProduct\Block\Sales\Order\Items;

/**
 * Order item render block
 *
 * @author      Murad Dweikat<m.dweikat1994@gmail.com>
 */
class Renderer extends \Magento\Bundle\Block\Sales\Order\Items\Renderer
{
    /**
     * @param mixed $item
     * @return string
     */
    public function getValueHtml($item)
    {
        if ($attributes = $this->getSelectionAttributes($item)) {

            $tmp = sprintf('%d', $attributes['qty']) . ' x ' . $this->escapeHtml($item->getName()) . " "
                . $this->getOrder()->formatPrice($attributes['price']);


            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $purchased_links_items = $objectManager->create('Magento\Downloadable\Model\Link\Purchased\Item'
                    )->getCollection(
                    )->addFieldToFilter('order_item_id',$item->getId());

            $htmlLinks = '';
            foreach($purchased_links_items->getItems() as $items){
                $linkHash = $items->getLinkHash();
                $Url = $this->_storeManager->getStore()->getBaseUrl()."downloadable/download/link/id/".$linkHash;
                $htmlLinks .= '<br/><a href="'.$Url.'">'.$items->getLinkTitle().'</a>';
            }

            if($item->getProductType() == 'combo' || $item->getProductType() == 'downloadable'){
                $tmp .= $htmlLinks;
            }
            
            return $tmp;
        } else {
            return $this->escapeHtml($item->getName());
        }
    }
}
