<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 17-12-2017.
 * @desc block to get form link in Move Order from customer to customer.
 */

namespace Endeavor\MoveOrderTo\Block;
 
class MoveOrderPage extends \Magento\Framework\View\Element\Template
 {

    /**
     * Return form action url for OutstandingInvoice page.
     *
     * @param Item $item
     * @return string
     */
    public function getFormAction() {
        return $this->getUrl('moveorder/form/*');
    }

}
