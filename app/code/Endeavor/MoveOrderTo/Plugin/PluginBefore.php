<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 17-12-2017.
 * @desc Plugin to set new button (move order to) in order page.
 */

namespace Endeavor\MoveOrderTo\Plugin;

class PluginBefore {
    
    /* @name beforePushButtons
     * @desc before Push Buttons in admin.
     * 
     * @param \Magento\Backend\Block\Widget\Button\Toolbar\Interceptor $subject
     * @param \Magento\Framework\View\Element\AbstractBlock $context
     * @param \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
     */
    public function beforePushButtons(
        \Magento\Backend\Block\Widget\Button\Toolbar\Interceptor $subject,
        \Magento\Framework\View\Element\AbstractBlock $context,
        \Magento\Backend\Block\Widget\Button\ButtonList $buttonList
    ) {
        $this->_request = $context->getRequest();
        if ($this->_request->getFullActionName() == 'sales_order_view') {
            $buttonList->add(
                    'mybutton', ['label' => __('Move Order To'), 'onclick' => 'setLocation(\'' . $context->getUrl('moveorderto/moveorderto/index') . '\')', 'class' => 'reset'], -1
            );
        }
    }

}
