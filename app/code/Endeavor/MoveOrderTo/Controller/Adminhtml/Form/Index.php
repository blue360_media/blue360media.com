<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 17-12-2017.
 * @desc Controller for form in Move Order from customer to customer.
 * 
 * @updateBy Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc in order to track data when you change order to new table.
 */

namespace Endeavor\MoveOrderTo\Controller\Adminhtml\Form;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action {

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    
    /**
     * @var \Magento\Downloadable\Model\Link\PurchasedFactory
     */
    protected $purchasedLinkFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Downloadable\Model\Link\PurchasedFactory $PurchasedLinkFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Downloadable\Model\Link\PurchasedFactory $PurchasedLinkFactory
    ) {
        $this->_purchasedLinkFactory = $PurchasedLinkFactory;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
        $this->_messageManager = $context->getMessageManager();
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute() {
        $postValues = $this->getRequest()->getPostValue();
        $orderId = $postValues['orderId'];
        $oldEmail = $postValues['demail'] ?? null;
        
        $moveOrderToHelper = $this->_objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
        
        //if email address is empty
        if (!empty($oldEmail)) {
            
            $oldEmail = trim($oldEmail);
            
            $resultRedirect = $this->isCustomerExist($oldEmail, $orderId);
            if (empty($resultRedirect)) {

                $oldCustomerData = $moveOrderToHelper->getCustomerData($oldEmail);

                $customerId = $oldCustomerData['customerid'];
                //if order does have same customer or not.
                $resultRedirect = $this->isCustomerExistInOrder($orderId, $customerId);
                if (empty($resultRedirect)) {
                    //set customer data to order
                    $moveOrderToHelper->setCustomerToOrder($orderId, $oldEmail, $oldCustomerData);
                    
                    $this->setCustomerToDownloadableOrder($orderId, $customerId);
                    
                    $message = 'Customer added to Order.';
                    $this->_messageManager->addSuccessMessage(__($message));
                    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $resultRedirect->setUrl($this->_redirect->getRefererUrl());
                    
                }
            }
        } else {
            $message = 'Please enter email address.';
            $this->_messageManager->addErrorMessage(__($message));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        }
        return $resultRedirect;
    }
    
    /**
     * set customer to purchased.
     * @param String $orderId Order Id
     * @param String $customerId Customer Id
     */
    protected function setCustomerToDownloadableOrder($orderId, $customerId) {
        $downloadablePurchasedLinks = $this->_purchasedLinkFactory->create()->getCollection()
                ->addFieldToFilter('order_id', $orderId);
        if ($downloadablePurchasedLinks) {
            foreach ($downloadablePurchasedLinks as $downloadablePurchasedLink) {
                $downloadablePurchasedLink->setCustomerId($customerId);
                $downloadablePurchasedLink->save();
            }
        }
    }

    /**
     * chick if customer is Assigned to order or not.
     * @param String $orderId Order Id
     * @param String $customerId Customer Id
     * 
     * @return \Magento\Framework\View\Result\Page
     */
    protected function isCustomerExistInOrder($orderId, $customerId) {
        $resultRedirect = null;
        $salesOrder = $this->_objectManager->create('\Magento\Sales\Model\Order')
                ->load($orderId);
        $orderCustomerId = $salesOrder->getCustomerId();
        if ($orderCustomerId == $customerId) {
            $message = 'Customer is already in order.';
            $this->_messageManager->addErrorMessage(__($message));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        }
        return $resultRedirect;
    }
    
    /**
     * check if customer exist or not.
     * @param String $oldEmail Customer Email
     * 
     * @return \Magento\Framework\View\Result\Page
     */
    protected function isCustomerExist($oldEmail,$orderId) {
        $resultRedirect = null;
        $moveOrderToHelper = $this->_objectManager->get('Endeavor\MoveOrderTo\Helper\Data');
        $oldCustomer = $moveOrderToHelper->getOldCustomerObject($oldEmail);
        if (!$oldCustomer->getId()) {
            //change email
            $moveOrderToHelper->changeOldCustomerEmail($oldEmail, $orderId);
            $message = 'Customer email is changed';
            $this->_messageManager->addSuccessMessage(__($message));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        }
        return $resultRedirect;
    }
}