<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc create new table endeavor_moveorderto for move order from customer to customer.
 */

namespace Endeavor\MoveOrderTo\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addMoveOrderCustomFields($setup, $context);
        }
        
    }
    protected function addMoveOrderCustomFields(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();

        $table_endeavor_moveorderto = $setup->getConnection()->newTable($setup->getTable('endeavor_moveorderto'));
    
        $table_endeavor_moveorderto->addColumn(
            'endeavor_moveorder_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            array('identity' => true,'nullable' => false,'primary' => true,'unsigned' => true,),
            'Entity ID'
        );
        
        $table_endeavor_moveorderto->addColumn(
            'change_by',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            [],
            'Changed By'
        );
        
        $table_endeavor_moveorderto->addColumn(
            'created_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false,'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
            'Created At'
        );

        $table_endeavor_moveorderto->addColumn(
            'order_number',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            32,
            ['nullable' => true],
            'Order Number'
        );
        
        $table_endeavor_moveorderto->addColumn(
            'new_customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['nullable' => false, 'unsigned' => true],
            'New Customer Id'
        );
        
        $table_endeavor_moveorderto->addColumn(
            'old_customer_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            10,
            ['nullable' => false, 'unsigned' => true],
            'Old Customer Id'
        );
        
        $table_endeavor_moveorderto->addColumn(
            'old_email',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Old Email'
        );

        $setup->getConnection()->createTable($table_endeavor_moveorderto);

        $setup->endSetup();
    }
}
