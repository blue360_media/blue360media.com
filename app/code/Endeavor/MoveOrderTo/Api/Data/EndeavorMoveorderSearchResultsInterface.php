<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc Api for endeavor_moveorderto table to manage items.
 */

namespace Endeavor\MoveOrderTo\Api\Data;

interface EndeavorMoveorderSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{


    /**
     * Get endeavor_moveorder list.
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface[]
     */
    public function getItems();

    /**
     * Set change_by list.
     * @param \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
