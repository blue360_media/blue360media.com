<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc Api for endeavor_moveorderto table.
 */

namespace Endeavor\MoveOrderTo\Api\Data;

interface EndeavorMoveorderInterface
{

    const OLD_CUSTOMER_ID = 'old_customer_id';
    const NEW_CUSTOMER_ID = 'new_customer_id';
    const CHANGE_BY = 'change_by';
    const CREATED_AT = 'created_at';
    const ORDER_NUMBER = 'order_number';
    const ENDEAVOR_MOVEORDER_ID = 'endeavor_moveorder_id';
    const OLD_EMAIL = 'old_email';

    /**
     * Get endeavor_moveorder_id
     * @return string|null
     */
    public function getEndeavorMoveorderId();

    /**
     * Set endeavor_moveorder_id
     * @param string $endeavor_moveorder_id
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setEndeavorMoveorderId($endeavorMoveorderId);

    /**
     * Get change_by
     * @return string|null
     */
    public function getChangeBy();

    /**
     * Set change_by
     * @param string $change_by
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setChangeBy($change_by);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $created_at
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setCreatedAt($created_at);

    /**
     * Get order_number
     * @return string|null
     */
    public function getOrderNumber();

    /**
     * Set order_number
     * @param string $order_number
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setOrderNumber($order_number);

    /**
     * Get new_customer_id
     * @return string|null
     */
    public function getNewCustomerId();

    /**
     * Set new_customer_id
     * @param string $new_customer_id
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setNewCustomerId($new_customer_id);

    /**
     * Get old_customer_id
     * @return string|null
     */
    public function getOldCustomerId();

    /**
     * Set old_customer_id
     * @param string $old_customer_id
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setOldCustomerId($old_customer_id);
    
    /**
     * Get old_customer_id
     * @return string|null
     */
    public function getOldEmail();

    /**
     * Set old_customer_id
     * @param string $old_customer_id
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setOldEmail($old_email);
}
