<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc Api for endeavor_moveorderto table to manage records by id and delete it or save it.
 */

namespace Endeavor\MoveOrderTo\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface EndeavorMoveorderRepositoryInterface
{


    /**
     * Save endeavor_moveorder
     * @param \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface $endeavorMoveorder
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface $endeavorMoveorder
    );

    /**
     * Retrieve endeavor_moveorder
     * @param string $endeavorMoveorderId
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($endeavorMoveorderId);

    /**
     * Retrieve endeavor_moveorder matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete endeavor_moveorder
     * @param \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface $endeavorMoveorder
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface $endeavorMoveorder
    );

    /**
     * Delete endeavor_moveorder by ID
     * @param string $endeavorMoveorderId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($endeavorMoveorderId);
}
