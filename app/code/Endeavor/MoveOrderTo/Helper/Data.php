<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc Helper to manage data from table endeavor_moveorderto and functionality for moveOrders.
 * 
 * @update_By: Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 27-12-2017.
 * @desc 1. in order to merge account due to eBook access which called in API.
 *       2. update to response customer status and record history.
 */

namespace Endeavor\MoveOrderTo\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {
    /**
     * set Order data to move order table when email is changed.
     * @param String $orderId Order Id
     * @param String $customerEmail Customer Email
     * @param Array $customerData CustomerData
     */
    public function setOrderDataToMoveTableChangedEmail($orderId, $oldCustomerId, $newCustomerId, $oldEmail,$salesforce = null) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $moveOrderRecords = $objectManager->create('\Endeavor\MoveOrderTo\Model\EndeavorMoveorder');
        
        $createdAt = date('Y-m-d H:i:s');
        $moveOrderRecords->setCreatedAt($createdAt);
        $moveOrderRecords->setOrderNumber($orderId);
        $moveOrderRecords->setNewCustomerId($newCustomerId);
        $moveOrderRecords->setOldCustomerId($oldCustomerId);
        $moveOrderRecords->setOldEmail($oldEmail);
        if ($salesforce != null) {
            $moveOrderRecords->setChangeBy($salesforce);
        }
        $moveOrderRecords->save();
    }
    /**
     * change orders to new email when customer changed email.
     * @param String $oldEmail Customer Email
     * 
     * @return Object Object for old customer
     */
    public function changeOldCustomerEmailOrders($newEmail,$oldEmail, $userName = null) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesOrders = $objectManager->create('\Magento\Sales\Model\Order')->getCollection()
                            ->addFieldToFilter('customer_email', $oldEmail);
        if($salesOrders){
            foreach($salesOrders as $salesOrder){
                
                $oldEmail = $salesOrder->getCustomerEmail();
                $customerId = $salesOrder->getCustomerId();
                $orderId = $salesOrder->getEntityId();
                
                $salesOrder->setCustomerEmail($newEmail);
                $salesOrder->save();
                
                $this->setOrderDataToMoveTableChangedEmail($orderId, $customerId, $customerId, $oldEmail, $userName);
            }
        }
    }
    
    /**
     * set customer to Order.
     * @param String $orderId Order Id
     * @param String $customerEmail Customer Email
     * @param Array $customerData CustomerData
     */
    public function setCustomerToOrder($orderId, $customerEmail, $customerData) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')
                ->load($orderId);
        
        $oldCustomerId = $salesOrder->getCustomerId();
        $newCustomerId = $customerData['customerid'];
        $this->setOrderDataToMoveTable($orderId, $oldCustomerId, $newCustomerId);
        
        $salesOrder->setCustomerId($newCustomerId);
        $salesOrder->setCustomerEmail($customerEmail);
        $salesOrder->setCustomerFirstname($customerData['firstName']);
        $salesOrder->setCustomerLastname($customerData['lastname']);
        $salesOrder->setCustomerMiddlename($customerData['middlename']);
        $salesOrder->save();
    }
    /**
     * set Order to Move order table.
     * @param String $orderId Order Id
     * @param String $customerEmail Customer Email
     * @param Array $customerData CustomerData
     */
    public function setOrderDataToMoveTable($orderId, $oldCustomerId, $newCustomerId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $moveOrderRecords = $objectManager->create('\Endeavor\MoveOrderTo\Model\EndeavorMoveorder');
        
        $createdAt = date('Y-m-d H:i:s');
        $moveOrderRecords->setCreatedAt($createdAt);
        $moveOrderRecords->setOrderNumber($orderId);
        $moveOrderRecords->setNewCustomerId($newCustomerId);
        $moveOrderRecords->setOldCustomerId($oldCustomerId);
        $moveOrderRecords->save();
    }
    /**
     * get Customer Data.
     * @param String $oldEmail Customer Email
     * 
     * @return Array $oldCustomerData
     */
    public function getCustomerData($oldEmail) {
        $oldCustomer = $this->getOldCustomerObject($oldEmail);
        $oldCustomerData = array();
        $oldCustomerData['firstName'] = $oldCustomer->getFirstname();
        $oldCustomerData['lastname'] = $oldCustomer->getLastname();
        $oldCustomerData['middlename'] = $oldCustomer->getMiddlename();
        $oldCustomerData['customerid'] = $oldCustomer->getId();
        return $oldCustomerData;
    }
    /**
     * get Customer Data.
     * @param String $oldEmail Customer Email
     * 
     * @return Array $oldCustomerData
     */
    public function setCustomerAttributes($customerId, $customerData) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface');
        $customerRep = $customerRepository->getById($customerId);
        
        if (isset($customerData['tax_exempt_status'])) {
            $customerRep->setCustomAttribute('customer_tax_exempt_status', $customerData['tax_exempt_status']);
        }
        
        if (isset($customerData['customer_blue_customer_number'])) {
            $customerRep->setCustomAttribute('customer_blue_customer_number', $customerData['customer_blue_customer_number']);
        }
        
        if (isset($customerData['customer_blue_account_id'])) {
            $customerRep->setCustomAttribute('customer_blue_account_id', $customerData['customer_blue_account_id']);

        }
        
        if (isset($customerData['customer_group'])) {
            $groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')
                    ->toOptionArray();
            
            foreach ($groupOptions as $group) {
                if (strtolower($group['label']) == strtolower($customerData['customer_group'])) {
                    $customerRep->setGroupId($group['value']);
                }
            }
        }

        if (isset($customerData['customer_role'])) {
            $attributeName = 'customer_role';
            $attributeModel = $objectManager->create('Magento\Customer\Model\Attribute')
                    ->load($attributeName,'attribute_code');
            $options = $attributeModel->getSource()->getAllOptions();
            $optionId = 0;
            foreach ($options as $value) {
                if (strtolower($value['label']) == strtolower($customerData['customer_role'])) {
                   $optionId =  $value['value'];
                }
            }
            $customerRep->setCustomAttribute('customer_role', $optionId);

        }
        $customerRepository->save($customerRep);
    }
    /**
     * get Old Customer Object.
     * @param String $oldEmail Customer Email
     * 
     * @return Object Object for old customer
     */
    public function getOldCustomerObject($oldEmail) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteId = $storeManager->getStore()->getWebsiteId();
        $oldCustomer = $objectManager->create('Magento\Customer\Model\Customer')
                ->setWebsiteId($websiteId)
                ->loadByEmail($oldEmail);
        return $oldCustomer;
    }
    /**
     * change Old Customer Object.
     * @param String $oldEmail Customer Email
     * 
     * @return Object Object for old customer
     */
    public function changeOldCustomerEmail($newEmail, $orderId, $userName = null) {
        //get customerId from salesOrder
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')
                ->load($orderId);
        $customerEmail = $salesOrder->getCustomerEmail();
        $customerId = $salesOrder->getCustomerId();
        //change customer email in customer
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteId = $storeManager->getStore()->getWebsiteId();
        $oldCustomer = $objectManager->create('Magento\Customer\Model\Customer')
                ->setWebsiteId($websiteId)
                ->load($customerId);
        $oldCustomer->setEmail($newEmail);
        $oldCustomer->save();
        
        $this->changeOldCustomerEmailOrders($newEmail,$customerEmail, $userName);
        
    }
    
    /*
     * 
     */
    public function getCustomerAttributes($customerId){
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface');
        $customerRep = $customerRepository->getById($customerId);
        
        $customerData = array();
        $customerData['tax_exempt_status'] = "";
        $customerData['customer_blue_customer_number'] = "";
        $customerData['customer_blue_account_id'] = "";
        $customerData['customer_role'] = "";
        $customerData['customer_group'] = "";
        if ($customerRep->getCustomAttribute('customer_tax_exempt_status')) {
            $customerData['tax_exempt_status'] = $customerRep->getCustomAttribute('customer_tax_exempt_status')->getValue();
        }
        if ($customerRep->getCustomAttribute('customer_blue_customer_number')) {
            $customerData['customer_blue_customer_number'] = $customerRep->getCustomAttribute('customer_blue_customer_number')->getValue();

        }
        if ($customerRep->getCustomAttribute('customer_blue_account_id')) {
            $customerData['customer_blue_account_id'] = $customerRep->getCustomAttribute('customer_blue_account_id')->getValue();

        }
        if ($customerRep->getCustomAttribute('customer_role')) {
            $attributeName = 'customer_role';
            $attributeModel = $objectManager->create('Magento\Customer\Model\Attribute')
                        ->load($attributeName,'attribute_code');
            
            $options = $attributeModel->getSource()->getAllOptions();
            $optionId = $customerRep->getCustomAttribute('customer_role')->getValue();
            
            foreach ($options as $value) {
                if ($value['value'] == $optionId) {
                    $customerData['customer_role'] = $value['label'];
                }
            }

        }
        $customerGroupId = $customerRep->getGroupId();
        $groupOptions = $objectManager->get('\Magento\Customer\Model\ResourceModel\Group\Collection')
                    ->toOptionArray();
        
        foreach ($groupOptions as $group) {
            if ($group['value'] == $customerGroupId) {
                $customerData['customer_group'] = $group['label'];
            }
        }
        
        return $customerData;
    }
    
    /*
     * 
     */
    public function getCustomerIdFromOrder($orderId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($orderId);
        $customerId = $salesOrder->getCustomerId();
        return $customerId;
    }
    
    public function setCustomerToAllOrders($oldCustomerId, $newCustomerData, $newEmail, $userName = null){
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesOrders = $objectManager->create('\Magento\Sales\Model\Order')->getCollection()
                            ->addFieldToFilter('customer_id', $oldCustomerId);
        
        if ($salesOrders) {
            foreach ($salesOrders as $salesOrder) {
                $oldEmail = $salesOrder->getCustomerEmail();
                $customerId = $salesOrder->getCustomerId();
                $orderId = $salesOrder->getEntityId();
                
                $salesOrder->setCustomerId($newCustomerData['customerid']);
                $salesOrder->setCustomerEmail($newEmail);
                $salesOrder->setCustomerFirstname($newCustomerData['firstName']);
                $salesOrder->setCustomerLastname($newCustomerData['lastname']);
                $salesOrder->setCustomerMiddlename($newCustomerData['middlename']);
                $salesOrder->save();
                
                $this->setCustomerToDownloadableOrder($orderId, $oldCustomerId, $newCustomerData['customerid']);
                if ($customerId != $newCustomerData['customerid']) {
                    $this->setOrderDataToMoveTableChangedEmail($orderId, $customerId, $newCustomerData['customerid'], $oldEmail, $userName);
                }
            }
        }
    }
    
    /*
     * 
     */
    public function getOrderIdFromSFOrderId($sfOrderId, $eCommerceOrderId = null) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if ($eCommerceOrderId != null) {
            $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($eCommerceOrderId, 'increment_id');
            $orderId = $salesOrder->getEntityId();
            
        } else {
            $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($sfOrderId, 'sf_order_id');
            $orderId = $salesOrder->getEntityId();
            
        }
        return $orderId;
    }

    /**
     * set customer to purchased.
     * @param String $orderId Order Id
     * @param String $customerId Customer Id
     */
    protected function setCustomerToDownloadableOrder($orderId, $customerId, $newCustomerId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $purchaseLink = $objectManager->get('\Magento\Downloadable\Model\Link\PurchasedFactory');
        $downloadablePurchasedLinks = $purchaseLink->create()->getCollection()
                ->addFieldToFilter('order_id', $orderId)
                ->addFieldToFilter('customer_id', $customerId);
        if ($downloadablePurchasedLinks) {
            foreach ($downloadablePurchasedLinks as $downloadablePurchasedLink) {
                $downloadablePurchasedLink->setCustomerId($newCustomerId);
                $downloadablePurchasedLink->save();
            }
        }
    }
    
    public function setCustomerRoleAttribute($customerId) {
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface');
        $customerRep = $customerRepository->getById($customerId);
        
        $customerRole = "customer";
        $attributeName = 'customer_role';
        
        $attributeModel = $objectManager->create('Magento\Customer\Model\Attribute')
                ->load($attributeName,'attribute_code');
        
        $options = $attributeModel->getSource()->getAllOptions();
        $optionId = 0;
        
        foreach ($options as $value) {
            if (strtolower($value['label']) == strtolower($customerRole)) {
               $optionId =  $value['value'];
            }
        }
        
        $customerRep->setCustomAttribute('customer_role', $optionId);
        $customerRepository->save($customerRep);
    }
    
    /**
     * @param string $oldCustomerId
     * @param Array $newCustomerData
     * @param string $newEmail
     * @param string $currentOrderId
     * @param string $userName
     * 
     */
    public function setCustomerToCurrentOrder($oldCustomerId, $newCustomerData, $newEmail, $currentOrderId, $userName = null){
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $salesOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($currentOrderId);
        $oldEmail = $salesOrder->getCustomerEmail();
        $customerId = $salesOrder->getCustomerId();
        $orderId = $salesOrder->getEntityId();

        $salesOrder->setCustomerId($newCustomerData['customerid']);
        $salesOrder->setCustomerEmail($newEmail);
        $salesOrder->setCustomerFirstname($newCustomerData['firstName']);
        $salesOrder->setCustomerLastname($newCustomerData['lastname']);
        $salesOrder->setCustomerMiddlename($newCustomerData['middlename']);
        $salesOrder->save();

        $this->setCustomerToDownloadableOrder($orderId, $oldCustomerId, $newCustomerData['customerid']);
        
        $this->setOrderDataToMoveTableChangedEmail($orderId, $customerId, $newCustomerData['customerid'], $oldEmail, $userName);
        
    }
    
    /**
     * @param string $customerId
     */
    public function sendWelcomeMessage($customerId) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteId = $storeManager->getStore()->getWebsiteId();
        
        $customer = $objectManager->create('Magento\Customer\Model\Customer')
                ->setWebsiteId($websiteId)
                ->load($customerId);
        
        $customer->sendNewAccountEmail('registered');
    }
}
