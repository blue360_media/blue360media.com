<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc initalization for Api calls to manage data for endeavor_moveorderto.
 */

namespace Endeavor\MoveOrderTo\Model;

use Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface;

class EndeavorMoveorder extends \Magento\Framework\Model\AbstractModel implements EndeavorMoveorderInterface
{

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\MoveOrderTo\Model\ResourceModel\EndeavorMoveorder');
    }

    /**
     * Get endeavor_moveorder_id
     * @return string
     */
    public function getEndeavorMoveorderId()
    {
        return $this->getData(self::ENDEAVOR_MOVEORDER_ID);
    }

    /**
     * Set endeavor_moveorder_id
     * @param string $endeavorMoveorderId
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setEndeavorMoveorderId($endeavorMoveorderId)
    {
        return $this->setData(self::ENDEAVOR_MOVEORDER_ID, $endeavorMoveorderId);
    }

    /**
     * Get change_by
     * @return string
     */
    public function getChangeBy()
    {
        return $this->getData(self::CHANGE_BY);
    }

    /**
     * Set change_by
     * @param string $change_by
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setChangeBy($change_by)
    {
        return $this->setData(self::CHANGE_BY, $change_by);
    }

    /**
     * Get created_at
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $created_at
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setCreatedAt($created_at)
    {
        return $this->setData(self::CREATED_AT, $created_at);
    }

    /**
     * Get order_number
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->getData(self::ORDER_NUMBER);
    }

    /**
     * Set order_number
     * @param string $order_number
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setOrderNumber($order_number)
    {
        return $this->setData(self::ORDER_NUMBER, $order_number);
    }

    /**
     * Get new_customer_id
     * @return string
     */
    public function getNewCustomerId()
    {
        return $this->getData(self::NEW_CUSTOMER_ID);
    }

    /**
     * Set new_customer_id
     * @param string $new_customer_id
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setNewCustomerId($new_customer_id)
    {
        return $this->setData(self::NEW_CUSTOMER_ID, $new_customer_id);
    }

    /**
     * Get old_customer_id
     * @return string
     */
    public function getOldCustomerId()
    {
        return $this->getData(self::OLD_CUSTOMER_ID);
    }

    /**
     * Set old_customer_id
     * @param string $old_customer_id
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setOldCustomerId($old_customer_id)
    {
        return $this->setData(self::OLD_CUSTOMER_ID, $old_customer_id);
    }
    
    /**
     * Get old_customer_id
     * @return string
     */
    public function getOldEmail()
    {
        return $this->getData(self::OLD_EMAIL);
    }

    /**
     * Set old_customer_id
     * @param string $old_customer_id
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface
     */
    public function setOldEmail($old_email)
    {
        return $this->setData(self::OLD_EMAIL, $old_email);
    }
}
