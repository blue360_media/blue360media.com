<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc Model collection from endeavor_moveorderto table.
 */

namespace Endeavor\MoveOrderTo\Model\ResourceModel\EndeavorMoveorder;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Endeavor\MoveOrderTo\Model\EndeavorMoveorder',
            'Endeavor\MoveOrderTo\Model\ResourceModel\EndeavorMoveorder'
        );
    }
}
