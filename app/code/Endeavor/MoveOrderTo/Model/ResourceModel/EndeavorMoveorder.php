<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc Model collection by id for endeavor_moveorderto table.
 */

namespace Endeavor\MoveOrderTo\Model\ResourceModel;

class EndeavorMoveorder extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('endeavor_moveorderto', 'endeavor_moveorder_id');
    }
}
