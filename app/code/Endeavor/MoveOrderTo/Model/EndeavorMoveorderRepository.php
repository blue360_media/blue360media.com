<?php
/**
 * Copyright © 2017 Endeavor, Inc. All rights reserved.
 * See COPYING.txt for license details.
 * 
 * @author Mohammad Hamadneh <mhamadneh@endeavorpal.com>
 * @date 19-12-2017.
 * @desc initalization for Api calls to manage data for endeavor_moveorderto for save and delete data.
 */

namespace Endeavor\MoveOrderTo\Model;

use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\CouldNotSaveException;
use Endeavor\MoveOrderTo\Model\ResourceModel\EndeavorMoveorder as ResourceEndeavorMoveorder;
use Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderSearchResultsInterfaceFactory;
use Endeavor\MoveOrderTo\Model\ResourceModel\EndeavorMoveorder\CollectionFactory as EndeavorMoveorderCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterfaceFactory;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Exception\CouldNotDeleteException;
use Endeavor\MoveOrderTo\Api\EndeavorMoveorderRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;

class EndeavorMoveorderRepository implements endeavorMoveorderRepositoryInterface
{

    protected $endeavorMoveorderFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    private $storeManager;

    protected $searchResultsFactory;

    protected $endeavorMoveorderCollectionFactory;

    protected $resource;

    protected $dataEndeavorMoveorderFactory;


    /**
     * @param ResourceEndeavorMoveorder $resource
     * @param EndeavorMoveorderFactory $endeavorMoveorderFactory
     * @param EndeavorMoveorderInterfaceFactory $dataEndeavorMoveorderFactory
     * @param EndeavorMoveorderCollectionFactory $endeavorMoveorderCollectionFactory
     * @param EndeavorMoveorderSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ResourceEndeavorMoveorder $resource,
        EndeavorMoveorderFactory $endeavorMoveorderFactory,
        EndeavorMoveorderInterfaceFactory $dataEndeavorMoveorderFactory,
        EndeavorMoveorderCollectionFactory $endeavorMoveorderCollectionFactory,
        EndeavorMoveorderSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager
    ) {
        $this->resource = $resource;
        $this->endeavorMoveorderFactory = $endeavorMoveorderFactory;
        $this->endeavorMoveorderCollectionFactory = $endeavorMoveorderCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataEndeavorMoveorderFactory = $dataEndeavorMoveorderFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface $endeavorMoveorder
    ) {
        /* if (empty($endeavorMoveorder->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $endeavorMoveorder->setStoreId($storeId);
        } */
        try {
            $endeavorMoveorder->getResource()->save($endeavorMoveorder);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the endeavorMoveorder: %1',
                $exception->getMessage()
            ));
        }
        return $endeavorMoveorder;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($endeavorMoveorderId)
    {
        $endeavorMoveorder = $this->endeavorMoveorderFactory->create();
        $endeavorMoveorder->getResource()->load($endeavorMoveorder, $endeavorMoveorderId);
        if (!$endeavorMoveorder->getId()) {
            throw new NoSuchEntityException(__('endeavor_moveorder with id "%1" does not exist.', $endeavorMoveorderId));
        }
        return $endeavorMoveorder;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->endeavorMoveorderCollectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() === 'store_id') {
                    $collection->addStoreFilter($filter->getValue(), false);
                    continue;
                }
                $condition = $filter->getConditionType() ?: 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
        
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setTotalCount($collection->getSize());
        $searchResults->setItems($collection->getItems());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderInterface $endeavorMoveorder
    ) {
        try {
            $endeavorMoveorder->getResource()->delete($endeavorMoveorder);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the endeavor_moveorder: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($endeavorMoveorderId)
    {
        return $this->delete($this->getById($endeavorMoveorderId));
    }
}
