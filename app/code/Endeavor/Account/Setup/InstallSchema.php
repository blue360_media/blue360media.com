<?php
namespace Endeavor\Account\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        if (!$installer->tableExists('endeavor_account')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('endeavor_account'))
                ->addColumn(
                    'account_id',
                    Table::TYPE_INTEGER,
                    10,
                    ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
                    'Account Id'
                )
                ->addColumn(
                    'organization_name',
                    Table::TYPE_TEXT, 
                    100, 
                    ['nullable' => false],
                    'Organization name'
                )
                ->addColumn(
                    'first_name',
                    Table::TYPE_TEXT, 
                    25, 
                    ['nullable' => false],
                    'First name'
                )    
                ->addColumn(
                    'last_name',
                    Table::TYPE_TEXT, 
                    25, 
                    ['nullable' => false],
                    'Last Name'
                )
                ->addColumn(
                    'email', 
                    Table::TYPE_TEXT,
                    50, 
                    ['nullable' => false],
                    'Email'
                )
                ->addColumn(
                    'phone',
                    Table::TYPE_TEXT,
                    12,
                    ['default' => ''],
                    'Primary Contact Phone'
                )
                ->addColumn(
                    'shipping_street_1',
                    Table::TYPE_TEXT,
                    255, 
                    ['nullable' => false],
                    'Shipping Street 1'
                )
                ->addColumn(
                    'shipping_street_2',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Shipping Street 2'
                )
                ->addColumn(
                'shipping_city',
                    Table::TYPE_TEXT,
                    30,
                    ['nullable' => true],
                    'Shipping City'
                )
                ->addColumn(
                    'shipping_state',
                    Table::TYPE_TEXT, 
                    3, 
                    ['nullable' => false],
                    'Shipping State'
                )
                ->addColumn(
                    'shipping_zip',
                    Table::TYPE_TEXT,
                    10,
                    ['nullable' => false],
                    'Shipping Zip'
                )
                ->addColumn(
                    'billing_street_1',
                    Table::TYPE_TEXT,
                    100,
                    ['nullable' => false],
                    'Billing Street 1'
                )
                ->addColumn(
                    'billing_street_2',
                    Table::TYPE_TEXT,
                    100, 
                    ['nullable' => true],
                    'Billing Street 2'
                )
                ->addColumn(
                    'billing_city',
                    Table::TYPE_TEXT,
                    30,
                    ['nullable' => true],
                    'Billing City'
                )
                ->addColumn(
                    'billing_state',
                    Table::TYPE_TEXT,
                    3,
                    ['nullable' => false],
                    'Billing State'
                )
                ->addColumn('billing_zip',
                    Table::TYPE_TEXT,
                    10,
                    ['nullable' => false],
                    'Billing Zip')
                ->addColumn(
                    'account_number',
                    Table::TYPE_TEXT,
                    50,
                    ['nullable' => false],
                    'Account Nmumber'
                )    
                ->addColumn(
                    'publication_name_1',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Publication Name 1'
                )
                ->addColumn(
                    'publication_number_1',
                    Table::TYPE_TEXT,
                    30,
                    ['nullable' => true],
                    'Publication Number 1'
                )
                ->addColumn('publication_name_2',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Publication Name 2')
                ->addColumn(
                    'publication_number_2',
                    Table::TYPE_TEXT,
                    30,
                    ['nullable' => true],
                    'Publication Number 2'
                )
                ->addColumn(
                    'publication_name_3',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Publication Name 3'
                )
                ->addColumn(
                    'publication_number_3',
                    Table::TYPE_TEXT,
                    30,
                    ['nullable' => true],
                    'Publication Number 3'
                )
                ->addColumn(
                    'number_of_people_in_your_organization',
                    Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => true],
                    'Number of people in your organization'
                )
                ->addColumn(
                    'do_we_have_permission_to_send_you_electronic_invoices',
                    Table::TYPE_SMALLINT,
                    1,
                    ['nullable' => true],
                    'Do we have permission to send you electronic invoices'
                )
                ->addColumn(
                    'do_you_want_to_continue_receiving_your_publications',
                    Table::TYPE_SMALLINT,
                    1,
                    ['nullable' => true],
                    'Do you want to continue receiving your publications with every update'
                )
                ->addColumn(
                    'do_you_want_to_continue_receiving_notifications',
                    Table::TYPE_SMALLINT,
                    1,
                    ['nullable' => true],
                    'Do you want to continue receiving notifications on the publications with every update'
                )    
                ->addColumn(
                    'anything_else_we_can_help_you_with',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Anything else we can help you with'
                )
                ->addColumn(
                    'ip_address',
                    Table::TYPE_TEXT,
                    20,
                    ['nullable' => true],
                    'Ip Address'
                )
                ->setComment('Endeavor Account');

            $installer->getConnection()->createTable($table);
        }
        
        $installer->endSetup();
    }
}
