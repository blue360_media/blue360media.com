<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Account\Controller\Index;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ObjectManager;

class Post extends \Endeavor\Account\Controller\Index
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * Post user question
     *
     * @return void
     * @throws \Exception
     */
    public function execute()
    {
        $post = $this->getRequest()->getPostValue();
        if (!$post) {
            $this->_redirect('*/*/');
            return;
        }
        $this->inlineTranslation->suspend();
        try {
            $postObject = new \Magento\Framework\DataObject();
            $postObject->setData($post);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $account = $objectManager->create('\Endeavor\Account\Model\Account');
            $account->setOrganizationName(trim($post['organization-name']));
            $account->setFirstName(trim($post['first-name']));
            $account->setLastName(trim($post['last-name']));
            $account->setEmail(trim($post['email']));
            $account->setPhone(trim($post['telephone']));
            $account->setShippingStreet1(trim($post['shipping-street1']));
            $account->setShippingStreet2(trim($post['shipping-street2']));
            $account->setShippingCity(trim($post['shipping-city']));
            $account->setShippingState(trim($post['shipping-state']));
            $account->setShippingZip(trim($post['shipping-zip']));
            $account->setBillingStreet1(trim($post['billing-street1']));
            $account->setBillingStreet2(trim($post['billing-street2']));
            $account->setBillingCity(trim($post['billing-city']));
            $account->setBillingState(trim($post['billing-state']));
            $account->setBillingZip(trim($post['billing-zip']));
            $account->setAccountNumber(trim($post['account']));
            $account->setPublicationName1(trim($post['publication1']));
            $account->setPublicationNumber1(trim($post['publication-number-1']));
            $account->setPublicationName2(trim($post['publication2']));
            $account->setPublicationNumber2(trim($post['publication-number-2']));
            $account->setPublicationName3(trim($post['publication3']));
            $account->setPublicationNumber3(trim($post['publication-number-3']));
            $account->setNumberOfPeopleInYourOrganization(trim($post['people']));
            if (strcasecmp( trim($post['permission']), "Yes" ) == 0) {
                $account->setDoWeHavePermissionToSendYouElectronicInvoices(1);    
                        
            } else {             
                $account->setDoWeHavePermissionToSendYouElectronicInvoices(0);
                    
            }
            if (strcasecmp( trim($post['subscribing']), "Yes" ) == 0) {
                $account->setDoYouWantToContinueReceivingYourPublications(1);
                
            } else {
                $account->setDoYouWantToContinueReceivingYourPublications(0);
                    
                }
            if (strcasecmp(trim($post['subscribing2']), "Yes" ) == 0) {
               $account->setDoYouWantToContinueReceivingNotifications(1);
               
            } else{
                $account->setDoYouWantToContinueReceivingNotifications(0);
                    
            }
            $account->setAnythingElseWeCanHelpYouWith(trim($post['help']));
            $objectAddress = \Magento\Framework\App\ObjectManager::getInstance();
            $address = $objectAddress->get('Magento\Framework\HTTP\PhpEnvironment\RemoteAddress');
            $account->setIpAddress($address->getRemoteAddress());
            $account->save();
            
            $error = false;
            if (!\Zend_Validate::is(trim($post['name']), 'NotEmpty')) {
                $error = true;
                
            }
            if (!\Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                $error = true;
                
            }
            if (!\Zend_Validate::is(trim($post['shipping']), 'NotEmpty')) {
                $error = true;
                
            }
            if (!\Zend_Validate::is(trim($post['billing']), 'NotEmpty')) {
                $error = true;
                
            }
            
            if (!\Zend_Validate::is(trim($post['account']), 'NotEmpty')) {
                $error = true;
                
            }
            if (!\Zend_Validate::is(trim($post['publication1']), 'NotEmpty')) {
                $error = true;
                
            }
            
            if (\Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                $error = true;
                
            }
            if ($error) {
                throw new \Exception();
                
            }
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $transport = $this->_transportBuilder
                ->setTemplateIdentifier($this->scopeConfig->getValue(self::XML_PATH_EMAIL_TEMPLATE, $storeScope))
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Backend\App\Area\FrontNameResolver::AREA_CODE,
                        'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars(['data' => $postObject])
                ->setFrom($this->scopeConfig->getValue(self::XML_PATH_EMAIL_SENDER, $storeScope))
                ->addTo($this->scopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, $storeScope))
                ->setReplyTo($post['email'])
                ->getTransport();
                
            $transport->sendMessage();
             
            $this->inlineTranslation->resume();
          
            $this->messageManager->addSuccess(
                __('Thank you for helping us keep our records straight! We will reach out to you to confirm your next shipment.')
            );
           
            $this->getDataPersistor()->clear('account');
              
            $this->_redirect('account');
            return;
        } catch (\Exception $e) {
            $this->inlineTranslation->resume();
            $this->messageManager->addError(
                __('We can\'t process your request right now. Sorry, that\'s all we know.')
            );
            $this->getDataPersistor()->set('account', $post);
            $this->_redirect('account');
            return;
        }
    }

    /**
     * Get Data Persistor
     *
     * @return DataPersistorInterface
     */
    private function getDataPersistor()
    {
        if ($this->dataPersistor === null) {
            $this->dataPersistor = ObjectManager::getInstance()
                ->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }
}
