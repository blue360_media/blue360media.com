<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Account\Block;

use Magento\Framework\View\Element\Template;

/**
 * Main account form block
 */
class AccountForm extends Template
{
    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
    }

    /**
     * Returns action url for account form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('account/index/post', ['_secure' => true]);
    }
}
