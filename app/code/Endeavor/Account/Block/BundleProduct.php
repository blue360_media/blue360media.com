<?php
/**
 * Copyright © 2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Endeavor\Account\Block;

/**
 * Main bundle product 
 */
class BundleProduct extends \Magento\Framework\View\Element\Template 
{   
     /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,           
        array $data = []
    ) {
       
        parent::__construct($context, $data);
    }
    
     /**
     * Get bundle product 
     * 
     * @return coluction for bundle product
     */
    public function getProductBundle()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
        $collectionB = $productCollection->create(
            )->addAttributeToSelect(array('name','publication_number')
            )->addFieldToFilter('type_id', array('eq' => 'bundle')
            )->load();   
        
        return $collectionB;  
    }
  
}
