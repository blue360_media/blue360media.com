<?php
namespace Endeavor\Account\Model\ResourceModel\Account;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'account_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Account\Model\Account', 'Endeavor\Account\Model\ResourceModel\Account');
    }
}
