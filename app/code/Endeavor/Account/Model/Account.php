<?php
namespace Endeavor\Account\Model;

use Magento\Framework\DataObject\IdentityInterface;

class Account extends \Magento\Framework\Model\AbstractModel implements IdentityInterface
{
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'end_account';

    /**
     * @var string
     */
    protected $_cacheTag = 'end_account';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'end_account';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\Account\Model\ResourceModel\Account');
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

}
