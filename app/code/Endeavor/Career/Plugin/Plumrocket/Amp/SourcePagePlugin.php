<?php
  

namespace Endeavor\Career\Plugin\Plumrocket\Amp;

/**
 * Plugin for source page (amp extension by Plumrocket)
 */
class SourcePagePlugin
{
    /**
     * Add endeavor career pages to soruce
     * @param  \Plumrocket\Amp\Model\System\Config\Source\Page $page
     * @param  array $pages
     * @return array
     */
    public function afterToArray(\Plumrocket\Amp\Model\System\Config\Source\Page $page, $pages)
    {
        $pages['endeavor_career_index_index'] = __('Career Main Page');
        $pages['endeavor_career_job_view'] = __('Career Job Pages');
        $pages['endeavor_career_category_view'] = __('Career Category Pages');
        $pages['endeavor_career_category_view'] = __('Career Category Pages');
        $pages['endeavor_career_archive_view'] = __('Career Archive Pages');
        $pages['endeavor_career_author_view'] = __('Career Author Pages');
        $pages['endeavor_career_tag_view'] = __('Career Tag Pages');

        return $pages;
    }

}