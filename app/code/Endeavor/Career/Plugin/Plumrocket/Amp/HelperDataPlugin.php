<?php
  

namespace Endeavor\Career\Plugin\Plumrocket\Amp;

/**
 * Plugin for helper data (amp extension by Plumrocket)
 */
class HelperDataPlugin
{
    /**
     * Add endeavor career actions to allowed list
     * @param  \Plumrocket\Amp\Helper\Data $helper
     * @param  array $allowedPages
     * @return array
     */
    public function afterGetAllowedPages(\Plumrocket\Amp\Helper\Data $helper, $allowedPages)
    {
        foreach ($allowedPages as &$value) {
            if (strpos($value, 'endeavor_career_') === 0) {
                $value = str_replace('endeavor_career_', 'career_', $value);
            }
        }

        return $allowedPages;
    }

}