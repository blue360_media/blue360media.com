<?php
  

namespace Endeavor\Career\Ui\DataProvider\Job\Related;

use \Magento\Ui\DataProvider\AbstractDataProvider;
use Endeavor\Career\Model\ResourceModel\Job\Collection;
use Endeavor\Career\Model\ResourceModel\Job\CollectionFactory;
use Magento\Framework\App\RequestInterface;

/**
 * Class JobDataProvider
 */
class JobDataProvider extends AbstractDataProvider
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var job
     */
    private $job;

    /**
     * Construct
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param RequestInterface $request
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $request,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->collection = $collectionFactory->create();
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function getCollection()
    {
        /** @var Collection $collection */
        $collection = parent::getCollection();

        if (!$this->getJob()) {
            return $collection;
        }

        $collection->addFieldToFilter(
            $collection->getIdFieldName(),
            ['nin' => [$this->getJob()->getId()]]
        );

        return $this->addCollectionFilters($collection);
    }

    /**
     * Retrieve job
     *
     * @return JobInterface|null
     */
    protected function getJob()
    {
        if (null !== $this->job) {
            return $this->job;
        }

        if (!($id = $this->request->getParam('current_job_id'))) {
            return null;
        }

        return $this->job = $this->jobRepository->getById($id);
    }
}
