<?php
  

namespace Endeavor\Career\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * Career setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'endeavor_career_job'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('endeavor_career_job'))
        ->addColumn(
            'job_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
            'Job ID'
        )
        ->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Job Title'
        )
        ->addColumn(
            'meta_keywords',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            ['nullable' => true],
            'Job Meta Keywords'
        )
        ->addColumn(
            'meta_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            ['nullable' => true],
            'Job Meta Description'
        )
        ->addColumn(
            'identifier',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            ['nullable' => true, 'default' => null],
            'Job String Identifier'
        )
        ->addColumn(
            'content_heading',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Job Content Heading'
        )
        ->addColumn(
            'content',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Job Content'
        )
        ->addColumn(
            'creation_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'Job Creation Time'
        )
        ->addColumn(
            'update_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'Job Modification Time'
        )
        ->addColumn(
            'publish_time',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [],
            'Job Publish Time'
        )
        ->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Is Job Active'
        )
        ->addIndex(
            $installer->getIdxName('endeavor_career_job', ['identifier']),
            ['identifier']
        )
        ->addIndex(
            $setup->getIdxName(
                $installer->getTable('endeavor_career_job'),
                ['title', 'meta_keywords', 'meta_description', 'identifier', 'content'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['title', 'meta_keywords', 'meta_description', 'identifier', 'content'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )
        ->setComment(
            'Endeavor Career Job Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'endeavor_career_job_store'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('endeavor_career_job_store')
        )
        ->addColumn(
            'job_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true, 'unsigned' => true],
            'Job ID'
        )
        ->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Store ID'
        )
        ->addIndex(
            $installer->getIdxName('endeavor_career_job_store', ['store_id']),
            ['store_id']
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_job_store', 'job_id', 'endeavor_career_job', 'job_id'),
            'job_id',
            $installer->getTable('endeavor_career_job'),
            'job_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_job_store', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->setComment(
            'Endeavor Career Job To Store Linkage Table'
        );
        $installer->getConnection()->createTable($table);


        $table = $installer->getConnection()->newTable(
            $installer->getTable('endeavor_career_category')
        )
        ->addColumn(
            'category_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true],
            'Category ID'
        )
        ->addColumn(
            'title',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Category Title'
        )
        ->addColumn(
            'meta_keywords',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            ['nullable' => true],
            'Category Meta Keywords'
        )
        ->addColumn(
            'meta_description',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '64k',
            ['nullable' => true],
            'Category Meta Description'
        )
        ->addColumn(
            'identifier',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            100,
            ['nullable' => true, 'default' => null],
            'Category String Identifier'
        )
        ->addColumn(
            'content_heading',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Category Content Heading'
        )
        ->addColumn(
            'content',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            '2M',
            [],
            'Category Content'
        )
        ->addColumn(
            'path',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Category Path'
        )
        ->addColumn(
            'position',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false],
            'Category Position'
        )
        ->addColumn(
            'is_active',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['nullable' => false, 'default' => '1'],
            'Is Category Active'
        )
        ->addIndex(
            $installer->getIdxName('endeavor_career_category', ['identifier']),
            ['identifier']
        )
        ->addIndex(
            $setup->getIdxName(
                $installer->getTable('endeavor_career_category'),
                ['title', 'meta_keywords', 'meta_description', 'identifier', 'content'],
                AdapterInterface::INDEX_TYPE_FULLTEXT
            ),
            ['title', 'meta_keywords', 'meta_description', 'identifier', 'content'],
            ['type' => AdapterInterface::INDEX_TYPE_FULLTEXT]
        )
        ->setComment(
            'Endeavor Career Category Table'
        );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'endeavor_career_category_store'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('endeavor_career_category_store')
        )
        ->addColumn(
            'category_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true, 'unsigned' => true],
            'Category ID'
        )
        ->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Store ID'
        )
        ->addIndex(
            $installer->getIdxName('endeavor_career_category_store', ['store_id']),
            ['store_id']
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_category_store', 'category_id', 'endeavor_career_category', 'category_id'),
            'category_id',
            $installer->getTable('endeavor_career_category'),
            'category_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_category_store', 'store_id', 'store', 'store_id'),
            'store_id',
            $installer->getTable('store'),
            'store_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->setComment(
            'Endeavor Career Category To Store Linkage Table'
        );
        $installer->getConnection()->createTable($table);


        /**
         * Create table 'endeavor_career_job_category'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('endeavor_career_job_category')
        )
        ->addColumn(
            'job_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true, 'unsigned' => true],
            'Job ID'
        )
        ->addColumn(
            'category_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true, 'unsigned' => true],
            'Category ID'
        )
        ->addIndex(
            $installer->getIdxName('endeavor_career_job_category', ['category_id']),
            ['category_id']
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_job_category', 'job_id', 'endeavor_career_job', 'job_id'),
            'job_id',
            $installer->getTable('endeavor_career_job'),
            'job_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_job_category', 'category_id', 'endeavor_career_category', 'category_id'),
            'category_id',
            $installer->getTable('endeavor_career_category'),
            'category_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->setComment(
            'Endeavor Career Job To Category Linkage Table'
        );
        $installer->getConnection()->createTable($table);
        

        /**
         * Create table 'endeavor_career_job_relatedproduct'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('endeavor_career_job_relatedproduct')
        )
        ->addColumn(
            'job_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true, 'unsigned' => true],
            'Job ID'
        )
        ->addColumn(
            'related_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'primary' => true],
            'Related Product ID'
        )
        ->addIndex(
            $installer->getIdxName('endeavor_career_job_relatedproduct', ['related_id']),
            ['related_id']
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_job_relatedproduct', 'job_id', 'endeavor_career_job', 'job_id'),
            'job_id',
            $installer->getTable('endeavor_career_job'),
            'job_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_job_relatedproduct', 'related_id', 'catalog_product_entity', 'entity_id'),
            'related_id',
            $installer->getTable('catalog_product_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->setComment(
            'Endeavor Career Job To Product Linkage Table'
        );
        $installer->getConnection()->createTable($table);


        /**
         * Create table 'endeavor_career_job_relatedproduct'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('endeavor_career_job_relatedjob')
        )
        ->addColumn(
            'job_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true, 'unsigned' => true],
            'Job ID'
        )
        ->addColumn(
            'related_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['nullable' => false, 'primary' => true],
            'Related Job ID'
        )
        ->addIndex(
            $installer->getIdxName('endeavor_career_job_relatedproduct', ['related_id']),
            ['related_id']
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_job_relatedproduct1', 'job_id', 'endeavor_career_job', 'job_id'),
            'job_id',
            $installer->getTable('endeavor_career_job'),
            'job_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->addForeignKey(
            $installer->getFkName('endeavor_career_job_relatedproduct2', 'related_id', 'endeavor_career_job', 'job_id'),
            'job_id',
            $installer->getTable('endeavor_career_job'),
            'job_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )
        ->setComment(
            'Endeavor Career Job To Job Linkage Table'
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
