<?php
  

namespace Endeavor\Career\Setup;

use Endeavor\Career\Model\Job;
use Endeavor\Career\Model\JobFactory;
use Magento\Framework\Module\Setup\Migration;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Job factory
     *
     * @var \Endeavor\Career\Model\JobFactory
     */
    private $_jobFactory;

    /**
     * Init
     *
     * @param \Endeavor\Career\Model\JobFactory $jobFactory
     */
    public function __construct(\Endeavor\Career\Model\JobFactory $jobFactory)
    {
        $this->_jobFactory = $jobFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
       /*$data = [
            'title' => 'Hello world!',
            'meta_keywords' => 'magento 2 career',
            'meta_description' => 'Magento 2 career default job.',
            'identifier' => 'hello-world',
            'content_heading' => 'Hello world!',
            'content' => '<p>Welcome to <a title="Endeavor - solutions for Magento 2" href="http://endeavor.com/" target="_blank">Endeavor</a> career extension for Magento&reg; 2. This is your first job. Edit or delete it, then start careerging!</p>
            <p><!-- pagebreak --></p>
            <p>Please also read&nbsp;<a title="Magento 2 Career online documentation" href="http://endeavor.com/docs/magento-2-career/" target="_blank">Online documentation</a>&nbsp;and&nbsp;<a href="http://endeavor.com/career/add-read-more-tag-to-career-job-content/" target="_blank">How to add "read more" tag to job content</a></p>
            <p>Follow Endeavor on:</p>
            <p><a title="Career Extension for Magento 2 code" href="https://github.com/endeavor/module-career" target="_blank">GitHub</a>&nbsp;|&nbsp;<a href="https://twitter.com/magento2fan" target="_blank">Twitter</a>&nbsp;|&nbsp;<a href="https://www.facebook.com/endeavor/" target="_blank">Facebook</a>&nbsp;|&nbsp;<a href="https://plus.google.com/+Endeavor_Magento_2/jobs/" target="_blank">Google +</a></p>',
            'store_ids' => [0]
        ];

        $this->_jobFactory->create()->setData($data)->save();*/
    }

}
