<?php
  

namespace Endeavor\Career\Model;

/**
 * Job management model
 */
class JobManagement extends AbstractManagement
{
    /**
     * @var \Endeavor\Career\Model\JobFactory
     */
    protected $_itemFactory;

    /**
     * Initialize dependencies.
     *
     * @param \Endeavor\Career\Model\JobFactory $jobFactory
     */
    public function __construct(
        \Endeavor\Career\Model\JobFactory $jobFactory
    ) {
        $this->_itemFactory = $jobFactory;
    }

}
