<?php
  

namespace Endeavor\Career\Model;

/**
 * Category management model
 */
class CategoryManagement extends AbstractManagement
{
    /**
     * @var \Endeavor\Career\Model\CategoryFactory
     */
    protected $_itemFactory;

    /**
     * Initialize dependencies.
     *
     * @param \Endeavor\Career\Model\CategoryFactory $categoryFactory
     */
    public function __construct(
        \Endeavor\Career\Model\CategoryFactory $categoryFactory
    ) {
        $this->_itemFactory = $categoryFactory;
    }

}
