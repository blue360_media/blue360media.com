<?php
  

namespace Endeavor\Career\Model\ResourceModel\Tag;

/**
 * Career tag collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Constructor
     * Configures collection
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Endeavor\Career\Model\Tag', 'Endeavor\Career\Model\ResourceModel\Tag');
    }

}
