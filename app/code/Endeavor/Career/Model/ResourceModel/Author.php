<?php
  

namespace Endeavor\Career\Model\ResourceModel;

/**
 * Career author resource model
 */
class Author extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model
     * Get tablename from config
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('admin_user', 'user_id');
    }

}
