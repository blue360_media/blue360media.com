<?php
  

namespace Endeavor\Career\Block\Catalog\Product;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\View\Element\AbstractBlock;

/**
 * Career job related jobs block
 */
class RelatedJobs extends \Endeavor\Career\Block\Job\JobList\AbstractList
{

    /**
     * Prepare jobs collection
     *
     * @return void
     */
    protected function _prepareJobCollection()
    {
        $pageSize = (int) $this->_scopeConfig->getValue(
            'mfcareer/product_page/number_of_related_jobs',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        if (!$pageSize) {
            $pageSize = 5;
        }
        $this->setPageSize($pageSize);

        parent::_prepareJobCollection();

        $product = $this->getProduct();
        $this->_jobCollection->getSelect()->joinLeft(
            ['rl' => $product->getResource()->getTable('endeavor_career_job_relatedproduct')],
            'main_table.job_id = rl.job_id',
            ['position']
        )->where(
            'rl.related_id = ?',
            $product->getId()
        );
    }

    /**
     * Retrieve true if Display Related Jobs enabled
     * @return boolean
     */
    public function displayJobs()
    {
        return (bool) $this->_scopeConfig->getValue(
            'mfcareer/product_page/related_jobs_enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Retrieve jobs instance
     *
     * @return \Endeavor\Career\Model\Category
     */
    public function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product',
                $this->_coreRegistry->registry('current_product')
            );
        }
        return $this->getData('product');
    }

    /**
     * Get Block Identities
     * @return Array
     */
    public function getIdentities()
    {
        return [\Magento\Catalog\Model\Product::CACHE_TAG . '_relatedjobs_'.$this->getJob()->getId()  ];
    }
}
