<?php
  

namespace Endeavor\Career\Block\Author;

use Magento\Store\Model\ScopeInterface;

/**
 * Career author jobs list
 */
class JobList extends \Endeavor\Career\Block\Job\JobList
{
    /**
     * Prepare jobs collection
     *
     * @return void
     */
    protected function _prepareJobCollection()
    {
        parent::_prepareJobCollection();
        if ($author = $this->getAuthor()) {
            $this->_jobCollection->addAuthorFilter($author);
        }
    }

    /**
     * Retrieve author instance
     *
     * @return \Endeavor\Career\Model\Author
     */
    public function getAuthor()
    {
        return $this->_coreRegistry->registry('current_career_author');
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        if ($author = $this->getAuthor()) {
            $this->_addBreadcrumbs($author);
            $this->pageConfig->addBodyClass('career-author-' . $author->getIdentifier());
            $this->pageConfig->getTitle()->set($author->getTitle());
            $this->pageConfig->addRemotePageAsset(
                $author->getAuthorUrl(),
                'canonical',
                ['attributes' => ['rel' => 'canonical']]
            );
        }

        return parent::_prepareLayout();
    }

    /**
     * Prepare breadcrumbs
     *
     * @param \Endeavor\Career\Model\Author $author
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function _addBreadcrumbs($author)
    {
        if ($this->_scopeConfig->getValue('web/default/show_cms_breadcrumbs', ScopeInterface::SCOPE_STORE)
            && ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs'))
        ) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                    'label' => __('Home'),
                    'title' => __('Go to Home Page'),
                    'link' => $this->_storeManager->getStore()->getBaseUrl()
                ]
            );

            $breadcrumbsBlock->addCrumb(
                'career',
                [
                    'label' => __('Careers'),
                    'title' => __('Go to Career Home Page'),
                    'link' => $this->_url->getBaseUrl()
                ]
            );

            $breadcrumbsBlock->addCrumb('career_author',[
                'label' => $author->getTitle(),
                'title' => $author->getTitle()
            ]);
        }
    }
}
