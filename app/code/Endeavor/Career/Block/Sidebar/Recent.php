<?php
  

namespace Endeavor\Career\Block\Sidebar;

/**
 * Career sidebar categories block
 */
class Recent extends \Endeavor\Career\Block\Job\JobList\AbstractList
{
    use Widget;

    /**
     * @var string
     */
    protected $_widgetKey = 'recent_jobs';

    /**
     * @return $this
     */
    public function _construct()
    {
        $this->setPageSize(
            (int) $this->_scopeConfig->getValue(
                'mfcareer/sidebar/'.$this->_widgetKey.'/jobs_per_page',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        );
        return parent::_construct();
    }

    /**
     * Retrieve block identities
     * @return array
     */
    public function getIdentities()
    {
        return [\Magento\Cms\Model\Block::CACHE_TAG . '_career_recent_jobs_widget'  ];
    }

}
