<?php
  

namespace Endeavor\Career\Block\Widget;

/**
 * Career recent jobs widget
 */
class Recent extends \Endeavor\Career\Block\Job\JobList\AbstractList implements \Magento\Widget\Block\BlockInterface
{

    /**
     * @var \Endeavor\Career\Model\CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var \Endeavor\Career\Model\Category
     */
    protected $_category;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Endeavor\Career\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
     * @param \Endeavor\Career\Model\Url $url
     * @param \Endeavor\Career\Model\CategoryFactory $categoryFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Endeavor\Career\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory,
        \Endeavor\Career\Model\Url $url,
        \Endeavor\Career\Model\CategoryFactory $categoryFactory,
        array $data = []
    ) {
        parent::__construct($context, $coreRegistry, $filterProvider, $jobCollectionFactory, $url, $data);
        $this->_categoryFactory = $categoryFactory;
    }

    /**
     * Set career template
     *
     * @return this
     */
    public function _toHtml()
    {
        $this->setTemplate(
            $this->getData('custom_template') ?: 'widget/recent.phtml'
        );

        return parent::_toHtml();
    }

    /**
     * Retrieve block title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getData('title') ?: __('Recent Career Jobs');
    }

    /**
     * Prepare jobs collection
     *
     * @return void
     */
    protected function _prepareJobCollection()
    {
        $size = $this->getData('number_of_jobs');
        if (!$size) {
            $size = (int) $this->_scopeConfig->getValue(
                'mfcareer/sidebar/recent_jobs/jobs_per_page',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        }

        $this->setPageSize($size);

        parent::_prepareJobCollection();

        if ($category = $this->getCategory()) {
            $categories = $category->getChildrenIds();
            $categories[] = $category->getId();
            $this->_jobCollection->addCategoryFilter($categories);
        }
    }

    /**
     * Retrieve category instance
     *
     * @return \Endeavor\Career\Model\Category
     */
    public function getCategory()
    {
        if ($this->_category === null) {
            if ($categoryId = $this->getData('category_id')) {
                $category = $this->_categoryFactory->create();
                $category->load($categoryId);

                $storeId = $this->_storeManager->getStore()->getId();
                if ($category->isVisibleOnStore($storeId)) {
                    $category->setStoreId($storeId);
                    return $this->_category = $category;
                }
            }

            $this->_category = false;
        }

        return $this->_category;
    }

    /**
     * Retrieve job short content
     * @param  \Endeavor\Career\Model\Job $job
     *
     * @return string
     */
    public function getShorContent($job)
    {
        $content = $this->_filterProvider->getPageFilter()->filter(
            $job->getContent()
        );
        $pageBraker = '<!-- pagebreak -->';

        if ($p = mb_strpos($content, $pageBraker)) {
            $content = mb_substr($content, 0, $p);
            try {
                libxml_use_internal_errors(true);
                $dom = new \DOMDocument();
                $dom->loadHTML('<?xml encoding="UTF-8">' . $content);
                $body = $dom->getElementsByTagName('body');
                if ( $body && $body->length > 0 ) {
                    $body = $body->item(0);
                    $_content = new \DOMDocument;
                    foreach ($body->childNodes as $child){
                        $_content->appendChild($_content->importNode($child, true));
                    }
                    $content = $_content->saveHTML();
                }
            } catch (\Exception $e) {}
        }

        return $content;
    }
}
