<?php
  

namespace Endeavor\Career\Block\Adminhtml;

/**
 * Admin career job
 */
class Job extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_job';
        $this->_blockGroup = 'Endeavor_Career';
        $this->_headerText = __('Job');
        $this->_addButtonLabel = __('Add New Job');
        parent::_construct();
    }
}
