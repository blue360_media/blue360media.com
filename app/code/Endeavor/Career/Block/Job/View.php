<?php
  
namespace Endeavor\Career\Block\Job;

use Magento\Store\Model\ScopeInterface;

/**
 * Career job view
 */
class View extends AbstractJob
{
    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $job = $this->getJob();
        if ($job) {
            $this->_addBreadcrumbs($job);
            $this->pageConfig->addBodyClass('career-job-' . $job->getIdentifier());
            $this->pageConfig->getTitle()->set($job->getMetaTitle());
            $this->pageConfig->setKeywords($job->getMetaKeywords());
            $this->pageConfig->setDescription($job->getMetaDescription());
            $this->pageConfig->addRemotePageAsset(
                $job->getJobUrl(),
                'canonical',
                ['attributes' => ['rel' => 'canonical']]
            );
        }

        return parent::_prepareLayout();
    }

    /**
     * Prepare breadcrumbs
     *
     * @param \Endeavor\Career\Model\Job $job
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function _addBreadcrumbs(\Endeavor\Career\Model\Job $job)
    {
        if ($this->_scopeConfig->getValue('web/default/show_cms_breadcrumbs', ScopeInterface::SCOPE_STORE)
            && ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs'))
        ) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                    'label' => __('Home'),
                    'title' => __('Go to Home Page'),
                    'link' => $this->_storeManager->getStore()->getBaseUrl()
                ]
            );
            $breadcrumbsBlock->addCrumb(
                'career',
                [
                    'label' => __('Careers'),
                    'title' => __('Go to Career Home Page'),
                    'link' => $this->_url->getBaseUrl()
                ]
            );
            $breadcrumbsBlock->addCrumb('career_job', [
                'label' => $job->getTitle(),
                'title' => $job->getTitle()
            ]);
        }
    }

}
