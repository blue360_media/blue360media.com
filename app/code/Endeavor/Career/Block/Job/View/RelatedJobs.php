<?php
  

namespace Endeavor\Career\Block\Job\View;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\View\Element\AbstractBlock;

/**
 * Career job related jobs block
 */
class RelatedJobs extends \Endeavor\Career\Block\Job\JobList\AbstractList
{
    /**
     * Prepare jobs collection
     *
     * @return void
     */
    protected function _prepareJobCollection()
    {
        $pageSize = (int) $this->_scopeConfig->getValue(
            'mfcareer/job_view/related_jobs/number_of_jobs',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $this->_jobCollection = $this->getJob()->getRelatedJobs()
            ->addActiveFilter()
            ->setPageSize($pageSize ?: 5);

        $this->_jobCollection->getSelect()->order('rl.position', 'ASC');
    }

    /**
     * Retrieve true if Display Related Jobs enabled
     * @return boolean
     */
    public function displayJobs()
    {
        return (bool) $this->_scopeConfig->getValue(
            'mfcareer/job_view/related_jobs/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Retrieve jobs instance
     *
     * @return \Endeavor\Career\Model\Category
     */
    public function getJob()
    {
        if (!$this->hasData('job')) {
            $this->setData('job',
                $this->_coreRegistry->registry('current_career_job')
            );
        }
        return $this->getData('job');
    }

    /**
     * Get Block Identities
     * @return Array
     */
    public function getIdentities()
    {
        return [\Magento\Cms\Model\Page::CACHE_TAG . '_relatedjobs_'.$this->getJob()->getId()  ];
    }
}
