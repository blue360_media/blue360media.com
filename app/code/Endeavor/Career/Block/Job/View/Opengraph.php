<?php
  
namespace Endeavor\Career\Block\Job\View;

use Magento\Store\Model\ScopeInterface;

/**
 * Career job view opengraph
 */
class Opengraph extends \Endeavor\Career\Block\Job\AbstractJob
{
    /**
     * Retrieve page type
     *
     * @return string
     */
    public function getType()
    {
        return $this->stripTags(
            $this->getJob()->getOgType()
        );
    }

    /**
     * Retrieve page title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->stripTags(
            $this->getJob()->getOgTitle()
        );
    }

    /**
     * Retrieve page short description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->stripTags(
            $this->getJob()->getOgDescription()
        );
    }

    /**
     * Retrieve page url
     *
     * @return string
     */
    public function getPageUrl()
    {
        return $this->stripTags(
            $this->getJob()->getJobUrl()
        );
    }

    /**
     * Retrieve page main image
     *
     * @return string | null
     */
    public function getImage()
    {
        $image = $this->getJob()->getOgImage();
        if (!$image) {
            $image = $this->getJob()->getFeaturedImage();
        }
        if (!$image) {
            $content = $this->getContent();
            $match = null;
            preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $content, $match);
            if (!empty($match['src'])) {
                $image = $match['src'];
            }
        }

        if ($image) {
            return $this->stripTags($image);
        }

    }

}
