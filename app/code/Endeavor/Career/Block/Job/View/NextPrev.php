<?php
  

namespace Endeavor\Career\Block\Job\View;

use Magento\Store\Model\ScopeInterface;

/**
 * Career job next and prev job links
 */
class NextPrev extends \Magento\Framework\View\Element\Template
{
    /**
     * Previous job
     *
     * @var \Endeavor\Career\Model\Job
     */
    protected $_prevJob;

    /**
     * Next job
     *
     * @var \Endeavor\Career\Model\Job
     */
    protected $_nextJob;

    /**
     * @var \Endeavor\Career\Model\ResourceModel\Job\CollectionFactory
     */
    protected $_jobCollectionFactory;

    /**
     * @var Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Endeavor\Career\Model\ResourceModel\Job\CollectionFactory $_tagCollectionFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Endeavor\Career\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_jobCollectionFactory = $jobCollectionFactory;
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * Retrieve true if need to display next-prev links
     *
     * @return boolean
     */
    public function displayLinks()
    {
        return (bool)$this->_scopeConfig->getValue(
            'mfcareer/job_view/nextprev/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Retrieve prev job
     * @return \Endeavor\Career\Model\Job || bool
     */
    public function getPrevJob()
    {
        if ($this->_prevJob === null) {
            $this->_prevJob = false;
            $collection = $this->_getFrontendCollection()->addFieldToFilter(
                'publish_time', [
                    'gteq' => $this->getJob()->getPublishTime()
                ])
                ->setOrder('publish_time', 'ASC')
                ->setPageSize(1);

            $job = $collection->getFirstItem();

            if ($job->getId()) {
                $this->_prevJob = $job;
            }
        }

        return $this->_prevJob;
    }

    /**
     * Retrieve next job
     * @return \Endeavor\Career\Model\Job || bool
     */
    public function getNextJob()
    {
        if ($this->_nextJob === null) {
            $this->_nextJob = false;
            $collection = $this->_getFrontendCollection()->addFieldToFilter(
                'publish_time', [
                    'lteq' => $this->getJob()->getPublishTime()
                ])
                ->setOrder('publish_time', 'DESC')
                ->setPageSize(1);

            $job = $collection->getFirstItem();

            if ($job->getId()) {
                $this->_nextJob = $job;
            }
        }

        return $this->_nextJob;
    }

    /**
     * Retrieve job collection with frontend filters and order
     * @return bool
     */
    protected function _getFrontendCollection()
    {
        $collection = $this->_jobCollectionFactory->create();
        $collection->addActiveFilter()
            ->addFieldToFilter('job_id', ['neq' => $this->getJob()->getId()])
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->setOrder('publish_time', 'DESC')
            ->setPageSize(1);
        return $collection;
    }

    /**
     * Retrieve job instance
     *
     * @return \Endeavor\Career\Model\Job
     */
    public function getJob()
    {
        return $this->_coreRegistry->registry('current_career_job');
    }

}
