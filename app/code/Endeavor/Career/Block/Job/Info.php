<?php
  

namespace Endeavor\Career\Block\Job;

use Magento\Store\Model\ScopeInterface;

/**
 * Career job info block
 */
class Info extends \Magento\Framework\View\Element\Template
{
    /**
     * Block template file
     * @var string
     */
    protected $_template = 'job/info.phtml';

    /**
     * DEPRECATED METHOD!!!!
     * Retrieve formated Added date
     * @var string
     * @return string
     */
    public function getAddedOn($format = 'Y-m-d H:i:s')
    {
        return $this->getJob()->getPublishDate($format);
    }

    /**
     * Retrieve 1 if display author information is enabled
     * @return int
     */
    public function authorEnabled()
    {
        return (int) $this->_scopeConfig->getValue(
            'mfcareer/author/enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Retrieve 1 if author page is enabled
     * @return int
     */
    public function authorPageEnabled()
    {
        return (int) $this->_scopeConfig->getValue(
            'mfcareer/author/page_enabled',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}
