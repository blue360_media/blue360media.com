<?php
  

namespace Endeavor\Career\Block\Job;

use Magento\Store\Model\ScopeInterface;

/**
 * Abstract job мшуц block
 */
abstract class AbstractJob extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    /**
     * @var \Endeavor\Career\Model\Job
     */
    protected $_job;

    /**
     * Page factory
     *
     * @var \Endeavor\Career\Model\JobFactory
     */
    protected $_jobFactory;

    /**
     * @var Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var string
     */
    protected $_defaultJobInfoBlock = 'Endeavor\Career\Block\Job\Info';

    /**
     * @var \Endeavor\Career\Model\Url
     */
    protected $_url;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Cms\Model\Page $job
     * @param \Magento\Framework\Registry $coreRegistry,
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Magento\Cms\Model\PageFactory $jobFactory
     * @param \Endeavor\Career\Model\Url $url
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Endeavor\Career\Model\Job $job,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Endeavor\Career\Model\JobFactory $jobFactory,
        \Endeavor\Career\Model\Url $url,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_job = $job;
        $this->_coreRegistry = $coreRegistry;
        $this->_filterProvider = $filterProvider;
        $this->_jobFactory = $jobFactory;
        $this->_url = $url;
    }

    /**
     * Retrieve job instance
     *
     * @return \Endeavor\Career\Model\Job
     */
    public function getJob()
    {
        if (!$this->hasData('job')) {
            $this->setData('job',
                $this->_coreRegistry->registry('current_career_job')
            );
        }
        return $this->getData('job');
    }

    /**
     * Retrieve job short content
     *
     * @return string
     */
    public function getShorContent()
    {
        $content = $this->getContent();
        $pageBraker = '<!-- pagebreak -->';

        if ($p = mb_strpos($content, $pageBraker)) {
            $content = mb_substr($content, 0, $p);
            try {
                libxml_use_internal_errors(true);
                $dom = new \DOMDocument();
                $dom->loadHTML('<?xml encoding="UTF-8">' . $content);
                $body = $dom->getElementsByTagName('body');
                if ( $body && $body->length > 0 ) {
                    $body = $body->item(0);
                    $_content = new \DOMDocument;
                    foreach ($body->childNodes as $child){
                        $_content->appendChild($_content->importNode($child, true));
                    }
                    $content = $_content->saveHTML();
                }
            } catch (\Exception $e) {}
        }

        return $content;
    }

    /**
     * Retrieve job content
     *
     * @return string
     */
    public function getContent()
    {
        $job = $this->getJob();
        $key = 'filtered_content';
        if (!$job->hasData($key)) {
            $cotent = $this->_filterProvider->getPageFilter()->filter(
                $job->getContent()
            );
            $job->setData($key, $cotent);
        }
        return $job->getData($key);
    }

    /**
     * Retrieve job info html
     *
     * @return string
     */
    public function getInfoHtml()
    {
        return $this->getInfoBlock()->toHtml();
    }

    /**
     * Retrieve job info block
     *
     * @return \Endeavor\Career\Block\Job\Info
     */
    public function getInfoBlock()
    {
        $k = 'info_block';
        if (!$this->hasData($k)) {
            $blockName = $this->getJobInfoBlockName();
            if ($blockName) {
                $block = $this->getLayout()->getBlock($blockName);
            }

            if (empty($block)) {
                $block = $this->getLayout()->createBlock($this->_defaultJobInfoBlock, uniqid(microtime()));
            }

            $this->setData($k, $block);
        }

        return $this->getData($k)->setJob($this->getJob());
    }

}
