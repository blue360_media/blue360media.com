<?php
  

namespace Endeavor\Career\Block\Job\JobList;

use Magento\Store\Model\ScopeInterface;

/**
 * Abstract career job list block
 */
abstract class AbstractList extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Cms\Model\Template\FilterProvider
     */
    protected $_filterProvider;

    /**
     * @var \Magento\Cms\Model\Page
     */
    protected $_job;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Endeavor\Career\Model\ResourceModel\Job\CollectionFactory
     */
    protected $_jobCollectionFactory;

    /**
     * @var \Endeavor\Career\Model\ResourceModel\Job\Collection
     */
    protected $_jobCollection;

    /**
     * @var \Endeavor\Career\Model\Url
     */
    protected $_url;

    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Cms\Model\Template\FilterProvider $filterProvider
     * @param \Endeavor\Career\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory
     * @param \Endeavor\Career\Model\Url $url
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Endeavor\Career\Model\ResourceModel\Job\CollectionFactory $jobCollectionFactory,
        \Endeavor\Career\Model\Url $url,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_coreRegistry = $coreRegistry;
        $this->_filterProvider = $filterProvider;
        $this->_jobCollectionFactory = $jobCollectionFactory;
        $this->_url = $url;
    }

    /**
     * Prepare jobs collection
     *
     * @return void
     */
    protected function _prepareJobCollection()
    {
        $this->_jobCollection = $this->_jobCollectionFactory->create()
            ->addActiveFilter()
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->setOrder('publish_time', 'DESC');

        if ($this->getPageSize()) {
            $this->_jobCollection->setPageSize($this->getPageSize());
        }
    }

    /**
     * Prepare jobs collection
     *
     * @return \Endeavor\Career\Model\ResourceModel\Job\Collection
     */
    public function getJobCollection()
    {
        if (is_null($this->_jobCollection)) {
            $this->_prepareJobCollection();
        }

        return $this->_jobCollection;
    }

}
