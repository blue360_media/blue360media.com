<?php
  

namespace Endeavor\Career\Block\Search;

use Magento\Store\Model\ScopeInterface;

/**
 * Career search result block
 */
class JobList extends \Endeavor\Career\Block\Job\JobList
{
	/**
	 * Retrieve query
	 * @return string
	 */
    public function getQuery()
    {
        return urldecode($this->getRequest()->getParam('q'));
    }

    /**
     * Prepare jobs collection
     *
     * @return void
     */
    protected function _prepareJobCollection()
    {
        parent::_prepareJobCollection();

        $q = $this->getQuery();
        $this->_jobCollection->addFieldToFilter(
            ['title', 'content_heading', 'content'],
            [
                ['like' => '%'.$q.'%'],
                ['like' => '%'.$q.'%'],
                ['like' => '% '.$q.' %']
            ]
        );
    }

    /**
     * Preparing global layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $title = $this->_getTitle();
        $this->_addBreadcrumbs($title);
        $this->pageConfig->getTitle()->set($title);

        return parent::_prepareLayout();
    }

    /**
     * Prepare breadcrumbs
     *
     * @param  string $title
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function _addBreadcrumbs($title)
    {
        if ($this->_scopeConfig->getValue('web/default/show_cms_breadcrumbs', ScopeInterface::SCOPE_STORE)
            && ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs'))
        ) {
            $breadcrumbsBlock->addCrumb(
                'home',
                [
                    'label' => __('Home'),
                    'title' => __('Go to Home Page'),
                    'link' => $this->_storeManager->getStore()->getBaseUrl()
                ]
            );
            $breadcrumbsBlock->addCrumb(
                'career',
                [
                    'label' => __('Careers'),
                    'title' => __('Go to Career Home Page'),
                    'link' => $this->_url->getBaseUrl()
                ]
            );
            $breadcrumbsBlock->addCrumb('career_search', ['label' => $title, 'title' => $title]);
        }
    }

    /**
     * Retrieve title
     * @return string
     */
    protected function _getTitle()
    {
        return __('Search "%1"', $this->getQuery());
    }

}
