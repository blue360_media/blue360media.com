<?php
  

namespace Endeavor\Career\Block\Amp\Ldjson;

/**
 * Career job list ldJson block
 */
if (class_exists('\Plumrocket\Amp\Block\Page\Head\Ldjson\Cms')) {
    class JobListIntermediate extends \Plumrocket\Amp\Block\Page\Head\Ldjson\Cms {}
} else {
    class JobListIntermediate extends \Magento\Framework\View\Element\AbstractBlock {}
}

class JobList extends JobListIntermediate
{
    /**
     * Retrieve page structure structure data in JSON
     *
     * @return string
     */
    public function getJson()
    {
        $time = time();
        if (!$this->_cmsPage->getCreationTime()) {
            $this->_cmsPage->setCreationTime(
                date('Y-m-01 00:00:00', $time - 86400 * 150)
            );
        }

        if (!$this->_cmsPage->getUpdateTime()) {
            $this->_cmsPage->setUpdateTime(
                date('Y-m-01 00:00:00', $time)
            );
        }

        if (!$this->_cmsPage->getTitle()) {
            $this->_cmsPage->setTitle(
                $this->pageConfig->getTitle()->get()
            );
        }

        return parent::getJson();
    }
}
