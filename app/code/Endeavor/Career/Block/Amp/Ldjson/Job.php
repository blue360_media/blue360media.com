<?php
  

namespace Endeavor\Career\Block\Amp\Ldjson;

/**
 * Career job list ldJson block
 */
class Job extends \Endeavor\Career\Block\Job\View\Richsnippets
{
    /**
     * Retrieve page structure structure data in JSON
     *
     * @return string
     */
    public function getJson()
    {
        $json = parent::getOptions();
        return str_replace('\/', '/', json_encode($json));
    }
}
