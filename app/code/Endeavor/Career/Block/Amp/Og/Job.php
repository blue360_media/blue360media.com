<?php
  
namespace Endeavor\Career\Block\Amp\Og;

use Magento\Store\Model\ScopeInterface;

/**
 * Career job opengraph for amp
 */

if (class_exists('\Plumrocket\Amp\Block\Page\Head\Og\AbstractOg')) {
    class JobIntermediate extends \Plumrocket\Amp\Block\Page\Head\Og\AbstractOg {}
} else {
    class JobIntermediate extends \Magento\Framework\View\Element\AbstractBlock {}
}

class Job extends JobIntermediate
{
    /**
     * Retrieve open graph params
     *
     * @return array
     */
    public function getOgParams()
    {
        $params = parent::getOgParams();
        $job = $this->getJob();

        return array_merge($params, [
            'type' => $job->getOgType() ?: 'article',
            'url' => $this->_helper->getCanonicalUrl($job->getJobUrl()),
            'image' => (string)$job->getImageUrl(),
        ]);
    }

    /**
     * Retrieve current job
     *
     * @return \Endeavor\Career\Model\Job
     */
    public function getJob()
    {
        return $this->_coreRegistry->registry('current_career_job');
    }

    /**
     * Retrieve page main image
     *
     * @return string | null
     */
    public function getImage()
    {
        $image = $this->getJob()->getOgImage();
        if (!$image) {
            $image = $this->getJob()->getFeaturedImage();
        }
        if (!$image) {
            $content = $this->getContent();
            $match = null;
            preg_match('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', $content, $match);
            if (!empty($match['src'])) {
                $image = $match['src'];
            }
        }

        if ($image) {
            return $this->stripTags($image);
        }

    }

}
