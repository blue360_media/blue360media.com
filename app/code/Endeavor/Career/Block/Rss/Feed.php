<?php
  

namespace Endeavor\Career\Block\Rss;

use Magento\Store\Model\ScopeInterface;

/**
 * Career ree feed block
 */
class Feed extends \Endeavor\Career\Block\Job\JobList\AbstractList
{
    /**
     * Retrieve rss feed url
     * @return string
     */
    public function getLink()
    {
        return $this->_url->getUrl('feed', 'rss');
    }

    /**
     * Retrieve rss feed title
     * @return string
     */
    public function getTitle()
    {
    	 return $this->_scopeConfig->getValue('mfcareer/rss_feed/title', ScopeInterface::SCOPE_STORE);
    }

    /**
     * Retrieve rss feed description
     * @return string
     */
    public function getDescription()
    {
    	 return $this->_scopeConfig->getValue('mfcareer/rss_feed/description', ScopeInterface::SCOPE_STORE);
    }

    /**
     * Retrieve block identities
     * @return array
     */
    public function getIdentities()
    {
        return [\Magento\Cms\Model\Page::CACHE_TAG . '_career_rss_feed'  ];
    }

}
