# [Endeavor](http://endeavor.com/) Career Extension for Magento 2

Career extension allows you to create a full-fledged career on your [Magento 2](http://magento.com/) Store.

## Features
  * Unlimited career jobs and categories
  * Job tags are available
  * Multi Stores Support
  * SEO friendly permalinks
  * Facebook,Disqus and Google+ comments on job page
  * Related Products and Related Job
  * Available jobs search
  * Lazy load on jobs list
  * Author information and jobs by author
  * Recent Jobs, Archive, Categories, Search From, Tag Cloud sidebar widgets
  * Import Jobs and Categories form WordPress and AW Career extension for M1
  * Recent Jobs Widget
  * Sitemap XML
  * Career Rss Feed
  * Open Graph (OG) meta tags
  * REST API
  * Compatible with [Porto Theme](https://themeforest.net/item/porto-ultimate-responsive-magento-theme/9725864?ref=endeavor) for Magento 2
  * Accelerated Mobile Pages (AMP) Project support. To enable AMP view on career pages [Magento Amp Extension](http://endeavor.com/accelerated-mobile-pages/) by Plumrocket is required.

## Simple Demo
http://endeavor.com/magento2-career-extension/#demo

## Online Documentation
http://endeavor.com/docs/magento-2-career/

## Requirements
  * Magento Community Edition 2.x or Magento Enterprise Edition 2.x

## Installation Method 1 - Installing via composer
  * Open command line
  * Using command "cd" navigate to your magento2 root directory
  * Run command: composer require endeavor/module-career

  

## Installation Method 2 - Installing using archive
  * Download [ZIP Archive](https://github.com/endeavor/module-career/archive/master.zip)
  * Extract files
  * In your Magento 2 root directory create folder app/code/Endeavor/Career
  * Copy files and folders from archive to that folder
  * In command line, using "cd", navigate to your Magento 2 root directory
  * Run commands:
```
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
```

## Support
If you have any issues, please [contact us](mailto:support@endeavor.com)
then if you still need help, open a bug report in GitHub's
[issue tracker](https://github.com/endeavor/module-career/issues).

Please do not use Magento Connect's Reviews or (especially) the Q&A for support.
There isn't a way for us to reply to reviews and the Q&A moderation is very slow.

## Donate to us
All Endeavor extension are absolutely free and licensed under the Open Software License version 3.0. We want to create more awesome features for you and bring up new releases as fast as we can. We hope for your support.
http://endeavor.com/donate/

## License
The code is licensed under [Open Software License ("OSL") v. 3.0](http://opensource.org/licenses/osl-3.0.php).
