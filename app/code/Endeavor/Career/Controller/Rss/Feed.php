<?php
  
namespace Endeavor\Career\Controller\Rss;

/**
 * Career rss feed view
 */
class Feed extends \Magento\Framework\App\Action\Action
{
    /**
     * View career rss feed action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->getResponse()
            ->setHeader('Content-type', 'text/xml; charset=UTF-8')
            ->setBody(
                $this->_view->getLayout()->getBlock('career.rss.feed')->toHtml()
            );
    }

}
