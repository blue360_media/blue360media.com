<?php
  

namespace Endeavor\Career\Controller\Author;

use \Magento\Store\Model\ScopeInterface;

/**
 * Career author jobs view
 */
class View extends \Magento\Framework\App\Action\Action
{
    /**
     * View career author action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $config = $this->_objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');

        $enabled = (int) $config->getValue('mfcareer/author/enabled',
            ScopeInterface::SCOPE_STORE);
        $pageEnabled = (int) $config->getValue('mfcareer/author/page_enabled',
            ScopeInterface::SCOPE_STORE);

        if (!$enabled || !$pageEnabled) {
            $this->_forward('index', 'noroute', 'cms');
            return;
        }

        $author = $this->_initAuthor();
        if (!$author) {
            $this->_forward('index', 'noroute', 'cms');
            return;
        }

        $this->_objectManager->get('\Magento\Framework\Registry')->register('current_career_author', $author);

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

    /**
     * Init author
     *
     * @return \Endeavor\Career\Model\Author || false
     */
    protected function _initAuthor()
    {
        $id = $this->getRequest()->getParam('id');

        $author = $this->_objectManager->create('Endeavor\Career\Model\Author')->load($id);

        if (!$author->getId()) {
            return false;
        }

        return $author;
    }

}
