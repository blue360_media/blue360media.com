<?php
  
namespace Endeavor\Career\Controller\Index;

/**
 * Career home page view
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * View career homepage action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

}
