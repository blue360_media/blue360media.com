<?php
  

namespace Endeavor\Career\Controller\Job;

/**
 * Career job view
 */
class View extends \Magento\Framework\App\Action\Action
{

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
    }

    /**
     * View Career job action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $job = $this->_initJob();
        if (!$job) {
            $this->_forward('index', 'noroute', 'cms');
            return;
        }

        $this->_objectManager->get('\Magento\Framework\Registry')
            ->register('current_career_job', $job);
        $resultPage = $this->_objectManager->get('Endeavor\Career\Helper\Page')
            ->prepareResultPage($this, $job);
        return $resultPage;
    }

    /**
     * Init Job
     *
     * @return \Endeavor\Career\Model\Job || false
     */
    protected function _initJob()
    {
        $id = $this->getRequest()->getParam('id');
        $storeId = $this->_storeManager->getStore()->getId();

        $job = $this->_objectManager->create('Endeavor\Career\Model\Job')->load($id);

        if (!$job->isVisibleOnStore($storeId)) {
            return false;
        }

        $job->setStoreId($storeId);

        return $job;
    }

}
