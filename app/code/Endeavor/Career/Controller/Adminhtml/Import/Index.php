<?php
  

namespace Endeavor\Career\Controller\Adminhtml\Import;

/**
 * Career available imports list controller
 */
class Index extends \Magento\Backend\App\Action
{
	/**
     * Start available import execute
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_setActiveMenu('Endeavor_Career::import');
        $title = __('Career Import');
        $this->_view->getPage()->getConfig()->getTitle()->prepend($title);
        $this->_addBreadcrumb($title, $title);
        $this->_view->renderLayout();
    }

    /**
     * Check is allowed access
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Endeavor_Career::import');
    }
}
