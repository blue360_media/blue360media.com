<?php
  

namespace Endeavor\Career\Controller\Adminhtml;

/**
 * Admin career job edit controller
 */
class Job extends Actions
{
	/**
	 * Form session key
	 * @var string
	 */
    protected $_formSessionKey  = 'career_job_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey      = 'Endeavor_Career::job';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass      = 'Endeavor\Career\Model\Job';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu      = 'Endeavor_Career::job';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField     = 'is_active';

}
