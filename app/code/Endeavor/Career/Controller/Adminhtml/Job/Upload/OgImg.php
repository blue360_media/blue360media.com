<?php
  

namespace Endeavor\Career\Controller\Adminhtml\Job\Upload;

use Endeavor\Career\Controller\Adminhtml\Upload\Image\Action;

/**
 * Career featured image upload controller
 */
class OgImg extends Action
{
    /**
     * File key
     *
     * @var string
     */
    protected $_fileKey = 'og_img';

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Endeavor_Career::job');
    }

}
