<?php
  

namespace Endeavor\Career\Controller\Adminhtml\Job\Upload;

use Endeavor\Career\Controller\Adminhtml\Upload\Image\Action;

/**
 * Career featured image upload controller
 */
class FeaturedImg extends Action
{
    /**
     * File key
     *
     * @var string
     */
    protected $_fileKey = 'featured_img';

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Endeavor_Career::job');
    }

}
