<?php
  

namespace Endeavor\Career\Controller\Adminhtml;

/**
 * Admin career category edit controller
 */
class Category extends Actions
{
	/**
	 * Form session key
	 * @var string
	 */
    protected $_formSessionKey  = 'career_category_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey      = 'Endeavor_Career::category';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass      = 'Endeavor\Career\Model\Category';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu      = 'Endeavor_Career::category';

    /**
     * Status field name
     * @var string
     */
    protected $_statusField     = 'is_active';
}