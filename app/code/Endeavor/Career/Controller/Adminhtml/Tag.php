<?php
  

namespace Endeavor\Career\Controller\Adminhtml;

/**
 * Admin career tag edit controller
 */
class Tag extends Actions
{
    /**
     * Form session key
     * @var string
     */
    protected $_formSessionKey  = 'career_tag_form_data';

    /**
     * Allowed Key
     * @var string
     */
    protected $_allowedKey      = 'Endeavor_Career::job';

    /**
     * Model class name
     * @var string
     */
    protected $_modelClass      = 'Endeavor\Career\Model\Tag';

    /**
     * Active menu key
     * @var string
     */
    protected $_activeMenu      = 'Endeavor_Career::job';
}
