<?php
  
namespace Endeavor\Career\Controller\Search;

/**
 * Career search results view
 */
class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * View career search results action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }

}
