<?php
namespace Endeavor\SalesItemSequence\Model\ResourceModel\SalesSequence;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Endeavor\SalesItemSequence\Model\SalesSequence', 'Endeavor\SalesItemSequence\Model\ResourceModel\SalesSequence');
      
    }
}
