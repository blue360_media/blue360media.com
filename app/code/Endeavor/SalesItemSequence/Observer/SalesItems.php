<?php
 
namespace Endeavor\SalesItemSequence\Observer;
  
class SalesItems implements \Magento\Framework\Event\ObserverInterface {
 
    /** @var \Magento\Framework\Logger\Monolog */
    protected $_logger;
    
    /**
     * @var \Magento\Framework\ObjectManager\ObjectManager
     */
    protected $_objectManager;
    
    protected $_orderFactory;
    protected $_checkoutSession;
    
    public function __construct(        
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Framework\ObjectManager\ObjectManager $objectManager
    ) {
        $this->_logger = $loggerInterface;
        $this->_objectManager = $objectManager;        
        $this->_orderFactory = $orderFactory;
        $this->_checkoutSession = $checkoutSession;        
    }
 
    /**
     * This is the method that fires when the event runs.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer ) {        
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $objectManager->create('\Magento\Checkout\Model\Session');
        $lastOrderId = $checkoutSession->getLastOrderId();     
        $saleOrder = $objectManager->create('\Magento\Sales\Model\Order')->load($lastOrderId);
        $orderItems = $saleOrder->getAllItems();
        $increment_id = 0;
        
        foreach ($orderItems as $orderItem) {
            $increment_id = $increment_id + 1;
            $orderItem->setCustomIncrementId(str_pad($increment_id,6,'0',STR_PAD_LEFT));
            $orderItem->save();
        }
        
       return $this;
    }
}