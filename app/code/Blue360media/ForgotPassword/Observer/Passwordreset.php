<?php
namespace Blue360media\ForgotPassword\Observer;

use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerRegistry;

class Passwordreset implements \Magento\Framework\Event\ObserverInterface
{

  protected $customerRepository;

  protected $customerSession;

  public function __construct(
      \Magento\Customer\Api\CustomerRepositoryInterfaceFactory $customerRepositoryFactory,
      \Magento\Customer\Model\Session $customerSession) {
      $this->customerRepository = $customerRepositoryFactory->create();
      $this->customerSession = $customerSession;
  }

  public function execute(\Magento\Framework\Event\Observer $observer)
  {

    $id = $observer->getRequest()->getquery('id');

    if($id){

      $customerDetail = $this->customerRepository->getById($id);
      $username = $customerDetail->getEmail();

    }else{

      $username = $this->customerSession->getCustomer()->getEmail(); 
      /*$change_email = $observer->getRequest()->getPost('change_email');
      $change_password = $observer->getRequest()->getPost('change_password');
      $emailid = $observer->getRequest()->getPost('email');*/

    }

    $newpassword = $observer->getRequest()->getPost('password');
    $newpassword_confirm = $observer->getRequest()->getPost('password_confirmation');

    if(!empty($newpassword) && !empty($newpassword_confirm))
    {

      $post = [
          'username' => "$username",
          'newpassword' => "$newpassword",
          'newpassword_confirm' => "$newpassword_confirm",
      ];
    }

    /*if(!empty($change_email))
    {
      $email_post = [
          'username' => "$username",
          'emailid' => "$emailid",
      ];

      if(!empty($change_password)){
        $post = array_merge($email_post, $post);
      }else{
        $post = $email_post;
      }
    }*/

    if(!empty($post))
    {
      $ch = curl_init('https://m.blue360media.com/restapi/resetpassword');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
      $response = curl_exec($ch);
      curl_close($ch);
    }

    return $this;
  }
}