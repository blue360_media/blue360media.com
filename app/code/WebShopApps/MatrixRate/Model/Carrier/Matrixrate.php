<?php
/**
 * WebShopApps
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * WebShopApps MatrixRate
 *
 * @category WebShopApps
 * @package WebShopApps_MatrixRate
 * @copyright Copyright (c) 2014 Zowta LLC (http://www.WebShopApps.com)
 * @license http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @author WebShopApps Team sales@webshopapps.com
 *
 */
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

/*
 * @updated_By: Mohammad Hamadenh
 * @email:  mhamadneh@endeavorpal.com
 * @date: 20/12/2017
 * @desc: in order to add promotion messages only at the top of page when rule is applied to shipping.
 */
namespace WebShopApps\MatrixRate\Model\Carrier;

use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Address\RateRequest;

class Matrixrate extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'matrixrate';

    /**
     * @var bool
     */
    protected $_isFixed = false;

    /**
     * @var string
     */
    protected $defaultConditionName = 'package_weight';

    /**
     * @var array
     */
    protected $conditionNames = [];

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $resultMethodFactory;

    /**
     * @var \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\MatrixrateFactory
     */
    protected $matrixrateFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $resultMethodFactory
     * @param \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\MatrixrateFactory $matrixrateFactory
     * @param array $data
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $resultMethodFactory,
        \WebShopApps\MatrixRate\Model\ResourceModel\Carrier\MatrixrateFactory $matrixrateFactory,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->resultMethodFactory = $resultMethodFactory;
        $this->matrixrateFactory = $matrixrateFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        foreach ($this->getCode('condition_name') as $k => $v) {
            $this->conditionNames[] = $k;
        }
    }

    /**
     * @update_by: Murad Dweikat
     * @date: 01/03/2017
     * @desc: calculate shipping rate by package price exclude virtual product, 
     *        and make sure that the shipping rate not less that MIN RATE price.
     * 
     * @param RateRequest $request
     * @return \Magento\Shipping\Model\Rate\Result
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }
		
		
		if($request->getPackageValue() == 0){
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$cart = $objectManager->get('\Magento\Checkout\Model\Cart'); 
			 
			$totalItems = $cart->getQuote()->getItemsCount();
			$total = $cart->getQuote()->getBaseSubtotal();
						
			$request->setPackageValue($total); 	
			
		}
     

        // exclude Virtual products price from Package value if pre-configured
        if (!$this->getConfigFlag('include_virtual_price') && $request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }
                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getProduct()->isVirtual()) {
                            $request->setPackageValue($request->getPackageValue() - $child->getBaseRowTotal());
                        }
                    }
                } elseif ($item->getProduct()->isVirtual()) {
                    $request->setPackageValue($request->getPackageValue() - $item->getBaseRowTotal());
                }
            }
        }

        // Free shipping by qty
        $freeQty = 0;
        $isFreeShipping = false;
        $multiShipTotalValue = 0;
        if ($request->getAllItems()) {
            $freePackageValue = 0;
            foreach ($request->getAllItems() as $item) {
                
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                    
                }
                
                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeShipping = is_numeric($child->getFreeShipping()) ? $child->getFreeShipping() : 0;
                            $freeQty += $item->getQty() * ($child->getQty() - $freeShipping);
                            $freePackageValue += $item->getBaseRowTotal();
                            $isFreeShipping = true;
                        }
                        
                    }
                    
                } else if ($item->getFreeShipping()) {
                    $freeShipping = is_numeric($item->getFreeShipping()) ? $item->getFreeShipping() : 0;
                    $freeQty += $item->getQty() - $freeShipping;
                    $freePackageValue += $item->getBaseRowTotal();
                    $isFreeShipping = true;
                    
                }
                
                if (!empty($item->getData()['quote_address_id'])) {
                    if (!$item->getFreeShipping() && !$item->getProduct()->isVirtual()) {
                        $multiShipTotalValue += $item->getData()['base_row_total'];

                    }
                    
                }
                
            }
            
            $oldValue = $request->getPackageValue();
            $request->setPackageValue($oldValue - $freePackageValue);
            if ($multiShipTotalValue > 0) {
                $request->setPackageValue($multiShipTotalValue);
                
            }
            
        }
        
        if (!$request->getConditionMRName()) {
            $conditionName = $this->getConfigData('condition_name');
            $request->setConditionMRName($conditionName ? $conditionName : $this->defaultConditionName);
        }

        // Package weight and qty free shipping
        $oldWeight = $request->getPackageWeight();
        $oldQty = $request->getPackageQty();
        $oldValue = $request->getPackageValue();

        $request->setPackageWeight($request->getFreeMethodWeight());
        $request->setPackageQty($oldQty - $freeQty);

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->rateResultFactory->create();
        $zipRange = $this->getConfigData('zip_range');
        if ($request->getConditionMRName() == 'package_value') {
            $request->setData($request->getConditionMRName(), 1);
        }
        $rateArray = $this->getRate($request, $zipRange);
        $minRateArrayMap = [];
        if ($request->getConditionMRName() == 'package_value') {
            $request->setData($request->getConditionMRName(), 1001);
            $minRateArray = $this->getRate($request, $zipRange);
            foreach ($minRateArray as $mrate) {
                $minRateArrayMap[$mrate['shipping_method']] = $mrate;

            }
        }

        $request->setPackageWeight($oldWeight);
        if ($this->getConfigData('condition_name') == 'package_qty') {
            $request->setPackageQty(1);
        } elseif ($this->getConfigData('condition_name') == 'package_value') {
            $request->setPackageValue(1);
        } else {
            $request->setPackageQty($oldQty);
            $request->setPackageValue($oldValue);
        }
        
        // @author: Murad Dweikat
        // @desc: To allow just Standard Shipping method for Pre-Order.
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $preOrderHelper = $objectManager->create('\Webkul\Preorder\Helper\Data');
        $product = $objectManager->create('\Magento\Catalog\Model\Product');
       // $allowedShippingMethods[] = 'Standard';
		$allowedShippingMethods[] = 'UPS';
        $isPreOrder = false;
        $foundRates = false;
        
        foreach ($request->getAllItems() as $item) {
            $product->load($item->getProductId());
            if ($product->getWkPreorder()){
                $isPreOrder = true;
                break;
            }
        }
        
        // if not pre-order allow other shipping method.
        if (!$isPreOrder && !$request->getFreeShipping() && !$isFreeShipping) {
            $allowedShippingMethods[] = 'Next Day';
            $allowedShippingMethods[] = 'Next 2-Day';
        }

        foreach ($rateArray as $rate) {
            if (in_array($rate['shipping_method'],$allowedShippingMethods)){
                if (!empty($rate) && $rate['price'] >= 0) {
                    /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                    $method = $this->resultMethodFactory->create();

                    $method->setCarrier('matrixrate');
                    
                    $method->setCarrierTitle($this->getConfigData('title'));
                     // start change by vishal shipping description

                    if($rate['shipping_method'] == "UPS"){
                        $method->setCarrierTitle("Standard Shipping");
                    }

                    if($rate['shipping_method'] == "Next Day" || $rate['shipping_method'] == "Next 2-Day"){
                        $method->setCarrierTitle('Order Placed Before 3 pm EST');
                    }

                    // end change by vishal shipping description

                    $method->setMethod('matrixrate_' . $rate['pk']);
                    if ($request->getFreeShipping() == true){
                        $method->setMethodTitle(__('Free Shipping'));
                    }else {
                        $method->setMethodTitle(__($rate['shipping_method']));
                    }

                    if ($request->getFreeShipping() === true || $request->getPackageQty() == $freeQty) {
                        $shippingPrice = 0;
                    } else {
                        $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);
                    }

                    if ($this->getConfigData('condition_name') == 'package_qty') {
                        $method->setPrice($shippingPrice * $oldQty );

                    } elseif ($this->getConfigData('condition_name') == 'package_value') {
                        $price = ($shippingPrice * $oldValue);
                        $mrate = $minRateArrayMap[$rate['shipping_method']];
                        if (!empty($mrate) && $mrate['price'] > $price) {
                            $price = $mrate['price'];
                        }
                        if ($request->getFreeShipping() == true) {
                            $price = 0;
                        }
                        $method->setPrice($price);
                    } else {
                        $method->setPrice($shippingPrice);

                    }
                    $method->setCost($rate['cost']);

                    $result->append($method);
                    $foundRates = true; // have found some valid rates
                }
            }
        }

        if (!$foundRates) {
            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Error $error */
            $error = $this->_rateErrorFactory->create(
                [
                    'data' => [
                        'carrier' => $this->_code,
                        'carrier_title' => $this->getConfigData('title'),
                        'error_message' => $this->getConfigData('specificerrmsg'),
                    ],
                ]
            );
            $result->append($error);
        }

        return $result;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @param bool $zipRange
     * @return array|bool
     */
    public function getRate(\Magento\Quote\Model\Quote\Address\RateRequest $request, $zipRange)
    {
        return $this->matrixrateFactory->create()->getRate($request, $zipRange);
    }

    /**
     * @param string $type
     * @param string $code
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCode($type, $code = '')
    {
        $codes = [
            'condition_name' => [
                'package_weight' => __('Weight vs. Destination'),
                'package_value' => __('Order Subtotal vs. Destination'),
                'package_qty' => __('# of Items vs. Destination'),
            ],
            'condition_name_short' => [
                'package_weight' => __('Weight'),
                'package_value' => __('Order Subtotal'),
                'package_qty' => __('# of Items'),
            ],
        ];

        if (!isset($codes[$type])) {
            throw new LocalizedException(__('Please correct Matrix Rate code type: %1.', $type));
        }

        if ('' === $code) {
            return $codes[$type];
        }

        if (!isset($codes[$type][$code])) {
            throw new LocalizedException(__('Please correct Matrix Rate code for type %1: %2.', $type, $code));
        }

        return $codes[$type][$code];
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['matrixrate' => $this->getConfigData('name')];
    }
}
