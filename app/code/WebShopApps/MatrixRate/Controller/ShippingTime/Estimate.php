<?php
namespace WebShopApps\MatrixRate\Controller\ShippingTime;

/**
 * @abstract get DateTmie from server.
 *
 * @author Murad Dweikat
 */
class Estimate extends \Magento\Framework\App\Action\Action {
    
    /**
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime 
     */
    protected $date;

    /**
     * 
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->date = $date;

        parent::__construct($context);
    }
    
    
    /**
     * Ajax action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quoteId = $this->getRequest()->getParam('quote');
        $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);
        $preOrderHelper = $objectManager->create('\Webkul\Preorder\Helper\Data');
        $currentDateTmie = $this->date->date('Y-m-d H:i:s','+5GMT');
        $preorderPreorderAvailability = $this->date->date('Y-m');
        $isPreorderOrdredItem = false;
        try {
            foreach ($quote->getAllItems() as $quoteItem) {
                $quoteProduct = $this->_objectManager->create("Magento\Catalog\Model\Product")
                    ->load($quoteItem->getProductId());
                if ($quoteProduct->getWkPreorder()) {
                    $isPreorderOrdredItem = true;
                    $quoteProduct = $objectManager->create("Magento\Catalog\Model\Product")
                            ->load($quoteItem->getProductId());
                    $preorderPreorderAvailability = substr($quoteProduct->getPreorderAvailability(),0,7);
                    break;
                }
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }
        
        $result = $this->resultJsonFactory->create();
        
        $output = ['isPreOrder' => $isPreorderOrdredItem, 'estimateDeliveryDate' => $currentDateTmie,
                'availability' => $preorderPreorderAvailability];
        
        return $result->setData($output);

    }
}
