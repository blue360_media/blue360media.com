<?php
/**
 * @category    CleverSoft
 * @package     CleverLayeredNavigation
 * @copyright   Copyright © 2017 CleverSoft., JSC. All Rights Reserved.
 * @author 		ZooExtension.com
 * @email       magento.cleversoft@gmail.com
 */

namespace CleverSoft\CleverMageConverter\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    protected $_version;
    /**
     * @var \Magento\Framework\DB\Select\QueryModifierFactory

     */

    private $table = [
        ['table'=>'widget_instance','key'=>'instance_id','column'=>'widget_parameters','where'=>'instance_type LIKE "CleverSoft%"'],
        ['key'=>'user_id','column'=>'extra','table'=>'admin_user','where'=>''],
        ['key'=>'option_id','column'=>'value','table'=>'quote_item_option','where'=>''],
        ['key'=>'item_id','column'=>'product_options','table'=>'sales_order_item','where'=>''],
    ];

//    private $fieldDataConverter;


    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $productMetadataInterface
    ) {
        $this->_version = $productMetadataInterface;
    }
    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface   $context
     *
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();
        $this->convertSerializedDataToJson($setup);
        $setup->endSetup();
    }
    /**
     * Upgrade to version 2.0.1, convert data for the sales_order_item.product_options and quote_item_option.value
     * from serialized to JSON format
     *
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @return void
     */
    private function convertSerializedDataToJson(\Magento\Framework\Setup\ModuleDataSetupInterface $setup)
    {
        foreach ($this->table as $table) {
            $select = $setup->getConnection()
                ->select()
                ->from(
                    $setup->getTable($table['table'])
                );
            if(!empty($table['where'])) {
                $select->where($table['where']);
            }
            foreach ($setup->getConnection()->fetchAll($select) as $row) {
                if($this->isSerialized($row[$table['column']])) {
                    $fixed_data = preg_replace_callback ( '!s:(\d+):"(.*?)";!', function($match) {
                        return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                    },$row[$table['column']] );
                } else {
                    break;
                }
                $fieldDataConverter  = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\DB\DataConverter\SerializedToJson');
                $setup->getConnection()->update(
                    $setup->getTable($table['table']),
                    [$table['column'] => $fieldDataConverter->convert($fixed_data)],
                    [$table['key'] .'= ?' => $row[$table['key']]]
                );
            }
        }
    }

    /**
     * Check if value is serialized string
     *
     * @param string $value
     * @return boolean
     */
    private function isSerialized($data,$strict = true)
    {
        // if it isn't a string, it isn't serialized.
        if ( ! is_string( $data ) ) {
            return false;
        }
        $data = trim( $data );
        if ( 'N;' == $data ) {
            return true;
        }
        if ( strlen( $data ) < 4 ) {
            return false;
        }
        if ( ':' !== $data[1] ) {
            return false;
        }
        if ( $strict ) {
            $lastc = substr( $data, -1 );
            if ( ';' !== $lastc && '}' !== $lastc ) {
                return false;
            }
        } else {
            $semicolon = strpos( $data, ';' );
            $brace     = strpos( $data, '}' );
            // Either ; or } must exist.
            if ( false === $semicolon && false === $brace )
                return false;
            // But neither must be in the first X characters.
            if ( false !== $semicolon && $semicolon < 3 )
                return false;
            if ( false !== $brace && $brace < 4 )
                return false;
        }
        $token = $data[0];
        switch ( $token ) {
            case 's' :
                if ( $strict ) {
                    if ( '"' !== substr( $data, -2, 1 ) ) {
                        return false;
                    }
                } elseif ( false === strpos( $data, '"' ) ) {
                    return false;
                }
            // or else fall through
            case 'a' :
            case 'O' :
                return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool) preg_match( "/^{$token}:[0-9.E-]+;$end/", $data );
        }
        return false;
    }
}
