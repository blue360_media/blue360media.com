<?php
/**
 * @copyright   Copyright © 2018 Endeavor Technology, Inc. All Rights Reserved.
 * @author 	Endeavor Developer Team
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Blue360Media/core',
    __DIR__
);