/**
 * @copyright   Copyright © 2018 Endeavor Technology, Inc. All Rights Reserved.
 * @author 	Endeavor Developer Team
 */

var config = {
    map: {
        '*': {
        	'bootstrapjs': 'js/bootstrap.min'
 
        }
    }
};
