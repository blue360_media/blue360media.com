FROM bitnami/minideb-extras:stretch-r431
LABEL maintainer="Bitnami <containers@bitnami.com>"

# Install required system packages and dependencies
RUN install_packages cron libbz2-1.0 libc6 libcomerr2 libcurl3 libexpat1 libffi6 libfreetype6 libgcc1 libgcrypt20 libgmp10 libgnutls30 libgpg-error0 libgssapi-krb5-2 libhogweed4 libicu57 libidn11 libidn2-0 libjpeg62-turbo libk5crypto3 libkeyutils1 libkrb5-3 libkrb5support0 libldap-2.4-2 liblzma5 libmemcached11 libmemcachedutil2 libncurses5 libnettle6 libnghttp2-14 libp11-kit0 libpcre3 libpng16-16 libpq5 libpsl5 libreadline7 librtmp1 libsasl2-2 libsqlite3-0 libssh2-1 libssl1.0.2 libssl1.1 libstdc++6 libsybdb5 libtasn1-6 libtidy5 libtinfo5 libunistring0 libxml2 libxslt1.1 zlib1g
RUN bitnami-pkg unpack apache-2.4.39-2 --checksum ff55ee9cccf484bac61fa91558fc7f8445e91ea00bb104aca216f08aea003c6b
RUN bitnami-pkg unpack php-7.2.20-0 --checksum 62514dfa03b8fc1c8b842a2fc28ddb7258c6a750082d99e1877c5b008e8216d7
RUN bitnami-pkg unpack mysql-client-10.2.25-0 --checksum 5842e2830b0d36e519cbcc43d4025217fbbb76f5c7af7a61cce1b256dda546fc
RUN bitnami-pkg install libphp-7.2.20-0 --checksum 88ec7e2013534aa3e1022884c087feb42d74c575b2cf9236634f7a8a48e71896
RUN bitnami-pkg unpack magento-2.3.2-2 --checksum 8f088f0bfc5e0ce0144598dd3cc175806ecca82b20b62a07eb9308a2352c9945
RUN ln -sf /dev/stdout /opt/bitnami/apache/logs/access_log
RUN ln -sf /dev/stderr /opt/bitnami/apache/logs/error_log
RUN sed -i -e '/pam_loginuid.so/ s/^#*/#/' /etc/pam.d/cron
RUN find /opt/bitnami/magento/htdocs -type d -print0 | xargs -0 chmod 775 && find /opt/bitnami/magento/htdocs -type f -print0 | xargs -0 chmod 664 && chown -R bitnami:daemon /opt/bitnami/magento/htdocs

COPY rootfs /
ENV ALLOW_EMPTY_PASSWORD="no" \
    APACHE_HTTPS_PORT_NUMBER="443" \
    APACHE_HTTP_PORT_NUMBER="80:80" \
    APACHE_SET_HTTPS_PORT="no" \
    APACHE_SET_HTTP_PORT="no" \
    PHP_DATE_TIMEZONE="EST" \
    PHP_DISPLAY_ERRORS="1" \
    PHP_MEMORY_LIMIT="2048M" \
    PHP_MAX_EXECUTION_TIME="300" \
    PHP_POST_MAX_SIZE="500M" \
    PHP_UPLOAD_MAX_FILESIZE="1024M" \
    BITNAMI_APP_NAME="magento" \
    BITNAMI_IMAGE_VERSION="2.3.2-debian-9-r30" \
    ELASTICSEARCH_HOST="elasticsearch" \
    ELASTICSEARCH_PORT_NUMBER="9200" \
    EXTERNAL_HTTPS_PORT_NUMBER="443" \
    EXTERNAL_HTTP_PORT_NUMBER="80:80" \
    MAGENTO_ADMINURI="admin_b360" \
    MAGENTO_DATABASE_NAME="bitnami_magento2" \
    MAGENTO_DATABASE_PASSWORD="aa9f3ba28e" \
    MAGENTO_DATABASE_USER="bn_magento" \
    MAGENTO_EMAIL="admin@blue360media.com" \
    MAGENTO_FIRSTNAME="FirstName" \
    MAGENTO_HOST="127.0.0.1" \
    MAGENTO_LASTNAME="LastName" \
    MAGENTO_MODE="default" \
    MAGENTO_PASSWORD="password1" \
    MAGENTO_USERNAME="user" \
    MARIADB_HOST="mariadb" \
    MARIADB_PORT_NUMBER="3306" \
    MARIADB_ROOT_PASSWORD="password1" \
    MARIADB_ROOT_USER="root" \
    MYSQL_CLIENT_CREATE_DATABASE_NAME="bitnami_magento2" \
    MYSQL_CLIENT_CREATE_DATABASE_PASSWORD="aa9f3ba28e" \
    MYSQL_CLIENT_CREATE_DATABASE_PRIVILEGES="ALL" \
    MYSQL_CLIENT_CREATE_DATABASE_USER="bn_magento" \
    PATH="/opt/bitnami/apache/bin:/opt/bitnami/php/bin:/opt/bitnami/php/sbin:/opt/bitnami/mysql/bin:/opt/bitnami/magento/bin:$PATH"

VOLUME [ "/certs" ]

EXPOSE 80:80 443

ENTRYPOINT [ "/app-entrypoint.sh" ]
CMD [ "/run.sh" ]
