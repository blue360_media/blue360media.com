<?php
namespace Smile\ElasticsuiteSwatches\Helper\Swatches;

/**
 * Interceptor class for @see \Smile\ElasticsuiteSwatches\Helper\Swatches
 */
class Interceptor extends \Smile\ElasticsuiteSwatches\Helper\Swatches implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory, \Magento\Catalog\Api\ProductRepositoryInterface $productRepository, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Swatches\Model\ResourceModel\Swatch\CollectionFactory $swatchCollectionFactory, \Magento\Catalog\Helper\Image $imageHelper, \Magento\Framework\Serialize\Serializer\Json $serializer = null, \Magento\Swatches\Model\SwatchAttributesProvider $swatchAttributesProvider = null)
    {
        $this->___init();
        parent::__construct($productCollectionFactory, $productRepository, $storeManager, $swatchCollectionFactory, $imageHelper, $serializer, $swatchAttributesProvider);
    }

    /**
     * {@inheritdoc}
     */
    public function getProductMediaGallery(\Magento\Catalog\Model\Product $product)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProductMediaGallery');
        if (!$pluginInfo) {
            return parent::getProductMediaGallery($product);
        } else {
            return $this->___callPlugins('getProductMediaGallery', func_get_args(), $pluginInfo);
        }
    }
}
