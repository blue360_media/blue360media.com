<?php
namespace Endeavor\MoveOrderTo\Api\Data;

/**
 * Factory class for @see \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderSearchResultsInterface
 */
class EndeavorMoveorderSearchResultsInterfaceFactory
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Factory constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Endeavor\\MoveOrderTo\\Api\\Data\\EndeavorMoveorderSearchResultsInterface')
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     * @return \Endeavor\MoveOrderTo\Api\Data\EndeavorMoveorderSearchResultsInterface
     */
    public function create(array $data = array())
    {
        return $this->_objectManager->create($this->_instanceName, $data);
    }
}
