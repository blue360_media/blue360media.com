<?php
namespace Ebook\Display\Controller\Adminhtml\News\MassDelete;

/**
 * Interceptor class for @see \Ebook\Display\Controller\Adminhtml\News\MassDelete
 */
class Interceptor extends \Ebook\Display\Controller\Adminhtml\News\MassDelete implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Ebook\Display\Model\ContactFactory $modelContactFactory)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $resultPageFactory, $modelContactFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
