<?php
namespace Magento\CatalogInventory\Model\Stock\Status;

/**
 * Interceptor class for @see \Magento\CatalogInventory\Model\Stock\Status
 */
class Interceptor extends \Magento\CatalogInventory\Model\Stock\Status implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory, \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory, \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = array())
    {
        $this->___init();
        parent::__construct($context, $registry, $extensionFactory, $customAttributeFactory, $stockRegistry, $resource, $resourceCollection, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getStockStatus()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getStockStatus');
        if (!$pluginInfo) {
            return parent::getStockStatus();
        } else {
            return $this->___callPlugins('getStockStatus', func_get_args(), $pluginInfo);
        }
    }
}
