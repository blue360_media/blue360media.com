<?php
namespace Magento\Customer\Api\Data;

/**
 * Extension class for @see \Magento\Customer\Api\Data\CustomerInterface
 */
class CustomerExtension extends \Magento\Framework\Api\AbstractSimpleObject implements CustomerExtensionInterface
{
    /**
     * @return boolean|null
     */
    public function getIsSpecialMultiShippingFlow()
    {
        return $this->_get('is_special_multi_shipping_flow');
    }

    /**
     * @param boolean $isSpecialMultiShippingFlow
     * @return $this
     */
    public function setIsSpecialMultiShippingFlow($isSpecialMultiShippingFlow)
    {
        $this->setData('is_special_multi_shipping_flow', $isSpecialMultiShippingFlow);
        return $this;
    }

    /**
     * @return boolean|null
     */
    public function getIsSubscribed()
    {
        return $this->_get('is_subscribed');
    }

    /**
     * @param boolean $isSubscribed
     * @return $this
     */
    public function setIsSubscribed($isSubscribed)
    {
        $this->setData('is_subscribed', $isSubscribed);
        return $this;
    }
}
