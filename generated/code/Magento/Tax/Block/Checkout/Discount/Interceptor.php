<?php
namespace Magento\Tax\Block\Checkout\Discount;

/**
 * Interceptor class for @see \Magento\Tax\Block\Checkout\Discount
 */
class Interceptor extends \Magento\Tax\Block\Checkout\Discount implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\View\Element\Template\Context $context, \Magento\Customer\Model\Session $customerSession, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Sales\Model\Config $salesConfig, \Magento\Tax\Model\Config $taxConfig, array $layoutProcessors = array(), array $data = array())
    {
        $this->___init();
        parent::__construct($context, $customerSession, $checkoutSession, $salesConfig, $taxConfig, $layoutProcessors, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function getItemRenderer($type = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getItemRenderer');
        if (!$pluginInfo) {
            return parent::getItemRenderer($type);
        } else {
            return $this->___callPlugins('getItemRenderer', func_get_args(), $pluginInfo);
        }
    }
}
