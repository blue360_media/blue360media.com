<?php
namespace Magento\SalesRule\Block\Adminhtml\Promo\Quote\Edit\DeleteButton;

/**
 * Interceptor class for @see \Magento\SalesRule\Block\Adminhtml\Promo\Quote\Edit\DeleteButton
 */
class Interceptor extends \Magento\SalesRule\Block\Adminhtml\Promo\Quote\Edit\DeleteButton implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\Block\Widget\Context $context, \Magento\Framework\Registry $registry)
    {
        $this->___init();
        parent::__construct($context, $registry);
    }

    /**
     * {@inheritdoc}
     */
    public function getButtonData()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getButtonData');
        if (!$pluginInfo) {
            return parent::getButtonData();
        } else {
            return $this->___callPlugins('getButtonData', func_get_args(), $pluginInfo);
        }
    }
}
