<?php
namespace Magento\OfflineShipping\Model\Carrier\Tablerate;

/**
 * Interceptor class for @see \Magento\OfflineShipping\Model\Carrier\Tablerate
 */
class Interceptor extends \Magento\OfflineShipping\Model\Carrier\Tablerate implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory, \Psr\Log\LoggerInterface $logger, \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory, \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $resultMethodFactory, \Magento\OfflineShipping\Model\ResourceModel\Carrier\TablerateFactory $tablerateFactory, array $data = array())
    {
        $this->___init();
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $rateResultFactory, $resultMethodFactory, $tablerateFactory, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function collectRates(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'collectRates');
        if (!$pluginInfo) {
            return parent::collectRates($request);
        } else {
            return $this->___callPlugins('collectRates', func_get_args(), $pluginInfo);
        }
    }
}
