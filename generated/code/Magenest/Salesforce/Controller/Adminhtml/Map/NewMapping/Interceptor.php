<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Map\NewMapping;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Map\NewMapping
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Map\NewMapping implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\Registry $coreRegistry, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magenest\Salesforce\Model\MapFactory $mapFactory, \Magenest\Salesforce\Model\ResourceModel\Map\CollectionFactory $collectionFactory)
    {
        $this->___init();
        parent::__construct($context, $coreRegistry, $resultPageFactory, $mapFactory, $collectionFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
