<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Report\Index;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Report\Index
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Report\Index implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magenest\Salesforce\Model\ReportFactory $reportFactory, \Magento\Framework\View\LayoutFactory $layoutFactory, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory)
    {
        $this->___init();
        parent::__construct($context, $reportFactory, $layoutFactory, $resultPageFactory, $resultForwardFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
