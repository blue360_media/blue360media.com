<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Sync\Contact;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Sync\Contact
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Sync\Contact implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magenest\Salesforce\Model\Sync\Contact $contact)
    {
        $this->___init();
        parent::__construct($context, $contact);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
