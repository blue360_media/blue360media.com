<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Sync\Order;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Sync\Order
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Sync\Order implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magenest\Salesforce\Model\Sync\Order $order)
    {
        $this->___init();
        parent::__construct($context, $order);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
