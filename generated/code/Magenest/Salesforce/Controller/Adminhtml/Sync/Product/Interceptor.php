<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Sync\Product;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Sync\Product
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Sync\Product implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magenest\Salesforce\Model\Sync\Product $product)
    {
        $this->___init();
        parent::__construct($context, $product);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
