<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Sync\Queue;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Sync\Queue
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Sync\Queue implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magenest\Salesforce\Model\Sync\Contact $contact, \Magenest\Salesforce\Model\Sync\Campaign $campaign, \Magenest\Salesforce\Model\Sync\Account $account, \Magenest\Salesforce\Model\Sync\Lead $lead, \Magenest\Salesforce\Model\Sync\Order $order, \Magenest\Salesforce\Model\Sync\Product $product, \Magenest\Salesforce\Model\Sync\Opportunity $opportunity, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Config\Model\ResourceModel\Config $resourceConfig, \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList)
    {
        $this->___init();
        parent::__construct($context, $contact, $campaign, $account, $lead, $order, $product, $opportunity, $scopeConfig, $resourceConfig, $cacheTypeList);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
