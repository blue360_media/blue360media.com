<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Sync\Account;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Sync\Account
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Sync\Account implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magenest\Salesforce\Model\Sync\Account $account)
    {
        $this->___init();
        parent::__construct($context, $account);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
