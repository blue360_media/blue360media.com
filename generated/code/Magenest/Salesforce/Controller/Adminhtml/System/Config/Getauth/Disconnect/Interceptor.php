<?php
namespace Magenest\Salesforce\Controller\Adminhtml\System\Config\Getauth\Disconnect;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\System\Config\Getauth\Disconnect
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\System\Config\Getauth\Disconnect implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Config\Model\ResourceModel\Config $configModel)
    {
        $this->___init();
        parent::__construct($context, $scopeConfig, $configModel);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
