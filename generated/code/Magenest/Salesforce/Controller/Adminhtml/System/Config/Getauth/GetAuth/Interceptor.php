<?php
namespace Magenest\Salesforce\Controller\Adminhtml\System\Config\Getauth\GetAuth;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\System\Config\Getauth\GetAuth
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\System\Config\Getauth\GetAuth implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magenest\Salesforce\Model\Connector $connector, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magenest\Salesforce\Model\Sync\Product $syncProduct)
    {
        $this->___init();
        parent::__construct($context, $connector, $scopeConfig, $syncProduct);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
