<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Field\AjaxGetFields;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Field\AjaxGetFields
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Field\AjaxGetFields implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\Controller\Result\JsonFactory $jsonFactory, \Magenest\Salesforce\Model\FieldFactory $fieldFactory, \Magenest\Salesforce\Model\MapFactory $mapFactory, \Magento\Framework\App\Action\Context $context)
    {
        $this->___init();
        parent::__construct($jsonFactory, $fieldFactory, $mapFactory, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
