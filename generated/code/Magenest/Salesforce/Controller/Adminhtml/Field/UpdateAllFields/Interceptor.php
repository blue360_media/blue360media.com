<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Field\UpdateAllFields;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Field\UpdateAllFields
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Field\UpdateAllFields implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magenest\Salesforce\Model\FieldFactory $fieldFactory)
    {
        $this->___init();
        parent::__construct($context, $fieldFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
