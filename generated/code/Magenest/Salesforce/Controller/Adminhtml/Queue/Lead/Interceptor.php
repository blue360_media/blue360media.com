<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Queue\Lead;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Queue\Lead
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Queue\Lead implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Customer\Model\CustomerFactory $customerFactory, \Magento\Config\Model\ResourceModel\Config $config, \Magento\Framework\App\Config\ScopeConfigInterface $configInterface, \Magenest\Salesforce\Model\QueueFactory $queueFactory)
    {
        $this->___init();
        parent::__construct($context, $customerFactory, $config, $configInterface, $queueFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
