<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Queue\Opportunity;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Queue\Opportunity
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Queue\Opportunity implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Sales\Model\OrderFactory $orderFactory, \Magenest\Salesforce\Model\QueueFactory $queueFactory)
    {
        $this->___init();
        parent::__construct($context, $orderFactory, $queueFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
