<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Queue\Campaign;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Queue\Campaign
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Queue\Campaign implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\CatalogRule\Model\RuleFactory $ruleFactory, \Magenest\Salesforce\Model\QueueFactory $queueFactory)
    {
        $this->___init();
        parent::__construct($context, $ruleFactory, $queueFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
