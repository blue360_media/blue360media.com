<?php
namespace Magenest\Salesforce\Controller\Adminhtml\Request\Report;

/**
 * Interceptor class for @see \Magenest\Salesforce\Controller\Adminhtml\Request\Report
 */
class Interceptor extends \Magenest\Salesforce\Controller\Adminhtml\Request\Report implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magenest\Salesforce\Model\ReportFactory $logFactory, \Magenest\Salesforce\Model\RequestLogFactory $requestLogFactory)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $logFactory, $requestLogFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
